#!/usr/bin/env python
import argparse
import os
from subprocess import check_output


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    subparsers.add_parser('create').set_defaults(func=create_proxy)
    subparsers.add_parser('update').set_defaults(func=update_proxy)
    args = parser.parse_args()
    args.func()


def create_proxy():
    grid_proxy = os.environ['ENCODED_GRID_PROXY']
    with open('/tmp/x509up_u'+str(os.getuid()), 'wt') as fp:
        fp.write(grid_proxy)
    print(check_output('lhcb-proxy-info'))


def update_proxy():
    import gitlab
    gl = gitlab.Gitlab('https://gitlab.cern.ch', private_token=os.environ['GITLAB_API_TOKEN'])
    gl.auth()

    with open('/tmp/x509up_u'+str(os.getuid()), 'rt') as fp:
        grid_proxy = fp.read()

    project = gl.projects.get('lhcb-datapkg/WG/CharmWGProd')
    project.variables.update('ENCODED_GRID_PROXY', new_data={'key': 'ENCODED_GRID_PROXY', 'value': grid_proxy})


if __name__ == '__main__':
    parse_args()
