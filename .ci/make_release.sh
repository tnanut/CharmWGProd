#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t '
set -x

NEW_TAG="$(git tag -l 'v*' | sort --version-sort | tail -n 1 | sed -E 's@v([0-9]+)r([0-9]+)([^0-9].+)?@echo "v\1r$((\2+1))"@ge')"
echo "Creating new release: ${NEW_TAG}"

sed -i "s/__-__VERSION__-__/${NEW_TAG}/g" .gitlab-ci.yml
git add .gitlab-ci.yml

sed -i "s#! Package     : CharmWGProd/.*#! Package     : CharmWGProd/${NEW_TAG}#g" doc/release.notes
mv doc/release.notes doc/release.notes.old
head -n5 doc/release.notes.old > doc/release.notes
echo "========================== $(date -u +"%Y-%m-%d") CharmWGProd ${NEW_TAG} ========================" >> doc/release.notes
echo "" >> doc/release.notes
tail -n +6 doc/release.notes.old >> doc/release.notes
rm doc/release.notes.old
git add doc/release.notes

sed -i "s#version   .*#version           ${NEW_TAG}#g" cmt/requirements
git add cmt/requirements

git commit -m "GitLab CI - Set version for release"
git tag "${NEW_TAG}"
git push origin "${NEW_TAG}"
