from glob import glob
import os


prod_conf_fns = glob('prodConf_*.py')
assert len(prod_conf_fns) == 1, (prod_conf_fns, os.listdir('.'))

with open(prod_conf_fns[0], 'rt') as fp:
    prod_conf = fp.read()
assert '\nProdConf(\n' in prod_conf, prod_conf

prod_conf = prod_conf.replace('\nProdConf(\n', '\nprod_conf = ProdConf(\n')
prod_conf += '\n'
prod_conf += 'import Configurables\n'
prod_conf += 'configurable_name = {"Castelao": "DaVinci"}.get(prod_conf.Application, prod_conf.Application)\n'
prod_conf += 'getattr(Configurables, configurable_name)().EvtMax = prod_conf.NOfEvents\n'

with open(prod_conf_fns[0], 'wt') as fp:
    fp.write(prod_conf)
