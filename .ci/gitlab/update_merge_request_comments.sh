#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t '

cd "${CI_PROJECT_DIR}"
git pull --tags
tags=$(git tag -l | grep '^prepared-')
echo "Will update comments for $(echo "${tags}" | wc -l | sed 's/ //g') tags"
for tag in $tags; do
    python "${CI_PROJECT_DIR}/.ci/gitlab/check_tests_succeeded.py" --tag-name "${tag}" || true
done
