#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
from collections import defaultdict
from datetime import datetime
import dateutil.parser
import os
import re
import time

import gitlab
from pytz import timezone


comment_line_pattern = (
    ' - __[{time}]__ Running tests for {hash} '
    'in pipeline [{pipeline}](https://gitlab.cern.ch/lhcb-datapkg/WG/CharmWGProd/pipelines/{pipeline}), '
    'status is {status}. {emoji}'
)
comment_line_re = re.compile(
    r'^ - __\[(?P<time>\d{4}-\d\d-\d\d \d\d:\d\d)]__ '
    r'Running tests for (?P<hash>[0-9a-fA-F]+) '
    r'in pipeline \[(?P<pipeline>\d+)\]\(.*\), '
    r'status is (?P<status>\w+). (?P<emoji>(?:\:[^:]+\:)?)$'
)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--tag-name", required=True)
    parser.add_argument("--monitor", action="store_true")
    args = parser.parse_args()

    if 'CHANGED_PRODUCTIONS' in os.environ:
        changed_productions = os.environ['CHANGED_PRODUCTIONS']
        if changed_productions:
            print('Changed productions are:', changed_productions)
            check_pipeline(args.tag_name, args.monitor)
        else:
            print('It looks like no productions changed, skipping step...')
    else:
        check_pipeline(args.tag_name, args.monitor)


def find_pipeline(project, tag_name, n_retries=5):
    matched_pipelines = project.pipelines.list(ref=tag_name)
    if len(matched_pipelines) == 0:
        print('Failed to find pipeline for '+tag_name+', retying in 60 seconds')
        time.sleep(60)
        if n_retries > 0:
            return find_pipeline(project, tag_name, n_retries=n_retries-1)
        else:
            raise ValueError('Failed to find pipeline for '+tag_name)
    elif len(matched_pipelines) == 1:
        return matched_pipelines[0]
    else:
        raise RuntimeError("More pipelines than I expected")


def find_merge_requests(project, tag_name):
    merge_requests = []
    for merge_request in project.mergerequests.list():
        if any([d.attributes['head_commit_sha'] == tag_name.split('-')[-1] for d in merge_request.diffs.list()]):
            merge_requests.append(merge_request)
    return merge_requests


def find_comment(merge_request, username):
    for comment in merge_request.notes.list():
        if comment.attributes['author']['username'] == username and comment.body.startswith('Testing status:\n'):
            return comment

    # We don't have a testing comment yet so create one
    return merge_request.notes.create({
        'body': 'Testing status:\n'
    })


def update_status_comments(project, tag_name, username, pipeline, status):
    if tag_name.startswith('prepared-master-'):
        print('Skipping updating MR comments for commit on master')
        return

    for merge_request in find_merge_requests(project, tag_name):
        comment = find_comment(merge_request, username)
        print('Updating comment', comment.id, 'on merge_request', merge_request.id)

        old_lines = comment.body.split('\n')
        new_lines = [old_lines.pop(0)]

        status_lines = {}
        for line in old_lines:
            match = comment_line_re.match(line)
            if match is None:
                raise ValueError('Failed to parse line: '+line)
            status_lines[match.group('pipeline')] = match.groupdict()

        if str(pipeline.id) not in status_lines:
            status_lines[str(pipeline.id)] = {
                'time': datetime.now(timezone('Europe/Zurich')).strftime('%Y-%m-%d %H:%M'),
                'hash': tag_name.split('-')[-1],
                'pipeline': str(pipeline.id)
            }
        status_lines[str(pipeline.id)]['status'] = status
        if status == 'good':
            status_lines[str(pipeline.id)]['emoji'] = ':+1:'
        elif status == 'error':
            status_lines[str(pipeline.id)]['emoji'] = ':x:'
        else:
            status_lines[str(pipeline.id)]['emoji'] = ':question:'

        new_body = '\n'.join(new_lines + [
            comment_line_pattern.format(**status_line)
            for status_line in sorted(status_lines.values(), key=lambda x: x['pipeline'])
        ])
        if new_body != comment.body:
            merge_request.notes.update(comment.get_id(), {'body': new_body})


def check_pipeline(tag_name, monitor):
    gl = gitlab.Gitlab("https://gitlab.cern.ch", private_token=os.environ["GITLAB_API_TOKEN"])
    gl.auth()
    current_user = gl.user.attributes['username']
    project = gl.projects.get(os.environ["CI_PROJECT_PATH"])
    matched_pipeline = find_pipeline(project, tag_name)
    update_status_comments(project, tag_name, current_user, matched_pipeline, 'unknown')

    try:
        check_pipeline_status(project, tag_name, matched_pipeline, monitor)
    except Exception:
        update_status_comments(project, tag_name, current_user, matched_pipeline, 'error')
        raise
    else:
        update_status_comments(project, tag_name, current_user, matched_pipeline, 'good')


def check_pipeline_status(project, tag_name, pipeline, monitor):
    if pipeline is None:
        pipeline = find_pipeline(project, tag_name)

    while True:
        jobs = pipeline.jobs.list(all=True)

        print("Checking pipeline status for", tag_name)
        assert len(jobs) >= 1, (tag_name, jobs)
        jobs_dict = defaultdict(list)
        for job in jobs:
            if job.attributes["stage"] == "test":
                job_name = job.attributes["name"]
                jobs_dict[job_name].append(job)

        still_running = False
        all_passed = True
        for key, matched_jobs in sorted(jobs_dict.items()):
            # Some jobs might have ran multiple times, require only the newest to have succeeded
            job = max(matched_jobs, key=lambda x: dateutil.parser.parse(x.attributes["created_at"]))

            status = job.attributes["status"]
            print("    "+key+": "+status)
            if status == "success":
                pass
            elif status in ["created","pending", "running"]:
                still_running = True
            elif status in ["canceled", "failed"]:
                if "success" in [j.attributes["status"] for j in matched_jobs]:
                    print("        Latest job failed but a previous jobs didn't!")
                else:
                    all_passed = False
            else:
                raise ValueError("Unrecognised status from GitLab CI: " + status)

        if not (monitor and still_running):
            break
        time.sleep(60)

    if not all_passed:
        raise ValueError("ERROR: Not all jobs have passed")


if __name__ == "__main__":
    parse_args()
