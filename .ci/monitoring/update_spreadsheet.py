#!/usr/bin/env python2
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from collections import defaultdict
import distutils.spawn
from itertools import izip_longest
import json
from os.path import dirname, join, splitext
import re
import subprocess
import time

from tqdm import tqdm
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from distutils.version import StrictVersion
from XRootD.client import FileSystem

assert StrictVersion(gspread.__version__) >= StrictVersion('3.0.1'), gspread.__version__

eos = FileSystem('root://eoslhcb.cern.ch')

USE_CACHE = False

if USE_CACHE:
    from diskcache import FanoutCache
    check_output = FanoutCache('/tmp/charm-wg-prod-cachedir').memoize()(subprocess.check_output)
else:
    check_output = subprocess.check_output

COL_DESCRIPTION = 4
COL_REQ_ID = 5
COL_REQ_STATUS = 6
COL_TRANS_WG_ID = 7
COL_TRANS_MERGE_ID = 8
COL_TRANS_WG_STATUS = 9
COL_TRANS_MERGE_STATUS = 10
COL_TRANS_WG_PROG = 11
COL_TRANS_MERGE_PROG = 12
COL_TRANS_MERGE_NFILES = 13
COL_FRAC_AT_CERN = 14
COL_CERN_PATH = 15
COLS_TO_KEEP = [2, 3]


def get_sheet():
    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']
    creds = ServiceAccountCredentials.from_json_keyfile_name(join(dirname(__file__), 'client_secret.json'), scope)
    client = gspread.authorize(creds)
    sheet = client.open("Charm working group productions").worksheet('All')

    existing_values = {}
    for row in sheet.get_all_values()[2:]:
        row = [None] + row
        existing_values[row[COL_REQ_ID]] = row

    return sheet, existing_values


def get_request_ids():
    # Try this 10 times before giving up
    for i in range(10):
        try:
            request_ids = check_output(['python', '-c', '\n'.join([
                "from __future__ import print_function",
                "import json",
                "from DIRAC.Core.Base.Script import parseCommandLine",
                "from DIRAC.Core.DISET.RPCClient import RPCClient",
                "parseCommandLine()",
                "bk_RPC = RPCClient('ProductionManagement/ProductionRequest')",
                "response = bk_RPC.getProductionRequestList(0, '', 'ASC', 0, 0, dict(RequestAuthor='cburr', RequestType='WGProduction'))",
                "response2 = bk_RPC.getProductionRequestList(0, '', 'ASC', 0, 0, dict(RequestAuthor='cburr', RequestType='AnalysisProduction'))",
                "if not (response['OK'] and response2['OK']):",
                "    print(response, response2)",
                "print(json.dumps({int(p['RequestID']): (p['RequestName'] if p['RequestType'] == 'WGProduction' else p['ProPath'], p['RequestState']) for p in response['Value']['Rows'] + response2['Value']['Rows']}))",
            ])])
        except subprocess.CalledProcessError:
            print('Failed in get_request_ids, retrying in 5 minutes')
            time.sleep(60*5)
        else:
            break
    else:
        raise

    try:
        request_ids = json.loads(request_ids)
    except Exception:
        print(request_ids)
        raise
    else:
        # Preserve manually added production IDs and descriptions
        sheet, existing_values = get_sheet()

        for k in sorted(request_ids):
            if k not in existing_values:
                print('Found new request ID', k)

        for k, v in existing_values.items():
            if k in request_ids:
                request_ids[k][0] = v[COL_DESCRIPTION]
            else:
                request_ids[k] = [v[COL_DESCRIPTION], 'FIXME!!!']
        return request_ids


def get_request_info(all_request_ids):
    failed_request_ids = []
    for request_ids in izip_longest(*([iter(all_request_ids)]*10)):
        request_ids = list(filter(None, request_ids))
        try:
            request_info = check_output(['dirac-production-shifter', '--requestID', ','.join(list(map(str, request_ids)))])
        except subprocess.CalledProcessError:
            failed_request_ids.extend(request_ids)
        else:
            for request_id in request_ids:
                yield request_id, request_info

    for request_id in failed_request_ids:
        try:
            request_info = check_output(['dirac-production-shifter', '--requestID', str(request_id)])
        except Exception:
            print('Failed to get request info for', request_id)
            yield request_id, None
        else:
            yield request_id, request_info


def get_fraction_at_cern(merge_trans_id, n_merged):
    if distutils.spawn.find_executable('dirac-bookkeeping-production-files'):
        env_prefix = ''
    else:
        env_prefix = 'lb-run -c best LHCbDIRAC/$LHCBDIRAC_VERSION '

    result = check_output(
        env_prefix+'dirac-bookkeeping-production-files '+str(merge_trans_id)+' ALL',
        shell=True
    )

    file_type = None
    for lfn in re.findall(r'([^\s]+)\s+\d+\s+', result, re.IGNORECASE):
        try:
            this_file_type = lfn.split('/')[4]
        except IndexError:
            print('Failed to get file type for', merge_trans_id, 'got', lfn, 'from', result)
            continue
        if this_file_type.lower().endswith('.root'):
            assert file_type is None or file_type == this_file_type, (lfn, result)
            file_type = this_file_type
        elif this_file_type.lower().endswith('log'):
            pass
        else:
            raise NotImplementedError(lfn)

    if file_type is None:
        return '', '', ''

    result = check_output(
        env_prefix + 'dirac-bookkeeping-get-files' +
        ' -P ' + str(merge_trans_id) +
        ' -f ' + file_type +
        ' --Rep Yes',
        shell=True
    )

    lfns = []
    GUID_REGEX_PATTERN = r'[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'
    for lfn in re.findall(r'^([^\s]+)\s+\d+\s+'+GUID_REGEX_PATTERN, result, re.IGNORECASE | re.MULTILINE):
        ext = splitext(lfn)[1]
        assert ext.lower() in ['.root'], (lfn, ext, result)
        lfns.append(lfn)

    if len(lfns) == 0:
        return '', '', ''

    eos_path = "/eos/lhcb/grid/prod"
    lfns_at_cern = []
    for lfn_dir in set([dirname(l) for l in lfns]):
        status, result = eos.dirlist(eos_path+lfn_dir)
        assert status.ok, status
        if result:
            lfns_at_cern.extend([lfn_dir+'/'+d.name for d in result.dirlist])

    n_at_cern = 0
    for lfn in lfns:
        if lfn in lfns_at_cern:
            n_at_cern += 1

    n_merged = len(lfns)
    assert n_merged > 0, (merge_trans_id, lfns)

    path = eos_path+dirname(dirname(lfns[0]))+'/*/*.root'
    return str(n_merged), '{:.2%}'.format(n_at_cern/n_merged), path


def update_row(existing_values, cells, i, request_id, request_info, result):
    cells.append(gspread.Cell(i, COL_DESCRIPTION, str(request_info[0])))
    cells.append(gspread.Cell(i, COL_REQ_ID, str(request_id)))
    cells.append(gspread.Cell(i, COL_REQ_STATUS, str(request_info[1])))

    wgprod_trans_id = sorted([k for k, v in result.items() if v[1] == 'WGProduction' and v[0] != 'Cleaned'])
    if wgprod_trans_id:
        wgprod_trans_id = wgprod_trans_id[-1]
        trans_status, trans_type, trans_progress, trans_nfiles = result[wgprod_trans_id]
        cells.append(gspread.Cell(i, COL_TRANS_WG_ID, wgprod_trans_id))
        cells.append(gspread.Cell(i, COL_TRANS_WG_STATUS, trans_status))
        try:
            trans_progress = '{:.2%}'.format(float(trans_progress)/100)
        except ValueError:
            trans_progress = trans_progress
        cells.append(gspread.Cell(i, COL_TRANS_WG_PROG, trans_progress))
    else:
        cells.append(gspread.Cell(i, COL_TRANS_WG_ID, ''))
        cells.append(gspread.Cell(i, COL_TRANS_WG_STATUS, ''))
        cells.append(gspread.Cell(i, COL_TRANS_WG_PROG, ''))

    merge_trans_id = sorted([k for k, v in result.items() if v[1] == 'Merge' and v[0] != 'Cleaned'])
    if merge_trans_id:
        merge_trans_id = merge_trans_id[-1]
        trans_status, trans_type, trans_progress, trans_nfiles = result[merge_trans_id]
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_ID, merge_trans_id))
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_STATUS, trans_status))
        try:
            trans_progress = '{:.2%}'.format(float(trans_progress)/100)
        except ValueError:
            trans_progress = trans_progress
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_PROG, trans_progress))
        n_merged, trans_cern_frac, trans_cern_path = get_fraction_at_cern(merge_trans_id, trans_nfiles)
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_NFILES, n_merged))
        cells.append(gspread.Cell(i, COL_FRAC_AT_CERN, trans_cern_frac))
        cells.append(gspread.Cell(i, COL_CERN_PATH, trans_cern_path))
    else:
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_ID, ''))
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_STATUS, ''))
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_PROG, ''))
        cells.append(gspread.Cell(i, COL_TRANS_MERGE_NFILES, ''))
        cells.append(gspread.Cell(i, COL_FRAC_AT_CERN, ''))
        cells.append(gspread.Cell(i, COL_CERN_PATH, ''))

    for j in COLS_TO_KEEP:
        if request_id in existing_values:
            value = existing_values[request_id][j]
        else:
            value = ''
        cells.append(gspread.Cell(i, j, value))

    assert len(cells) % 14 == 0, len(cells)


def update_spreadsheet():
    all_request_ids = get_request_ids()
    results = defaultdict(dict)
    for request_id, request_info in tqdm(get_request_info(all_request_ids), total=len(all_request_ids)):
        if request_info is None:
            results[request_id] = {}
            if str(request_id) == '46153':
                results[request_id]['70688'] = ['Archived', 'WGProduction', '0', '0']
                results[request_id]['70689'] = ['Archived', 'Merge', '0', '0']
            elif str(request_id) == '46112':
                results[request_id]['70577'] = ['Archived', 'WGProduction', '0', '0']
                results[request_id]['70578'] = ['Archived', 'Merge', '0', '0']
            else:
                print("Skipping", request_id)
            continue

        pattern = r'^'+str(request_id)+r'\s+(\d+)\s+(\w+)\s+(\w+)'
        matches = re.findall(pattern, request_info, re.MULTILINE)
        assert request_info.count(str(request_id)) == 1+len(matches), (request_id, request_info, matches)
        for trans_id, trans_status, trans_type in matches:
            base_pattern = r'^'+str(request_id)+r'\s+'+trans_id+r'\s+'+trans_status+r'\s+'+trans_type+r'\s+'
            if re.findall(base_pattern+r'\.\.(No files at all|Internal error)\.\.', request_info, re.MULTILINE):
                trans_progress, trans_total_nfiles, trans_nfiles = '0', '0', '0'
            else:
                trans = re.findall(base_pattern+r'(\d+\.\d+)%\s+\((\d+)\)\s+(\d+)', request_info, re.MULTILINE)
                try:
                    assert len(trans) == 1, trans
                except AssertionError:
                    raise
                trans_progress, trans_total_nfiles, trans_nfiles = trans[0]
            results[request_id][trans_id] = [trans_status, trans_type, trans_progress, trans_nfiles]

    sheet, existing_values = get_sheet()

    cells = []
    for i, (request_id, result) in tqdm(enumerate(sorted(results.items(), reverse=True), 3), total=len(results)):
        update_row(existing_values, cells, i, request_id, all_request_ids[request_id], result)

    first_col = min([c.col for c in cells])
    last_col = max([c.col for c in cells])
    for i in range(i, sheet.row_count):
        for j in range(first_col, last_col+1):
            cells.append(gspread.Cell(i, j, ''))

    missing_requests = set(existing_values).difference(all_request_ids)
    if missing_requests:
        raise RuntimeError('Missing updates for '+repr(missing_requests))

    for n_retry in range(5):
        try:
            sheet.update_cells(sorted(cells, key=lambda x: (x.row, x.col)), 'USER_ENTERED')
        except gspread.exceptions.APIError:
            print('Attempt', n_retry, ' to update the spreadsheet failed due',
                  'to a gspread API error, will retry in 30 seconds')
            time.sleep(30)
            sheet, new_existing_values = get_sheet()
            assert existing_values == new_existing_values, (
                len(existing_values), len(new_existing_values),
                existing_values, new_existing_values
            )
        else:
            break
    else:
        raise
    return sheet, cells


if __name__ == '__main__':
    sheet, cells = update_spreadsheet()
