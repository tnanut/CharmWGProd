#!/usr/bin/env python
from __future__ import print_function
from __future__ import division

import argparse
from collections import defaultdict
from io import BytesIO
import os
from os.path import basename, dirname, isfile, isdir, join
from tempfile import NamedTemporaryFile
from zipfile import ZipFile, BadZipfile

import gitlab
import uproot


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--pipeline-id')
    parser.add_argument('--tag-name')
    parser.add_argument('--output-dir')
    args = parser.parse_args()
    check_pipeline(args.pipeline_id, args.tag_name, output_dir=args.output_dir)


def walk_root_file(tdirectory, path=None):
    if path is None:
        path = []
    for key in tdirectory:
        _path = path + [key.decode('utf-8').split(';')[0]]
        if tdirectory[key].__class__.__name__ == 'TTree':
            print('    Found TTree:', '/'.join(_path), 'with',
                  tdirectory[key].numentries, 'entries and',
                  tdirectory[key].numbranches, 'branches')
        elif isinstance(tdirectory[key], uproot.rootio.ROOTDirectory):
            walk_root_file(tdirectory[key], _path)
        else:
            print('Not sure what to do with', key, tdirectory[key])


def check_number_of_trees(root_file):
    min_required = 1
    if 'GetIntegratedLuminosity' in root_file:
        min_required = 2
    if len(root_file) < min_required:
        print('    ERROR: Expected a non-luminosity TTree!!!')
        return False
    else:
        return True


def check_pipeline(pipeline_id, tag_name, output_dir=None):
    assert bool(pipeline_id) + bool(tag_name) == 1, 'Only one of --pipeline-id and --tag-name should be used'
    results = {}
    job_status_okay = defaultdict(lambda: False)

    gl = gitlab.Gitlab('https://gitlab.cern.ch', private_token=os.environ['GITLAB_API_TOKEN'])
    gl.auth()
    project = gl.projects.get('lhcb-datapkg/WG/CharmWGProd')

    if tag_name:
        matched_pipelines = project.pipelines.list(ref=tag_name)
        assert len(matched_pipelines) == 1, matched_pipelines
        pipeline = matched_pipelines[0]
    else:
        pipeline = project.pipelines.get(pipeline_id)

    for pipeline_job in pipeline.jobs.list(all=True):
        job_name = pipeline_job.attributes['name']
        job_id = pipeline_job.attributes['id']
        job_status = pipeline_job.attributes['status']

        print(job_name, job_id, job_status)

        if job_name == 'make_release':
            print('    Skipping')
            continue

        # Ensure that a status is present for this job in the defaultdict
        job_status_okay[job_name]
        if job_status != 'success':
            print('    ERROR: Skipping due to invalid status')
            continue

        job = project.jobs.get(pipeline_job.id, lazy=True)
        # Get the size in bytes
        file_size = pipeline_job.attributes['artifacts_file']['size']
        # Get the run time in seconds
        duration = pipeline_job.attributes['duration']

        # Check the build artefacts to make sure they're valid
        # Use 3 retries as the GitLab API sometimes returns junk
        for i in range(3):
            f = BytesIO()
            job.artifacts(streamed=True, action=f.write)
            try:
                zf = ZipFile(f)
            except BadZipfile:
                print('Got bad ZipFile, retrying...')
                continue
            else:
                break
        else:
            raise

        # Check that ROOT files inside the artefacts file look reasonable
        for fn in zf.namelist():
            # if not fn.lower().endswith('dst'):
            #     print('WARNING: Unable to check status of', fn)
            #     job_status_okay[job_name] = True
            #     continue
            if not fn.lower().endswith('.root'):
                continue

            # Check if the ROOT file is valid
            with NamedTemporaryFile(suffix='.root', mode='wb') as fp:
                fp.write(zf.read(fn))
                fp.flush()

                root_file = uproot.open(fp.name)
                job_status_okay[job_name] |= check_number_of_trees(root_file)
                walk_root_file(root_file)

            # Save the output file if requested
            if output_dir:
                downloaded_fn = join(output_dir, job_name, basename(fn))
                if not isdir(dirname(downloaded_fn)):
                    os.makedirs(dirname(downloaded_fn))
                # It's possible for the same file to exist multiple times if a job is retried
                # Add a counter to the later copies if this is the case
                _n = 0
                while isfile(downloaded_fn):
                    downloaded_fn = join(output_dir, job_name, basename(fn))[:-len('.root')] + '-'+str(_n)+'.root'
                    assert _n < 10, downloaded_fn
                with open(downloaded_fn, 'wb') as fp:
                    fp.write(zf.read(fn))

            # Record the output filesize and time taken to give to the production system
            results[job_name] = file_size, duration

    print()
    print()
    print()
    for name, (file_size, duration) in sorted(results.items()):
        print('{name} took {duration:.1f} hours and produced a {file_size:.1f} MB file'
              .format(name=name, duration=duration/60/60, file_size=file_size/1e6))
    print()
    print()
    print()

    if not all(job_status_okay.values()):
        for job_name, job_is_okay in job_status_okay.items():
            if not job_is_okay:
                print(job_name, 'appears to have failed')
        raise ValueError('Errors occurred while inspecting job output')


if __name__ == '__main__':
    parse_args()
