# Required for numpy to work with LCG95
yum install -y libgfortran5 libquadmath

source /cvmfs/lhcb.cern.ch/group_login.sh

# Set up Python
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py --user
python -m pip install --user "python-gitlab<2" python-dateutil pytz gitpython==2.1.11 "gitdb2<3"
yum install -y xz-devel
python -m pip install --user "cachetools<4" uproot backports.lzma

# Set up pushing to GitLab using SSH
git config --global user.email "charm-wg-prod-ci@none"
git config --global user.name "Charm WG Productions CI"
echo "${GITLAB_SSH_KEY}" >> id_rsa
chmod 600 id_rsa
echo 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i '"${PWD}"'/id_rsa $*' > ssh
chmod +x ssh
export GIT_SSH="${PWD}/ssh"
git remote set-url origin "ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/WG/CharmWGProd.git"

export PYTHONPATH=${CI_PROJECT_DIR}${PYTHONPATH:+:${PYTHONPATH}}

# Create a grid proxy and ensure it is valid for at least 48 hours
"${CI_PROJECT_DIR}/.ci/voms.py" create
set -x
lb-run -c 'best' LHCbDIRAC/$LHCBDIRAC_VERSION voms-proxy-info --exists --valid 48:00
set +x

# Compute the name for the new commit
if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then
    export NEW_TAG_NAME="prepared-master-${CI_COMMIT_SHA}"
else
    export NEW_TAG_NAME="prepared-${CI_COMMIT_SHA}"
fi

# Find the directories that changed in the last commit
CHANGED_PRODUCTIONS="$(python -m wg_prod_ci_tools changed-productions)"
export CHANGED_PRODUCTIONS

# If CI_CHARMWGPROD_IS_DEBUG is set to any value debugging mode is enabled
# export CI_CHARMWGPROD_IS_DEBUG="TRUE"

