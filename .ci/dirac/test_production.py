#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
from os.path import basename, dirname, isfile, join
import shutil
import random
from subprocess import check_output
import sys
from tempfile import NamedTemporaryFile


dirac_config_source_fn = join(os.getcwd(), dirname(__file__), 'dirac.cfg')
dirac_config_dest_fn = join(os.environ['HOME'], '.dirac.cfg')


def get_lfns(bk_path, nlfns):
    with NamedTemporaryFile(suffix='.lfns') as fp:
        command = 'dirac-bookkeeping-get-files --DQFlags=OK --BKQuery="{bk_path}" --Output={output_fn}'
        command = command.format(bk_path=bk_path, output_fn=fp.name)
        check_output(command, shell=True)
        with open(fp.name, 'rt') as fp:
            lfns = [s.split(' ')[0] for s in fp.readlines()]
        if not len(lfns) > 0:
            raise Exception(command, bk_path)
    random.shuffle(lfns)
    lfns = lfns[:nlfns]
    print(check_output('dirac-bookkeeping-file-metadata '+' '.join(lfns), shell=True))
    return lfns


def get_platform(app, app_version):
    import subprocess
    platforms = subprocess.check_output(
        'lb-run --list-platforms {app}/{app_version}'
        .format(app=app, app_version=app_version), shell=True).strip().split('\n')
    host_os = os.environ['LCG_hostos']
    # Start by trying to use the same OS as the host and the "opt" variant
    filtered_platforms = [p for p in platforms if p.startswith(host_os) and p.endswith('opt')]
    # Fall back to using any platform with the same OS as the host
    if not filtered_platforms:
        filtered_platforms = [p for p in platforms if p.startswith(host_os)]
    # Fall back to using any "opt" variant
    if not filtered_platforms:
        filtered_platforms = [p for p in platforms if p.endswith('opt')]
    # Else just accept any platform
    if not filtered_platforms:
        filtered_platforms = platforms
    if not len(filtered_platforms) >= 1:
        raise Exception(host_os, platforms)
    return sorted(filtered_platforms)[0]


def run_with_step(step_ids, bk_path, nlfns, n_events, configure_dirac=True):
    lfns = get_lfns(bk_path, nlfns)
    run_test_production(lfns, stepReady=True, stepsList=step_ids,
                        configure_dirac=configure_dirac, n_events=n_events)


def run_without_step(app, app_version, options, bk_path, nlfns, output_types,
                     extra_packages, n_events, configure_dirac=True):
    lfns = get_lfns(bk_path, nlfns)
    output_types = [o.upper() for o in output_types]

    # FIXME: This MUST be kept in sync with step_management.py
    step = {
        'StepId': 12345, 'StepName': 'Whatever',
        'ApplicationName': app,
        'ApplicationVersion': app_version,
        'ExtraPackages': ';'.join(extra_packages),
        'ProcessingPass': 'whoCares',
        'Visible': 'N',
        'Usable': 'Yes',
        'DDDB': '',
        'CONDDB': '',
        'DQTag': '',
        'OptionsFormat': 'WGProd',
        'OptionFiles': ';'.join(options),
        'isMulticore': 'N',
        'SystemConfig': get_platform(app, app_version),
        'mcTCK': '',
        'ExtraOptions': '',
        'fileTypesIn': [basename(bk_path)],
        'fileTypesOut': output_types,
        'OutputFilePrefix': '',
        'visibilityFlag': [
            {'Visible': 'Y', 'FileType': output_types}
        ]
    }

    print('\n' + '*'*80 + '\n')
    print('Step is:', step)
    print('\n' + '*'*80 + '\n')

    run_test_production(lfns, fileTypesOut=output_types, steps=[step],
                        configure_dirac=configure_dirac, n_events=n_events)


def run_test_production(lfns, steps=None, fileTypesOut=None, n_events=-1,
                        stepReady=False, stepsList=None, configure_dirac=True):
    if configure_dirac:
        if isfile(dirac_config_dest_fn):
            raise Exception(dirac_config_dest_fn)
        shutil.copy(dirac_config_source_fn, dirac_config_dest_fn)

    from DIRAC.Core.Base.Script import parseCommandLine
    sys.argv = ['-ddd']
    parseCommandLine()

    from LHCbDIRAC.Interfaces.API.DiracProduction import DiracProduction
    from LHCbDIRAC.ProductionManagementSystem.Client.ProductionRequest import ProductionRequest

    # Options that I don't understand/support yet
    includeAncestors = False
    ancestorsDepth = 1
    mergingStep = False

    print('Using LFNs:', *lfns)

    pr = ProductionRequest()
    diracProduction = DiracProduction()

    if stepReady:
        pr.stepsList = stepsList
        pr.outputVisFlag = [{"1": 'Y'}]
        pr.resolveSteps()
        steps = pr.stepsListDict
        pr.outputSEs = ['Tier1-DST']
        pr.specialOutputSEs = [{}]
        pr._determineOutputSEs()
        outDict = pr.outputSEsPerFileType[0]
    else:
        pr.outputSEs = ['Tier1-DST']
        pr.specialOutputSEs = [{}]
        outDict = {t: 'Tier1-DST' for t in fileTypesOut}

    if not includeAncestors:
        ancestorsDepth = 0

    if mergingStep:
        jobType = 'Merge'
    else:
        jobType = 'Turbo'

    prod = pr._buildProduction(
        jobType, steps, outDict, 0, 100, inputDataPolicy='protocol',
        inputDataList=lfns, ancestorDepth=ancestorsDepth, events=n_events
    )

    diracProduction.launchProduction(prod, False, True, 0)

    if configure_dirac:
        os.remove(dirac_config_dest_fn)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Test WG production')
    parser.add_argument('--nlfns', type=int, default=1)
    parser.add_argument('--bk-path', required=True)
    parser.add_argument('--app', default=None)
    parser.add_argument('--app-version', default=None)
    parser.add_argument('-o', '--options', nargs='+', default=None)
    parser.add_argument('--output-type', action='append', default=None)
    parser.add_argument('--extra-package', action='append', default=[])
    parser.add_argument('--no-dirac-config', action='store_true')
    parser.add_argument('--n-events', type=int, default=-1)
    parser.add_argument('--step-id', action='append', default=None)

    args = parser.parse_args()

    if args.nlfns < 1 or args.nlfns > 100:
        raise ValueError('Number of testing LFNs must be between 1 and 10, got '+str(args.nlfns))

    group = [
        args.app is not None,
        args.app_version is not None,
        args.options is not None,
        args.output_type is not None,
    ]
    print(group, args)
    if not (all(group) and not args.step_id) or (args.step_id and not any(group)):
        raise Exception()

    if all(group):
        run_without_step(
            args.app, args.app_version, args.options, args.bk_path, args.nlfns,
            args.output_type, args.extra_package, args.n_events,
            configure_dirac=not args.no_dirac_config
        )
    else:
        raise RuntimeError('This has been disabled')
        steps = list(map(int, args.step_id))
        run_with_step(steps, args.bk_path, args.nlfns, args.n_events,
                      configure_dirac=not args.no_dirac_config)
