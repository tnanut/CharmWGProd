from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os


def dw(response):
    """Simple wrapper to check responses from DIRAC"""
    assert response['OK'], response
    return response['Value']


def get_step_id(bk_RPC, step_name, app, app_version, extra_packages,
                options, options_format, input_file_type, output_file_type):
    # Register the file types if they don't already exist
    available_file_types = dict(dw(bk_RPC.getAvailableFileTypes()))
    if input_file_type not in available_file_types:
        assert input_file_type.split('.')[-1] == 'ROOT', input_file_type
        dw(bk_RPC.insertFileTypes(input_file_type, 'Charm WGProd ntuples', 'ROOT'))
    if output_file_type not in available_file_types:
        assert output_file_type.split('.')[-1] == 'ROOT', output_file_type
        dw(bk_RPC.insertFileTypes(output_file_type, 'Charm WGProd ntuples', 'ROOT'))

    # FIXME: This MUST be kept in sync with test_production.py
    step_info = {
        'Step': {
            'ApplicationName': app,
            'ApplicationVersion': app_version,
            # 'CONDDB': '',
            # 'DDDB': '',
            # 'DQTag': '',
            'ExtraPackages': extra_packages,
            # 'isMulticore': 'N',
            # 'mcTCK': '',
            'OptionFiles': ';'.join(options),
            'ProcessingPass': step_name,
            # 'StepId': 0,
            'StepName': step_name,
            # 'SystemConfig': '',
            'Usable': 'Yes',
            'Visible': 'Y',
        },
        'InputFileTypes': [{'Visible': 'Y', 'FileType': input_file_type.upper()}],
        'OutputFileTypes': [{'Visible': 'Y', 'FileType': output_file_type.upper()}],
        # 'RuntimeProjects': []
    }
    if options_format is not None:
        step_info['Step']['OptionsFormat'] = options_format
    if lookup_step(step_info) is None:
        print('Running insertStep with', step_info)
        if 'CI_CHARMWGPROD_IS_DEBUG' not in os.environ:
            print('Got response', dw(bk_RPC.insertStep(step_info)))

    if 'CI_CHARMWGPROD_IS_DEBUG' in os.environ:
        return 133570 if app == 'Noether' else 133569
    else:
        return lookup_step(step_info)['StepId']


def lookup_step(step_info):
    """Find matching steps from DIRAC"""
    from DIRAC.Core.DISET.RPCClient import RPCClient
    bk_RPC = RPCClient('Bookkeeping/BookkeepingManager')

    query_dict = {
        'StepName': step_info['Step']['StepName'],
        'ApplicationName': step_info['Step']['ApplicationName'],
        'ApplicationVersion': step_info['Step']['ApplicationVersion'],
        'ProcessingPass': step_info['Step']['ProcessingPass'],
        'OptionFiles': step_info['Step']['OptionFiles'],
        'ExtraPackages': step_info['Step']['ExtraPackages'],
        'Usable': step_info['Step']['Usable'],
        'Visible': step_info['Step']['Visible'],
    }

    steps = [{k: v for k, v in zip(record[-1]['ParameterNames'], record[:-1])}
             for record in dw(bk_RPC.getAvailableSteps(query_dict))['Records']]
    matched_steps = []
    for step in steps:
        result_input = dw(bk_RPC.getStepInputFiles(step['StepId']))
        ift = [dict(zip(result_input['ParameterNames'], x)) for x in result_input['Records']]
        step['InputFileTypes'] = [str(x['FileType']) for x in ift]

        result_output = dw(bk_RPC.getStepOutputFiles(step['StepId']))
        oft = [dict(zip(result_output['ParameterNames'], x)) for x in result_output['Records']]
        step['OutputFileTypes'] = [str(x['FileType']) for x in oft]

        if set(step['InputFileTypes']) != set(ft['FileType'] for ft in step_info['InputFileTypes']):
            continue
        if set(step['OutputFileTypes']) != set(ft['FileType'] for ft in step_info['OutputFileTypes']):
            continue
        matched_steps.append(step)

    if len(matched_steps) == 0:
        return None
    elif len(matched_steps) == 1:
        return matched_steps[0]
    else:
        raise RuntimeError(matched_steps)


def get_steps(version, name, app, app_version, options, input_file_type, output_file_type):
    from DIRAC.Core.DISET.RPCClient import RPCClient
    bk_RPC = RPCClient('Bookkeeping/BookkeepingManager')

    process_id = get_step_id(bk_RPC, 'CharmWGProd-'+name, app, app_version, 'WG/CharmWGProd.'+version,
                             options, 'WGProd', input_file_type, output_file_type)

    merge_id = get_step_id(bk_RPC, 'CharmWGProd-Merge-'+output_file_type, 'Noether', 'v1r4',
                           'AppConfig.v3r350', ['$APPCONFIGOPTS/DataQuality/DQMergeRun.py'],
                           None, output_file_type, output_file_type)

    return [process_id, merge_id]
