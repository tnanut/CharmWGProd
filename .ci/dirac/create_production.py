#!/usr/bin/env python2
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import cPickle
from os.path import basename, dirname, isfile, join, relpath
from pprint import pprint
import sys

sys.path.append(dirname(dirname(dirname(__file__))))  # NOQA
sys.path.append(dirname(__file__))  # NOQA
from step_management import get_steps


def dw(response):
    """Simple wrapper to check responses from DIRAC"""
    if not response['OK']:
        raise Exception(response)
    return response['Value']


step_name_mapping = {
    'ApplicationName': 'App',
    'ApplicationVersion': 'Ver',
    # 'CONDDB': 'CDb',
    # 'DDDB': 'DDDb',
    # 'DQTag': 'DQT',
    'ExtraPackages': 'EP',
    'OptionFiles': 'Opt',
    'OptionsFormat': 'OptF',
    'ProcessingPass': 'Pass',
    'StepId': 'Step',
    'StepName': 'Name',
    'SystemConfig': 'SConf',
    'Usable': 'Use',
    'Visible': 'Vis',
    'isMulticore': 'IsM',
    # 'mcTCK': 'mcTCK',
    'textInputFileTypes': 'IFT',
    'textOutputFileTypes': 'OFT',
    # 'textRuntimeProjects': 'TRP'
}

bkSimCondFields = [
    'simCondID',
    'simDesc',
    'BeamCond',
    'BeamEnergy',
    'Generator',
    'MagneticField',
    'DetectorCond',
    'Luminosity',
    'G4settings'
]


def parse_args():
    parser = argparse.ArgumentParser(description='Create and submit a working group production')
    subparsers = parser.add_subparsers()

    # Submit using json info
    parser_json = subparsers.add_parser('json')
    parser_json.add_argument('--json-fn', required=True)
    parser_json.add_argument('--key', required=True)
    parser_json.add_argument('--version', required=True)
    parser_json.set_defaults(func=lambda a: get_production_info_json(a.json_fn, a.key, a.version, a.submit))

    # Submit manually
    parser_manual = subparsers.add_parser('manual')
    parser_manual.add_argument('-a', '--author', required=True,
                               help='The author of the request')
    parser_manual.add_argument('-n', '--name', required=True,
                               help='Name of the request')
    parser_manual.add_argument('-s', '--step', dest='steps', type=int, action='append', required=True,
                               help='The ids of steps to run in order')
    parser_manual.add_argument('-q', '--bk-query', required=True,
                               help='Bookkeeping path to run the production over')
    parser_manual.add_argument('-d', '--dq-flag', default='OK',
                               help='DQ flag to filter the data by')
    parser_manual.add_argument('-i', '--inform-email', default=None,
                               help='Additional email address to send notifications to')
    parser_manual.set_defaults(func=lambda a: get_production_info_manual(a.author, a.name, a.steps, a.bk_query,
                                                                         a.dq_flag, a.inform_email, a.submit))

    # Control flow
    parser.add_argument('--compare', type=int, default=False,
                        help='Compare to an exiting production request')
    parser.add_argument('--print', action='store_true')
    parser.add_argument('--create', action='store_true')
    parser.add_argument('--submit', action='store_true')

    args = parser.parse_args()

    # Initialise DIRAC
    from DIRAC.Core.Base.Script import parseCommandLine  # NOQA
    sys.argv = []
    parseCommandLine()

    production_dict = args.func(args)

    # Run the desired operations
    if args.print:
        pprint(production_dict)

    if args.compare:
        compare(production_dict, args.compare)

    # Dictionaries need to be pickled before submitting
    production_dict = {
        k: cPickle.dumps(v) if isinstance(v, dict) else v
        for k, v in production_dict.items()
    }

    if args.create:
        from DIRAC.Core.DISET.RPCClient import RPCClient

        prod_RPC = RPCClient('ProductionManagement/ProductionRequest')
        production_id = dw(prod_RPC.createProductionRequest(production_dict))
        print('Created production request with ID', production_id)


def get_production_info_manual(author, name, steps, bk_query, dq_flag='OK', inform_email=None, also_submit=False):
    from DIRAC.Core.DISET.RPCClient import RPCClient
    from LHCbDIRAC.BookkeepingSystem.Client.BKQuery import BKQuery, getProcessingPasses
    from LHCbDIRAC.BookkeepingSystem.Client.LHCB_BKKDBClient import LHCB_BKKDBClient

    bk_RPC = RPCClient('Bookkeeping/BookkeepingManager')

    bkQuery = BKQuery(bk_query)  # , prods, runs, fileTypes, visible)
    if not bkQuery:
        raise Exception(bk_query, bkQuery)
    # fileType = bkQuery.getFileTypeList()
    processingPasses = getProcessingPasses(bkQuery)
    # FIXME: If this triggers, find out what it means
    # https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/LHCbDIRAC/BookkeepingSystem/Client/ScriptExecutors.py#L982
    if not len(processingPasses) == 1:
        raise Exception(processingPasses)

    request = {
        # 'RequestID': 42540,
        'RequestAuthor': author,
        'RequestType': 'WGProduction',
        'RequestName': name,
        'Inform': ','.join(inform_email if isinstance(inform_email, list) else [inform_email]) or 'cburr',
        'RequestPriority': '1a',

        'RequestState': 'Submitted' if also_submit else 'New',
        'RequestPDG': '',
        'Comments': None,
        'Description': None,
        'Extra': None,
        'FastSimulationType': 'None',
        'IsModel': 0,
        'MasterID': None,
        'NumberOfEvents': -1,
        'ParentID': None,
        'RequestWG': 'Charm',
        'RetentionRate': 1.0,
        'RealNumberOfEvents': 0,
        'ProID': None,

        # 'StartingDate': None,
        # 'FinalizationDate': None,
        # 'HasSubrequest': 0,
        # 'bk': None,
        # 'bkSrTotal': 0,
        # 'bkTotal': 0,
        # 'rqTotal': 0,
        # 'crTime': datetime.datetime(2017, 11, 24, 13, 8, 12),
        # 'upTime': datetime.datetime(2017, 12, 1, 14, 5, 6),
    }

    bk_query_dict = bkQuery.getQueryDict()
    if not bk_query_dict['ProcessingPass'].startswith('/'):
        raise Exception(bk_query_dict)
    request['SimCondDetail'] = {
        'configName': unicode(bk_query_dict['ConfigName']),
        'configVersion': unicode(bk_query_dict['ConfigVersion']),
        'inFileType': unicode(bk_query_dict['FileType']),
        'inProPass': unicode(bk_query_dict['ProcessingPass'])[1:],
        'inDataQualityFlag': unicode(dq_flag),
        'inProductionID': u'ALL',
        'inTCKs': u'ALL',
    }
    request['EventType'] = bk_query_dict['EventType']

    bk_RPC.getProductions({
        'EventTypeId': bk_query_dict['EventType'],
        'ConfigVersion': bk_query_dict['ConfigVersion'],
        'ProcessingPass': bk_query_dict['ProcessingPass'],
        'ConfigName': bk_query_dict['ConfigName'],
        'ConditionDescription': bk_query_dict['ConditionDescription'],
    })

    conditions_path = '/'.join(bk_query.split('/')[:4])
    for node in LHCB_BKKDBClient(web=True).list('/'.join(bk_query.split('/')[:3])):
        if node['fullpath'] == conditions_path:
            break
    if not node['fullpath'] == conditions_path:
        raise Exception(bk_query, node)
    if 'DaqperiodId' in node:
        # mc = False
        request['SimCondID'] = node['DaqperiodId']
    elif 'SimId' in node:
        # mc = True
        request['SimCondID'] = node['SimId']
    else:
        raise ValueError('Error parsing node: '+repr(dict(node))+' for path '+bk_query)

    request['SimCondition'] = bk_query_dict['ConditionDescription']

    request['ProDetail'] = {'pAll': [], 'pDsc': []}
    previous_oft = None
    for i, step in enumerate(steps, start=1):
        result = dw(bk_RPC.getAvailableSteps({'StepId': step}))
        step_info = dict(zip(result['ParameterNames'], result['Records'][0][:-1]))

        # getAvailableSteps doesn't return everything that is needed
        result_input = dw(bk_RPC.getStepInputFiles(step))
        ift = [dict(zip(result_input['ParameterNames'], x)) for x in result_input['Records']]
        step_info['textInputFileTypes'] = ','.join([str(x['FileType']) for x in ift])

        result_output = dw(bk_RPC.getStepOutputFiles(step))
        oft = [dict(zip(result_output['ParameterNames'], x)) for x in result_output['Records']]
        step_info['textOutputFileTypes'] = ','.join([str(x['FileType']) for x in oft])

        if previous_oft is not None:
            if not any(a['FileType'] == b['FileType'] for a in previous_oft for b in ift):
                raise Exception(i, previous_oft, ift)
        previous_oft = oft

        if 'RuntimeProjects' in step_info:
            # https://gitlab.cern.ch/lhcb-dirac/LHCbWebDIRAC/blob/master/WebApp/handler/LHCbStepManagerHandler.py#L162
            raise NotImplementedError()
        else:
            step_info['textRuntimeProjects'] = ''

        # Create the ProDetail entries for this step
        for key, value in step_info.items():
            if key not in step_name_mapping:
                # If the key is missing from step_name_mapping just continue
                continue

            full_key = 'p' + str(i) + step_name_mapping[key]
            if value is None:
                if key in ['SConf']:
                    request['ProDetail'][full_key] = value
            else:
                request['ProDetail'][full_key] = unicode(value)

        full_key = 'p' + str(i) + 'Html'
        request['ProDetail'][full_key] = (
            '<b>Step {i}</b> '
            '{StepName}({StepId}/{ProcessingPass}) : {ApplicationName}-{ApplicationVersion}<br/>'
            'System config: {SystemConfig} MC TCK: {mcTCK}<br/>'
            'Options: {OptionFiles} Options format: {OptionsFormat} '
            'Multicore: {isMulticore}<br/>'
            'DDDB: {DDDB} Condition DB: {CONDDB} DQTag: {DQTag}<br/>'
            'Extra: {ExtraPackages} '
            'Runtime projects: {textRuntimeProjects}<br/>'
            'Visible: {Visible} Usable:{Usable}<br/>'
            'Input file types: {textInputFileTypes} '
            'Output file types: {textOutputFileTypes}<br/><br/>'
        ).format(i=i, **{k: v or '' for k, v in step_info.items()})

        # Add the entries for the aggregated ProDetail keys
        request['ProDetail']['pAll'].append(step_info['ApplicationName'] + '-' + step_info['ApplicationVersion'])

        if step_info['ApplicationName'] and step_info['ProcessingPass'] and step_info['Visible'] == 'Y':
            request['ProDetail']['pDsc'].append(step_info['ProcessingPass'])

    # Merge the entries for the aggregated ProDetail keys
    request['ProDetail']['pAll'] = ','.join(request['ProDetail']['pAll'])
    request['ProDetail']['pDsc'] = '/'.join(request['ProDetail']['pDsc'])
    request['ProPath'] = request['ProDetail']['pDsc']

    # There should always be a description if there are steps
    if not request['ProDetail']['pDsc']:
        raise Exception(request)

    return request


def get_production_info_json(json_fn, job_name, version, also_submit):
    import wg_prod_ci_tools
    jobs = wg_prod_ci_tools.load_info(json_fn)

    prod_name = relpath(json_fn, dirname(dirname(dirname(__file__))))
    if not prod_name.startswith('productions'):
        raise Exception(prod_name)
    job_info = jobs[job_name]

    name = '-'.join(dirname(prod_name).split('/')[1:])
    full_name = name+'-'+job_name
    app = job_info['application']
    app_version = job_info['application_version']
    options = job_info['options'] if isinstance(job_info['options'], list) else [job_info['options']]
    options = [join('$CHARMWGPRODROOT', dirname(prod_name), o) for o in options]
    # Ensure all of the options files exist
    if not all(isfile(fn.replace('$CHARMWGPRODROOT', '/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/WG/CharmWGProd/'+version))
               for fn in options):
        raise Exception(options)
    input_file_type = basename(job_info['bookkeeping_path'])
    output_file_type = job_info['output_type']

    steps = get_steps(version, name, app, app_version, options, input_file_type, output_file_type)
    print('Steps for', job_name, 'are', steps)

    if 'to_notify' in job_info:
        if isinstance(job_info, basestring):
            job_info['to_notify'] = [job_info['to_notify']]
        inform_email = job_info['to_notify']
    else:
        inform_email = []

    # TODO Set the author correctly
    return get_production_info_manual('cburr', full_name, steps, job_info['bookkeeping_path'], job_info['dq_flag'],
                                      inform_email, also_submit)


def compare(production_dict, compare_id):
    from DIRAC.Core.DISET.RPCClient import RPCClient

    bk_RPC = RPCClient('ProductionManagement/ProductionRequest')
    existing_request_raw = dw(bk_RPC.getProductionRequest([compare_id]))[compare_id]
    existing_request = {
        k: cPickle.loads(v) if isinstance(v, str) and v.startswith('(dp1') else v
        for k, v in existing_request_raw.items()}
    # pprint(existing_request)

    keys_to_ignore = [
        'RequestID',
        'RequestState',

        'StartingDate',
        'FinalizationDate',
        'HasSubrequest',
        'bk',
        'bkSrTotal',
        'bkTotal',
        'rqTotal',
        'crTime',
        'upTime',
    ]

    print('*'*30, 'Start comparison', '*'*30)
    for key in existing_request:
        if key in keys_to_ignore:
            continue
        if isinstance(existing_request[key], dict):
            for sub_key in existing_request[key]:
                if sub_key not in production_dict[key]:
                    print('Missing subkey', sub_key)
                    continue
                if existing_request[key][sub_key] != production_dict[key][sub_key]:
                    print(key, sub_key, existing_request[key][sub_key], production_dict[key][sub_key], sep=' * ')
        else:
            if existing_request[key] != production_dict[key]:
                print(key, existing_request[key], production_dict[key], sep=' * ')
        print()

    for key in production_dict:
        if key in keys_to_ignore:
            continue
        if isinstance(existing_request[key], dict):
            for sub_key in production_dict[key]:
                if sub_key not in existing_request[key]:
                    print('Found extra subkey:', key, sub_key)
            for sub_key in existing_request[key]:
                if sub_key not in production_dict[key]:
                    print('Found missing subkey:', key, sub_key)
        else:
            if key not in existing_request:
                print('Found extra key:', key)
    print('*'*31, 'End comparison', '*'*31)


if __name__ == '__main__':
    try:
        from DIRAC.Core.Base.Script import parseCommandLine  # NOQA
    except ImportError:
        raise
    else:
        parse_args()
