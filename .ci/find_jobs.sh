#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t '
set -x

echo "Running for commit: ${CI_COMMIT_SHA}"

# TODO: I think this is broken
if [[ "$(git rev-parse origin/master > /dev/null 2>&1 && echo success || echo failed)" == "failed" ]]; then
    echo "ERROR: tag (${NEW_TAG_NAME}) already exists"
    exit 10
fi

if [[ -z ${CHANGED_PRODUCTIONS} ]]; then
    echo "No changed productions found, exiting"
    exit
fi

for PROD_NAME in ${CHANGED_PRODUCTIONS}; do
    echo "Preparing test for ${PROD_NAME}"
    python -m wg_prod_ci_tools prepare-ci-tests "${PROD_NAME}"
done

if [[ -n "$(git diff-index --name-only HEAD .gitlab-ci.yml)" ]]; then
    git add .gitlab-ci.yml
    git commit -m "GitLab CI - Prepare tag for testing"
    git tag "${NEW_TAG_NAME}"
    git push origin "${NEW_TAG_NAME}"
fi
