from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from os.path import basename, dirname, join

from .schema import load_info


__all__ = [
    'prepare_ci_tests',
]


TEST_JOB_TEMPLATE = '''
{production_name}-{name}:
  stage: test
  tags:
    - cvmfs
  before_script:
    - source ${{CI_PROJECT_DIR}}/.ci/setup.sh
  script:
    - ({command}) || (cat ${{CI_PROJECT_DIR}}/output/{production_name}/{name}/Local*/*.log && exit 12)
    - (grep "something wrong with execution" ${{CI_PROJECT_DIR}}/output/{production_name}/{name}/DIRAC.log &&
       cat ${{CI_PROJECT_DIR}}/output/{production_name}/{name}/Local*/*.log &&
       exit 11) || true
  artifacts:
    paths:
      - output/
    when: always
    expire_in: 1 week
  only:
    - /^prepared-.*$/
'''

COMMAND_TEMPLATE = '${{CI_PROJECT_DIR}}/test_locally.py {production_name} {job_name} --n-events={n_events}'

SUBMIT_JOB_TEMPLATE = '''
submit_{production_name}_jobs:
  stage: submit
  tags:
    - cvmfs
  before_script:
    - source ${{CI_PROJECT_DIR}}/.ci/setup.sh
  script:
    - {submission_commands}
  only:
    - /^v(\\d+)r(\\d+)$/
  when: manual
  allow_failure: false
'''


def prepare_ci_tests(info_fn):
    production_name = basename(dirname(info_fn))
    info = load_info(info_fn)

    submission_commands = []
    for job_name, job_config in sorted(info.items()):
        if os.environ['CI_COMMIT_REF_NAME'] != 'master':
            n_events = job_config['n_events']
        elif 'CI_CHARMWGPROD_IS_DEBUG' in os.environ:
            n_events = 100
        else:
            n_events = -1

        command = COMMAND_TEMPLATE.format(
            production_name=production_name,
            job_name=job_name,
            n_events=n_events
        )
        job = TEST_JOB_TEMPLATE.format(
            name=job_name,
            production_name=production_name,
            command=command,
        )
        if 'CI_CHARMWGPROD_IS_DEBUG' in os.environ:
            command = '--print json --json-fn {json_fn} --key {key} --version __-__VERSION__-__'
        else:
            command = '--print --create --submit json --json-fn {json_fn} --key {key}  --version __-__VERSION__-__'
        submission_commands.append(
            'lb-run -c best LHCbDIRAC/$LHCBDIRAC_VERSION ${CI_PROJECT_DIR}/.ci/dirac/create_production.py ' +
            command.format(json_fn=info_fn, key=job_name)
        )

        gitlab_ci_config_fn = join(os.environ['CI_PROJECT_DIR'], '.gitlab-ci.yml')
        with open(gitlab_ci_config_fn, 'at') as fp:
            fp.write(job+'\n')

    if submission_commands:
        job = SUBMIT_JOB_TEMPLATE.format(submission_commands='\n    - '.join(submission_commands),
                                         production_name=production_name)
        gitlab_ci_config_fn = join(os.environ['CI_PROJECT_DIR'], '.gitlab-ci.yml')
        with open(gitlab_ci_config_fn, 'at') as fp:
            fp.write(job+'\n')
