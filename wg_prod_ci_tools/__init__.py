from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import distutils.spawn
from subprocess import check_call, CalledProcessError

from .schema import load_info

try:
    FileNotFoundError
except NameError:
    FileNotFoundError = IOError


__all__ = [
    'check_proxy',
    'load_info',
]


def check_proxy():
    try:
        if distutils.spawn.find_executable("voms-proxy-info"):
            check_call(["voms-proxy-info", "--exists", "--valid", "6:00"])
        else:
            check_call("lb-run -c 'best' LHCbDIRAC/$LHCBDIRAC_VERSION voms-proxy-info --exists --valid 6:00", shell=True)
    except CalledProcessError:
        raise RuntimeError('Unable to find valid grid proxy with more than 6 hours of validity')


check_proxy()
