# $Id: $
# Test your line(s) of the stripping

from Gaudi.Configuration import *
from Configurables import DaVinci

DaVinci().PrintFreq = 5000
#DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().TupleFile = "MCtuples.root"
#DaVinci().EvtMax = 10000
DaVinci().EvtMax = -1
DaVinci().DataType = "2016"
DaVinci().InputType = 'MDST'
DaVinci().Simulation = True
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
DaVinci().Lumi = False
DaVinci().RootInTES = '/Event/AllStreams'

from Configurables import MCDecayTreeTuple, TupleToolDecayTreeFitter, TupleToolDecay, TupleToolMCTruth, TupleToolMCBackgroundInfo, MCTupleToolHierarchy, TupleToolTrigger, TupleToolTISTOS, TupleToolPropertime, PropertimeFitter, TupleToolKinematic, MCTupleToolKinematic, TupleToolGeometry, TupleToolEventInfo, TupleToolPrimaries, TupleToolPid, TupleToolTrackInfo, TupleToolRecoStats

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData 
from Configurables import LoKi__Hybrid__TupleTool, FitDecayTrees
import GaudiKernel.SystemOfUnits as Units

dir = [
  'Phys/b2D0MuXKsPiPiLLCharmFromBSemiLine/Particles',
  'Phys/b2D0MuXKsPiPiDDCharmFromBSemiLine/Particles',
]

dec = [
  '[B- => ^(D0 => ^(KS0 => ^pi+ ^pi-) ^pi+ ^pi-) ^mu- Nu]CC',
  '[B- => ^(D0 => ^(KS0 => ^pi+ ^pi-) ^pi+ ^pi-) ^mu- Nu]CC'
]

br = [
  {"B": '[^(B- => (D0 => (KS0 => pi+ pi-) pi+ pi-) mu- Nu)]CC'
   , "D0": '[B- => ^(D0 => (KS0 => pi+ pi-) pi+ pi-) mu- Nu]CC'
   , "mu": '[B- => (D0 => (KS0 => pi+ pi-) pi+ pi-) ^mu- Nu]CC'
   , "piplus": '[B- => (D0 => (KS0 => pi+ pi-) ^pi+ pi-) mu- Nu]CC'
   , "piminus": '[B- => (D0 => (KS0 => pi+ pi-) pi+ ^pi-) mu- Nu]CC'
   , "piplus_KS0": '[B- => (D0 => (KS0 => pi+ pi-) ^pi+ pi-) mu- Nu]CC'
   , "piminus_KS0": '[B- => (D0 => (KS0 => pi+ pi-) pi+ ^pi-) mu- Nu]CC'
   },
  {"B":'[^(B- => (D0 => (KS0 => pi+ pi-) pi+ pi-) mu- Nu)]CC'
   , "D0": '[B- => ^(D0 => (KS0 => pi+ pi-) pi+ pi-) mu- Nu]CC'
   , "mu": '[B- => (D0 => (KS0 => pi+ pi-) pi+ pi-) ^mu- Nu]CC'
   , "piplus": '[B- => (D0 => (KS0 => pi+ pi-) ^pi+ pi-) mu- Nu]CC'
   , "piminus":'[B- => (D0 => (KS0 => pi+ pi-) pi+ ^pi-) mu- Nu]CC'
   , "piplus_KS0": '[B- => (D0 => (KS0 => pi+ pi-) ^pi+ pi-) mu- Nu]CC'
   , "piminus_KS0": '[B- => (D0 => (KS0 => pi+ pi-) pi+ ^pi-) mu- Nu]CC'
   }
]

file = [
  'TupleD02KSPiPiLL',
  'TupleD02KSPiPiDD',
]

tree = [
  'D02KSPiPiLLTuple',
  'D02KSPiPiDDTuple',
]

line_infos = [
  [dir[0], dec[0], br[0], file[0], tree[0]],
  [dir[1], dec[1], br[1], file[1], tree[1]],
]

l0List = ['Muon',
          'DiMuon',
          'Hadron',
          'MuonHigh',
          'Electron',
          'Photon',
         ]
hlt1List = ['Hlt1TrackAllL0',
            'Hlt1TrackMuon',
            'Hlt1SingleMuonHighPT',
           ]
hlt2List = ['Hlt2SingleMuon',
            'Hlt2SingleMuonHighPT',
            'Hlt2TopoMu2BodyBBDT', 
            'Hlt2TopoMu3BodyBBDT', 
            'Hlt2TopoMu4BodyBBDT',
           ]
triggerList = []
for trigger in l0List:
  triggerList.append( 'L0' + trigger + 'Decision')
for trigger in hlt1List:
  triggerList.append( trigger + 'Decision')
for trigger in hlt2List:
  triggerList.append( trigger + 'Decision')

def makeMCDecayTreeTuple( location, decay, branch, alg_name, tuple_name, Algo ):
   tuple = MCDecayTreeTuple( alg_name )
   print alg_name, triggerList    
 
   tuple.ToolList = [ "MCTupleToolKinematic"
                    , "MCTupleToolDecayType"
                    , "MCTupleToolReconstructed" 
                    , "MCTupleToolHierarchy" 
                    , "MCTupleToolPrimaries" 
                    ]

#   tuple.Inputs = [ location ]
   tuple.Decay = decay
   tuple.Branches = branch
   tuple.TupleName = tuple_name

   Algo.append( tuple )

   return tuple

Algos = []
for line_info in line_infos:
  print line_info
  tuple = makeMCDecayTreeTuple( line_info[0], line_info[1], line_info[2], line_info[3], line_info[4] , Algos)
for Algo in Algos:
  DaVinci().appendToMainSequence( [ Algo ])

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

