from Gaudi.Configuration import *

from Configurables import FilterDesktop
from Configurables import CombineParticles
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import DeterministicPrescaler
import GaudiKernel.SystemOfUnits as Units

dir = [
  'Phys/b2D0MuXKsPiPiLLCharmFromBSemiLine/Particles',
  'Phys/b2D0MuXKsPiPiDDCharmFromBSemiLine/Particles',
  'Phys/b2DstarMuXKsPiPiLLCharmFromBSemiLine/Particles',
  'Phys/b2DstarMuXKsPiPiDDCharmFromBSemiLine/Particles',
]
dec = [
  '[B- -> ^(D0 -> ^(KS0 -> ^pi+^pi-) ^pi+^pi-) ^mu-]CC',
  '[B- -> ^(D0 -> ^(KS0 -> ^pi+^pi-) ^pi+^pi-) ^mu-]CC',
  '[B~0 -> ^(D*(2010)+ -> ^(D0 -> ^(KS0 -> ^pi+^pi-) ^pi+^pi-) ^pi+) ^mu-]CC',
  '[B~0 -> ^(D*(2010)+ -> ^(D0 -> ^(KS0 -> ^pi+^pi-) ^pi+^pi-) ^pi+) ^mu-]CC'
]

dtr = [
  "DECTREE('[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC')",
  "DECTREE('[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC')",
  "DECTREE('[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC')",
  "DECTREE('[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC')",
]

br = [
  {"B": "[^(B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-)]CC"
  , "D0": "[B- -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC"
  , "mu": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^mu-]CC"
  , "piplus": "[B- -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) mu-]CC"
  , "piminus": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) mu-]CC"
  , "piplus_KS0": "[B- -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) mu-]CC"
  , "piminus_KS0": "[B- -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) mu-]CC"},
  {"B": "[^(B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-)]CC"
  , "D0": "[B- -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC"
  , "mu": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^mu-]CC"
  , "piplus": "[B- -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) mu-]CC"
  , "piminus": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) mu-]CC"
  , "piplus_KS0": "[B- -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) mu-]CC"
  , "piminus_KS0": "[B- -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) mu-]CC"},
  {"B": "[^(B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-)]CC"
  , "Dst": "[B~0 -> ^(D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "pi_Dst": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^pi+) mu-]CC"
  , "D0": "[B~0 -> (D*(2010)+ -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piplus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) pi+) mu-]CC"
  , "piminus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) pi+) mu-]CC"
  , "mu": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) ^mu-]CC"
  , "piplus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piminus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) pi+) mu-]CC"},
  {"B": "[^(B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-)]CC"
  , "Dst": "[B~0 -> ^(D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "pi_Dst": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^pi+) mu-]CC"
  , "D0": "[B~0 -> (D*(2010)+ -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piplus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) pi+) mu-]CC"
  , "piminus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) pi+) mu-]CC"
  , "mu": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) ^mu-]CC"
  , "piplus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piminus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) pi+) mu-]CC"},
]
file = [
  'TupleD02KSPiPiLL',
  'TupleD02KSPiPiDD',
  'TupleDstarD02KSPiPiLL',
  'TupleDstarD02KSPiPiDD',
] 
tree = [
  'D02KSPiPiLLTuple',
  'D02KSPiPiDDTuple',
  'DstarD02KSPiPiLLTuple',
  'DstarD02KSPiPiDDTuple',
] 

line_infos = [
   [dir[0], dec[0], dtr[0], br[0], file[0], tree[0]],
   [dir[1], dec[1], dtr[1], br[1], file[1], tree[1]],
   [dir[2], dec[2], dtr[2], br[2], file[2], tree[2]],
   [dir[3], dec[3], dtr[3], br[3], file[3], tree[3]],
]   

l0List = ['Muon', 
          'DiMuon',
	  'Hadron', 
	  'MuonHigh',
	  'Electron', 
	  'Photon',
	 ]
hlt1List = ['Hlt1TrackMVA',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1SingleMuonHighPT',
            ]
 
hlt2List = ['Hlt2SingleMuon',
            'Hlt2SingleMuonHighPT',
	    'Hlt2TopoMu2Body', 
            'Hlt2TopoMu3Body', 
            'Hlt2TopoMu4Body',
            ]

triggerList = []
for trigger in l0List:
  triggerList.append( 'L0' + trigger + 'Decision')
for trigger in hlt1List:
  triggerList.append( trigger + 'Decision')
for trigger in hlt2List:
  triggerList.append( trigger + 'Decision')
from Configurables import DecayTreeTuple, TupleToolDecayTreeFitter, TupleToolDecay, TupleToolMCTruth, TupleToolTrigger, TupleToolTISTOS, TupleToolPropertime, PropertimeFitter, TupleToolKinematic, TupleToolGeometry, TupleToolEventInfo, TupleToolPrimaries, TupleToolPid, TupleToolTrackInfo, TupleToolRecoStats
from Configurables import FitDecayTrees

def makeDecayTreeTuple( location, decay, dectree, branch, alg_name, tuple_name, Algo):
  
  tuple = DecayTreeTuple( alg_name )
  print alg_name, triggerList    

  tuple.ToolList = [ "TupleToolPropertime"
                    , "TupleToolTrigger"
                    , "TupleToolKinematic"
                    , "TupleToolGeometry"
                    , "TupleToolEventInfo"
                    , "TupleToolPrimaries"
#                    , "TupleToolTISTOS"
                    , "TupleToolPid"
                    , "TupleToolTrackInfo"
                    , "TupleToolRecoStats"
                      ]
  PRESCALER =  DeterministicPrescaler("PRESCALE_"+alg_name, AcceptFraction = 0.1)
  code = FilterDesktop("bla", Code = "PT > 0.") 
  FILTER = Selection("FILTER_"+alg_name,
                     Algorithm = code,
                     RequiredSelections =[AutomaticData(Location = location)])

  SEQ = SelectionSequence("SEQ_"+alg_name,
                          TopSelection=FILTER,
                          EventPreSelector = [ PRESCALER ])
  Algo.append(SEQ)

  tuple.Inputs = [ SEQ.outputLocation() ]
  #tuple.Inputs = [ location ]
  tuple.Decay = decay
  tuple.Branches = branch
  tuple.TupleName = tuple_name

  tuple.addTool(TupleToolDecay, name = "B")

  LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
  LoKi_B.Variables =  {
      "BPVCORRM" : "BPVCORRM"
      }
  tuple.B.addTool(LoKi_B)
  tuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]
  tuple.B.addTool( TupleToolTISTOS() )
  tuple.B.TupleToolTISTOS.VerboseL0 = True
  tuple.B.TupleToolTISTOS.VerboseHlt1 = True
  tuple.B.TupleToolTISTOS.VerboseHlt2 = True
  tuple.B.TupleToolTISTOS.Verbose = True
  tuple.B.TupleToolTISTOS.TriggerList = triggerList
  tuple.B.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "mu")
  tuple.mu.addTool( TupleToolTISTOS() )
  tuple.mu.TupleToolTISTOS.VerboseL0 = True
  tuple.mu.TupleToolTISTOS.VerboseHlt1 = True
  tuple.mu.TupleToolTISTOS.VerboseHlt2 = True
  tuple.mu.TupleToolTISTOS.Verbose = True
  tuple.mu.TupleToolTISTOS.TriggerList = triggerList
  tuple.mu.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "piplus")
  tuple.piplus.addTool( TupleToolTISTOS() )
  tuple.piplus.TupleToolTISTOS.VerboseL0 = True
  tuple.piplus.TupleToolTISTOS.VerboseHlt1 = True
  tuple.piplus.TupleToolTISTOS.VerboseHlt2 = True
  tuple.piplus.TupleToolTISTOS.Verbose = True
  tuple.piplus.TupleToolTISTOS.TriggerList = triggerList
  tuple.piplus.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "piminus")
  tuple.piminus.addTool( TupleToolTISTOS() )
  tuple.piminus.TupleToolTISTOS.VerboseL0 = True
  tuple.piminus.TupleToolTISTOS.VerboseHlt1 = True
  tuple.piminus.TupleToolTISTOS.VerboseHlt2 = True
  tuple.piminus.TupleToolTISTOS.Verbose = True
  tuple.piminus.TupleToolTISTOS.TriggerList = triggerList
  tuple.piminus.ToolList += ["TupleToolTISTOS"]

  Algo.append( tuple )

  return tuple

def makeDecayTreeTupleDTF( location, decay, dectree, branch, alg_name, tuple_name, Algo):
  
  tuple = DecayTreeTuple( alg_name )
  print alg_name, triggerList    

  tuple.ToolList = [ "TupleToolPropertime"
                    , "TupleToolTrigger"
                    , "TupleToolKinematic"
                    , "TupleToolGeometry"
                    , "TupleToolEventInfo"
                    , "TupleToolPrimaries"
#                   , "TupleToolTISTOS"
                    , "TupleToolPid"
                    , "TupleToolTrackInfo"
                    , "TupleToolRecoStats"
                      ]
  
  tuple.Inputs = [ location ]
  tuple.Decay = decay
  tuple.Branches = branch
  tuple.TupleName = tuple_name
  
  tuple.addTool( TupleToolPropertime() )
  tuple.addTool(TupleToolDecay, name = "B")
  
  LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
  LoKi_B.Variables =  {
      "BPVCORRM" : "BPVCORRM"
      }
  tuple.B.addTool(LoKi_B)
  tuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]
  
  tuple.B.addTool( TupleToolTISTOS() )
  tuple.B.TupleToolTISTOS.VerboseL0 = True
  tuple.B.TupleToolTISTOS.VerboseHlt1 = True
  tuple.B.TupleToolTISTOS.VerboseHlt2 = True
  tuple.B.TupleToolTISTOS.Verbose = True
  tuple.B.TupleToolTISTOS.TriggerList = triggerList
  tuple.B.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "mu")
  tuple.mu.addTool( TupleToolTISTOS() )
  tuple.mu.TupleToolTISTOS.VerboseL0 = True
  tuple.mu.TupleToolTISTOS.VerboseHlt1 = True
  tuple.mu.TupleToolTISTOS.VerboseHlt2 = True
  tuple.mu.TupleToolTISTOS.Verbose = True
  tuple.mu.TupleToolTISTOS.TriggerList = triggerList
  tuple.mu.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "piplus")
  tuple.piplus.addTool( TupleToolTISTOS() )
  tuple.piplus.TupleToolTISTOS.VerboseL0 = True
  tuple.piplus.TupleToolTISTOS.VerboseHlt1 = True
  tuple.piplus.TupleToolTISTOS.VerboseHlt2 = True
  tuple.piplus.TupleToolTISTOS.Verbose = True
  tuple.piplus.TupleToolTISTOS.TriggerList = triggerList
  tuple.piplus.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "piminus")
  tuple.piminus.addTool( TupleToolTISTOS() )
  tuple.piminus.TupleToolTISTOS.VerboseL0 = True
  tuple.piminus.TupleToolTISTOS.VerboseHlt1 = True
  tuple.piminus.TupleToolTISTOS.VerboseHlt2 = True
  tuple.piminus.TupleToolTISTOS.Verbose = True
  tuple.piminus.TupleToolTISTOS.TriggerList = triggerList
  tuple.piminus.ToolList += ["TupleToolTISTOS"]

  tuple.addTool(TupleToolDecay, name = "D0")
  LoKi_DTFMASS = LoKi__Hybrid__TupleTool("LoKi_DTFMASS")
  # DTF_FUN(function, usePV, mass constraint)
  LoKi_DTFMASS.Variables = {
     "LoKi_DTF_CHI2"        : "DTF_CHI2 (False, 'KS0' )",
     "LoKi_DTF_NDOF"        : "DTF_NDOF (False, 'KS0' )",
     "LoKi_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, 'KS0' )",
     "LoKi_DTF_PROB"        : "DTF_PROB (False, 'KS0' )",
     "LoKi_DTF_M"           : "DTF_FUN ( M , False, 'KS0' )",
     "LoKi_DTF_MM"          : "DTF_FUN ( MM, False, 'KS0' )",
     "LoKi_DTF_P"           : "DTF_FUN ( P , False, 'KS0' )",
     "LoKi_DTF_PT"          : "DTF_FUN ( PT, False, 'KS0' )",
     "LoKi_DTF_PE"          : "DTF_FUN ( E, False, 'KS0' )",
     "LoKi_DTF_PX"          : "DTF_FUN ( PX, False, 'KS0' )",
     "LoKi_DTF_PY"          : "DTF_FUN ( PY, False, 'KS0' )",
     "LoKi_DTF_PZ"          : "DTF_FUN ( PZ, False, 'KS0' )",
     "LoKi_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, 'KS0' )",
     "LoKi_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, 'KS0' )",
     "LoKi_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, 'KS0' )",
     "LoKi_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, 'KS0' )",
     "LoKi_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, 'KS0' )",
     "LoKi_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, 'KS0' )",
     "LoKi_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, 'KS0' )",
     "LoKi_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, 'KS0' )",
     "LoKi_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, 'KS0' )",
     "LoKi_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, 'KS0' )",
     "LoKi_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, 'KS0' )",
     "LoKi_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, 'KS0' )",
     "LoKi_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, 'KS0' )",
     "LoKi_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, 'KS0' )",
     "LoKi_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, 'KS0' )",
     "LoKi_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, 'KS0' )",
     "LoKi_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, 'KS0' )",
     "LoKi_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, 'KS0' )",
     "LoKi_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, 'KS0' )",
     "LoKi_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, 'KS0' )",
     "LoKi_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, 'KS0' )",
     "LoKi_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, 'KS0' )",
     "LoKi_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, 'KS0' )",
     "LoKi_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, 'KS0' )",
     "LoKi_mD0KS0_DTF_CHI2"        : "DTF_CHI2 (False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_NDOF"        : "DTF_NDOF (False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PROB"        : "DTF_PROB (False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_M"           : "DTF_FUN ( M , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_MM"          : "DTF_FUN ( MM, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_P"           : "DTF_FUN ( P , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PT"          : "DTF_FUN ( PT, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PE"          : "DTF_FUN ( E, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PX"          : "DTF_FUN ( PX, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PY"          : "DTF_FUN ( PY, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_PZ"          : "DTF_FUN ( PZ, False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, strings(['D0','KS0']))",
     "LoKi_mD0KS0_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, strings(['D0','KS0']) )",
     "LoKi_mD0KS0_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, strings(['D0','KS0']) )",
  }
  tuple.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_DTFMASS"]
  tuple.D0.addTool(LoKi_DTFMASS)

  Algo.append( tuple )

  return tuple

################
# Configure DaVinci
################

from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ','/Event/pRec']

print 'DaVinci configuration'

TESlocation = "/Event/Charm/"
from Configurables import TrackScaleState
scaler = TrackScaleState( 'Scaler', RootInTES =TESlocation )

from Configurables import DaVinci, L0Conf
dv = DaVinci()
dv.RootInTES = TESlocation
dv.DataType = '2016'
dv.EvtMax = -1
#dv.EvtMax = 10000
dv.PrintFreq = 5000
dv.Simulation   = False
# MagUp tags
#dv.DDDBtag = "dddb-20120831"
#from Configurables import CondDB
# TrackScaleState works only with latest CondDB tags
#CondDB().UseLatestTags = ["2012"]
#dv.CondDBtag = "cond-20130114"
 
dv.Lumi = True
dv.HistogramFile = "DVHistos.root"    # Histogram file
dv.TupleFile = "DVNtuples.root"       # Ntuple
dv.InputType = 'MDST'
dv.appendToMainSequence( [ scaler ])
dv.UserAlgorithms += [eventNodeKiller]

Algos = []
for line_info in line_infos:
  print line_info
  tuple = makeDecayTreeTuple( line_info[0], line_info[1], line_info[2], line_info[3], line_info[4], line_info[5] , Algos)
  tuple_DTF = makeDecayTreeTupleDTF( line_info[0], line_info[1], line_info[2], line_info[3], line_info[4]+'_DTF', line_info[5]+'_DTF', Algos)

for Algo in Algos:
  dv.appendToMainSequence( [ Algo ])

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from Configurables import Gaudi__IODataManager as IODataManager
IODataManager().UseGFAL=False
