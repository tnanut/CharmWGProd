from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import DecayTreeTuple, TupleToolDecayTreeFitter, TupleToolDecay, TupleToolMCTruth, TupleToolTrigger, TupleToolTISTOS, TupleToolPropertime, PropertimeFitter, TupleToolKinematic, TupleToolGeometry, TupleToolEventInfo, TupleToolPrimaries, TupleToolPid, TupleToolTrackInfo, TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool

# Stream and stripping line we want to use
stream = 'AllStreams'
line = 'b2DstarMuXKsPiPiLLCharmFromBSemiLine'

# Create an ntuple to capture D*+ decays from the StrippingLine line
dtt = DecayTreeTuple('Tuple_DstarD02KSPiPiLL')
dtt.Inputs = ['/Event/Phys/{0}/Particles'.format(line)]
dtt.Decay = '[B~0 -> ^(D*(2010)+ -> ^(D0 -> ^(KS0 -> ^pi+^pi-) ^pi+^pi-) ^pi+) ^mu-]CC'

dtt.addBranches({"B": "[^(B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-)]CC",
                 "Dst": "[B~0 -> ^(D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC",
                 "pi_Dst": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^pi+) mu-]CC",
                 "D0": "[B~0 -> (D*(2010)+ -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC",
                 "piplus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) pi+) mu-]CC",
                 "piminus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) pi+) mu-]CC",
                 "mu": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) ^mu-]CC",
                 "piplus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) pi+) mu-]CC",
                 "piminus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) pi+) mu-]CC"})

dtt.ToolList = [ "TupleToolPropertime"
                   , "TupleToolTrigger"
                   , "TupleToolKinematic"
                   , "TupleToolGeometry"
                   , "TupleToolEventInfo"
                   , "TupleToolPrimaries"
                   , "TupleToolPid"
                   , "TupleToolTrackInfo"
                   , "TupleToolRecoStats"
                   ]

l0List = ['Muon',
          'DiMuon',
          'Hadron',
          'MuonHigh',
          'Electron',
          'Photon',
         ]
hlt1List = ['Hlt1TrackMVA',
            'Hlt1TrackMuon',
            'Hlt1TrackMuonMVA',
            'Hlt1SingleMuonHighPT',
            ]

hlt2List = ['Hlt2SingleMuon',
            'Hlt2SingleMuonHighPT',
            'Hlt2TopoMu2Body',
            'Hlt2TopoMu3Body',
            'Hlt2TopoMu4Body',
            ]

triggerList = []
for trigger in l0List:
  triggerList.append( 'L0' + trigger + 'Decision')
for trigger in hlt1List:
  triggerList.append( trigger + 'Decision')
for trigger in hlt2List:
  triggerList.append( trigger + 'Decision')

dtt.addTool( TupleToolPropertime() )
dtt.addTool(TupleToolDecay, name = "B")

LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
LoKi_B.Variables =  {
    "BPVCORRM" : "BPVCORRM"
    }
dtt.B.addTool(LoKi_B)
dtt.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]

dtt.B.addTool( TupleToolTISTOS() )
dtt.B.TupleToolTISTOS.VerboseL0 = True
dtt.B.TupleToolTISTOS.VerboseHlt1 = True
dtt.B.TupleToolTISTOS.VerboseHlt2 = True
dtt.B.TupleToolTISTOS.Verbose = True
dtt.B.TupleToolTISTOS.TriggerList = triggerList
dtt.B.ToolList += ["TupleToolTISTOS"]

dtt.addTool(TupleToolDecay, name = "mu")
dtt.mu.addTool( TupleToolTISTOS() )
dtt.mu.TupleToolTISTOS.VerboseL0 = True
dtt.mu.TupleToolTISTOS.VerboseHlt1 = True
dtt.mu.TupleToolTISTOS.VerboseHlt2 = True
dtt.mu.TupleToolTISTOS.Verbose = True
dtt.mu.TupleToolTISTOS.TriggerList = triggerList
dtt.mu.ToolList += ["TupleToolTISTOS"]

dtt.addTool(TupleToolDecay, name = "piplus")
dtt.piplus.addTool( TupleToolTISTOS() )
dtt.piplus.TupleToolTISTOS.VerboseL0 = True
dtt.piplus.TupleToolTISTOS.VerboseHlt1 = True
dtt.piplus.TupleToolTISTOS.VerboseHlt2 = True
dtt.piplus.TupleToolTISTOS.Verbose = True
dtt.piplus.TupleToolTISTOS.TriggerList = triggerList
dtt.piplus.ToolList += ["TupleToolTISTOS"]

dtt.addTool(TupleToolDecay, name = "piminus")
dtt.piminus.addTool( TupleToolTISTOS() )
dtt.piminus.TupleToolTISTOS.VerboseL0 = True
dtt.piminus.TupleToolTISTOS.VerboseHlt1 = True
dtt.piminus.TupleToolTISTOS.VerboseHlt2 = True
dtt.piminus.TupleToolTISTOS.Verbose = True
dtt.piminus.TupleToolTISTOS.TriggerList = triggerList
dtt.piminus.ToolList += ["TupleToolTISTOS"]

dtt.addTool(TupleToolDecay, name = "D0")
LoKi_DTFMASS = LoKi__Hybrid__TupleTool("LoKi_DTFMASS")
LoKi_DTFMASS.Variables = {
    "LoKi_DTF_CHI2"        : "DTF_CHI2 (False, 'KS0' )",
    "LoKi_DTF_NDOF"        : "DTF_NDOF (False, 'KS0' )",
    "LoKi_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, 'KS0' )",
    "LoKi_DTF_PROB"        : "DTF_PROB (False, 'KS0' )",
    "LoKi_DTF_M"           : "DTF_FUN ( M , False, 'KS0' )",
    "LoKi_DTF_MM"          : "DTF_FUN ( MM, False, 'KS0' )",
    "LoKi_DTF_P"           : "DTF_FUN ( P , False, 'KS0' )",
    "LoKi_DTF_PT"          : "DTF_FUN ( PT, False, 'KS0' )",
    "LoKi_DTF_PE"          : "DTF_FUN ( E, False, 'KS0' )",
    "LoKi_DTF_PX"          : "DTF_FUN ( PX, False, 'KS0' )",
    "LoKi_DTF_PY"          : "DTF_FUN ( PY, False, 'KS0' )",
    "LoKi_DTF_PZ"          : "DTF_FUN ( PZ, False, 'KS0' )",
    "LoKi_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, 'KS0' )",
    "LoKi_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, 'KS0' )",
    "LoKi_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, 'KS0' )",
    "LoKi_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, 'KS0' )",
    "LoKi_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, 'KS0' )",
    "LoKi_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, 'KS0' )",
    "LoKi_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, 'KS0' )",
    "LoKi_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, 'KS0' )",
    "LoKi_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, 'KS0' )",
    "LoKi_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, 'KS0' )",
    "LoKi_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, 'KS0' )",
    "LoKi_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, 'KS0' )",
    "LoKi_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, 'KS0' )",
    "LoKi_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, 'KS0' )",
    "LoKi_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, 'KS0' )",
    "LoKi_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, 'KS0' )",
    "LoKi_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, 'KS0' )",
    "LoKi_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, 'KS0' )",
    "LoKi_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, 'KS0' )",
    "LoKi_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, 'KS0' )",
    "LoKi_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, 'KS0' )",
    "LoKi_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, 'KS0' )",
    "LoKi_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, 'KS0' )",
    "LoKi_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, 'KS0' )",
    "LoKi_mD0KS0_DTF_CHI2"        : "DTF_CHI2 (False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_NDOF"        : "DTF_NDOF (False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PROB"        : "DTF_PROB (False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_M"           : "DTF_FUN ( M , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_MM"          : "DTF_FUN ( MM, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_P"           : "DTF_FUN ( P , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PT"          : "DTF_FUN ( PT, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PE"          : "DTF_FUN ( E, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PX"          : "DTF_FUN ( PX, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PY"          : "DTF_FUN ( PY, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_PZ"          : "DTF_FUN ( PZ, False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, strings(['D0','KS0']))",
    "LoKi_mD0KS0_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, strings(['D0','KS0']) )",
    "LoKi_mD0KS0_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, strings(['D0','KS0']) )"
    }
dtt.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_DTFMASS"]
dtt.D0.addTool(LoKi_DTFMASS)

from Configurables import EventNodeKiller

event_node_killer = EventNodeKiller('StripKiller')
event_node_killer.Nodes = ['/Event/AllStreams', '/Event/Strip']

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

strip = 'stripping28r1'
streams = buildStreams(stripping=strippingConfiguration(strip),
                       archive=strippingArchive(strip))

custom_stream = StrippingStream('CustomStream')
custom_line = 'Stripping'+line

for stream in streams:
    for sline in stream.lines:
        if sline.name() == custom_line:
            custom_stream.appendLines([sline])

from Configurables import ProcStatusCheck
filterBadEvents = ProcStatusCheck()

sc = StrippingConf(Streams=[custom_stream],
                   MaxCandidates=2000,
                   AcceptBadEvents=False,
                   BadEventSelection=filterBadEvents)

from Configurables import DaVinci

# Configure DaVinci
DaVinci().UserAlgorithms += [dtt]
DaVinci().InputType = 'DST'
DaVinci().TupleFile = 'DVntuple.root'
DaVinci().PrintFreq = 1000
DaVinci().DataType = '2016'
DaVinci().Simulation = True
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().EvtMax = -1
DaVinci().DDDBtag = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
DaVinci().appendToMainSequence([event_node_killer, sc.sequence()])
