# $Id: $
# Test your line(s) of the stripping

from Gaudi.Configuration import *
from Configurables import DaVinci

from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

# Standard stripping17 
stripping='stripping21r0p1'
config  = strippingConfiguration(stripping)
archive = strippingArchive(stripping)
streams = buildStreams(stripping=config, archive=archive) 

# Select my line
Charm_stream = StrippingStream("_Charm_muDST")
MyLines = [ 'Strippingb2D0MuXKsPiPiLLCharmFromBSemiLine', 'Strippingb2D0MuXKsPiPiDDCharmFromBSemiLine', 'Strippingb2DstarMuXKsPiPiLLCharmFromBSemiLine', 'Strippingb2DstarMuXKsPiPiDDCharmFromBSemiLine' ]

for stream in streams:
  for line in stream.lines:
    if line.name() in MyLines:
      Charm_stream.appendLines( [ line ] ) 

from Configurables import  ProcStatusCheck
filterBadEvents =  ProcStatusCheck()

# Configure the stripping using the same options as in Reco06-Stripping10
sc = StrippingConf( HDRLocation = "SomeNonExistingLocation", Streams = [ Charm_stream],
                    AcceptBadEvents = False,
                    BadEventSelection = filterBadEvents )

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from Configurables import StrippingReport
sr = StrippingReport(Selections = sc.selections())

from Configurables import FilterDesktop
from Configurables import TrackSmearState
# Apply the momentum smearing (for MC only)
smear = TrackSmearState( 'Smear' ) ## default configuration is perfectly fine

DaVinci().PrintFreq = 5000
DaVinci().HistogramFile = 'DV_stripping_histos.root'
DaVinci().TupleFile = "MCtuples.root"
#DaVinci().EvtMax = 1000
DaVinci().EvtMax = -1
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ sr ] )
DaVinci().DataType = "2012"
DaVinci().InputType = 'DST'
DaVinci().Simulation = True
# MagUp tags
DaVinci().DDDBtag = "Sim08-20130503-1"
DaVinci().CondDBtag = "Sim08-20130503-1-vc-mu100"
DaVinci().Lumi = False
DaVinci().appendToMainSequence( [ smear  ] )

from Configurables import DecayTreeTuple, TupleToolDecayTreeFitter, TupleToolDecay, TupleToolMCTruth, TupleToolMCBackgroundInfo, MCTupleToolHierarchy, TupleToolTrigger, TupleToolTISTOS, TupleToolPropertime, PropertimeFitter, TupleToolKinematic, TupleToolGeometry, TupleToolEventInfo, TupleToolPrimaries, TupleToolPid, TupleToolTrackInfo, TupleToolRecoStats

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand, AutomaticData 
from Configurables import LoKi__Hybrid__TupleTool, FitDecayTrees
import GaudiKernel.SystemOfUnits as Units

dir = [
  'Phys/b2D0MuXKsPiPiLLCharmFromBSemiLine/Particles',
  'Phys/b2D0MuXKsPiPiDDCharmFromBSemiLine/Particles',
  'Phys/b2DstarMuXKsPiPiLLCharmFromBSemiLine/Particles',
  'Phys/b2DstarMuXKsPiPiDDCharmFromBSemiLine/Particles'
]

dec = [
  '[B- -> ^(D0 -> ^(KS0 -> ^pi+ ^pi-) ^pi+ ^pi-) ^mu-]CC',
  '[B- -> ^(D0 -> ^(KS0 -> ^pi+ ^pi-) ^pi+ ^pi-) ^mu-]CC',
  '[B~0 -> ^(D*(2010)+ -> ^(D0 -> ^(KS0 -> ^pi+ ^pi-) ^pi+ ^pi-) ^pi+) ^mu-]CC',
  '[B~0 -> ^(D*(2010)+ -> ^(D0 -> ^(KS0 -> ^pi+ ^pi-) ^pi+ ^pi-) ^pi+) ^mu-]CC'
]

br = [
  {"B": "[^(B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-)]CC"
  , "D0": "[B- -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC"
  , "mu": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^mu-]CC"
  , "piplus": "[B- -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) mu-]CC"
  , "piminus": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) mu-]CC"
  , "piplus_KS0": "[B- -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) mu-]CC"
  , "piminus_KS0": "[B- -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) mu-]CC"},
  {"B": "[^(B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-)]CC"
  , "D0": "[B- -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) mu-]CC"
  , "mu": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^mu-]CC"
  , "piplus": "[B- -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) mu-]CC"
  , "piminus": "[B- -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) mu-]CC"
  , "piplus_KS0": "[B- -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) mu-]CC"
  , "piminus_KS0": "[B- -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) mu-]CC"},
  {"B": "^([B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC)"
  , "Dst": "[B~0 -> ^(D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "pi_Dst": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^pi+) mu-]CC"
  , "D0": "[B~0 -> (D*(2010)+ -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piplus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) pi+) mu-]CC"
  , "piminus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) pi+) mu-]CC"
  , "mu": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) ^mu-]CC"
  , "piplus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piminus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) pi+) mu-]CC"},
  {"B": "[^(B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-)]CC"
  , "Dst": "[B~0 -> ^(D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "pi_Dst": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) ^pi+) mu-]CC"
  , "D0": "[B~0 -> (D*(2010)+ -> ^(D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piplus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) ^pi+ pi-) pi+) mu-]CC"
  , "piminus": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ ^pi-) pi+) mu-]CC"
  , "mu": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ pi-) pi+ pi-) pi+) ^mu-]CC"
  , "piplus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> ^pi+ pi-) pi+ pi-) pi+) mu-]CC"
  , "piminus_KS0": "[B~0 -> (D*(2010)+ -> (D0 -> (KS0 -> pi+ ^pi-) pi+ pi-) pi+) mu-]CC"}
]

file = [
  'TupleD02KSPiPiLL',
  'TupleD02KSPiPiDD',
  'TupleDstarD0KSPiPiLL',
  'TupleDstarD0KSPiPiDD'
]

tree = [
  'D02KSPiPiLLTuple',
  'D02KSPiPiDDTuple',
  'DstarD0KSPiPiLLTuple',
  'DstarD0KSPiPiDDTuple'
]

line_infos = [
  [dir[0], dec[0], br[0], file[0], tree[0]],
  [dir[1], dec[1], br[1], file[1], tree[1]],
  [dir[2], dec[2], br[2], file[2], tree[2]],
  [dir[3], dec[3], br[3], file[3], tree[3]]
]

l0List = ['Muon',
          'DiMuon',
          'Hadron',
          'MuonHigh',
          'Electron',
          'Photon',
         ]
hlt1List = ['Hlt1TrackAllL0',
            'Hlt1TrackMuon',
            'Hlt1SingleMuonHighPT',
           ]
hlt2List = ['Hlt2SingleMuon',
            'Hlt2SingleMuonHighPT',
            'Hlt2TopoMu2BodyBBDT', 'Hlt2TopoMu3BodyBBDT', 'Hlt2TopoMu4BodyBBDT',
           ]
triggerList = []
for trigger in l0List:
  triggerList.append( 'L0' + trigger + 'Decision')
for trigger in hlt1List:
  triggerList.append( trigger + 'Decision')
for trigger in hlt2List:
  triggerList.append( trigger + 'Decision')

def makeDecayTreeTuple( location, decay, branch, alg_name, tuple_name, Algo ):
   tuple = DecayTreeTuple( alg_name )
   print alg_name, triggerList    
 
   tuple.ToolList = [ "TupleToolPropertime"
                     , "TupleToolTrigger"
                     , "TupleToolKinematic"
                     , "TupleToolGeometry"
                     , "TupleToolEventInfo"
                     , "TupleToolPrimaries"
                     , "TupleToolTISTOS"
                     , "TupleToolPid"
                     , "TupleToolTrackInfo"
                     , "TupleToolRecoStats"
                       ]

   tuple.Inputs = [ location ]
   tuple.Decay = decay
   tuple.Branches = branch
   tuple.TupleName = tuple_name
   
   tuple.addTool( TupleToolPropertime() )
 
   tuple.addTool( TupleToolTISTOS() )
   tuple.TupleToolTISTOS.VerboseL0 = True
   tuple.TupleToolTISTOS.VerboseHlt1 = True
   tuple.TupleToolTISTOS.VerboseHlt2 = True
   tuple.TupleToolTISTOS.Verbose = True
   tuple.TupleToolTISTOS.TriggerList = triggerList
   tuple.addTool(TupleToolDecay, name = "B")
   
   LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
   LoKi_B.Variables =  {
       "BPVCORRM" : "BPVCORRM"
       }
   tuple.B.addTool(LoKi_B)
   tuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]
   
   tuple.addTool(TupleToolMCBackgroundInfo, name="TupleToolMCBackgroundInfo")
   tuple.TupleToolMCBackgroundInfo.Verbose=True
   tuple.addTool(TupleToolMCTruth, name="truth")
   tuple.truth.ToolList+=["MCTupleToolHierarchy"]
   tuple.ToolList+=["TupleToolMCBackgroundInfo/TupleToolMCBackgroundInfo"]
   tuple.ToolList+=["TupleToolMCTruth/truth"]

   Algo.append( tuple )

   return tuple

def makeDecayTreeTupleDTF( location, decay, branch, alg_name, tuple_name, Algo ):
   tuple = DecayTreeTuple( alg_name )
   print alg_name, triggerList    
 
   tuple.ToolList = [ "TupleToolPropertime"
                     , "TupleToolTrigger"
                     , "TupleToolKinematic"
                     , "TupleToolGeometry"
                     , "TupleToolEventInfo"
                     , "TupleToolPrimaries"
                     , "TupleToolTISTOS"
                     , "TupleToolPid"
                     , "TupleToolTrackInfo"
                     , "TupleToolRecoStats"
                       ]

   tuple.Inputs = [ location ]
   tuple.Decay = decay
   tuple.Branches = branch
   tuple.TupleName = tuple_name
   
   tuple.addTool( TupleToolPropertime() )
 
   tuple.addTool( TupleToolTISTOS() )
   tuple.TupleToolTISTOS.VerboseL0 = True
   tuple.TupleToolTISTOS.VerboseHlt1 = True
   tuple.TupleToolTISTOS.VerboseHlt2 = True
   tuple.TupleToolTISTOS.Verbose = True
   tuple.TupleToolTISTOS.TriggerList = triggerList
   tuple.addTool(TupleToolDecay, name = "B")
   
   LoKi_B=LoKi__Hybrid__TupleTool("LoKi_B")
   LoKi_B.Variables =  {
       "BPVCORRM" : "BPVCORRM"
       }
   tuple.B.addTool(LoKi_B)
   tuple.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_B"]

   tuple.addTool(TupleToolMCBackgroundInfo, name="TupleToolMCBackgroundInfo")
   tuple.TupleToolMCBackgroundInfo.Verbose=True
   tuple.addTool(TupleToolMCTruth, name="truth")
   tuple.truth.ToolList+=["MCTupleToolHierarchy"]
   tuple.ToolList+=["TupleToolMCBackgroundInfo/TupleToolMCBackgroundInfo"]
   tuple.ToolList+=["TupleToolMCTruth/truth"]

   tuple.addTool(TupleToolDecay, name = "D0")
   LoKi_DTFMASS = LoKi__Hybrid__TupleTool("LoKi_DTFMASS")
   # DTF_FUN(function, usePV, mass constraint)
   LoKi_DTFMASS.Variables = {
      "LoKi_DTF_CHI2"        : "DTF_CHI2 (False, 'KS0' )",
      "LoKi_DTF_NDOF"        : "DTF_NDOF (False, 'KS0' )",
      "LoKi_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, 'KS0' )",
      "LoKi_DTF_PROB"        : "DTF_PROB (False, 'KS0' )",
      "LoKi_DTF_M"           : "DTF_FUN ( M , False, 'KS0' )",
      "LoKi_DTF_MM"          : "DTF_FUN ( MM, False, 'KS0' )",
      "LoKi_DTF_P"           : "DTF_FUN ( P , False, 'KS0' )",
      "LoKi_DTF_PT"          : "DTF_FUN ( PT, False, 'KS0' )",
      "LoKi_DTF_PE"          : "DTF_FUN ( E, False, 'KS0' )",
      "LoKi_DTF_PX"          : "DTF_FUN ( PX, False, 'KS0' )",
      "LoKi_DTF_PY"          : "DTF_FUN ( PY, False, 'KS0' )",
      "LoKi_DTF_PZ"          : "DTF_FUN ( PZ, False, 'KS0' )",
      "LoKi_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, 'KS0' )",
      "LoKi_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, 'KS0' )",
      "LoKi_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, 'KS0' )",
      "LoKi_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, 'KS0' )",
      "LoKi_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, 'KS0' )",
      "LoKi_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, 'KS0' )",
      "LoKi_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, 'KS0' )",
      "LoKi_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, 'KS0' )",
      "LoKi_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, 'KS0' )",
      "LoKi_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, 'KS0' )",
      "LoKi_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, 'KS0' )",
      "LoKi_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, 'KS0' )",
      "LoKi_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, 'KS0' )",
      "LoKi_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, 'KS0' )",
      "LoKi_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, 'KS0' )",
      "LoKi_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, 'KS0' )",
      "LoKi_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, 'KS0' )",
      "LoKi_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, 'KS0' )",
      "LoKi_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, 'KS0' )",
      "LoKi_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, 'KS0' )",
      "LoKi_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, 'KS0' )",
      "LoKi_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, 'KS0' )",
      "LoKi_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, 'KS0' )",
      "LoKi_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_M"    : "DTF_FUN ( CHILD(CHILD(M, 1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_MM"   : "DTF_FUN ( CHILD(CHILD(MM,1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_PE"   : "DTF_FUN ( CHILD(CHILD(E, 1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_PX"   : "DTF_FUN ( CHILD(CHILD(PX,1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_PY"   : "DTF_FUN ( CHILD(CHILD(PY,1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h1_PZ"   : "DTF_FUN ( CHILD(CHILD(PZ,1),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_M"    : "DTF_FUN ( CHILD(CHILD(M, 2),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_MM"   : "DTF_FUN ( CHILD(CHILD(MM,2),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_PE"   : "DTF_FUN ( CHILD(CHILD(E, 2),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_PX"   : "DTF_FUN ( CHILD(CHILD(PX,2),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_PY"   : "DTF_FUN ( CHILD(CHILD(PY,2),1) , False, 'KS0' )",
      "LoKi_DTF_KS0_h2_PZ"   : "DTF_FUN ( CHILD(CHILD(PZ,2),1) , False, 'KS0' )",

      "LoKi_mD0KS0_DTF_CHI2"        : "DTF_CHI2 (False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_NDOF"        : "DTF_NDOF (False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_CHI2NDOF"    : "DTF_CHI2NDOF (False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PROB"        : "DTF_PROB (False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_M"           : "DTF_FUN ( M , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_MM"          : "DTF_FUN ( MM, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_P"           : "DTF_FUN ( P , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PT"          : "DTF_FUN ( PT, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PE"          : "DTF_FUN ( E, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PX"          : "DTF_FUN ( PX, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PY"          : "DTF_FUN ( PY, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_PZ"          : "DTF_FUN ( PZ, False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_M"       : "DTF_FUN ( CHILD(M, 1) , False, strings(['D0','KS0']))",
      "LoKi_mD0KS0_DTF_KS0_MM"      : "DTF_FUN ( CHILD(MM,1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_P"       : "DTF_FUN ( CHILD(P, 1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_PT"      : "DTF_FUN ( CHILD(PT,1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_PE"      : "DTF_FUN ( CHILD(E, 1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_PX"      : "DTF_FUN ( CHILD(PX,1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_PY"      : "DTF_FUN ( CHILD(PY,1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_PZ"      : "DTF_FUN ( CHILD(PZ,1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_M"        : "DTF_FUN ( CHILD(M, 2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_MM"       : "DTF_FUN ( CHILD(MM,2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_P"        : "DTF_FUN ( CHILD(P, 2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_PT"       : "DTF_FUN ( CHILD(PT,2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_PE"       : "DTF_FUN ( CHILD(E, 2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_PX"       : "DTF_FUN ( CHILD(PX,2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_PY"       : "DTF_FUN ( CHILD(PY,2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h1_PZ"       : "DTF_FUN ( CHILD(PZ,2) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_M"        : "DTF_FUN ( CHILD(M, 3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_MM"       : "DTF_FUN ( CHILD(MM,3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_P"        : "DTF_FUN ( CHILD(P, 3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_PT"       : "DTF_FUN ( CHILD(PT,3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_PE"       : "DTF_FUN ( CHILD(E, 3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_PX"       : "DTF_FUN ( CHILD(PX,3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_PY"       : "DTF_FUN ( CHILD(PY,3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_h2_PZ"       : "DTF_FUN ( CHILD(PZ,3) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_M"    : "DTF_FUN ( CHILD(CHILD(M, 1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_MM"   : "DTF_FUN ( CHILD(CHILD(MM,1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_PE"   : "DTF_FUN ( CHILD(CHILD(E, 1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_PX"   : "DTF_FUN ( CHILD(CHILD(PX,1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_PY"   : "DTF_FUN ( CHILD(CHILD(PY,1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h1_PZ"   : "DTF_FUN ( CHILD(CHILD(PZ,1),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_M"    : "DTF_FUN ( CHILD(CHILD(M, 2),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_MM"   : "DTF_FUN ( CHILD(CHILD(MM,2),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_PE"   : "DTF_FUN ( CHILD(CHILD(E, 2),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_PX"   : "DTF_FUN ( CHILD(CHILD(PX,2),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_PY"   : "DTF_FUN ( CHILD(CHILD(PY,2),1) , False, strings(['D0','KS0']) )",
      "LoKi_mD0KS0_DTF_KS0_h2_PZ"   : "DTF_FUN ( CHILD(CHILD(PZ,2),1) , False, strings(['D0','KS0']) )",
   }
   tuple.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_DTFMASS"]
   tuple.D0.addTool(LoKi_DTFMASS)
   
   Algo.append( tuple )

   return tuple

Algos = []
for line_info in line_infos:
  print line_info
#  tuple = makeDecayTreeTuple( line_info[0], line_info[1], line_info[2], line_info[3], line_info[4] , Algos)
  tuple_DTF = makeDecayTreeTupleDTF( line_info[0], line_info[1], line_info[2], line_info[3]+'_DTF', line_info[4]+'_DTF', Algos)
for Algo in Algos:
  DaVinci().appendToMainSequence( [ Algo ])


MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

from Configurables import Gaudi__IODataManager as IODataManager
IODataManager().UseGFAL=False

from GaudiConf import IOHelper

# Use the local input data
#IOHelper().inputFiles([
#    './00029155_00000001_1.allstreams.dst'
#], clear=True)
