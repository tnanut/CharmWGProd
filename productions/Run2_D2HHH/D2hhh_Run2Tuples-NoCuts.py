from Configurables import DaVinci
from Configurables import DecayTreeTuple
import DecayTreeTuple.Configuration

year = DaVinci().DataType
channel = DaVinci().TupleFile.split('.')[0]
print channel
print year


rootInTES = '/Event/Turbo'  #2015  and #2016
if (year == '2017' or year == '2018' ):
   rootInTES =  '/Event/Charmcharged/Turbo'
   if (channel == 'D2KPP' or channel == 'Ds2KKP') : rootInTES = '/Event/Charmspec/Turbo'

Dhhh_line = ''
Dhhh_decay = ''
Dhhh_branches = ''
Dhhh_Tuplename = ''
Dhhh_Tuplename1 = ''
Dhhh_Daug1 = ''
Daug1='1'
Daug2='2'
Daug3='3'
mother = 'D'
motherDTF = 'D+'
ndaughters = 3

if (channel == 'D2KPP'):
   Dhhh_Tuplename = "ntp_KPP"
   Dhhh_line = 'Hlt2CharmHadDpToKmPipPipTurbo'
   Dhhh_decay = "[D+ -> K- pi+ pi+]CC"
   Dhhh_Daug1 = 'Km'

if (channel == 'D2KKP'):
   Dhhh_Tuplename = "ntp_KKP"
   Dhhh_line = 'Hlt2CharmHadDpToKmKpPipTurbo'
   Dhhh_decay = "[D+ -> K- K+ pi+]CC"
   Dhhh_Daug1 = 'Km'

if (channel == 'D2PPP'):
   Dhhh_Tuplename = "ntp_PPP"
   Dhhh_line = 'Hlt2CharmHadDpToPimPipPipTurbo'
   Dhhh_decay = "[D+ -> pi- pi+ pi+]CC"
   Dhhh_Daug1 = 'Pim'

if (channel == 'D2PPK'):
   Dhhh_Tuplename = "ntp_KPPos"
   Dhhh_line = 'Hlt2CharmHadDpToKpPimPipTurbo'
   Dhhh_decay = "[D+ -> K+ pi- pi+]CC"
   Daug1='2'
   Daug2='3'
   Daug3='1'
   Dhhh_Daug1 = 'Pim'

if (channel == 'D2KKK'):
   Dhhh_Tuplename = "ntp_KKK"
   Dhhh_line = 'Hlt2CharmHadDpToKmKpKpTurbo'
   Dhhh_decay = "[D+ -> K- K+ K+]CC"
   Dhhh_Daug1 = 'Km'

if (channel == 'Ds2PKK'):
   Dhhh_Tuplename = "ntp_KKPos"
   Dhhh_line = 'Hlt2CharmHadDspToKpKpPimTurbo'
   Dhhh_decay = "[D_s+ -> K+ K+ pi-]CC"
   Daug1='3'
   Daug2='1'
   Daug3='2'
   Dhhh_Daug1 = 'Pim'
   mother = 'Ds'
   motherDTF = 'D_s+'

if (channel == 'Ds2KKP'):
   Dhhh_Tuplename = "ntp_DsKKP"
   Dhhh_line = 'Hlt2CharmHadDspToKmKpPipTurbo'
   Dhhh_decay = "[D_s+ -> K- K+ pi+]CC"
   Dhhh_Daug1 = 'Km'
   mother = 'Ds'
   motherDTF = 'D_s+'

if (channel == 'Ds2PPP'):
   Dhhh_Tuplename = "ntp_DsPPP"
   Dhhh_line = 'Hlt2CharmHadDspToPimPipPipTurbo'
   Dhhh_decay = "[D_s+ -> pi- pi+ pi+]CC"
   Dhhh_Daug1 = 'Pim'
   mother = 'Ds'
   motherDTF = 'D_s+'

if (channel == 'Ds2PPK'):
   Dhhh_Tuplename = "ntp_DsKPPos"
   Dhhh_line = 'Hlt2CharmHadDspToKpPimPipTurbo'
   Dhhh_decay = "[D_s+ -> K+ pi- pi+]CC"
   Daug1='2'
   Daug2='3'
   Daug3='1'
   Dhhh_Daug1 = 'Pim'
   mother = 'Ds'
   motherDTF = 'D_s+'

if (channel == 'Ds2KKK'):
   Dhhh_Tuplename = "ntp_DsKKK"
   Dhhh_line = 'Hlt2CharmHadDspToKmKpKpTurbo'
   Dhhh_decay = "[D_s+ -> K- K+ K+]CC"
   Dhhh_Daug1 = 'Km'
   mother = 'Ds'
   motherDTF = 'D_s+'

if (channel == 'Ds2KPP'):
   Dhhh_Tuplename = "ntp_DsKPP"
   Dhhh_line = 'Hlt2CharmHadDspToKmPipPipTurbo'
   Dhhh_decay = "[D_s+ -> K- pi+ pi+]CC"
   Dhhh_Daug1 = 'Km'
   mother = 'Ds'
   motherDTF = 'D_s+'


from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *


from Configurables import  TupleToolTISTOS,L0TriggerTisTos, TriggerTisTos, TupleToolPrimaries

from Configurables import GaudiSequencer, ChargedProtoANNPIDConf
from Configurables import  LoKi__Hybrid__TupleTool


tmpToolList = [ "TupleToolPrimaries", "TupleToolEventInfo", "TupleToolRecoStats", "TupleToolL0Data"]
motherToolList = [ "TupleToolKinematic", "TupleToolGeometry","TupleToolPropertime","TupleToolTISTOS" ,"LoKi::Hybrid::TupleTool/LoKi_Mother"]

tmpTriggerList =  [
    "L0PhotonDecision",
    "L0HadronDecision",
    "L0MuonDecision",
    "L0ElectronDecision",
    "L0DiMuonDecision",
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1GlobalDecision",
    "L0GlobalDecision",
]

LoKiTupleMother  = LoKi__Hybrid__TupleTool("LoKi_Mother")
LoKiTupleMother.Preambulo = ["from LoKiTracks.decorators import *"]
LoKiTupleMother.Variables =  {
     "ETA"        : "ETA"
    , "PHI"        : "PHI"
    , "BPVTRGPOINTING" : "BPVTRGPOINTING"
    , "DOCA" : "DOCAMAX"
    , "DOCA12" : "DOCA("+Daug1+","+Daug2+")"
    , "DOCA23" : "DOCA("+Daug2+","+Daug3+")"
    , "DOCA13" : "DOCA("+Daug1+","+Daug3+")"
    , "DOCA12_CHI2" : "DOCACHI2("+Daug1+","+Daug2+")"
    , "DOCA23_CHI2" : "DOCACHI2("+Daug2+","+Daug3+")"
    , "DOCA13_CHI2" : "DOCACHI2("+Daug1+","+Daug3+")"

    , "p1_ETA"     : "CHILD("+Daug1+", ETA)"
    , "p1_PHI"     : "CHILD("+Daug1+", PHI)"
    , "p1_IP_OWNPV"     : "CHILD("+Daug1+", BPVIP())"
    , "p1_IPCHI2_OWNPV"     : "CHILD("+Daug1+", BPVIPCHI2())"
    , "p1_PX"     : "CHILD("+Daug1+", PX)"
    , "p1_PY"     : "CHILD("+Daug1+", PY)"
    , "p1_PZ"     : "CHILD("+Daug1+", PZ)"
    , "p1_P"     : "CHILD("+Daug1+", P)"
    , "p1_PT"     : "CHILD("+Daug1+", PT)"
    , "p1_PE"     : "CHILD("+Daug1+", E)"
    , "p1_M"     : "CHILD("+Daug1+", M)"
    , "p1_ID"     : "CHILD("+Daug1+", ID)"
    , "p1_PIDe"     : "CHILD("+Daug1+", PIDe)"
    , "p1_PIDmu"     : "CHILD("+Daug1+", PIDmu)"
    , "p1_PIDK"     : "CHILD("+Daug1+", PIDK)"
    , "p1_PIDp"     : "CHILD("+Daug1+", PIDp)"
    , "p1_isMuon"     : "CHILD("+Daug1+", switch(ISMUON, 1, 0))"
    , "p1_hasMuon"     : "CHILD("+Daug1+", switch(HASMUON, 1, 0))"
    , "p1_hasRich"     : "CHILD("+Daug1+", switch(HASRICH, 1, 0))"
    , "p1_hasCalo"     : "CHILD("+Daug1+", switch(HASCALOS, 1, 0))"
    , "p1_KEY"       : "CHILD("+Daug1+", KEY)"
    , "p1_TRCHI2DOF"       : "CHILD("+Daug1+", TRCHI2DOF)"
    , "p1_MC15TuneV1_ProbNNk"       : "CHILD("+Daug1+", ANNPID('K','MC15TuneV1'))"
    , "p1_MC15TuneV1_ProbNNe"       : "CHILD("+Daug1+", ANNPID('e','MC15TuneV1'))"
    , "p1_MC15TuneV1_ProbNNpi"       : "CHILD("+Daug1+", ANNPID('pi','MC15TuneV1'))"
    , "p1_MC15TuneV1_ProbNNmu"       : "CHILD("+Daug1+", ANNPID('mu','MC15TuneV1'))"
    , "p1_MC15TuneV1_ProbNNp"       : "CHILD("+Daug1+", ANNPID('p','MC15TuneV1'))"
    , "p1_MC15TuneV1_ProbNNghost"       : "CHILD("+Daug1+", ANNPID('ghost','MC15TuneV1'))"

    , "p1_Hlt1TrackMVADecision_TOS"   : "CHILD("+Daug1+", switch(tos('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p1_Hlt1TwoTrackMVADecision_TOS"   : "CHILD("+Daug1+", switch(tos('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p1_L0PhotonDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0Photon.*')  , 1 , 0 ))"
    , "p1_L0HadronDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0Hadron.*')  , 1 , 0 ))"
    , "p1_L0MuonDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0Muon.*')  , 1 , 0 ))"
    , "p1_L0ElectronDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0Electron.*')  , 1 , 0 ))"
    , "p1_L0DiMuonDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0DiMuon.*')  , 1 , 0 ))"
    , "p1_L0GlobalDecision_TOS"   : "CHILD("+Daug1+", switch(tos('L0Global.*')  , 1 , 0 ))"

    , "p1_Hlt1TrackMVADecision_TIS"   : "CHILD("+Daug1+", switch(tis('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p1_Hlt1TwoTrackMVADecision_TIS"   : "CHILD("+Daug1+", switch(tis('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p1_L0PhotonDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0Photon.*')  , 1 , 0 ))"
    , "p1_L0HadronDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0Hadron.*')  , 1 , 0 ))"
    , "p1_L0MuonDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0Muon.*')  , 1 , 0 ))"
    , "p1_L0ElectronDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0Electron.*')  , 1 , 0 ))"
    , "p1_L0DiMuonDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0DiMuon.*')  , 1 , 0 ))"
    , "p1_L0GlobalDecision_TIS"   : "CHILD("+Daug1+", switch(tis('L0Global.*')  , 1 , 0 ))"

    , "p2_ETA"     : "CHILD("+Daug2+", ETA)"
    , "p2_PHI"     : "CHILD("+Daug2+", PHI)"
    , "p2_IP_OWNPV"     : "CHILD("+Daug2+", BPVIP())"
    , "p2_IPCHI2_OWNPV"     : "CHILD("+Daug2+", BPVIPCHI2())"
    , "p2_PX"     : "CHILD("+Daug2+", PX)"
    , "p2_PY"     : "CHILD("+Daug2+", PY)"
    , "p2_PZ"     : "CHILD("+Daug2+", PZ)"
    , "p2_P"     : "CHILD("+Daug2+", P)"
    , "p2_PT"     : "CHILD("+Daug2+", PT)"
    , "p2_PE"     : "CHILD("+Daug2+", E)"
    , "p2_M"     : "CHILD("+Daug2+", M)"
    , "p2_ID"     : "CHILD("+Daug2+", ID)"
    , "p2_PIDe"     : "CHILD("+Daug2+", PIDe)"
    , "p2_PIDmu"     : "CHILD("+Daug2+", PIDmu)"
    , "p2_PIDK"     : "CHILD("+Daug2+", PIDK)"
    , "p2_PIDp"     : "CHILD("+Daug2+", PIDp)"
    , "p2_isMuon"     : "CHILD("+Daug2+", switch(ISMUON, 1, 0))"
    , "p2_hasMuon"     : "CHILD("+Daug2+", switch(HASMUON, 1, 0))"
    , "p2_hasRich"     : "CHILD("+Daug2+", switch(HASRICH, 1, 0))"
    , "p2_hasCalo"     : "CHILD("+Daug2+", switch(HASCALOS, 1, 0))"
    , "p2_KEY"       : "CHILD("+Daug2+", KEY)"
    , "p2_TRCHI2DOF"       : "CHILD("+Daug2+", TRCHI2DOF)"
    , "p2_MC15TuneV1_ProbNNk"       : "CHILD("+Daug2+", ANNPID('K','MC15TuneV1'))"
    , "p2_MC15TuneV1_ProbNNe"       : "CHILD("+Daug2+", ANNPID('e','MC15TuneV1'))"
    , "p2_MC15TuneV1_ProbNNpi"       : "CHILD("+Daug2+", ANNPID('pi','MC15TuneV1'))"
    , "p2_MC15TuneV1_ProbNNmu"       : "CHILD("+Daug2+", ANNPID('mu','MC15TuneV1'))"
    , "p2_MC15TuneV1_ProbNNp"       : "CHILD("+Daug2+", ANNPID('p','MC15TuneV1'))"
    , "p2_MC15TuneV1_ProbNNghost"       : "CHILD("+Daug2+", ANNPID('ghost','MC15TuneV1'))"

    , "p2_Hlt1TrackMVADecision_TOS"   : "CHILD("+Daug2+", switch(tos('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p2_Hlt1TwoTrackMVADecision_TOS"   : "CHILD("+Daug2+", switch(tos('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p2_L0PhotonDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0Photon.*')  , 1 , 0 ))"
    , "p2_L0HadronDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0Hadron.*')  , 1 , 0 ))"
    , "p2_L0MuonDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0Muon.*')  , 1 , 0 ))"
    , "p2_L0ElectronDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0Electron.*')  , 1 , 0 ))"
    , "p2_L0DiMuonDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0DiMuon.*')  , 1 , 0 ))"
    , "p2_L0GlobalDecision_TOS"   : "CHILD("+Daug2+", switch(tos('L0Global.*')  , 1 , 0 ))"

    , "p2_Hlt1TrackMVADecision_TIS"   : "CHILD("+Daug2+", switch(tis('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p2_Hlt1TwoTrackMVADecision_TIS"   : "CHILD("+Daug2+", switch(tis('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p2_L0PhotonDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0Photon.*')  , 1 , 0 ))"
    , "p2_L0HadronDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0Hadron.*')  , 1 , 0 ))"
    , "p2_L0MuonDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0Muon.*')  , 1 , 0 ))"
    , "p2_L0ElectronDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0Electron.*')  , 1 , 0 ))"
    , "p2_L0DiMuonDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0DiMuon.*')  , 1 , 0 ))"
    , "p2_L0GlobalDecision_TIS"   : "CHILD("+Daug2+", switch(tis('L0Global.*')  , 1 , 0 ))"

    , "p3_ETA"     : "CHILD("+Daug3+", ETA)"
    , "p3_PHI"     : "CHILD("+Daug3+", PHI)"
    , "p3_IP_OWNPV"     : "CHILD("+Daug3+", BPVIP())"
    , "p3_IPCHI2_OWNPV"     : "CHILD("+Daug3+", BPVIPCHI2())"
    , "p3_PX"     : "CHILD("+Daug3+", PX)"
    , "p3_PY"     : "CHILD("+Daug3+", PY)"
    , "p3_PZ"     : "CHILD("+Daug3+", PZ)"
    , "p3_P"     : "CHILD("+Daug3+", P)"
    , "p3_PT"     : "CHILD("+Daug3+", PT)"
    , "p3_PE"     : "CHILD("+Daug3+", E)"
    , "p3_M"     : "CHILD("+Daug3+", M)"
    , "p3_ID"     : "CHILD("+Daug3+", ID)"
    , "p3_PIDe"     : "CHILD("+Daug3+", PIDe)"
    , "p3_PIDmu"     : "CHILD("+Daug3+", PIDmu)"
    , "p3_PIDK"     : "CHILD("+Daug3+", PIDK)"
    , "p3_PIDp"     : "CHILD("+Daug3+", PIDp)"
    , "p3_isMuon"     : "CHILD("+Daug3+", switch(ISMUON, 1, 0))"
    , "p3_hasMuon"     : "CHILD("+Daug3+", switch(HASMUON, 1, 0))"
    , "p3_hasRich"     : "CHILD("+Daug3+", switch(HASRICH, 1, 0))"
    , "p3_hasCalo"     : "CHILD("+Daug3+", switch(HASCALOS, 1, 0))"
    , "p3_KEY"       : "CHILD("+Daug3+", KEY)"
    , "p3_TRCHI2DOF"       : "CHILD("+Daug3+", TRCHI2DOF)"
    , "p3_MC15TuneV1_ProbNNk"       : "CHILD("+Daug3+", ANNPID('K','MC15TuneV1'))"
    , "p3_MC15TuneV1_ProbNNe"       : "CHILD("+Daug3+", ANNPID('e','MC15TuneV1'))"
    , "p3_MC15TuneV1_ProbNNpi"       : "CHILD("+Daug3+", ANNPID('pi','MC15TuneV1'))"
    , "p3_MC15TuneV1_ProbNNmu"       : "CHILD("+Daug3+", ANNPID('mu','MC15TuneV1'))"
    , "p3_MC15TuneV1_ProbNNp"       : "CHILD("+Daug3+", ANNPID('p','MC15TuneV1'))"
    , "p3_MC15TuneV1_ProbNNghost"       : "CHILD("+Daug3+", ANNPID('ghost','MC15TuneV1'))"

    , "p3_Hlt1TrackMVADecision_TOS"   : "CHILD("+Daug3+", switch(tos('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p3_Hlt1TwoTrackMVADecision_TOS"   : "CHILD("+Daug3+", switch(tos('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p3_L0PhotonDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0Photon.*')  , 1 , 0 ))"
    , "p3_L0HadronDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0Hadron.*')  , 1 , 0 ))"
    , "p3_L0MuonDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0Muon.*')  , 1 , 0 ))"
    , "p3_L0ElectronDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0Electron.*')  , 1 , 0 ))"
    , "p3_L0DiMuonDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0DiMuon.*')  , 1 , 0 ))"
    , "p3_L0GlobalDecision_TOS"   : "CHILD("+Daug3+", switch(tos('L0Global.*')  , 1 , 0 ))"

    , "p3_Hlt1TrackMVADecision_TIS"   : "CHILD("+Daug3+", switch(tis('Hlt1TrackMVA.*')  , 1 , 0 ))"
    , "p3_Hlt1TwoTrackMVADecision_TIS"   : "CHILD("+Daug3+", switch(tis('Hlt1TwoTrackMVA.*')  , 1 , 0 ))"
    , "p3_L0PhotonDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0Photon.*')  , 1 , 0 ))"
    , "p3_L0HadronDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0Hadron.*')  , 1 , 0 ))"
    , "p3_L0MuonDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0Muon.*')  , 1 , 0 ))"
    , "p3_L0ElectronDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0Electron.*')  , 1 , 0 ))"
    , "p3_L0DiMuonDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0DiMuon.*')  , 1 , 0 ))"
    , "p3_L0GlobalDecision_TIS"   : "CHILD("+Daug3+", switch(tis('L0Global.*')  , 1 , 0 ))"

    ,'CONE1ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEANGLE', -10. )"
    ,'CONE1MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEMULT', -10. )"
    ,'CONE1PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar05',  'CONEPTASYM', -10. )"
    ,'CONE1ANGLEDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEANGLE', -10. )"
    ,'CONE1MULTDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEMULT', -10. )"
    ,'CONE1PTASYMDaughter':"RELINFO('" + rootInTES + "/" + Dhhh_line + "/"+ Dhhh_Daug1+"ConeVar10', 'CONEPTASYM', -10. )"
    ,'CONE2ANGLE'  : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEANGLE', -10. )"
    ,'CONE2MULT'   : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEMULT', -10. )"
    ,'CONE2PTASYM' : "RELINFO('" + rootInTES + "/" + Dhhh_line + "/DConeVar15',  'CONEPTASYM', -10. )"
}

from Configurables import LoKi__Hybrid__EvtTupleTool
LoKiEVENTVariables = LoKi__Hybrid__EvtTupleTool('LoKiEVENTVariables')
LoKiEVENTVariables.Preambulo = ["from LoKiTracks.decorators import *",
                                 "from LoKiNumbers.decorators import *",
                                 "from LoKiCore.functions import *"
                                ]

LoKiTuple = LoKi__Hybrid__TupleTool("LoKi_DTF")
LoKiTuple.Variables =  {
    "DTF_M"       : "DTF_FUN ( M , True, '"+motherDTF+"' )",
    "DTF_PX"      : "DTF_FUN ( PX , True, '"+motherDTF+"' )",
    "DTF_PY"      : "DTF_FUN ( PY , True, '"+motherDTF+"' )",
    "DTF_PZ"      : "DTF_FUN ( PZ , True, '"+motherDTF+"' )",
    "DTF_PE"      : "DTF_FUN ( E , True, '"+motherDTF+"' )",
    "DTF_p1_PX"   : "DTF_FUN ( CHILD("+Daug1+", PX), True, '"+motherDTF+"' )",
    "DTF_p1_PY"   : "DTF_FUN ( CHILD("+Daug1+", PY), True, '"+motherDTF+"' )",
    "DTF_p1_PZ"   : "DTF_FUN ( CHILD("+Daug1+", PZ), True, '"+motherDTF+"' )",
    "DTF_p1_PE"   : "DTF_FUN ( CHILD("+Daug1+", E), True, '"+motherDTF+"' )",
    "DTF_p2_PX"   : "DTF_FUN ( CHILD("+Daug2+", PX), True, '"+motherDTF+"' )",
    "DTF_p2_PY"   : "DTF_FUN ( CHILD("+Daug2+", PY), True, '"+motherDTF+"' )",
    "DTF_p2_PZ"   : "DTF_FUN ( CHILD("+Daug2+", PZ), True, '"+motherDTF+"' )",
    "DTF_p2_PE"   : "DTF_FUN ( CHILD("+Daug2+", E), True, '"+motherDTF+"' )",
    "DTF_p3_PX"   : "DTF_FUN ( CHILD("+Daug3+", PX), True, '"+motherDTF+"' )",
    "DTF_p3_PY"   : "DTF_FUN ( CHILD("+Daug3+", PY), True, '"+motherDTF+"' )",
    "DTF_p3_PZ"   : "DTF_FUN ( CHILD("+Daug3+", PZ), True, '"+motherDTF+"' )",
    "DTF_p3_PE"   : "DTF_FUN ( CHILD("+Daug3+", E), True, '"+motherDTF+"' )",
#    "DTF_CHI2"    : "DTF_CHI2( True, '"+motherDTF+"' )",
#    "DTF_NDOF"    : "DTF_NDOF( True, '"+motherDTF+"' )",
    "DTF_CHI2NDOF": "DTF_CHI2NDOF( True, '"+motherDTF+"' )"

# constrain only in vertex
,   "DTF_VTX_M"       : "DTF_FUN ( M , True )",
    "DTF_VTX_PX"      : "DTF_FUN ( PX , True )",
    "DTF_VTX_PY"      : "DTF_FUN ( PY , True )",
    "DTF_VTX_PZ"      : "DTF_FUN ( PZ , True )",
    "DTF_VTX_PE"      : "DTF_FUN ( E , True )",
    "DTF_VTX_p1_PX"   : "DTF_FUN ( CHILD("+Daug1+", PX), True )",
    "DTF_VTX_p1_PY"   : "DTF_FUN ( CHILD("+Daug1+", PY), True )",
    "DTF_VTX_p1_PZ"   : "DTF_FUN ( CHILD("+Daug1+", PZ), True )",
    "DTF_VTX_p1_PE"   : "DTF_FUN ( CHILD("+Daug1+", E), True )",
    "DTF_VTX_p2_PX"   : "DTF_FUN ( CHILD("+Daug2+", PX), True )",
    "DTF_VTX_p2_PY"   : "DTF_FUN ( CHILD("+Daug2+", PY), True )",
    "DTF_VTX_p2_PZ"   : "DTF_FUN ( CHILD("+Daug2+", PZ), True )",
    "DTF_VTX_p2_PE"   : "DTF_FUN ( CHILD("+Daug2+", E), True )",
    "DTF_VTX_p3_PX"   : "DTF_FUN ( CHILD("+Daug3+", PX), True )",
    "DTF_VTX_p3_PY"   : "DTF_FUN ( CHILD("+Daug3+", PY), True )",
    "DTF_VTX_p3_PZ"   : "DTF_FUN ( CHILD("+Daug3+", PZ), True )",
    "DTF_VTX_p3_PE"   : "DTF_FUN ( CHILD("+Daug3+", E), True )",
#    "DTF_VTX_CHI2"    : "DTF_CHI2( True )",
#    "DTF_VTX_NDOF"    : "DTF_NDOF( True )",
    "DTF_VTX_CHI2NDOF": "DTF_CHI2NDOF( True )"
    }

from Gaudi.Configuration import *
from PhysConf.Selections import Selection, AutomaticData, PrintSelection, MomentumScaling

mysel = AutomaticData   (Dhhh_line+"/Particles")
#mysel = PrintSelection  ( mysel )
mysel = MomentumScaling ( mysel, Turbo = True , Year = year )

ntp1 = DecayTreeTuple(Dhhh_Tuplename)
ntp1.Decay = Dhhh_decay
ntp1.TupleName = "ntp1"

if( year=='2015' or year=='2016'):
   ntp1.WriteP2PVRelations = False
   ntp1.InputPrimaryVertices = "/Event/Turbo/Primary"


ntp1.Inputs = [ mysel.outputLocation() ]


ntp1.ToolList += tmpToolList
#ntp1.addTool(TupleToolPrimaries)
#ntp1.TupleToolPrimaries.InputLocation= "/Event/Turbo/Primary"
ntp1.addTool(LoKiEVENTVariables , name = 'LoKiEVENTVariables' )

ntp1.ToolList += motherToolList
ntp1.addTool(LoKiTupleMother)
ntp1.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_DTF"]
ntp1.addTool(LoKiTuple)

# define branches to add TupleToolTrackPosition to all tracks
# this needs some ugly string manipulation....
def nth_replace(string, old, new, n=0):
  groups = string.split(old)
  nth_split = [old.join(groups[:n+1]), old.join(groups[n+1:])]
  return new.join(nth_split)
stupid_dict = {'0' : Daug1, '1' : Daug2, '2' : Daug3}
rhs = Dhhh_decay[Dhhh_decay.index('>'):]
for i in range (ndaughters):
  rhs = nth_replace(rhs,' '," ${{p{}}}".format(stupid_dict[str(i)]),i)

instances = ntp1.setDescriptorTemplate(Dhhh_decay[0:Dhhh_decay.index('>')] + rhs)
for k,v in instances.iteritems():
  v.InheritTools = False
  v.ToolList = [] #switch off default tools
  tttp = v.addTupleTool("TupleToolTrackPosition/"+k+"TTTP")
  tttp.Z = 8630. # mm

tttt = TupleToolTISTOS()
tttt.addTool(L0TriggerTisTos())
tttt.addTool(TriggerTisTos())
ntp1.addTool(tttt)

ntp1.TupleToolTISTOS.Verbose = True
ntp1.TupleToolTISTOS.TriggerList = tmpTriggerList
ntp1.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
ntp1.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.
ntp1.TupleToolTISTOS.FillHlt2 = True

#=================

# Necessary DaVinci parameters #################
from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary-NoCuts.xml'

from PhysConf.Filters import LoKi_Filters
trigger_filter = LoKi_Filters(
    HLT2_Code="HLT_PASS_RE('.*'+'%s'+'.*')" % (Dhhh_line)
)
DaVinci().EventPreFilters = trigger_filter.filters('TriggerFilter')

DaVinci().Simulation   = False
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1   
DaVinci().Lumi = True
DaVinci().TupleFile = 'TeslaTuplesNoCuts_Loki.root'
DaVinci().PrintFreq = 10000
DaVinci().UserAlgorithms = []
DaVinci().UserAlgorithms.append (mysel)
DaVinci().UserAlgorithms.append (ntp1)

DaVinci().InputType = 'MDST'
DaVinci().RootInTES = rootInTES

DaVinci().Turbo = True


from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = year)
