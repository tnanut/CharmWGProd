"""Configure DaVinci for creating ntuples from Turbo data."""
import os

from Configurables import DaVinci, DecayTreeTuple, TupleToolDecay, LoKi__Hybrid__TupleTool
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
from LoKiPhys.decorators import *

########################################
# User configuration begins
########################################

# # Data-taking year
# data_type = '2017'
data_type = DaVinci().DataType
# # data or MC?
IsMC=DaVinci().Simulation

# The full name of the HLT2 line
lnames = ['KmPimPipPip','KpPimPimPip',
          'PimPimPipPip','KmKpPimPip',
          'KmKmKpPip','KmKpKpPim']
hlt2_line_names = {}
for x in lnames: hlt2_line_names[x]='Hlt2CharmHadDstp2D0Pip_D02'+x+'Turbo'

# Stream the line belongs to (ignored for 2016 data)
turbo_stream = 'Charmmultibody'

# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the branches should be
# called
decay_descriptors = {
    'KmPimPipPip' : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KpPimPimPip' : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}pi- ${D0_P2}pi+ ${D0_P3}pi-) ${Dst_pi}pi+]CC',
    'PimPimPipPip': '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}pi- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKpPimPip'  : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}pi- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKmKpPip'   : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}K- ${D0_P3}pi+) ${Dst_pi}pi+]CC',
    'KmKpKpPim'   : '${Dst}[D*(2010)+ -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}K- ${D0_P2}K+ ${D0_P3}pi-) ${Dst_pi}pi+]CC'
}

# Probnn tunings
Probnn = ["MC15TuneV1"] if data_type == '2015' else []# uncomment if using DV < v40r0 or new tuning becomes available

########################################
# User configuration ends
########################################

if data_type in ['2015','2016']:
    turbo_stream = ''

# DecayTreeTuple input location template
turbo_input = '{0}/Particles'

# Define some trigger lines to check for TISTOS
commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
commonTriggerList += ['Hlt1'+x+'Decision' for x in ['TrackMVA','TwoTrackMVA']]
TriggerLists = {'Dst'   : commonTriggerList,
                'D0'    : commonTriggerList,
                'D0_P0' : commonTriggerList,
                'D0_P1' : commonTriggerList,
                'D0_P2' : commonTriggerList,
                'D0_P3' : commonTriggerList,
                'Dst_pi': commonTriggerList}

def CreateDTT(lname, IsMC=False):
    hlt2_line_name = hlt2_line_names[lname]
    decay_desc = decay_descriptors[lname]
    IsTurbo = 'Turbo' in hlt2_line_name
    name = hlt2_line_name.replace('CharmHad', '').replace('Turbo', '')
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(name))
    dtt.Inputs = [turbo_input.format(hlt2_line_name)]
    dtt.setDescriptorTemplate(decay_desc)
    # list of general tools
    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
    dtt.ToolList = ['TupleTool'+tool for tool in GeneralTools]
    # DTF Refit info
    Dst_Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), 'Dst') ]
    fit = Dst_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose= True
    fit.constrainToOriginVertex = True
    fit.UpdateDaughters = True
    # DTF Mass-Constrained D0 fit
    fit_D0_MC = Dst_Node.addTupleTool('TupleToolDecayTreeFitter/D0Fit')
    fit_D0_MC.Verbose= True
    fit_D0_MC.constrainToOriginVertex = True
    fit_D0_MC.daughtersToConstrain += ['D0']
    fit_D0_MC.UpdateDaughters = True
    # Setup the long-lived particles branches
    IntermediateTools = [ 'Geometry', 'Kinematic', 'Propertime']
    for vtx in ['D0']:
        Vtx = dtt.allConfigurables['%s.%s' % ( dtt.name(), vtx) ]
        Vtx.InheritTools = False
        Vtx.ToolList = [  "TupleTool"+name for name in IntermediateTools ]
        if IsMC:
            Vtx.addTool( dtt.allConfigurables['ToolSvc.TupleToolMCTruth'] )
            Vtx.ToolList += [ "TupleToolMCTruth" ]
    # Setup the stable particles branches
    TrackTools = ['Geometry','Kinematic','Pid','TrackInfo']
    for trk in ['D0_P0', 'D0_P1', 'D0_P2', 'D0_P3', 'Dst_pi' ]:
        branch = dtt.allConfigurables['%s.%s' % ( dtt.name(), trk) ]
        # P info
        branch.InheritTools = False
        branch.ToolList = [ "TupleTool"+name for name in TrackTools ]
        if len(Probnn):
            from Configurables import TupleToolANNPID
            branch.ToolList += [ "TupleToolANNPID" ]
            branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
            branch.TupleToolANNPID.ANNPIDTunes = Probnn
        if IsMC:
            branch.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            branch.ToolList += [ "TupleToolMCTruth",'TupleToolL0Calo' ]
    # TISTOS
    from Configurables import TupleToolTISTOS
    for nodeName, tList in TriggerLists.iteritems():
        Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList
        if IsTurbo: Node.TupleToolTISTOS.FillHlt2 = False
    # Finishing
    return dtt
# Create a tuple for each line
ntuples = [CreateDTT(lname,IsMC) for lname in lnames]

# Speed up processing by filtering out events with no positive signal decisions
# trigger_filter = LoKi_Filters(
#     HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(hlt2_line_name)
# )

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

dv = DaVinci()
#dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = [MomentumCorrection(IsMC)]+ntuples
dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.DataType = data_type
dv.Turbo = True
dv.InputType = 'MDST'
dv.RootInTES = os.path.join('/Event', turbo_stream, 'Turbo')
dv.Lumi = True
