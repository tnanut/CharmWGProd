"""Configure DaVinci for creating ntuples from Turbo/Stripping data."""
import os

from Configurables import DaVinci, DecayTreeTuple, TupleToolDecay, LoKi__Hybrid__TupleTool
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
from LoKiPhys.decorators import *
from DecayTreeTuple.Configuration import *

########################################
# User configuration begins
########################################

# # Data-taking year
# data_type = '2017'
data_type = DaVinci().DataType
# data or MC?
IsMC=DaVinci().Simulation
# RootInTES
root_in_tes = '/Event/Charm/'

# The full name of the HLT2 line
lnames = {'KmPimPipPip': 'K3Pi', 'KpPimPimPip': 'K3Pi',
          'PimPimPipPip': '4Pi','KmKpPimPip': '2K2Pi',
          'KmKmKpPip': '3KPi','KmKpKpPim': '3KPi'}
line_names = {}
for x in lnames.keys(): line_names[x]='b2D0MuX'+lnames[x]+'CharmFromBSemiLine'

# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the branches should be
# called
decay_descriptors = {
    'KmPimPipPip' : '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Mu}mu-]CC',
    'KpPimPimPip' : '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}pi- ${D0_P2}pi+ ${D0_P3}pi-) ${Mu}mu-]CC',
    'PimPimPipPip': '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}pi- ${D0_P1}pi+ ${D0_P2}pi- ${D0_P3}pi+) ${Mu}mu-]CC',
    'KmKpPimPip'  : '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}pi- ${D0_P3}pi+) ${Mu}mu-]CC',
    'KmKmKpPip'   : '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}K- ${D0_P1}K+ ${D0_P2}K- ${D0_P3}pi+) ${Mu}mu-]CC',
    'KmKpKpPim'   : '${B}[B- -> ${D0}([D0]CC -> ${D0_P0}K+ ${D0_P1}K- ${D0_P2}K+ ${D0_P3}pi-) ${Mu}mu-]CC'
}

# Probnn tunings
Probnn = []#["MC15TuneV1"] # uncomment if using DV < v40r0 or new tuning becomes available

########################################
# User configuration ends
########################################

# DecayTreeTuple input location template
particles_input = 'Phys/{0}/Particles'

# Define some trigger lines to check for TISTOS
commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
commonTriggerList += ['Hlt1'+x+'Decision' for x in ['TrackMVA','TwoTrackMVA']]
additionalMuonTriggerList = ['Hlt1'+x+'Decision' for x in ['TrackMuon','TrackMuonMVA','SingleMuonHighPT']]
additionalMuonTriggerList += ['Hlt2'+x+'Decision' for x in ['SingleMuon','SingleMuonHighPT']]
TopoBody = ['Hlt2'+x+'Decision' for x in ['Topo2Body','Topo3Body','Topo4Body']]
TopoMuBody = ['Hlt2'+x+'Decision' for x in ['TopoMu2Body','TopoMu3Body','TopoMu4Body']]
additionalBmesTriggerList = ['Hlt1'+x+'Decision' for x in ['TrackMuon','TrackMuonMVA','SingleMuonHighPT']]
additionalBmesTriggerList += TopoBody+TopoMuBody+['Hlt2'+x+'Decision' for x in ['SingleMuon','SingleMuonHighPT']]

TriggerLists = {'B'     : commonTriggerList+additionalBmesTriggerList,
                'D0'    : commonTriggerList+TopoBody,
                'D0_P0' : commonTriggerList,
                'D0_P1' : commonTriggerList,
                'D0_P2' : commonTriggerList,
                'D0_P3' : commonTriggerList,
                'Mu'    : commonTriggerList+additionalMuonTriggerList}

def CreateDTT(lname, IsMC=False):
    line_name = line_names[lname]
    decay_desc = decay_descriptors[lname]
    IsTurbo = 'Turbo' in line_name
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(lname))
    dtt.Inputs = [particles_input.format(line_name)]
    dtt.setDescriptorTemplate(decay_desc)
    # list of general tools
    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
    dtt.ToolList = ['TupleTool'+tool for tool in GeneralTools]
    # DTF Refit info
    B_Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), 'B') ]
    fit = B_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose= True
    fit.constrainToOriginVertex = False
    fit.UpdateDaughters = True
    # DTF Mass-Constrained D0 fit
    fit_D0_MC = B_Node.addTupleTool('TupleToolDecayTreeFitter/D0Fit')
    fit_D0_MC.Verbose= True
    fit_D0_MC.constrainToOriginVertex = False
    fit_D0_MC.daughtersToConstrain += ['D0']
    fit_D0_MC.UpdateDaughters = True
    # Setup the long-lived particles branches
    IntermediateTools = [ 'Geometry', 'Kinematic', 'Propertime']
    for vtx in ['D0']:
        Vtx = dtt.allConfigurables['%s.%s' % ( dtt.name(), vtx) ]
        Vtx.InheritTools = False
        Vtx.ToolList = [  "TupleTool"+name for name in IntermediateTools ]
        if IsMC:
            Vtx.addTool( dtt.allConfigurables['ToolSvc.TupleToolMCTruth'] )
            Vtx.ToolList += [ "TupleToolMCTruth" ]
    # Setup the stable particles branches
    TrackTools = ['Geometry','Kinematic','Pid','TrackInfo']
    for trk in ['D0_P0', 'D0_P1', 'D0_P2', 'D0_P3', 'Mu' ]:
        branch = dtt.allConfigurables['%s.%s' % ( dtt.name(), trk) ]
        # P info
        branch.InheritTools = False
        branch.ToolList = [ "TupleTool"+name for name in TrackTools ]
        if len(Probnn):
            from Configurables import TupleToolANNPID
            branch.ToolList += [ "TupleToolANNPID" ]
            branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
            branch.TupleToolANNPID.ANNPIDTunes = Probnn
        if IsMC:
            branch.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            branch.ToolList += [ "TupleToolMCTruth",'TupleToolL0Calo' ]
    # TISTOS
    from Configurables import TupleToolTISTOS
    for nodeName, tList in TriggerLists.iteritems():
        Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList
        if IsTurbo: Node.TupleToolTISTOS.FillHlt2 = False
    # LoKi Variables
    LoKiVars = LoKi__Hybrid__TupleTool('LoKi_Variables')
    lokivars = {"BPVCORRM" : "BPVCORRM"}
    conepath = root_in_tes+'Phys/'+line_name+'/P2ConeVar{0}_{1}'
    conemult = 'ConeMult{0}_{1}'
    coneptas = 'ConePtAsym{0}_{1}'
    for coneangle in ['08','10','13','17']:
        for par in ['Mu','1','2','3','4','D','B']:
            cpath = conepath.format(coneangle,par)
            lokivars.update({
                conemult.format(coneangle,par): "RELINFO('%s','CONEMULT',-999)"%(cpath),
                coneptas.format(coneangle,par): "RELINFO('%s','CONEPTASYM',-999)"%(cpath)
            })
    vtxisopath = root_in_tes+'Phys/'+line_name+'/VertexIsoInfo_'
    for vtxisovar in ['VTXISONUMVTX','VTXISODCHI2ONETRACK','VTXISODCHI2MASSONETRACK','VTXISODCHI2TWOTRACK','VTXISODCHI2MASSTWOTRACK']:
        for par in ['D','B']:
            lokivars.update({vtxisovar+'_'+par: "RELINFO('%s','%s',-999)"%(vtxisopath+par,vtxisovar)})# FIXXX
    LoKiVars.Variables = lokivars
    dtt.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Variables"]
    dtt.addTool(LoKiVars)
    # Finishing
    return dtt
# Create a tuple for each line
ntuples = [CreateDTT(lname,IsMC) for lname in lnames]

# Speed up processing by filtering out events with no positive signal decisions
# trigger_filter = LoKi_Filters(
#     HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(hlt2_line_name)
# )

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

dv = DaVinci()
#dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = [MomentumCorrection(IsMC)]+ntuples
dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.DataType = data_type
dv.Turbo = False
dv.InputType = 'MDST'
dv.RootInTES = root_in_tes
dv.Lumi = True
