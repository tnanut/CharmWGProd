########################################
# User configuration for 2015 data tuple
########################################
from Configurables import DaVinci
# Data-taking year
DaVinci().DataType = '2015'
# data or MC?
DaVinci().Simulation = False
