# Helper file to define data type for davinci

year      = '2018'
fileType  = 'MDST'
rootintes = "/Event/AllStreams"
#rootintes = "/Event/Charm"

from Configurables import DaVinci, CondDB
dv = DaVinci (  DataType                  = year           ,
                InputType                 = fileType       ,
                RootInTES                 = rootintes      ,
                Simulation                = True,
                Lumi                      = False, 
                DDDBtag                   = "dddb-20170721-3",
                CondDBtag                 = "sim-20190430-vc-mu100",
                EvtMax                    = -1
             )
