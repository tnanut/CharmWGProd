# Helper file to define data type for davinci

year      = '2012'
fileType  = 'DST'
rootintes = "/Event/D02HHMuMu.Strip/"

from Configurables import DaVinci, CondDB
dv = DaVinci (  DataType                  = year           ,
                InputType                 = fileType       ,
                #RootInTES                 = rootintes      ,
                Simulation                = True,
                Lumi                      = False,
                DDDBtag                   = "dddb-20130929-1",
                CondDBtag                 = "sim-20130522-1-vc-md100"
             )

