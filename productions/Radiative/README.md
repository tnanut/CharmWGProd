# Radiative Ntuple Scripts for WGProductions

Radiative Working Group Option files for creating ntuples via WGProductions, adapted from old radiative centralized procedure.

## General Information

Most files names are self explanatory, relevant:
 * `caloOptions.py`: Specific options for the Calorimeter used in Radiative, namely PostCalibration and ReCalibration
 * `DaVinciMC_Sim09*.py`: Check for simdb and condb tags.
 * `tupSequencer.py`: Core of the tupling files.
 * `modules`: Folder containing functions used throughout the main files.

The ntuple scripts only work with Castelao >= v3r1 due to usage of [restoreRelations](https://gitlab.cern.ch/lhcb/Castelao/tree/master/WGProductions/WGProdRadiative) Algorithm. This unables to make tuples using DaVinci versions older than v45r2.

Ntuple options are divided by radiative categories, that can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCb/RaDecayTupleStat). Some of these categories are only available for Run2.

MC requests for Radiative can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/LHCbPhysics/RadiativeDecaysMC).

### What can be done?

 * `Data`: All possible years (Run1 & Run2) are available for pretty much all Leptonic Stream radiative categories. In any case for 2016 we should be waiting for new restripping campaign that already has the reCalibration applied on. (TODO: Bhadron Stream should b
e checked)
 * `MC`: MC tupling is available for Run1 (check conddb and simdb tags) and Run2 2016, for 2017 and 2018 bugg. Restripping that requires an old DVversion are not directly available, a Castelao release with the DV should be asked for.

# Datasets

Available ntuples
## Data

### 2018 
#### > Stripping34r0p1
 * `2hG`
 * `3hG`

## MC
### KstGamma - 111102202
#### 2016 
* `2hG`: S28r1p1 No Restripping

