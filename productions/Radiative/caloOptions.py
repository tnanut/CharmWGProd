"""
Radiative calorimeter corrections, both POSTCALIB and RECALIB(2016) (soon outdated)
One single algorithm run RestoreCaloRecoChain, official DaVinci Algorithm

"""

# ========================================== #
# ====== Configure CaloReco chain update === #
# ========================================== #
import CaloRecoChain
from Configurables import CondDB
import os, sys
from Configurables import DaVinci

# Import environment variables
category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP",'0')


# Function to provide the line name depending on the year
def stripName(tag) :
    if (stripping == 'stripping21' or stripping == 'stripping21r1'):
        prefix='B2XGamma'
    else :
        prefix='Beauty2XGamma'  # all strippings but old-basic s21 and s21r1
    if tag.find('Line') == -1 :
        return prefix+tag+'_Line'
    else :
        return prefix+tag

flag='Exclusive' # str24/26/28/29 naming
if stripping.find("21") != -1 :
	flag='Excl' # s21 name


processedLines = []


# Selection depending on the category by allowing RestoreCaloRecoChain to refit the tree (Careful)

DVArefit = True # By default to use RestoreCaloRecoChain as True

if category == "2hG":
	location = [stripName('2pi')]
	
elif category == "2hGee":
	location = [stripName('2pi_wCNV'),stripName('2pi_wCNVDD'),stripName('2pi_wCNVLL')]
	
elif category == "2hksG":
	location = [stripName('2pi_Ks0')]

elif category == "2hksG_ExclTDCPV": # S34r0p1 onwards
	location = [stripName('ExclTDCPVBd2KspipiGammaLine'),stripName('ExclTDCPVBs2KsKpiGammaLine'),stripName('ExclTDCPVBd2KstGammaLine'),stripName('ExclTDCPVBs2PhiGammaLine')]
	
elif category == "2hpi0G":
	location = [stripName('phiOmega_2pipi0R'),stripName('2pi_pi0R')]
	DVArefit = False
	
elif category == "3hGee":
	location = [stripName('3pi_wCNV'),stripName('3pi_wCNVDD'),stripName('3pi_wCNVLL')]
	
elif category == "3hG":
	location = [stripName('3pi')]
	
elif category == "4hG":
	location = [stripName('4pi')]
	
elif category == "4hGs":
	location = [stripName('4pi')]
	
elif category == "ExclKstG":
	location = [stripName(flag+'Bd2KstGammaLine')]
	
elif category == "ExclPhiG":
	location = [stripName(flag+'Bs2PhiGammaLine')]
	
elif category == "GG":
	location = ["Bs2GammaGammaWide_NoConvLine","Bs2GammaGamma_NoConvLine","Bs2GammaGamma_LLLine","Bs2GammaGamma_DDLine","Bs2GammaGamma_doubleLine"]
	
elif category == "hhpi0MG":
	location = [stripName('2pi_pi0M')]
	DVArefit = False
	
elif category == "hhpi0RG":
	location = [stripName('2pi_pi0R')]
	DVArefit = False
	
elif category == "hksG":
	location = [stripName('2pi_Ks0')]
	
elif category == "LG":
	location = [stripName('2pi'), stripName('2pi_wCNV'), stripName('2pi_wCNVDD'), stripName('2pi_wCNVLL'), 'Lb2L0Gamma', 'Lb2L0GammaConverted']
	DVArefit = False

elif category == "NoBias":
	location = ['Beauty2XGammaNoBiasBd2KstGammaLine', 'Beauty2XGammaNoBiasBs2PhiGammaLine']
	
elif category == "VG":
	location = [stripName('2pi'), stripName('pi_Ks0')]
	
elif category == "VGee":
	location = [stripName('2pi_wCNV'), stripName('2pi_wCNVDD'), stripName('2pi_wCNVLL')]
	
else:
	location = []
	print("NO CATEGORY FOUND; NO CALO OPTIONS APPLIED")

# Setup the path needed to give to RestoreCaloRecoChain depending on type
if location:  # Boolean property of python lists
	for line in location:
		if DaVinci().InputType == 'MDST':
			processedLines.append('Phys/'+line+'/Particles')
		elif restrip == "0" and DaVinci().getProp('Simulation') and  DaVinci().InputType !='MDST':
			processedLines.append('/Event/AllStreams/Phys/'+line+'/Particles')     
		elif not DaVinci().getProp('Simulation') and DaVinci().InputType == 'DST':
			if 'ExclTDCPV' in line: 
				processedLines.append('/Event/BhadronCompleteEvent/Phys/'+line+'/Particles') 
			else : 
				processedLines.append('/Event/Radiative/Phys/'+line+'/Particles') 
			

if processedLines != []:
    print ' '
    print ' --- !! --- UPDATE CALORECO CHAIN(S) for :',processedLines
#    print ' --- !! --- USING DB TAG(S)    : ',DaVinci().CondDBtag
    if len( CondDB().LocalTags ) != 0 :
        print ' --- !! --- USING LOCAL TAG(S)    : ', str(CondDB().LocalTags)
    print ' '
    # Run algorithm
    CaloRecoChain.update(processedLines, UpdateLevel=1, RefitDecayTree=DVArefit)    


