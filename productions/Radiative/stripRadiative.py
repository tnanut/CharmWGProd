from StrippingConf.StrippingLine import *
from StrippingConf.Configuration import *
from StrippingConf.StrippingStream import StrippingStream
from Configurables import DecayTreeTuple
from Configurables import LoKi__HDRFilter as StripFilter
from Configurables import DaVinci

category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP")


import tupStripping



# ====
#
# Replay Radiative lines as for pre-registred stripping and fill decReport
# 
# ==== 

#====  Put the track+calo sequence on top of filters 
from Configurables import DaVinci
filters = DaVinci().EventPreFilters  # save user filters if any
DaVinci().EventPreFilters = []       # re-init filters

#=================================#
# EXTRA SETTING FOR MC            #
#=================================#

#=== Track smearing 

from Configurables import GaudiSequencer,DaVinci
seq=GaudiSequencer("TrackSmear")

if DaVinci().getProp("Simulation")  :    
    from Configurables import TrackSmearState
    smear = TrackSmearState('TrackSmear')
    
    seq.Members = [smear]

smear=GaudiSequencer("TrackSmearSeq")

if DaVinci().getProp("Simulation")  and smear.Members != [] :    
    DaVinci().EventPreFilters  += [  smear ]
    print "============ TRACK SMEARING IS ADDED TO PREFILTER"
        
#== Calo Reprocessing
from Configurables import PhysConf
if PhysConf().getProp("CaloReProcessing") :
    from Configurables import CaloProcessor
    print "============ CALO REPROCESSING IS ADDED TO PREFILTER"
    DaVinci().EventPreFilters += [CaloProcessor().caloSequence(),CaloProcessor().protoSequence()]


#== add restripping to the prefilter
tags= tupStripping.radStrippingTags()
print tags
killer,strip,report,correl,listing = tupStripping.strippingAlgs('RadiativeFilterStream',stripping,restrip,tags,True)
DaVinci().EventPreFilters += [killer,strip.sequence(),report,correl] # kill node + reprocess partial stripping + filter

#== complete with user filters
if len(filters) > 0 :
    DaVinci().EventPreFilters.append(filters)

print " ... EventPreFilters :"
for f in DaVinci().EventPreFilters :
    print '   .. '+f.getName()




radTuple= DecayTreeTuple('radTuple')
# Adds to the Tuple variables referencing what stripping line the events have gone through
tupStripping.addStrippingList(radTuple, listing)
