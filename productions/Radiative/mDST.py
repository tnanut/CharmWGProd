"""
Basic mDST Davinci Inputtype

"""

import os, sys
# Setup environment options
os.environ['INPUTTYPE'] = 'mDST'

from Gaudi.Configuration import *
from Configurables import DaVinci

DaVinci().InputType="MDST"

import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()
