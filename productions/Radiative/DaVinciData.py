"""
Adapted for RadTuple production
01/2019

"""

from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import CondDB
import os


# Setup environment options
os.environ['DATATYPE'] = 'Data'

# Import environment options
year = 		os.getenv("YEAR")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
localDB =	os.getenv("localDB", "")

# DV Configuration for 2011/2012 data
DaVinci().Simulation   = False
DaVinci().Lumi         = True


DaVinci().DataType = year

'''
Old ConDB and DDDB proccedure + 2016 localTag for recalibration
Waiting for 2015/16 restripping to remove.

# ==== dictionary for latest db ==== #
cond={} #[update on 2018/02/02] 
cond['2010,reco14++']='cond-20150527'   # 2010
cond['2011,reco14++']='cond-20150409'   # 2011
cond['2012,reco14++']='cond-20150409-1' # 2012/2013
cond['2013,reco14++']='cond-20150409-1'
cond['2015,reco14++']='cond-20170323'
cond['2016,reco14++']='cond-20170325' 
cond['2017,reco14++']='cond-20180202' # as for 2018/02/02
cond['2018,reco14++']='cond-20180202' # as for 2018/02/02

dddb={} #[update on 2018/02/02] 
dddb['2010']='dddb-20171030'   # 2010   
dddb['2011']='dddb-20171030-1' # 2011 
dddb['2012']='dddb-20171030-2' # 2012/2013 
dddb['2013']='dddb-20171030-2'
dddb['2015']='dddb-20171030-3' # 2015/2016/2017
dddb['2016']='dddb-20171030-3' 
dddb['2017']='dddb-20171030-3' # as for 2018/02/02
dddb['2018']='dddb-20171030-3' # as for 2018/02/02

dq={} #[update on 2017/21/09] 
dq['2011']='dq-20140801-3'
dq['2012']='dq-20140822'
dq['2013']='dq-20130418'
dq['2015']='dq-20160909'
dq['2016']='dq-20170627'  
dq['2017']='dq-20170829'  # as for 2017/21/09 
dq['2018']='dq-20170829'  # as for 2017/21/09 

calo='reco14++'
if stripping.find('28') != -1 and year == "2016" :  # 2016
    # == apply offline recalibration for S28 2016 data (using 'calo-20170505' localDB)
    updateCalo="1"  
    CondDB().LocalTags["LHCBCOND"] = ["calo-20170505"]  # offline recalibration for 2016 S28 data
    os.environ["updateCalo"]="1"


# DaVinci tags configuration (year dependant)
if cond[year+','+calo] != '' :
    DaVinci().CondDBtag  = cond[year+','+calo]

if dddb[year] != '' :
    DaVinci().DDDBtag    = dddb[year]

if dq[year] != '' :
    DaVinci().DQFLAGStag = dq[year]

# ==== include user-requested localDB + update reco chain ==== # NEW 2017/11
updateCalo = "0"
tags=[]
if localDB != '' :
    tags=[localDB]
    CondDB().LocalTags["LHCBCOND"].append(tags)
    updateCalo="1"  
    os.environ["updateCalo"]="1"
# ============================================================ #

# Database tags printout 
print ' = DB settings for '+year+' data - stripping='+stripping+' / calo='+calo
print '  - condDB   : "'+DaVinci().CondDBtag+'"'
print '  - DDDB     : "'+DaVinci().DDDBtag+'"'
print '  - DQ       : "'+DaVinci().DQFLAGStag+'"'
print '  - localDB  : "'+str(CondDB().LocalTags)+'"'
if updateCalo == "1" : 
   print '  !! Calo reco chain to be updated against CondDB tag(s)'
'''

print stripping
print '------------------------------'


CondDB(LatestGlobalTagByDataType = year )

import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()
