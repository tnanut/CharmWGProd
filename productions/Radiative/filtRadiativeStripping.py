"""
Adapted for RadTuple production
01/2019

Prefilter with all Radiative stripping line
Stripping line decisions also added to the tuple 

"""

######## stripping filtering
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from Configurables import LoKi__HDRFilter as StripFilter

import tupStripping

import os

# Import environment options
category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP")

issim = DaVinci().getProp("Simulation")

from Configurables import CheckPV
DaVinci().appendToMainSequence( [ CheckPV() ] )      

# Same name definition
radTuple= DecayTreeTuple('radTuple')

# Adds to the tuple the tradiative tags
tupStripping.addStrippingList(radTuple, tupStripping.radStrippingList(stripping,restrip,issim,verbose=True)        )


# Stripping prefilter of radiative lines
sf = StripFilter( 'StripPassFilter', Code="HLT_PASS_RE('StrippingStreamRadiativeDecision') | HLT_PASS_RE('Stripping.*B2VG_LineDecision') | HLT_PASS_RE('StrippingBs2Gamma.*LineDecision') | HLT_PASS_RE('StrippingBeauty2XGamma.*') | HLT_PASS_RE('StrippingB2XGamma.*') | HLT_PASS_RE('StrippingLb2L0Gamma.*') | HLT_PASS_RE('StrippingB2HHPi0.*') | HLT_PASS_RE('StrippingB2HHpi0.*')", Location="/Event/Strip/Phys/DecReports" )

DaVinci().EventPreFilters += [sf]
print "... adding filter to EventPreFilters :"+sf.Code


import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()
