"""
Basic DST Davinci Inputtype
"""

from Gaudi.Configuration import *
from Configurables import DaVinci


import os, sys
# Setup environment options
os.environ['INPUTTYPE'] = 'DST'

DaVinci().InputType="DST"
