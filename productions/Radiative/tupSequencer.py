"""
Adapted for RadTuple production
01/2019

CORE
A GaudiSequence is created for every category, containing the different subcategories inside.
Highly hardcoded, needs an overhaul


"""
# Core, pending general modification --
# ------------------------------------------ #
# == Tupling radiative stripping lines ===== #
# ------------------------------------------ #

# -- v3 : perform tupling of all valid stripping v20r{0,1}(p1) & v21r{0,1} lines
# -- NEW : create exclusive decays from (semi-)inclusive lines via PID subtitution

# == modules 
from LHCbKernel.Configuration import *
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from Configurables import LoKi__HDRFilter as StripFilter
from Configurables import PrintDecayTree,  DecayTreeTuple, TupleToolMCTruth,  DaVinci
from Configurables import SelDSTWriter, FilterDesktop,GaudiSequencer,CheckPV,OfflineVertexFitter
from Configurables import CombineParticles,DataOnDemandSvc,FitDecayTrees
from Configurables import SubstitutePID,GaudiSequencer
from Configurables import LoKi__Hybrid__TupleTool,TupleToolVtxIsoln,TupleToolGeometry,ConeVariables, TupleToolConeIsolation, TupleToolTrackIsolation
from Configurables import TupleToolDecay 
from Configurables import TupleToolDecayTreeFitter 
from Configurables import DaVinci
from DecayTreeTuple.Configuration import *
from Configurables import RestoreCaloRecoChain

from CommonParticles.Utils import *
from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand,MergedSelection
from PhysSelPython.Wrappers import Selection,DataOnDemand,SelectionSequence,MergedSelection
from Configurables import CombineParticles, PVReFitterAlg,BestPVAlg,CopyParticle2MCRelations

import os


import tupDTF,tupTisTos,tupStripping


# Import environment options
category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP",'0')
filtered = 	os.getenv("FILTERED")

# ==   user setings :  ==
# ======================= #
defRefitTree = False      # process DecayTreeFitter refit before tupling by default
usePVConstraint   = False #
useMassConstraint = False #
# ======================= #
printDoD  = False         # DataOnDemand printout
# ======================= #

# == get stripping version

# == DEFINE SOME STRIPPING CATEGORIES

# -- run2 : vertex isolation related info name change for v24r0p1 (2015) , v28 (2016) & v29 (2017) & v34 (2018)
STRIP_21             =  stripping.find('21') != -1
STRIP_run1           = STRIP_21
STRIP_run2           = not STRIP_run1

# Run1 stripping versions
STRIP_run1_p2        = (stripping.find('21r0p2') != -1 or stripping.find('21r1p2') != -1 )
STRIP_run1_r0p1      = (stripping.find('21r0p1') != -1 or stripping.find('21r1p1') != -1 ) 
STRIP_run1_r0        = (stripping.find('21') != -1     or stripping.find('21r1')   != -1 ) and not STRIP_run1_r0p1 and not STRIP_run1_p2

# Run2 stripping versions
STRIP_run2_r0       = (stripping.find('24') != -1     or stripping.find('26')     != -1 )                               and stripping.find('24r') == -1 
STRIP_run2_r0p1     = (stripping.find('24r0p1') != -1 or stripping.find('28')     != -1 or stripping.find('29') != -1 ) and stripping.find('28r') == -1 and stripping.find('29r') == -1
STRIP_run2_r1       = (stripping.find('24r1') != -1   or stripping.find('28r1')   != -1  or stripping.find('29r1') != -1 )    and stripping.find('r1p') == -1  # full restripping
STRIP_run2_special = STRIP_run2_r1 or STRIP_run2_r0p1  # DecayDescriptor and/or some naming change for these version
STRIP_run2_r1p1     = (stripping.find('24r1p1') != -1 or stripping.find('28r1p1') != -1 or stripping.find('29r2') ) or stripping.find('34')  # incremental restripping + 2018
STRIP_run2_r2       = (stripping.find('24r2') != -1 or stripping.find('28r2') != -1 or stripping.find('29r2p1') != -1 or stripping.find('34r0p1') != -1)

# 2019 campaign
STRIP_2019campaign  = ("34r0p1" in stripping or "29r2p1" in stripping or "28r2" in stripping or "24r2" in stripping or "21r0p2" in stripping or "21r1p2" in stripping) 

defBestPV=False
    
# == default TES root
defROOT=''
defStream=''
if DaVinci().InputType=='MDST' :    
    defStream='Leptonic'
    if DaVinci().getProp('Simulation') :
        defStream='{0}'.format(filtered) # as for 2hksG production (?)        
    cat=os.getenv('CATEGORY','NONE')
    if cat.find('2hpi0M') != -1 or cat.find('2hpi0R') != -1 :
        print ' == !! == BnoC analysis : the default Stream is set to Bhadron'
        defStream='Bhadron'   
    defROOT='/Event/'+defStream+'/'       # == Radiative lines live in LEPTONIC stream when MDST :   (v21rx and above) 
    DaVinci().RootInTES= defStream     # == propagate to the whole main sequence in case of mDST

elif not DaVinci().getProp('Simulation')  :
    defStream='Radiative'
    if "2hksG_ExclTDCPV" in category: defStream = 'BhadronCompleteEvent' # for new KshhG module
    defROOT='/Event/'+defStream+'/'     # == radiative lines live in RADIATIVE/BHADCOMPLETE stream when full DST    (v20rx(p1,p2)  ('Calibration' in str21 for ExclBd)

# == MC settings
if restrip == "0" and DaVinci().getProp('Simulation') and  DaVinci().InputType !='MDST'   :    
    defStream="AllStreams"
    defROOT='/Event/'+defStream+'/'      
    print "DEFAULT path : ",defROOT

# == str21 line naming
def stripName(tag) :
    if STRIP_run1_r0 :
        prefix='B2XGamma'
    else :
        prefix='Beauty2XGamma'  # all strippings but old-basic s21 and s21r1
    if tag.find('Line') == -1 :
        return prefix+tag+'_Line'
    else :
        return prefix+tag

# ==                                                         == #
# == Tools to create exclusive decays from inclusive lines : == #
# ==                                                         == #


# == define some useful filters :
filtKst   = "INTREE( ('K*(892)0'==ABSID  )  & (ADMASS('K*(892)0'  )   < 300 ) )"  # kst0 mass window
filtKstp  = "INTREE( ('K*(892)+'==ABSID  )  & (ADMASS('K*(892)+'  )   < 300 ) )"  # kst+ mass window
filtPhi   = "INTREE( ('phi(1020)'==ABSID )  & (ADMASS('phi(1020)' )   < 75  ) )"  # phi mass window
filtRho   = "INTREE( ('rho(770)0'==ABSID )  & (ADMASS('rho(770)0' )   < 500 ) )"  # rho mass window
filtOmega = "INTREE( ('omega(782)'==ABSID)  & (ADMASS('omega(782)')   < 150 ) )"  # omega mass window

# ==
# == setup the PID substitution for generic Xgamma decays
def RadSubstitute(name,typ,b,r,d,inputs,extra='') :
    # default name for intermediate resonnance in stripped decay (str21/24/26)
    rhh   ='rho(770)0'
    rhhp  ='rho(770)+'
    rhhm  ='rho(770)-'
    rhhhp ='K_1(1270)+'
    rhhhm ='K_1(1270)-'
    rhhpi0='eta'
    rhhKs0='K*_2(1430)0'
    rhhh0 ='K_1(1270)0'
    rhhhh='f_2(1270)'
    
    if typ == 'hh' :   # default : rho0->pi+pi-
        sub = SubstitutePID('hhgSubst_'+name , Code="DECTREE('X0 -> (X0 -> X+ X-) gamma')")
        if d[0] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 -> ^X+  X- ) gamma']=  d[0]
        if d[1] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->  X+ ^X- ) gamma']=  d[1]
        if r[0] != rhh         : sub.Substitutions[' X0 -> ^( X0 ->  X+  X- ) gamma']=  r[0]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->  X+  X- ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1])    
    elif typ == 'hhh+' :  # default : K_1(1270)+->pi+pi-pi+  (stripping 21)!!
        sub = SubstitutePID('hhh+gSubst_'+name , Code="DECTREE('X+ -> (X+ -> X+ X- X+) gamma')  ")        
        if d[0] != 'pi+'       : sub.Substitutions[' X+ ->  ( X+ -> ^X+  X-  X+ ) gamma']=  d[0]
        if d[1] != 'pi-'       : sub.Substitutions[' X+ ->  ( X+ ->  X+ ^X-  X+ ) gamma']=  d[1]
        if d[2] != 'pi+'       : sub.Substitutions[' X+ ->  ( X+ ->  X+  X- ^X+ ) gamma']=  d[2]
        if r[0] != rhhhp       : sub.Substitutions[' X+ -> ^( X+ ->  X+  X-  X+ ) gamma']=  r[0]
        if b    != 'B+'        : sub.Substitutions[' X+ ->  ( X+ ->  X+  X-  X+ ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1],d[2])    
    elif typ == 'hhh-' :
        sub = SubstitutePID('hhh-gSubst_'+name , Code="DECTREE('X- -> (X- -> X- X+ X-) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X- ->  ( X- -> ^X-  X+  X- ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X- ->  ( X- ->  X- ^X+  X- ) gamma']=  d[1]
        if d[2] != 'pi-'       : sub.Substitutions[' X- ->  ( X- ->  X-  X+ ^X- ) gamma']=  d[2]
        if r[0] != rhhhm       : sub.Substitutions[' X- -> ^( X- ->  X-  X+  X- ) gamma']=  r[0]
        if b    != 'B-'        : sub.Substitutions[' X- ->  ( X- ->  X-  X+  X- ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1],d[2])    
    elif typ == 'kpipi+' :  # default : K_1(1270)+->K+pi-pi+  (stripping 20)!!
        sub = SubstitutePID('hhh+gSubst_'+name , Code="DECTREE('X+ -> (X+ -> X+ X- X+) gamma')  ")        
        if d[0] != 'K+'        : sub.Substitutions[' X+ ->  ( X+ -> ^X+  X-  X+ ) gamma']=  d[0]
        if d[1] != 'pi-'       : sub.Substitutions[' X+ ->  ( X+ ->  X+ ^X-  X+ ) gamma']=  d[1]
        if d[2] != 'pi+'       : sub.Substitutions[' X+ ->  ( X+ ->  X+  X- ^X+ ) gamma']=  d[2]
        if r[0] != rhhhp       : sub.Substitutions[' X+ -> ^( X+ ->  X+  X-  X+ ) gamma']=  r[0]
        if b    != 'B+'        : sub.Substitutions[' X+ ->  ( X+ ->  X+  X-  X+ ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1],d[2])    
    elif typ == 'kpipi-' :  # default : K_1(1270)-->K-pi+pi-  (stripping 20)!!
        sub = SubstitutePID('hhh+gSubst_'+name , Code="DECTREE('X- -> (X- -> X- X+ X-) gamma')  ")        
        if d[0] != 'K-'        : sub.Substitutions[' X- ->  ( X- -> ^X-  X+  X- ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X- ->  ( X- ->  X- ^X+  X- ) gamma']=  d[1]
        if d[2] != 'pi-'       : sub.Substitutions[' X- ->  ( X- ->  X-  X+ ^X- ) gamma']=  d[2]
        if r[0] != rhhhm       : sub.Substitutions[' X- -> ^( X- ->  X-  X+  X- ) gamma']=  r[0]
        if b    != 'B-'        : sub.Substitutions[' X- ->  ( X- ->  X-  X+  X- ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1],d[2])    
    elif typ=='hks-' :       # default : rho(770)+->pi+ KS0  (sic !!)
        sub = SubstitutePID('hks-gSubst_'+name , Code="DECTREE('X- ->  (X- ->  X-  KS0 ) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X- ->    (X- -> ^X-  KS0 ) gamma']=  d[0]
        if r[0] != rhhm  : sub.Substitutions[' X- ->   ^(X- ->  X-  KS0 ) gamma']=  r[0]
        if b    != 'B-'        : sub.Substitutions[' X- ->    (X- ->  X-  KS0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0])    
    elif typ=='hks+' :
        sub = SubstitutePID('hks+gSubst_'+name , Code="DECTREE('X+ ->  (X+ ->  X+  KS0 ) gamma') ")                
        if d[0] != 'pi+'       : sub.Substitutions[' X+ ->    (X+ -> ^X+  KS0 ) gamma']=  d[0]
        if r[0] != rhhp  : sub.Substitutions[' X+ ->   ^(X+ ->  X+  KS0 ) gamma']=  r[0]
        if b    != 'B+'        : sub.Substitutions[' X+ ->    (X+ ->  X+  KS0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0])    
    elif typ == 'hhpi0' :
        sub = SubstitutePID('hhh-gSubst_'+name , Code="DECTREE('X0 -> (X0 -> (X0 -> X+ X- ) pi0 ) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->  (X0 -> ^X+   X- )  pi0 ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->  (X0 ->  X+  ^X- )  pi0 ) gamma']=  d[1]
        if r[0] !=  rhhpi0     : sub.Substitutions[' X0 -> ^( X0 ->  (X0 ->  X+   X- )  pi0 ) gamma']=  r[0]
        if r[1] !=  rhh        : sub.Substitutions[' X0 ->  ( X0 -> ^(X0 ->  X+   X- )  pi0 ) gamma']=  r[1]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->  (X0 ->  X+  X-  )  pi0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],r[1],d[0],d[1])    
    elif typ == 'hhks' :
        sub = SubstitutePID('hhh-gSubst_'+name , Code="DECTREE('X0 -> (X0 -> (X0 -> X+ X- ) KS0 ) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->  (X0 -> ^X+   X- )  KS0 ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->  (X0 ->  X+  ^X- )  KS0 ) gamma']=  d[1]
        if r[0] !=  rhhKs0     : sub.Substitutions[' X0 -> ^( X0 ->  (X0 ->  X+   X- )  KS0 ) gamma']=  r[0]
        if r[1] !=  rhh        : sub.Substitutions[' X0 ->  ( X0 -> ^(X0 ->  X+   X- )  KS0 ) gamma']=  r[1]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->  (X0 ->  X+  X-  )  KS0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],r[1],d[0],d[1])    
    elif typ == 'KmPipks' :
        sub = SubstitutePID('hhh-gSubst_'+name , Code="DECTREE('X0 -> (X0 -> (X0 -> X- X+ ) KS0 ) gamma') ")
        if d[0] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->  (X0 -> ^X-   X+ )  KS0 ) gamma']=  d[0]
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],r[1],d[0],d[1])    
    elif typ == '2hpi0' : # S24r0p1/S28  => S24r1/S28r1 (restripping due to TisTos bug) !! S24r1p2/S28r1p1 incr. stripping is back to run1 DecayDesc.
        sub = SubstitutePID('2hpi0-gSubst_'+name , Code="DECTREE('X0 -> (X0 -> X+ X-  pi0 ) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->  ^X+   X-   pi0 ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->   X+  ^X-   pi0 ) gamma']=  d[1]
        if r[0] !=  rhhh0      : sub.Substitutions[' X0 -> ^( X0 ->   X+   X-   pi0 ) gamma']=  r[0]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->   X+   X-   pi0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1])    
    elif typ == '2hks' :
        sub = SubstitutePID('2hks-gSubst_'+name , Code="DECTREE('X0 -> (X0 -> X+ X-  KS0 ) gamma') ")                
        if d[0] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 -> ^X+   X-   KS0 ) gamma']=  d[0]
        if d[1] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->  X+  ^X-   KS0 ) gamma']=  d[1]
        if r[0] !=  rhhh0      : sub.Substitutions[' X0 -> ^( X0 ->  X+   X-   KS0 ) gamma']=  r[0]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->  X+   X-   KS0 ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1])    

    elif typ == 'hhhh' :
        sub = SubstitutePID('hhhh-gSubst_'+name , Code="DECTREE('X0 -> ( X0 -> X+ X- X+ X- ) gamma') ")                
        if d[0] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->  ^X+   X-   X+   X-  ) gamma']=  d[0]
        if d[1] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->   X+  ^X-   X+   X-  ) gamma']=  d[1]
        if d[2] != 'pi+'       : sub.Substitutions[' X0 ->  ( X0 ->   X+   X-  ^X+   X-  ) gamma']=  d[2]
        if d[3] != 'pi-'       : sub.Substitutions[' X0 ->  ( X0 ->   X+   X-   X+  ^X-  ) gamma']=  d[3]
        if r[0] !=  rhhhh      : sub.Substitutions[' X0 -> ^( X0 ->   X+   X-   X+   X-  ) gamma']=  r[0]
        if b    != 'B0'        : sub.Substitutions[' X0 ->  ( X0 ->   X+   X-   X+   X-  ) gamma']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,r[0],d[0],d[1],d[2],d[3])    
    else :
        print 'Error : unknown substition typ = ',typ
        return []
    # -- define the selection (+ filter)
    sub.MaxParticles=2000
    sub.MaxChi2PerDoF = -1   # !! NO DECAYTREE REFIT HERE !! (to be done later when requested)
    sel = Selection(typ+'Sel_'+name,Algorithm=sub, RequiredSelections=inputs)
    if extra != '' : filter += ' & ' +extra
    filt=FilterDesktop(typ+'FD_'+name,Code=filter)
    return Selection(typ+'_'+name , Algorithm=filt,RequiredSelections=[sel])





# == Define relevant exclusive decays applying PID substitution to generic decays
def RadDecayMaker(name, typ , data , extra='') :
    input=[DataOnDemand(data)]
    
    # -- B->(V->hh)g modes   : B0->K*(Kpi)G / B_s0->phi(KK)G / Lb->L(1520)G
    if typ == 'b2kpig' :
        sel1 = RadSubstitute('kpi_'+name   , 'hh' , 'B0'  , ['K*(892)0']  ,['K+','pi-'],input,extra)
        sel2 = RadSubstitute('kpi_CC_'+name, 'hh' , 'B~0' , ['K*(892)~0'] ,['pi+','K-'],input,extra)
        sel  = MergedSelection ('kpi_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'bs2kkg' :
        sel = RadSubstitute('kk_'+name  , 'hh' , 'B_s0'  , ['phi(1020)']  ,['K+','K-'],input,extra)
    elif typ == 'lb2pkg' :
        sel1 = RadSubstitute('pk_'+name   , 'hh' , 'Lambda_b0'  , ['Lambda(1520)0']  ,['p+','K-' ],input,extra)
        sel2 = RadSubstitute('pk_CC_'+name, 'hh' , 'Lambda_b~0'  , ['Lambda(1520)~0'] ,['K+','p~-'],input,extra)
        sel  = MergedSelection ('pk_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'lb2ppig' :
        sel1 = RadSubstitute('ppi_'+name   , 'hh' , 'Lambda_b0'  , ['N(1440)0']  ,['p+','pi-' ],input,extra)
        sel2 = RadSubstitute('ppi_CC_'+name, 'hh' , 'Lambda_b~0'  , ['N(1440)~0'] ,['pi+','p~-'],input,extra)
        sel  = MergedSelection ('ppi_merger_'+name,RequiredSelections=[sel1,sel2])

    # -- ISO mode B+->(h+KS0)g
    elif typ == 'b2piksg' :
        sel1 = RadSubstitute('piks_'+name     , 'hks+' , 'B+'  , ['K*(892)+'] , ['pi+'] ,input,extra)   #['pi+' ,'KS0']
        sel2 = RadSubstitute('piks_CC_'+name  , 'hks-' , 'B-'  , ['K*(892)-'] , ['pi-'] ,input,extra)
        sel  = MergedSelection ('piks_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kksg' :
        sel1 = RadSubstitute('kks_'+name     , 'hks+' , 'B+'  , ['a_2(1320)+']  ,['K+'],input,extra)
        sel2 = RadSubstitute('kks_CC_'+name  , 'hks-' , 'B-'  , ['a_2(1320)-']  ,['K-'],input,extra)
        sel  = MergedSelection ('kks_merger_'+name,RequiredSelections=[sel1,sel2])

    # -- B->(X->hhh)g modes : B+->K_1(Kpipi)G / B+->a_1(pipipi)G ( STRIPPING 21 : default : B+->(K1+ -> pi+pi-pi+) g
    elif typ == 'b2kpipig' :
        sel1 = RadSubstitute('kpipi_'+name   , 'hhh+' , 'B+'  , ['K_1(1270)+']  ,['K+' ,'pi-','pi+'],input,extra)
        sel2 = RadSubstitute('pipik_'+name   , 'hhh+' , 'B+'  , ['K_1(1270)+']  ,['pi+','pi-','K+' ],input,extra)
        sel3 = RadSubstitute('kpipi_CC'+name , 'hhh-' , 'B-'  , ['K_1(1270)-']  ,['K-' ,'pi+','pi-'],input,extra)
        sel4 = RadSubstitute('pipik_CC'+name , 'hhh-' , 'B-'  , ['K_1(1270)-']  ,['pi-','pi+','K-' ],input,extra)
        sel  = MergedSelection ('kpipi_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])
    elif typ == 'b2pipipig' :
        sel1 = RadSubstitute('pipipi_'+name   , 'hhh+' , 'B+'  , ['a_1(1260)+']  ,['pi+','pi-','pi+'],input,extra)
        sel2 = RadSubstitute('pipipi_CC_'+name, 'hhh-' , 'B-'  , ['a_1(1260)-']  ,['pi-','pi+','pi-'],input,extra)
        sel  = MergedSelection ('pipipi_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kkpig' :
        sel1 = RadSubstitute('kkpi_'+name   , 'hhh+' , 'B+'  , ['rho_3(1690)+']  ,['K+','K-','pi+'],input,extra)
        sel2 = RadSubstitute('pikk_'+name   , 'hhh+' , 'B+'  , ['rho_3(1690)+']  ,['pi+','K-','K+'],input,extra)
        sel3 = RadSubstitute('kkpi_CC_'+name, 'hhh-' , 'B-'  , ['rho_3(1690)-']  ,['K-','K+','pi-'],input,extra)
        sel4 = RadSubstitute('pikk_CC_'+name, 'hhh-' , 'B-'  , ['rho_3(1690)-']  ,['pi-','K+','K-'],input,extra)
        sel  = MergedSelection ('kkpi_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])
    elif typ == 'b2kkkg' :
        sel1 = RadSubstitute('kkk_'+name   , 'hhh+' , 'B+'  , ['K_2(1770)+']  ,['K+','K-','K+'],input,extra)
        sel2 = RadSubstitute('kkk_CC_'+name, 'hhh-' , 'B-'  , ['K_2(1770)-']  ,['K-','K+','K-'],input,extra)
        sel  = MergedSelection ('kkk_merger_'+name,RequiredSelections=[sel1,sel2])
    # -- B->(X->hhh)g modes : B+->a_1(pipipi)G ( STRIPPING 20 : default : B+->(K1+ -> K+pi-pi+) g
    elif typ == 'b2pipipig_2' :
        sel1 = RadSubstitute('pipipi2_'+name   , 'kpipi+' , 'B+'  , ['a_1(1260)+']  ,['pi+','pi-','pi+'],input,extra)
        sel2 = RadSubstitute('pipipi2_CC_'+name, 'kpipi-' , 'B-'  , ['a_1(1260)-']  ,['pi-','pi+','pi-'],input,extra)
        sel  = MergedSelection ('pipipi2_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kkpig_2' :
        sel1 = RadSubstitute('kkpi_2_'+name   , 'kpipi+' , 'B+'  , ['rho_3(1690)+']  ,['K+','K-','pi+'],input,extra)
        sel2 = RadSubstitute('kkpi_2_CC_'+name, 'kpipi-' , 'B-'  , ['rho_3(1690)-']  ,['K-','K+','pi-'],input,extra)
        sel  = MergedSelection ('kk_pi2_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kkkg_2' :
        sel1 = RadSubstitute('kkk_2_'+name   , 'kpipi+' , 'B+'  , ['K_2(1770)+']  ,['K+','K-','K+'],input,extra)
        sel2 = RadSubstitute('kkk_2_CC_'+name, 'kpipi-' , 'B-'  , ['K_2(1770)-']  ,['K-','K+','K-'],input,extra)
        sel  = MergedSelection ('kkk_2_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'xib2pkkg_2' :
        sel1 = RadSubstitute('pkk_2_'+name   , 'kpipi+' , 'Xi_b~+'  , ['Xi(1820)~+']  ,['K+' ,'p~-','K+'],input,extra)
        sel2 = RadSubstitute('pkk_2_CC_'+name, 'kpipi-' , 'Xi_b-'  , ['Xi(1820)-']  ,['K-' ,'p+' ,'K-'],input,extra)
        sel  = MergedSelection ('pkk_2_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2pkpg_2' :
        sel1 = RadSubstitute('pkp_2_'+name   , 'kpipi+' , 'B+'  , ['Xu+']  ,['K+' ,'p~-','p+' ],input,extra)
        sel2 = RadSubstitute('pkp_2_CC_'+name, 'kpipi-' , 'B-'  , ['Xu-']  ,['K-' ,'p+' ,'p~-'],input,extra)
        sel  = MergedSelection ('pkp_2_merger_'+name,RequiredSelections=[sel1,sel2])

    # --  B->(X->Y(hh)pi0)g modes : B0->K_1(K*(Kpi)pi0)G /  B0->a_1(rho0(pipi)pi0)G / B0->omega(rho0(pipi)pi0)G
    elif typ == 'b2omegag' :
        sel = RadSubstitute('omega_'+name   , 'hhpi0' , 'B0'  , ['omega(782)','rho(770)0']  ,['pi+','pi-'],input,extra)
    elif typ == 'b2pipipi0g' :
        sel = RadSubstitute('pipipi0_'+name , 'hhpi0' , 'B0'  , ['a_1(1260)0','rho(770)0']  ,['pi+','pi-'],input,extra)
    elif typ == 'b2kpipi0g' :
        sel1 = RadSubstitute('kpipi0_'+name     , 'hhpi0' , 'B0'  , ['K_1(1270)0' ,'K*(892)0' ]  ,['K+' ,'pi-'],input,extra)
        sel2 = RadSubstitute('kpipi0_CC_'+name , 'hhpi0' , 'B~0' , ['K_1(1270)~0','K*(892)~0']  ,['pi+','K-'],input,extra)
        sel  = MergedSelection ('kpipi0_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kkpi0g' : # new 2017/10  Bs -> (K+K-pi0)g
        sel = RadSubstitute('kpi0_'+name     , 'hhpi0' , 'B_s0'  , ['phi(1680)' ,'phi(1020)' ]  ,['K+' ,'K-'],input,extra)
    elif typ == 'lb2pkpi0g' : # new 2017/10  Lb -> (pK-pi0)g
        sel1 = RadSubstitute('pkpi0_'+name     , 'hhpi0' , 'Lambda_b0'  , ['Sigma(1660)0' ,'Lambda(1520)0' ]  ,['p+','K-'],input,extra)
        sel2 = RadSubstitute('pkpi0_CC_'+name  , 'hhpi0' , 'Lambda_b~0' , ['Sigma(1660)~0','Lambda(1520)~0']  ,['K+','p~-'],input,extra)
        sel  = MergedSelection ('pkpi0_merger_'+name,RequiredSelections=[sel1,sel2])


    #-- S24r0p1/S28/29 change of decayDescriptor
    elif typ == 'NEWb2pipipi0g' :
        sel = RadSubstitute('NEWhhpi0_'+name , '2hpi0' , 'B0'  , ['a_1(1260)0']  ,['pi+','pi-'],input,extra)
    elif typ == 'NEWb2kpipi0g' :
        sel1 = RadSubstitute('NEWkpipi0_'+name     , '2hpi0' , 'B0'  , ['K_1(1270)0' ]  ,['K+' ,'pi-'],input,extra)
        sel2 = RadSubstitute('NEWkipipi0_CC_'+name , '2hpi0' , 'B~0' , ['K_1(1270)~0']  ,['pi+','K-'],input,extra)
        sel  = MergedSelection ('NEWkpipi0_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'NEWb2kkpi0g' :# new 2017/10
        sel = RadSubstitute('NEWkkpi0_'+name     , '2hpi0' , 'B_s0'  , ['phi(1680)' ]  ,['K+' ,'K-'],input,extra)
    elif typ == 'NEWlb2pkpi0g' :
        sel1 = RadSubstitute('NEWpkpi0_'+name     , '2hpi0' , 'Lambda_b0'  , ['Sigma(1660)0' ]  ,['p+' ,'K-'],input,extra)
        sel2 = RadSubstitute('NEWpkpi0_CC_'+name  , '2hpi0' , 'Lambda_b~0' , ['Sigma(1660)~0']  ,['K+' ,'pi-'],input,extra)
        sel  = MergedSelection ('NEWpkpi0_merger_'+name,RequiredSelections=[sel1,sel2])

    # -- B->(X->Y(hh)KS0)g modes : B0->K_1(rho0(pipi) KS0 )G

    elif typ == 'b2pipiksg' :
        sel = RadSubstitute('pipiks_'+name , 'hhks' , 'B0'  , ['K*_2(1430)0','rho(770)0']  ,['pi+','pi-'],input,extra)
    elif typ == 'b2kkksg' :
        sel = RadSubstitute('pipiks_'+name , 'hhks' , 'B0'  , ['K_2(1770)0','phi(1020)']  ,['K+','K-'],input,extra)
    elif typ == 'b2pikksg' : # Warning : unphysical decay descriptor
        sel1 = RadSubstitute('pikks_'+name , 'hhks' , 'B~0'  , ['K_2(1820)~0','K*(892)~0']  ,['pi+','K-'],input,extra)
        sel2 = RadSubstitute('kpiks_'+name , 'hhks' , 'B0'   , ['K_2(1820)0' ,'K*(892)0']   ,['K+','pi-'],input,extra)
        sel  = MergedSelection ('kpiks_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'xib2pkksg' : # new 2017/10
        sel1 = RadSubstitute('pkks_'+name , 'hhks' , 'Xi_b0'  , ['Xi(1820)0','Lambda(1520)0']  ,['p+','K-'],input,extra)
        sel2 = RadSubstitute('kpks_'+name , 'hhks' , 'Xi_b~0'   , ['Xi(1820)~0' ,'Lambda(1520)~0']   ,['K+','p~-'],input,extra)
        sel  = MergedSelection ('pkks_merger_'+name,RequiredSelections=[sel1,sel2])

    # For Beauty2XGammaExclTDCPV
    elif typ == 'b2pipiksg_ExclTDCPV' :
        sel = RadSubstitute('pipiks_'+name , 'hhks' , 'B0'  , ['K*_2(1430)0','rho(770)0']  ,['pi+','pi-'],input,extra)
    elif typ == 'b2pikksg_ExclTDCPV' : # Warning : unphysical decay descriptor
        sel = RadSubstitute('pikks_'+name , 'KmPipks' , 'B_s~0' , ['K*_2(1430)~0','rho(770)0']  ,['K-','pi+'],input,extra)       
        #sel = RadSubstitute('pikks_'+name , 'hhks' , 'B_s~0' , ['K*_2(1430)~0','rho(770)0']  ,['K-','pi+'],input,extra)
    elif typ == 'b2kpiksg_ExclTDCPV' : # Warning : unphysical decay descriptor
        # I need a separate tuple for kpiKs
        sel = RadSubstitute('kpiks_'+name , 'hhks' , 'B_s0'  , ['K*_2(1430)0','rho(770)0']    ,['K+','pi-'],input,extra)
    elif typ == 'b2kpig_ExclTDCPV' :
        sel1 = RadSubstitute('kpi_'+name   , 'hh' , 'B0'  , ['K*(892)0']  ,['K+','pi-'],input,extra)
        sel2 = RadSubstitute('kpi_CC_'+name, 'hh' , 'B~0' , ['K*(892)~0'] ,['pi+','K-'],input,extra)
        sel  = MergedSelection ('kpi_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'bs2kkg_ExclTDCPV' :
        sel = RadSubstitute('kk_'+name  , 'hh' , 'B_s0'  , ['phi(1020)']  ,['K+','K-'],input,extra)

    #-- S24r0p1/S28/29 change of decayDescriptor

    elif typ == 'NEWb2pipiksg' :
        sel = RadSubstitute('NEWpipiks_'+name , '2hks' , 'B0'  , ['K*_2(1430)0']  ,['pi+','pi-'],input,extra) # same resonance as S21/S24/26
    elif typ == 'NEWb2kkksg' :
        sel = RadSubstitute('NEWpipiks_'+name , '2hks' , 'B0'  , ['K_2(1770)0']  ,['K+','K-'],input,extra)
    elif typ == 'NEWb2pikksg' :  # unphysical decay descriptor (account for B0/B0bar->[rho0(1700)->[K*0KS0+]CC]Gamma or  Bs/Bsbar->[phi(1600)->[K*+(Kspi+)K-]CC]gamma
        sel1 = RadSubstitute('NEWpikks_'+name , '2hks' , 'B~0'  , ['K_2(1820)~0']   ,['pi+','K-'],input,extra)
        sel2 = RadSubstitute('NEWkpiks_'+name , '2hks' , 'B0'   , ['K_2(1820)0' ]   ,['K+','pi-'],input,extra)
        sel  = MergedSelection ('NEWkpiks_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'NEWxib2pkksg' :
        sel1 = RadSubstitute('NEWpkks_'+name , '2hks' , 'Xi_b0'  , ['Xi(1820)0']    ,['pi+','K-'],input,extra)
        sel2 = RadSubstitute('NEWkpks_'+name , '2hks' , 'Xi_b~0' , ['Xi(1820)~0' ]  ,['K+' ,'p~-'],input,extra)
        sel  = MergedSelection ('NEWkpks_merger_'+name,RequiredSelections=[sel1,sel2])

    # -- B->(X->hhhh)g modes
    elif typ == 'bs2phiphig' :
        sel = RadSubstitute('kkkk_'+name , 'hhhh' , 'B_s0'  , ['f_2(2010)']  ,['K+','K-','K+','K-'],input,extra)
    elif typ == 'b2kstphig' :
        sel1 = RadSubstitute('kpikk_'+name    , 'hhhh' , 'B0'  , ['K*_4(2045)0']  ,['K+','pi-','K+','K-'],input,extra)
        sel2 = RadSubstitute('kkkpi_'+name    , 'hhhh' , 'B0'  , ['K*_4(2045)0']  ,['K+','K-','K+','pi-'],input,extra)
        sel3 = RadSubstitute('kpikk_CC_'+name , 'hhhh' , 'B~0'  , ['K*_4(2045)~0']  ,['pi+','K-','K+','K-'],input,extra)
        sel4 = RadSubstitute('kkkpi_CC_'+name , 'hhhh' , 'B~0'  , ['K*_4(2045)~0']  ,['K+','K-','pi+','K-'],input,extra)
        sel  = MergedSelection ('kstphi_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])
    elif typ == 'bs2phikstg' : # the same inverting CC's in Bs decays
        sel1 = RadSubstitute('pikkk_'+name    , 'hhhh' , 'B_s0'  , ['K*_4(2045)~0']  ,['pi+','K-','K+','K-'],input,extra)
        sel2 = RadSubstitute('kkpik_'+name    , 'hhhh' , 'B_s0'  , ['K*_4(2045)~0']  ,['K+','K-','pi+','K-'],input,extra)
        sel3 = RadSubstitute('pikkk_CC_'+name , 'hhhh' , 'B_s~0'  , ['K*_4(2045)0']  ,['K+','pi-','K+','K-'],input,extra)
        sel4 = RadSubstitute('kkpik_CC_'+name , 'hhhh' , 'B_s~0'  , ['K*_4(2045)0']  ,['K+','K-','K+','pi-'],input,extra)
        sel  = MergedSelection ('phikst_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])
    elif typ == 'bs2kstkstg' :
        sel1 = RadSubstitute('kpipik_'+name , 'hhhh' , 'B_s0'  , ['f_2(1950)']  ,['K+','pi-','pi+','K-'],input,extra)
        sel2 = RadSubstitute('pikkpi_'+name , 'hhhh' , 'B_s0'  , ['f_2(1950)']  ,['pi+','K-','K+','pi-'],input,extra)
        sel  = MergedSelection ('kstkst_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'b2kstrhog' :
        sel1 = RadSubstitute('kpipipi_'+name    , 'hhhh' , 'B0'  , ['K*_2(1430)0']  ,['K+','pi-','pi+','pi-'],input,extra)
        sel2 = RadSubstitute('pipikpi_'+name    , 'hhhh' , 'B0'  , ['K*_2(1430)0']  ,['pi+','pi-','K+','pi-'],input,extra)
        sel3 = RadSubstitute('kpipipi_CC_'+name , 'hhhh' , 'B~0'  , ['K*_2(1430)~0']  ,['pi+','K-','pi+','pi-'],input,extra)
        sel4 = RadSubstitute('pipikpi_CC_'+name , 'hhhh' , 'B~0'  , ['K*_2(1430)~0']  ,['pi+','pi-','pi+','K-'],input,extra)
        sel  = MergedSelection ('kstrho_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])
    elif typ == 'bs2rhokstg' : # the same inverting CC's in Bs decay
        sel1 = RadSubstitute('pikpipi_'+name    , 'hhhh' , 'B_s0'  , ['K*_2(1430)~0']  ,['pi+','K-','pi+','pi-'],input,extra)
        sel2 = RadSubstitute('pipipik_'+name    , 'hhhh' , 'B_s0'  , ['K*_2(1430)~0']  ,['pi+','pi-','pi+','K-'],input,extra)
        sel3 = RadSubstitute('pikpipi_CC_'+name , 'hhhh' , 'B_s~0'  , ['K*_2(1430)0']  ,['K+','pi-','pi+','pi-'],input,extra)
        sel4 = RadSubstitute('pipipik_CC_'+name , 'hhhh' , 'B_s~0'  , ['K*_2(1430)0']  ,['pi+','pi-','K+','pi-'],input,extra)
        sel  = MergedSelection ('rhokst_merger_'+name,RequiredSelections=[sel1,sel2,sel3,sel4])

    # -- TO BE COMPLETED FOR MISSING DECAYS 
    else :        
        print 'Undefined maker type = ',typ
        sel=[]

    # -- define the selection sequence
    seq=SelectionSequence(name+'_'+typ+'Seq',TopSelection=sel)
    return seq

# ==
# == tuple sequence creator :
if defRefitTree :
    print 'Will apply DecayTreeFitter before tupling by default'


# == Line merger
def LineMerger (name, lines) :
    sels=[]
    for line in lines :
        sels.append( DataOnDemand(line) )
    merger=MergedSelection(name+'_LineMerger',RequiredSelections=sels)
    mergedSel=SelectionSequence(name+'_MergedSell',TopSelection=merger )            
    return mergedSel

    
processedLines=[]
tupNames=[]
def RadTupler(category, name, bh, rdecay, line='none', maker  = '', extra='',massfit=[],gamma='^gamma',STREAM=defStream, useBestPV=defBestPV, refitTree=defRefitTree,tistosTree=False,addTag=False,merge=['','DD','LL'],fullDTF=False,recombineTree=True) : # recombineTree = True to be the default ##OD
    print "line = ", line


    # ==== handle multiple-categories
    cats=[]
    if list == type(category) :    
        cats=category
    else :
        cats=[category]


    #-- force data location (DST only)
    path=defROOT
    if STREAM != defStream and not DaVinci().getProp('Simulation') :
        path='/Event/'+STREAM+'/'
        if DaVinci().InputType =='MDST' :
            print "InputType is MDST - cannot configure a non default stream for",name
            return 0

            
    PARTICLES='Phys/'+line+'/Particles'
    if DaVinci().InputType !='MDST' and (not DaVinci().getProp('Simulation') or os.getenv("RESTRIP","0")=="0") :
        PARTICLES=path+'Phys/'+line+'/Particles'


    # -- prepare line merging
    lines=[]
    if line.find('.*') != -1 :
        if merge == [] :
            print 'empty merging tags'
            sys.exit(-1)
        for tag in merge :
            lineName= line.replace('.*',tag)
            locate='Phys/'+lineName+'/Particles'
            if DaVinci().InputType !='MDST' and ''.join(cats).find('Fake') == -1 and (not DaVinci().getProp('Simulation') or os.getenv("RESTRIP","0")=="0"):
                locate=defROOT+'Phys/'+lineName+'/Particles'
            lines.append( locate )
    else:
        lines.append( PARTICLES )


    # ==== check the decay agains the requested category(ies)
    ok=True

    # ==== check the tuple naming is unique
    if name in tupNames :
        print 'ERROR : a sequence with name "'+name+'" already exists'
        sys.exit(-1)
    tupNames.append(name)

    
    # ==== collect the processed lines (to configure recochain update)
    if PARTICLES not in processedLines :
        processedLines.append(PARTICLES)


    # ==========================#
    # == CREATE THE SEQUENCE == #
    # ==========================#

    seq =GaudiSequencer(name + 'Seq')
    #-- Add the stripping line pre-filter
    print "Stripping{}Decision".format(line)
    if line != 'none' :
        filter = StripFilter( name+'PreFilter', Code="HLT_PASS_RE('Stripping"+line+"Decision')", Location='/Event/Strip/Phys/DecReports' )
        seq.Members = [filter]



    # ===========================#
    # == CONFIGURE THE TUPLE  == #
    # ===========================#
    
    #-- clone the generic tuple (as defined in radTuple.py)
    if DaVinci().getProp('Simulation')  :
        tuple = DecayTreeTuple('radTuple').clone(name+'TupleMC')
    else :
        tuple = DecayTreeTuple('radTuple').clone(name+'Tuple')


    # add current stripping line to TupleToolStripping in case automatic setting fails
    if line != 'none' : 
        tupStripping.addStrippingList(tuple,['Stripping'+line+'Decision'],append=True)

    #-- create the full decay descriptor
    if rdecay.find(']CC') != -1 :
        rdecay=rdecay.replace(']CC','')
        rdecay=rdecay.replace('[','')
        if gamma == 'none' or gamma == '' :
            decay = '['+bh+' -> '+rdecay+' ]CC'
        else :
            decay = '['+bh+' -> '+rdecay+' '+gamma+' ]CC'
    else :
        if gamma == 'none' or gamma == ' ' :
            decay = bh+' -> '+rdecay
        else :
            decay = bh+' -> '+rdecay+' '+gamma
    tuple.Decay = decay                

    #-- create and complete the B-branch (DTF with B mass constraint)
    tuple.Branches = { 'B'  : '^('+decay.replace('^','')+')'
                       #,'gamma'  : decay.replace('^','').replace(' gamma',' ^gamma')  Dunno why this is not working
                     } 
    tuple.addBranches({ 
                       'gamma'  : decay.replace('^','').replace(' gamma',' ^gamma')
                      }) 


    tupDTF.addInstanceDTF(tuple,'BMassFit','B',ConstMass=[bh])
    #tupDTF.addInstanceDTF(tuple,'PVandBMassFit','B',ConstMass=[bh],ConstVtx=True)
    if len(massfit) != 0 :
        tupDTF.addInstanceDTF(tuple,'MassFit','B',ConstMass=massfit)    
        if fullDTF :
            tupDTF.addInstanceDTF(tuple,'PVandMassFit','B',ConstMass=massfit,ConstVtx=True)
            allmassfit=massfit+[bh]
            tupDTF.addInstanceDTF(tuple,'PVandAllMassFit','B',ConstMass=allmassfit,ConstVtx=True)


    # down TupleToolTISTOS from its branch to the ground
    if tistosTree :
	# Obtain trigger list from B branch (defined in radTuples)
        tlist=tupTisTos.getTriggerList(tuple,'B')
        tupTisTos.addTriggerList(tuple,tlist)

    # Adding conevariables, if the stripping doesnt have them Warnings on the log.
    if DaVinci().InputType=='MDST': 
        import tupConeVars
        pathConeInfo = path+'Phys/'+line+'/'
        tupConeVars.addConeVars(tuple,'B',pathConeInfo) # Conevariables 3 angles 2 particles
        tupConeVars.addConeIsoVars(tuple,'B',pathConeInfo) # Neutral Conevariables 4 angles 2 particles
        tupConeVars.addVtxRadInfo(tuple,'B',pathConeInfo) # Vertex iso Radiative
        tuple.B.addTool(TupleToolVtxIsoln)
        tuple.B.ToolList+=['TupleToolVtxIsoln'] # not propagated to daughters !!
        tuple.B.TupleToolVtxIsoln.Verbose=True

    else : # REEVALUATE ON DST (for ExclTDCPV)
        # TupleToolVtxIsoln (->for RelInfoVertexIsolation)
        # https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/DecayTreeTuple/src/TupleToolVtxIsoln.h
        import decayTree, tupTool
        #ttool=tupTool.add(tuple,'TupleToolVtxIsoln/TupleToolVtxIsoln_B','B',True,Code=["Verbose=True"])
        #ttoolg=tupTool.add(tuple,'TupleToolVtxIsoln/TupleToolVtxIsoln_gamma','gamma',True,Code=["Verbose=True"])
        tuple.B.addTool(TupleToolVtxIsoln)
        tuple.B.ToolList+=['TupleToolVtxIsoln']
        tuple.B.TupleToolVtxIsoln.Verbose=True    

        # TupleToolConeIsolation ( for RelInfoConeIsolation)
        # https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/DecayTreeTupleTracking/src/TupleToolConeIsolation.h
        tuple.gamma.addTool(TupleToolConeIsolation)
        tuple.gamma.ToolList+=['TupleToolConeIsolation/TupleToolConeIsolation_gamma']
        tuple.gamma.TupleToolConeIsolation.FillComponents=True    
        tuple.gamma.TupleToolConeIsolation.MinConeSize=0.4   
        tuple.gamma.TupleToolConeIsolation.SizeStep=0.3   
        tuple.gamma.TupleToolConeIsolation.MaxConeSize=1.6   
        tuple.gamma.TupleToolConeIsolation.FillAsymmetry=True   
        tuple.gamma.TupleToolConeIsolation.FillDeltas=True   
        tuple.gamma.TupleToolConeIsolation.FillPi0Info=True   
        tuple.gamma.TupleToolConeIsolation.FillMergedPi0Info=True   
        #nominally muons from SL analyses, but we are missing pions...
        tuple.gamma.TupleToolConeIsolation.ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles" 
        tuple.gamma.TupleToolConeIsolation.MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles" 
        #----
        tuple.B.addTool(TupleToolConeIsolation)
        tuple.B.ToolList+=['TupleToolConeIsolation/TupleToolConeIsolation_B']
        tuple.B.TupleToolConeIsolation.FillComponents=True    
        tuple.B.TupleToolConeIsolation.MinConeSize=0.4   
        tuple.B.TupleToolConeIsolation.SizeStep=0.3   
        tuple.B.TupleToolConeIsolation.MaxConeSize=1.6   
        tuple.B.TupleToolConeIsolation.FillAsymmetry=True   
        tuple.B.TupleToolConeIsolation.FillDeltas=True   
        tuple.B.TupleToolConeIsolation.FillPi0Info=True   
        tuple.B.TupleToolConeIsolation.FillMergedPi0Info=True   
        #nominally muons from SL analyses, but we are missing pions...
        tuple.B.TupleToolConeIsolation.ExtraParticlesLocation="Phys/StdAllNoPIDsPions/Particles" 
        tuple.B.TupleToolConeIsolation.MaxPtParticlesLocation="Phys/StdAllNoPIDsPions/Particles" 

        # TupleToolTrackIsolation ( for RelInfoConeVariables)
        # https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/DecayTreeTupleTracking/src/TupleToolTrackIsolation.h
        tuple.gamma.ToolList+=['TupleToolTrackIsolation/TupleToolTrackIsolation_gamma']
        tuple.gamma.addTool(TupleToolTrackIsolation)
        tuple.gamma.TupleToolTrackIsolation.MinConeAngle=0.4    
        tuple.gamma.TupleToolTrackIsolation.StepSize=0.3    
        tuple.gamma.TupleToolTrackIsolation.MaxConeAngle=1.6    
        tuple.gamma.TupleToolTrackIsolation.Verbose=True    

        tuple.B.ToolList+=['TupleToolTrackIsolation/TupleToolTrackIsolation_B']
        tuple.B.addTool(TupleToolTrackIsolation)
        tuple.B.TupleToolTrackIsolation.MinConeAngle=0.4    
        tuple.B.TupleToolTrackIsolation.StepSize=0.3    
        tuple.B.TupleToolTrackIsolation.MaxConeAngle=1.6    
        tuple.B.TupleToolTrackIsolation.Verbose=True   


    # for stripping20 DST use vertexfitter to have a correct evaluation of VertexIsolation
    useOfflineVtx = False
    if DaVinci().InputType !='MDST' :
            useOfflineVtx = True
            tuple.VertexFitters.update( { "" : "OfflineVertexFitter"} )
            tuple.addTool(OfflineVertexFitter)
            tuple.OfflineVertexFitter.useResonanceVertex=False

    # == printout
    print '# Rad. tup from [',STREAM,'] - [bestPV|flavTag|fitTree]=[',int(useBestPV),'|',int(addTag),'|',int(refitTree),'] : ',category,':', name,' {',tuple.Decay,'}'
    # =================================#
    # == CONFIGURE THE DECAY MAKER  == #
    # =================================#


    # -- Merge in case of multiple lines 
    if line.find('.*') != -1 :
        lineMerger=LineMerger(name,lines)
        seq.Members+=[lineMerger.sequence()]
        PARTICLES=lineMerger.outputLocation()
    
    # -- configure Tree refit before tupling when requested
    refit='none'
    if refitTree :
        # always refit the decay with the default combiner (Loki:VertexFitter) !
        refit = FitDecayTrees ( name+'TreeFitter',
                                Code          = " DECTREE('"+decay.replace('^','')+"')",
                                MaxChi2PerDoF = 10
                                )
        if usePVConstraint   : refit.UsePVConstraint = True
        if useMassConstraint : refit.MassConstraints = massfit
    if not refitTree and defRefitTree : print '   ... refitTree is disabled'
        

    #-- Apply post-calibration when needed (Stripping v20 and MC)
    if os.getenv('POSTCALIBRATION') != '0' and (gamma == '^gamma' or gamma==Dec['pi0:Merged'] ):
        postcalib =RestoreCaloRecoChain('RestoreCaloRecoChain').clone(name+'PostCalib')
	#postcalib =CaloPostCalibAlg('RestoreCaloRecoChain').clone(name+'PostCalib')
        postcalib.Inputs=[PARTICLES]
        if gamma == Dec['pi0:Merged'] :
            print "hello this is a try to see merged pi0"
            #postcalib.ParticleID=111
            #postcalib.SetOriginToVertex=False
            
        if maker.find('CombineParticles') != -1 :
            poscalib.Inputs=["Phys/StdLooseAllPhotons"]        
        seq.Members+=[postcalib]

            
    #-- configure tuple input
    #-- Invoque a decay maker/filter when needed or just tuple the stripped candidates
    if maker != '' :
        if maker.find('CombineParticles') != -1 :  # === COMBINEPARTICLES MAKER 
            comb=maker.replace('CombineParticles(','')
            comb=comb.replace(')','')
            print '   ... Apply CombineParticles(',comb,')'
            alg=CombineParticles(comb)             
            if useBestPV :
                alg.ReFitPVs = True                
            if useOfflineVtx :
                alg.ParticleCombiners.update( { "" : "OfflineVertexFitter"} )
                alg.addTool(OfflineVertexFitter)
                alg.OfflineVertexFitter.useResonanceVertex=False
            seq.Members+=[alg]
            if refitTree :  # insert decaytree refitter if requested
                refit.Inputs=['Phys/'+comb]
                seq.Members+=[refit]
                tuple.Inputs=["Phys/"+name+"TreeFitter"]
            else :
                tuple.Inputs=['Phys/'+comb]
            if extra != '' :
                print "WARNING : RecoChainextra filtering won't be applied -it has to be included in the CombineParticles configuration"
        else :                                      # === PID-SUBTITUTE STRIPPED CANDIDATES
            if gamma == Dec['pi0:Merged'] or gamma == Dec['pi0'] :
                make = NocDecayMaker(name+'Maker',maker ,PARTICLES ,extra) # propagate the extra filter here
            else :
                make = RadDecayMaker(name+'Maker',maker ,PARTICLES ,extra) # propagate the extra filter here
            seq.Members+=[make.sequence()]

            # apply vertexFitter along the tree (first)
            if recombineTree :
                tuple.Inputs=make.outputLocations()

	         # ReCombiner is already on RestoreCaloRecoChain (so deprecated)
#                from Configurables import ReCombiner
#                recombine=ReCombiner(name+'ReCombiner')
#                recombine.Inputs=make.outputLocations()
#                seq.Members+=[recombine]

            # apply DTF refitter (already in Substitute !)
            if refitTree : # insert decaytree refitter if requested
                refit.Inputs=make.outputLocations()
                seq.Members+=[refit]
                tuple.Inputs=["Phys/"+name+"TreeFitter"]
                 

    elif extra != '' :                               # === FILTER STRIPPED CANDIDATES
        filt=FilterDesktop(name+'_FD',Code=extra)
        sel = Selection('filt_'+name, Algorithm=filt , RequiredSelections=[DataOnDemand(PARTICLES)] )
        filtseq=SelectionSequence('filtseq_'+name,TopSelection=sel)
        seq.Members+=[filtseq.sequence()]
        if refitTree :  # insert decaytree refitter if requested
            refit.Inputs=filtseq.outputLocations()
            seq.Members+=[refit]
            tuple.Inputs=["Phys/"+name+"TreeFitter"]
        else:
            tuple.Inputs=filtseq.outputLocations()
    else :                                           # === DIRECT STRIPPED CANDIDATES
        if refitTree :  # insert decaytree refitter if requested
            refit.Inputs=[PARTICLES]  
            seq.Members+=[refit]
            tuple.Inputs=["Phys/"+name+"TreeFitter"]
        else:        
            tuple.Inputs = [PARTICLES]  



    # == propagate original relations (Tagging/BestPV/...) to subsituted B
    if tuple.Inputs != [PARTICLES] :
        from Configurables import restoreRelations
        restore=restoreRelations(name+'RestoreRelations')
        restore.Origin=lines
        restore.Inputs=tuple.Inputs
        seq.Members+=[restore]

    # ====== reFitPVs & best PV  ======= # 
    if useBestPV and stripping.find('20') == -1 : 
        print 'Warning : best PV is not defined for mDST lines in stripping > v20'
    
    if useBestPV :
        if line.find('.*') != -1 :
            print 'BestPV does not support line merging'
            sys.exit(-1)
        p2pvloc=[]
        loc=path+'Phys/'+line
        pvReFitSeq=GaudiSequencer(name+"PVReFitSeq");
        pvReFitSeq.IgnoreFilterPassed=True
        if DaVinci().InputType=="MDST" :  # useP2BestPV as stored on mDST
            p2pvloc=[loc+"/BestPV_"+STREAM+"_P2PV"]
        else :  # refit explicitely
            pvReFit=PVReFitterAlg(name+"_ReFitPV")
            pvReFit.ParticleInputLocations=tuple.Inputs
            bestPV=BestPVAlg(name+"_BestPV")
            bestPV.P2PVRelationsInputLocations=[loc+"/"+pvReFit.name()+"_P2PV"]
            pvReFitSeq.Members = [pvReFit,bestPV]
            p2pvloc=[loc+"/"+bestPV.name()+"_P2PV"]
            seq.Members += [pvReFitSeq]
        tuple.IgnoreP2PVFromInputLocations=False
        tuple.UseP2PVRelations=True
        tuple.WriteP2PVRelations=False
        tuple.P2PVInputLocations=p2pvloc
        print "    - Will rely on Best PVs as in : ",p2pvloc
        print ' '

    # === add Tagging info when relevant
    hasTag=False
    if  stripName('2pi_pi0R')     == line : hasTag=False
    if  stripName('3pi_pi0R')     == line : hasTag=False
    if  'Lb2L0Gamma'          == line : hasTag=False
    if  'Lb2L0GammaConverted' == line : hasTag=False    
    if addTag and hasTag :
            import decayTree, tupTool
            tagtool=tupTool.add(tuple,"TupleToolTagging" , 'B',False, Code='Verbose=True' )
            # == why changing the property names ???
            try: # try old tagtool syntax
                tagtool.useFTonDST=True
            except AttributeError :
                print 'WARNING : failed to configure tagtool.useFTonDST'
                print '        : try new tagging tool configuration'
                tagtool.UseFTfromDST=True
                tagtool.ActiveTaggerTypes = ["OSCharm","OSElectron","OSElectronLatest","OSKaon","OSKaonLatest","OSMuon","OSMuonLatest","OSVtxCh","SSKaon","SSKaonLatest","SSPion","SSProton"]                            
            tagtool.TaggingToolName = "BTaggingTool/BTaggingTool"
            if DaVinci().Simulation==True and DaVinci().InputType!='MDST':
                from FlavourTagging.Tunings import TuneTool
                tuning="Stripping21_MC"
                try:
                    tagtool.useFTonDST=False
                except AttributeError :
                    tagtool.UseFTfromDST=True
                tuneTool = TuneTool(tagtool,tuning,"BTaggingTool") # valid for S24/26/28/29 ??
                #tagtool.BTaggingTool.OutputLevel=DEBUG
                print "Configure BTagging TuneTool with '",tuning,"'"

    rerunFT=False
    if "2hksG_ExclTDCPV" in category: rerunFT=True
    if addTag and rerunFT :
            #https://gitlab.cern.ch/lhcb-ft/TupleProduction/tree/update-instructions#case-1-re-running-a-specific-ft-tuning
            import decayTree, tupTool
            tt_tagging=tupTool.add(tuple,"TupleToolTagging" , 'B',False, Code='Verbose=True' )
            tt_tagging.Verbose = True
            tt_tagging.AddMVAFeatureInfo = True # Writes out all MVA features for the final tag
            tt_tagging.AddTagPartsInfo = False  # If True: Writes out all features of all tagging
                                    # particles used in the selection

            from Configurables import BTaggingTool
            btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool")

            from FlavourTagging.Tunings import applyTuning as applyFTTuning
            if int(year) > 2012: applyFTTuning(btagtool, tuning_version="Summer2017Optimisation_v4_Run2")
            else : applyFTTuning(btagtool) # should we specify tuning for Run1?
            tt_tagging.TaggingToolName = btagtool.getFullName()

    
    seq.Members+=[tuple ]        

    # ================================#
    # == CONFIGURE THE CATEGORIES  == #
    # ================================#

    # feed category-sequencer
    for cat in cats :
        catSeq=GaudiSequencer(cat)
        catSeq.IgnoreFilterPassed=True
        catSeq.Members+=[seq]

    return tuple 

# == 
# == decay descriptors dictionary :
from decayDict import Dec,doDec

# =============                           ==============#
#                                                       #
#      DECAYTREES and TUPLE SEQUENCES  DEFINITION       #
#                                                       #
# =============                           ==============#

# ==                                               == #
# == invoke tupler & create the category sequences == #
# ==                                               == #
# == RadTupler usage for B -> (X-> a b ... )   gamma  :
# == RadTupler( 'category', 'name' , 'B head'  , 'X decay'  ,  'stripping line' , <maker='creator'>  , <extra = 'add filter>' ,  <massfit=['mass constraints']> )
# == NB : < > are facultative arguments
# == 'maker' is either a predefined substitution algorithm or a decay combiner (maker='CombineParticles(name)'
# ==


# =============================================#
# ======= Stripping v21r0 & v21r1   ===========#
# ======= and above ...             ===========#
# =============================================#

if category == "VG":
    # => cat : VG  (inclusive B0->(hh)gamma decay + extra (hh) mass filter)
    RadTupler('VG','kstG'  , 'B0'   , Dec['K*(892)0' ]  , stripName('2pi') , maker='b2kpig', extra=filtKst,massfit=['K*(892)0' ],addTag=True)
    RadTupler('VG','rhoG'  , 'B0'   , Dec['rho(770)0']  , stripName('2pi')                 , extra=filtRho,massfit=['rho(770)0'],addTag=True)
    RadTupler('VG','phiG'  , 'B_s0' , Dec['phi(1020)']  , stripName('2pi')   , maker='bs2kkg' ,extra=filtPhi,massfit=['phi(1020)'],addTag=True)
    RadTupler('VG','kstIsoG','B+'   , Dec['K*(892)+']   , stripName('pi_Ks0'), maker='b2piksg',extra=filtKstp, massfit=['K*(892)+']) 

if category == "hksG":
    # => cat : hksG
    RadTupler('hksG' ,'kksG'  ,'B+'   , Dec['a_2(1320)+'] , stripName('pi_Ks0'), maker='b2kksg',  massfit=['KS0']) 
    RadTupler('hksG' ,'piksG' ,'B+'   , Dec['K*(892)+']   , stripName('pi_Ks0'), maker='b2piksg', massfit=['KS0'])
 
if category == "VGee":   
    # => cat : VGee  (inclusive B0->(hh)gamma decay + extra (hh) mass filter) with conversion
    RadTupler('VGee','kstGee', 'B0'  , Dec['K*(892)0' ], stripName('2pi_wCNV.*'),maker='b2kpig',extra=filtKst,massfit=['K*(892)0' ],gamma=Dec['Conversion'])
    RadTupler('VGee','rhoGee', 'B0'  , Dec['rho(770)0'], stripName('2pi_wCNV.*')               ,extra=filtRho,massfit=['rho(770)0'],gamma=Dec['Conversion'])
    RadTupler('VGee','phiGee', 'B_s0', Dec['phi(1020)'], stripName('2pi_wCNV.*'),maker='bs2kkg',extra=filtPhi,massfit=['phi(1020)'],gamma=Dec['Conversion'])
    
if category == "2hG":
    # => cat : 2hG (inclusive B0->hh gamma decays)
    RadTupler('2hG','pipiG' , 'B0'        , Dec['rho(770)0'   ] , stripName('2pi') ) # the direct line (no creator)!  (addTag ??)
    RadTupler('2hG','kpiG'  , 'B0'        , Dec['K*(892)0'    ] , stripName('2pi') , maker='b2kpig',addTag=True)
    RadTupler('2hG','kkG'   , 'B_s0'      , Dec['phi(1020)'   ] , stripName('2pi') , maker='bs2kkg',addTag=True)
    RadTupler('2hG','pkG'   , 'Lambda_b0' , Dec['Lambda(1520)0'], stripName('2pi') , maker='lb2pkg')

if "ExclKstG" in category or "ExclPhiG" in category:
    # = cat Excl KstG (full DST)
    flag='Excl' # str21 naming
    if not STRIP_21 :
        flag='Exclusive' # str24/26/28/29 naming
    RadTupler('ExclKstG','ExclKstG', 'B0'   , Dec['K*(892)0' ],stripName(flag+'Bd2KstGammaLine'), massfit=['K*(892)0' ],STREAM='Calibration')
    RadTupler('ExclPhiG','ExclPhiG', 'B_s0' , Dec['phi(1020)'],stripName(flag+'Bs2PhiGammaLine'), massfit=['phi(1020)'],STREAM='Radiative')
     
if category == "2hGee":
    # => cat : 2hGee (inclusive B0->hh gamma decays) with conversion
    RadTupler('2hGee','pipiGee' , 'B0'  , Dec['rho(770)0'] , stripName('2pi_wCNV.*')                         ,gamma=Dec['Conversion'],addTag=True ) 
    RadTupler('2hGee','kpiGee'  , 'B0'  , Dec['K*(892)0' ] , stripName('2pi_wCNV.*')         , maker='b2kpig',gamma=Dec['Conversion'],addTag=True)
    RadTupler('2hGee','kkGee'   , 'B_s0', Dec['phi(1020)'] , stripName('2pi_wCNV.*')         , maker='bs2kkg',gamma=Dec['Conversion'],addTag=True)
    RadTupler('2hGee','pkGee'   , 'Lambda_b0' , Dec['Lambda(1520)0'], stripName('2pi_wCNV.*'), maker='lb2pkg',gamma=Dec['Conversion'])

if category == "LG":
    # => cat  : LG : (Lb->ph gamma) / Lb->V0gamma
    RadTupler('LG','ppiG'  , 'Lambda_b0' , Dec['N(1440)0']     , stripName('2pi')         , maker='lb2ppig')    
    RadTupler('LG','ppiGee', 'Lambda_b0' , Dec['N(1440)0']     , stripName('2pi_wCNV.*')    , maker='lb2ppig',gamma=Dec['Conversion'])
    RadTupler('LG','lG'    , 'Lambda_b0' , Dec['Lambda0']      , 'Lb2L0Gamma')    
    RadTupler('LG','lGee'  , 'Lambda_b0' , Dec['Lambda0']      , 'Lb2L0GammaConverted',gamma=Dec['Conversion'])    
   
if category == "3hG":
    # == cat : 3hG : (B+->(hhh)^+ gamma)
    RadTupler('3hG','kpipiG'   , 'B+'     , Dec['3:K_1(1270)+']  , stripName('3pi')  , maker='b2kpipig') 
    RadTupler('3hG','pipipiG'  , 'B+'     , Dec['3:a_1(1260)+']  , stripName('3pi')  , maker='b2pipipig')
    RadTupler('3hG','kkpiG'    , 'B+'     , Dec['3:rho_3(1690)+'], stripName('3pi')  , maker='b2kkpig'  ) 
    RadTupler('3hG','kkkG'     , 'B+'     , Dec['3:K_2(1770)+']  , stripName('3pi')  , maker='b2kkkg'   ) 
    
if category == "3hGee":
    # == cat : 3hGee : (B+->(hhh)^+ gamma) with conversion
    RadTupler('3hGee','kpipiGee'   , 'B+'     , Dec['3:K_1(1270)+']  , stripName('3pi_wCNV.*')  , maker='b2kpipig',gamma=Dec['Conversion']) 
    RadTupler('3hGee','pipipiGee'  , 'B+'     , Dec['3:a_1(1260)+']  , stripName('3pi_wCNV.*')  , maker='b2pipipig',gamma=Dec['Conversion'])
    RadTupler('3hGee','kkpiGee'    , 'B+'     , Dec['3:rho_3(1690)+'], stripName('3pi_wCNV.*')  , maker='b2kkpig',gamma=Dec['Conversion']  ) 
    RadTupler('3hGee','kkkGee'     , 'B+'     , Dec['3:K_2(1770)+']  , stripName('3pi_wCNV.*')  , maker='b2kkkg',gamma=Dec['Conversion']   ) 
  
if category == "2hksG_ExclTDCPV":
    RadTupler('2hksG_ExclTDCPV','pipiksG_ExclTDCPV', 'B0' , Dec['3:K*_2(1430)0']    , stripName('ExclTDCPVBd2KspipiGammaLine'), maker='b2pipiksg_ExclTDCPV' , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=False,recombineTree=True)
   # I called rho0 ->Kpi which doesn't have a cc, so need two separate tuples for Bs->KpiKsg (should have made this K*)
    RadTupler('2hksG_ExclTDCPV','pikksG_ExclTDCPV' , 'B_s~0' , Dec['3piK:K*_2(1430)~0'] , stripName('ExclTDCPVBs2KsKpiGammaLine'), maker='b2pikksg_ExclTDCPV' , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=False,recombineTree=True)
    RadTupler('2hksG_ExclTDCPV','kpiksG_ExclTDCPV' , 'B_s0' , Dec['3Kpi:K*_2(1430)0'] , stripName('ExclTDCPVBs2KsKpiGammaLine'), maker='b2kpiksg_ExclTDCPV' , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=False,recombineTree=True)
    RadTupler('2hksG_ExclTDCPV','KstG_ExclTDCPV' , 'B0' , Dec['K*(892)0'] , stripName('ExclTDCPVBd2KstGammaLine'), maker='b2kpig_ExclTDCPV', addTag=True,fullDTF=True,tistosTree=False,recombineTree=True)
    RadTupler('2hksG_ExclTDCPV','PhiG_ExclTDCPV' , 'B_s0' , Dec['phi(1020)'] , stripName('ExclTDCPVBs2PhiGammaLine'), maker='bs2kkg_ExclTDCPV', addTag=True,fullDTF=True,tistosTree=False,recombineTree=True)


if STRIP_run2_special :
    if category == "2hpi0G":
        # == cat : 3h0G (B0->(hhh)^0 gamma) => S28 & S24r0p1 NAMING and DECAY !!!!    
        RadTupler('2hpi0G','omegaG'   , 'B0'   , Dec['omega(782):rhopi'], stripName('phiOmega_2pipi0R'), maker='b2omegag'   ,extra=filtOmega ,massfit=['omega(782)','pi0']) 
        RadTupler('2hpi0G','pipipi0G' , 'B0'   , Dec['3:a_1(1260)0']    , stripName('phiOmega_2pipi0R'), maker='b2pipipi0g' ,massfit=['pi0']) 
        RadTupler('2hpi0G','kpipi0G'  , 'B0'   , Dec['3:K_1(1270)0']    , stripName('phiOmega_2pipi0R'), maker='b2kpipi0g'  ,massfit=['pi0'])     

    if category == "2hksG":
        RadTupler('2hksG','pipiksG' , 'B0'     , Dec['33:K*_2(1430)0'] , stripName('2pi_Ks0'), maker='NEWb2pipiksg', massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=True,recombineTree=True)
        RadTupler('2hksG','kpiksG'  , 'B0'     , Dec['33:K_2(1820)0']  , stripName('2pi_Ks0'), maker='NEWb2pikksg' , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=True,recombineTree=True)
        RadTupler('2hksG','kkksG'   , 'B0'     , Dec['33:K_2(1770)0']  , stripName('2pi_Ks0'), maker='NEWb2kkksg'  , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=True,recombineTree=True)
        RadTupler('2hksG','pkksG'     ,'Xi_b0' , Dec['33:Xi(1820)0']   , stripName('2pi_Ks0'), maker='NEWxib2pkksg', massfit=['KS0'],fullDTF=True,tistosTree=True,recombineTree=True)

    if category == "hhpi0RG":
        RadTupler('hhpi0RG','pipipi0RG', 'B0'       ,    Dec['33:a_1(1260)0']   , stripName('2pi_pi0R'),maker='NEWb2pipipi0g', massfit=['pi0'])
        RadTupler('hhpi0RG','kpipi0RG' , 'B0'       ,    Dec['33:K_1(1270)0']   , stripName('2pi_pi0R'),maker='NEWb2kpipi0g', massfit=['pi0'])
        RadTupler('hhpi0RG','kkpi0RG'  , 'B_s0'     ,    Dec['33:phi(1680)']    , stripName('2pi_pi0R'),maker='NEWb2kkpi0g' , massfit=['pi0'])
        RadTupler('hhpi0RG','pkpi0RG'  , 'Lambda_b0',    Dec['33:Sigma(1660)0'] , stripName('2pi_pi0R'),maker='NEWlb2pkpi0g' , massfit=['pi0'])

    if category == "hhpi0MG":
        RadTupler('hhpi0MG','pipipi0MG' , 'B0'       ,    Dec['33M:a_1(1260)0']  , stripName('2pi_pi0M'),maker='NEWb2pipipi0g')
        RadTupler('hhpi0MG','kpipi0MG' , 'B0'       ,    Dec['33M:K_1(1270)0']  , stripName('2pi_pi0M'),maker='NEWb2kpipi0g')
        RadTupler('hhpi0MG','kkpi0MG'  , 'B_s0'     ,    Dec['33M:phi(1680)']   , stripName('2pi_pi0M'),maker='NEWb2kkpi0g')
        RadTupler('hhpi0MG','pkpi0MG'  , 'Lambda_b0',    Dec['33M:Sigma(1660)0'], stripName('2pi_pi0M'),maker='NEWlb2pkpi0g')

else :
    if category == "2hpi0G":
        # == cat : 3h0G (B0->(hhh)^0 gamma) => S21/24/26 + 24r1p1+28r1p1+29r2 NAMING and Decay
        RadTupler('2hpi0G','omegaG'   , 'B0' , Dec['omega(782):rhopi'], stripName('2pi_pi0R'), maker='b2omegag'   ,extra=filtOmega ,massfit=['omega(782)','pi0']) 
        RadTupler('2hpi0G','pipipi0G' , 'B0' , Dec['3:a_1(1260)0']        , stripName('2pi_pi0R'), maker='b2pipipi0g' ,massfit=['pi0']) 
        RadTupler('2hpi0G','kpipi0G'  , 'B0' , Dec['3:K_1(1270)0']        , stripName('2pi_pi0R'), maker='b2kpipi0g'  ,massfit=['pi0'])     

    if category == "2hksG":
        RadTupler('2hksG','pipiksG' , 'B0'     , Dec['3:K*_2(1430)0']  , stripName('2pi_Ks0'), massfit=['KS0']  , addTag=True,fullDTF=True,tistosTree=True) # direct line (no maker)
        RadTupler('2hksG','kpiksG'  , 'B0'     , Dec['3:K_2(1820)0']   , stripName('2pi_Ks0'), maker='b2pikksg' , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=True,recombineTree=True)
        RadTupler('2hksG','kkksG'   , 'B0'     , Dec['3:K_2(1770)0']   , stripName('2pi_Ks0'), maker='b2kkksg'  , massfit=['KS0'],addTag=True,fullDTF=True,tistosTree=True,recombineTree=True)
        RadTupler('2hksG','pkksG'   , 'Xi_b0'  , Dec['3:Xi(1820)0']    , stripName('2pi_Ks0'), maker='xib2pkksg', massfit=['KS0'],fullDTF=True,tistosTree=True,recombineTree=True) # NEW 2017/10

    if category == "hhpi0RG":
        RadTupler('hhpi0RG','pipipi0RG'  , 'B0' , Dec['3:a_1(1260)0']    , stripName('2pi_pi0R'), maker='b2pipipi0g'  ,massfit=['pi0'])     
        RadTupler('hhpi0RG','kpipi0RG'  , 'B0' , Dec['3:K_1(1270)0']    , stripName('2pi_pi0R'), maker='b2kpipi0g'  ,massfit=['pi0'])     
        RadTupler('hhpi0RG','kkpi0RG'  , 'B_s0'     ,    Dec['3:phi(1680)']    , stripName('2pi_pi0R'),maker='b2kkpi0g' , massfit=['pi0'])
        RadTupler('hhpi0RG','pkpi0RG'  , 'Lambda_b0',    Dec['3:Sigma(1660)0'] , stripName('2pi_pi0R'),maker='lb2pkpi0g' , massfit=['pi0'])

    if category == "hhpi0MG":
        RadTupler('hhpi0MG','pipipi0MG' , 'B0'       ,    Dec['3M:a_1(1260)0']  , stripName('2pi_pi0M'),maker='b2pipipi0g')
        RadTupler('hhpi0MG','kpipi0MG' , 'B0'       ,    Dec['3M:K_1(1270)0']  , stripName('2pi_pi0M'),maker='b2kpipi0g')
        RadTupler('hhpi0MG','kkpi0MG'  , 'B_s0'     ,    Dec['3M:phi(1680)']   , stripName('2pi_pi0M'),maker='b2kkpi0g')
        RadTupler('hhpi0MG','pkpi0MG'  , 'Lambda_b0',    Dec['3M:Sigma(1660)0'], stripName('2pi_pi0M'),maker='lb2pkpi0g')

if category == "4hG":  
    # == cat : 4hG
    RadTupler('4hG','rhorhoG'   , 'B0'     , Dec['4:f_2(1270)']  , stripName('4pi') )
    RadTupler('4hG','phiphiG'   , 'B_s0'   , Dec['4:f_2(2010)']  , stripName('4pi'), maker='bs2phiphig' )
    RadTupler('4hG','kstkstG'   , 'B_s0'   , Dec['4:f_2(1950)']  , stripName('4pi'), maker='bs2kstkstg' )
    # phirho ??

if category == "4hGs":
    RadTupler('4hGs','kstphiG'   , 'B0'     , Dec['4:K*_4(2045)0']  , stripName('4pi'), maker='b2kstphig' )
    RadTupler('4hGs','kstrhoG'   , 'B0'     , Dec['4:K*_2(1430)0']  , stripName('4pi'), maker='b2kstrhog' )
    RadTupler('4hGs','phikstG'   , 'B_s0'   , Dec['4:K*_4(2045)~0']  , stripName('4pi'), maker='bs2phikstg' )
    RadTupler('4hGs','rhokstG'   , 'B_s0'   , Dec['4:K*_2(1430)~0']  , stripName('4pi'), maker='bs2rhokstg' )

    
    # == SPECIAL/EXPERIMENTAL CATEGORIES (run2 only)
if category == "GG":
    RadTupler('GG','GGwide'   , 'B_s0'         , "^gamma"               , "Bs2GammaGammaWide_NoConvLine" )
    RadTupler('GG','GG'       , 'B_s0'         , "^gamma"               , "Bs2GammaGamma_NoConvLine"     )
    RadTupler('GG','GGee'     , 'B_s0'         , "^(gamma -> ^e+ ^e-)"  , "Bs2GammaGamma_.*Line"         ) # merging DD & LL
    RadTupler('GG','GeeGee'   , 'B_s0'         , "^(gamma -> ^e+ ^e-)"  , "Bs2GammaGamma_doubleLine"     ,gamma=Dec['Conversion'])

if category == "NoBias":
    RadTupler('NoBias','kstGNoBias', 'B0'  , Dec['K*(892)0']  , 'Beauty2XGammaNoBiasBd2KstGammaLine',massfit=['K*(892)0'] )
    RadTupler('NoBias','phiGNoBias', 'B_s0', Dec['phi(1020)'] , 'Beauty2XGammaNoBiasBs2PhiGammaLine',massfit=['phi(1020)'])



    # MISSING :
    # B->hhgammagamma B->hhhgammagamma
    # missing phirho in 4hG(s)
    # == cat : 3hh0 K*K*- / phiK*- / rho0K*- / rho0K*+ / rho+K* / rho-K*
    # == cat : 3hKS K*K*- / phiK*- / rho0K*-     
    # == cat : 2h+Lambda0 & 3h+Lambda0
    # == cat : BG (str21) ?
    # == cat : 2pi_2ks  (K*+K*-)g ...

# ==================================================== #
# === CREATE FAKE CATEGORIES : DiMuon as  fake Photon  #
# === stripping filter set in main/catFake(WS).py      #
# ==================================================== #
# == Convert RadTuple into FakeTuple
def Fake( tuple ) :
    if tuple != 0 :
        # adapt TupleToolTISTOS and TupleToolStripping to diMuon 
        tupStripping.addStrippingList(tuple,tupStripping.radFakeStrippingList() )
        tupTisTos.addTriggerList     (tuple,tupTisTos.radFakeTriggerList()     , '->B' ) # overwrite B branch setting and move it down
        print " ...  "+tuple.getName()+" is converted to Fake Tuple"

#importOptions("$DVTUPLESCRIPTS/libRadiative/settings/fakeRadDecays.py")

if category.find('Fake') != -1 :
	Fake(RadTupler('Fake'  ,'fakePhiG'  ,'B_s0',doDec('phi(1020)','J/psi(1S):mm')   ,maker='CombineParticles(fakeB2phiG)',massfit=['phi(1020)'],gamma='none',tistosTree=True))
	Fake(RadTupler('Fake'  ,'fakeKstG'  ,'B0'  ,doDec('K*(892)0' ,'J/psi(1S):mm')   ,maker='CombineParticles(fakeB2kstG)',massfit=['K*(892)0'] ,gamma='none',tistosTree=True))
	Fake(RadTupler('FakeWS','fakePhiWSG','B_s0',doDec('phi(1020):WS','J/psi(1S):mm'),maker='CombineParticles(fakeB2phiWSG)',massfit=['phi(1020)'],gamma='none',tistosTree=True))
	Fake(RadTupler('FakeWS','fakeKstWSG','B0'  ,doDec('K*(892)0:WS' ,'J/psi(1S):mm'),maker='CombineParticles(fakeB2kstWSG)',massfit=['K*(892)0'] ,gamma='none',tistosTree=True))

	#  clone 'Fake' category  : naming change only for producing fake and full PsiV in the same job
	Fake(RadTupler('PsiV'  ,'PsiPhi'  ,'B_s0',doDec('phi(1020)','J/psi(1S):mm'),maker='CombineParticles(B2PsiPhi)',massfit=['phi	(1020)'],gamma='none',tistosTree=True))
	Fake(RadTupler('PsiV'  ,'PsiKst'  ,'B0'  ,doDec('K*(892)0' ,'J/psi(1S):mm'),maker='CombineParticles(B2PsiKst)',massfit=['K*(892)0'] ,gamma='none',tistosTree=True))

	# Faking  the 2hks(G) category
	Fake(RadTupler('2hksFake','pipiksFake' , 'B0'   , doDec('3:K*_2(1430)0','J/psi(1S):mm'), maker='CombineParticles(fakeB2pipiksG)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))
	Fake(RadTupler('2hksFake','kpiksFake'  , 'B0'   , doDec('3:K_2(1820)0' ,'J/psi(1S):mm'), maker='CombineParticles(fakeB2kpiksG)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))
	Fake(RadTupler('2hksFake','kkksFake'   , 'B0'   , doDec('3:K_2(1770)0' ,'J/psi(1S):mm'), maker='CombineParticles(fakeB2kkksG)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))

# clone
	Fake(RadTupler('Psi2hks','Psipipiks' , 'B0'   , doDec('3:K*_2(1430)0','J/psi(1S):mm'), maker='CombineParticles(B2Psipipiks)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))
	Fake(RadTupler('Psi2hks','Psikpiks'  , 'B0'   , doDec('3:K_2(1820)0' ,'J/psi(1S):mm'), maker='CombineParticles(B2Psikpiks)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))
	Fake(RadTupler('Psi2hks','Psikkks'   , 'B0'   , doDec('3:K_2(1770)0' ,'J/psi(1S):mm'), maker='CombineParticles(B2Psikkks)',massfit=['KS0'] ,gamma='none',addTag=True,tistosTree=True,fullDTF=True))




# ================================================ #
# ============= Extension to Charmless hhpi0 ===== #
# ================================================ #

# == Charmless B->hhpi0 decay substitions
def NocSubstitute(name,typ,b,d,inputs,extra='') :

    if typ == 'hh' :   # default : rho0->pi+pi-
        sub = SubstitutePID('hhpi0Subst_'+name , Code="DECTREE('X0 -> X+ X- pi0')")
        if d[0] != 'pi+'       : sub.Substitutions[' X0 ->  ^X+  X-  pi0']=  d[0]
        if d[1] != 'pi-'       : sub.Substitutions[' X0 ->   X+ ^X-  pi0']=  d[1]
        if b    != 'B0'        : sub.Substitutions[' X0 ->   X+  X-  pi0']=  b
        filter =  "INTREE(ID=='%s') & INTREE(ID=='%s') & INTREE(ID=='%s')" % (b,d[0],d[1])    
    else :
        print 'Error : unknown hh0substition typ = ',typ
        return []
    # -- define the selection (+ filter)
    sub.MaxParticles=2000
    sub.MaxChi2PerDoF = -1   # !! NO DECAYTREE REFIT HERE !! (to be done later when requested)
    sel = Selection(typ+'Sel_'+name,Algorithm=sub, RequiredSelections=inputs)
    if extra != '' : filter += ' & ' +extra
    filt=FilterDesktop(typ+'FD_'+name,Code=filter)
    return Selection(typ+'_'+name , Algorithm=filt,RequiredSelections=[sel])


    
def NocDecayMaker(name, typ , data , extra='') :
    input=[DataOnDemand(data)]
    if typ == 'b2kpipi0' :
        sel1 = NocSubstitute('kpi_'+name   , 'hh' , 'B0'  , ['K+','pi-'],input,extra)
        sel2 = NocSubstitute('kpi_CC_'+name, 'hh' , 'B~0' , ['pi+','K-'],input,extra)
        sel  = MergedSelection ('kpi_merger_'+name,RequiredSelections=[sel1,sel2])
    elif typ == 'bs2kkpi0' :
        sel = NocSubstitute('kk_'+name   , 'hh' , 'B_s0'  , ['K+','K-'],input,extra)
    elif typ == 'lb2pkpi0' :
        sel1 = NocSubstitute('pk_'+name   , 'hh' , 'Lambda_b0'  , ['p+','K-'],input,extra)
        sel2 = NocSubstitute('pk_CC_'+name, 'hh' , 'Lambda_b~0' , ['K+','p~-'],input,extra)
        sel  = MergedSelection ('kpi_merger_'+name,RequiredSelections=[sel1,sel2])
    else :        
        print 'Undefined maker type = ',typ
        sel=[]
    # -- define the selection sequence
    seq=SelectionSequence(name+'_'+typ+'Seq',TopSelection=sel)
    return seq

# =================================== #
# == Charmless B->HHPI0 w/ merged
if category == "2hpi0M":
    RadTupler('2hpi0M','pipipi0M'   , 'B0'         , "^pi+^pi-"      , "B2HHPi0_M", gamma=Dec['pi0:Merged'],STREAM="Bhadron" ,addTag=True,tistosTree=True)
    RadTupler('2hpi0M','kpipi0M'    , 'B0'         , "[^K+^pi-]CC"   , "B2HHPi0_M", gamma=Dec['pi0:Merged'],STREAM="Bhadron",maker='b2kpipi0' , addTag=True,tistosTree=True)
    RadTupler('2hpi0M','kkpi0M'     , 'B_s0'       , "^K+^K-"        , "B2HHPi0_M", gamma=Dec['pi0:Merged'],STREAM="Bhadron",maker='bs2kkpi0' , addTag=True,tistosTree=True)
    RadTupler('2hpi0M','pkpi0M'     , 'Lambda_b0'  , "[^p+^K-]CC"    , "B2HHPi0_M", gamma=Dec['pi0:Merged'],STREAM="Bhadron",maker='lb2pkpi0' , addTag=True,tistosTree=True)

if category == "2hpi0R":
    # == Resolved pi0
    RadTupler('2hpi0R','pipipi0R'   , 'B0'      , "^pi+^pi-"      , "B2HHPi0_R", gamma=Dec['pi0'],STREAM="Bhadron" ,addTag=True,massfit=['pi0'],tistosTree=True)
    RadTupler('2hpi0R','kpipi0R'    , 'B0'      , "[^K+^pi-]CC"   , "B2HHPi0_R", gamma=Dec['pi0'],STREAM="Bhadron",maker='b2kpipi0' , addTag=True,massfit=['pi0'],tistosTree=True)
    RadTupler('2hpi0R','kkpi0R'     , 'B_s0'     , "^K+^K-"        , "B2HHPi0_R", gamma=Dec['pi0'],STREAM="Bhadron",maker='bs2kkpi0' , addTag=True,massfit=['pi0'],tistosTree=True)
    RadTupler('2hpi0R','pkpi0R'     , 'Lambda_b0', "[^p+^K-]CC"    , "B2HHPi0_R", gamma=Dec['pi0'],STREAM="Bhadron",maker='lb2pkpi0' , addTag=True,massfit=['pi0'],tistosTree=True)


# =================================== #
if printDoD :
    print locationsDoD()

