"""
Adapted for RadTuple production
01/2019

Basic DaVinci options for Data type Ntupling

"""
from Gaudi.Configuration import *
from Configurables import DaVinci
import os


# Import emvironment options
category = 	os.getenv("CATEGORY")
year = 		os.getenv("YEAR")
dataType = 	os.getenv("DATATYPE")
inputType =	os.getenv("INPUTTYPE")
stripping =  	os.getenv("STRIPPINGVERSION")
polarity = 	os.getenv("POLARITY")
restrip = 	os.getenv("RESTRIP", "0")

# Name of the output Tuple
restripping = False
if restrip=="1":
	restripping = True

nameTuple = "RadTuple_{5}_{0}_{1}_{2}_{3}_Restrip{4}".format(dataType,stripping,year,polarity,restripping,category)

##### DaVinci basic settings ##########
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "{0}_his.root".format(nameTuple)
DaVinci().TupleFile     = "{0}.root".format(nameTuple)
DaVinci().PrintFreq = 50000

#from Configurables import CondDB,CondDBAccessSvc
#CondDB(IgnoreHeartBeat=True)

#MessageSvc().OutputLevel = ERROR
MessageSvc().OutputLevel = INFO
#MessageSvc().OutputLevel = DEBUG
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

# -- reduce verbosity
import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()
