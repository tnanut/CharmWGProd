"""
Adapted for RadTuple production
01/2019

Append to the DaVinci algorithms only the category given
The Gaudisequencer has the name of the category and it is created in tupSequencer.py

"""

from Gaudi.Configuration import *
from Configurables import DaVinci
import os,sys


# Import environtment options 
cat=os.getenv('CATEGORY','unset')
if cat == 'unset' :
    print "'CATEGORY' must be set"
    sys.exit(-1)

# Should be possible to do 2 categories at the same time, has to be further checked.
_cat=cat.split(',')
for _c in _cat :
    if GaudiSequencer(_c).Members==[] : 
        print 'Empty sequence for category '+_c+' - stop here ...'
        sys.exit(-1)
    print 'Add sequence for category '+_c
    DaVinci().UserAlgorithms  += [GaudiSequencer(_c)]


import GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions.PrintOff()


