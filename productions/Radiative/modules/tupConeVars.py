from Configurables import DecayTreeTuple
from Configurables import TupleToolTISTOS
from Configurables import TupleToolDecay
from Configurables import LoKi__Hybrid__TupleTool
from LHCbKernel.Configuration import *
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import DaVinci, DecayTreeTuple, MCDecayTreeTuple
from Configurables import GaudiSequencer
from PhysSelPython.Wrappers import AutomaticData
import os


# Functions to add cone-stripping-isolation related variables to the tuple
# The branch of the head is always named B in this productions 

def addConeVars(tuple, branch = 'B', conevars_dir = ""):
	
	LoKi_conevars = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_conevars")
	
	daughters = ['B','Res', 'Gamma']
	angles = [1.0, 1.35, 1.7]
	cone_vars_list = ['CONEANGLE', 'CONEMULT', 'CONEP',
	                  'CONEPASYM', 'CONEPT', 'CONEPTASYM']
	paths_conevar = dict(( (dau, ang),
                      os.path.join(conevars_dir,
	                                   "ConeVarsInfo/{}/{}".format(dau, ang)) )
	                     for dau in daughters for ang in angles)
	cone_vars = dict(("ConeVars_{}_{}_{}".format(dau, ang, var),
        	          "RELINFO('{}', '{}', -100)".format(full_path, var))
        	         for (dau, ang), full_path in paths_conevar.items()
        	         for var in cone_vars_list)
	LoKi_conevars.Variables = cone_vars
	

def addConeIsoVars(tuple, branch = 'B', conevars_dir = ""):

	LoKi_coneiso = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_coneiso")

	daughters = ['B','Res', 'Gamma']
	isoangles = [0.4, 1.0, 1.35, 1.7]
	# Res can be charged
	cone_iso_list_charged = ['CC_ANGLE','CC_MULT','CC_SPT','CC_VPT','CC_PX','CC_PY',
                 'CC_PZ','CC_PASYM','CC_PTASYM','CC_PXASYM','CC_PYASYM',
                 'CC_PZASYM','CC_DELTAETA','CC_DELTAPHI','CC_IT',
                 'CC_MAXPT_PT','CC_MAXPT_PX', 'CC_MAXPT_PY', 'CC_MAXPT_PZ']

	cone_iso_list_neutral = ['NC_ANGLE','NC_MULT','NC_SPT','NC_VPT','NC_PX','NC_PY',
                 'NC_PZ','NC_PASYM','NC_PTASYM','NC_PXASYM','NC_PYASYM',
                 'NC_PZASYM','NC_DELTAETA','NC_DELTAPHI','NC_IT',
                 'NC_MAXPT_PT','NC_MAXPT_PX', 'NC_MAXPT_PY', 'NC_MAXPT_PZ']
	cone_iso_list = cone_iso_list_charged+cone_iso_list_neutral
	paths_iso = dict(( (dau, ang),
        	           os.path.join(conevars_dir,
        	                        "NeutralConeVarsInfo/{}/{}".format(dau, ang)) )
        		         for dau in daughters for ang in isoangles)
	cone_iso = dict(("ConeIso_{}_{}_{}".format(dau, ang, var),
        	         "RELINFO('{}','{}', -100)".format(full_path, var))
        	        for (dau, ang), full_path in paths_iso.items()
        	        for var in cone_iso_list)
	LoKi_coneiso.Variables = cone_iso


def addVtxRadInfo(tuple, branch = 'B', conevars_dir = ""):
	LoKi_VtxRadInfo = tuple.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_VtxRadInfo")
	LoKi_VtxRadInfo.Variables = {
	"NEWVTXISONUMVTX": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISONUMVTX', -100)".format(conevars_dir),
    	"NEWVTXISOTRKRELD0": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKRELD0', -100)".format(conevars_dir),
    	"NEWVTXISOTRKDCHI2": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISOTRKDCHI2', -100)".format(conevars_dir),
    	"NEWVTXISODCHI2MASS": "RELINFO('{}VertexIsoInfoRadiative', 'NEWVTXISODCHI2MASS', -100)".format(conevars_dir),
}






