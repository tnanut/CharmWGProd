"""
Obtain lines for restripping/stripping
"""

from Configurables import DecayTreeTuple
from Configurables import TupleToolStripping
from Configurables import TupleToolDecay

from StrippingConf.Configuration import *
from StrippingConf.StrippingStream import StrippingStream
from StrippingConf.StrippingLine import *


from StrippingArchive import strippingArchive
from StrippingArchive.Utils import buildStreams
from StrippingSettings.Utils import strippingConfiguration
from StrippingSettings.Utils import dbase
import GaudiKernel.ProcessJobOptions


def docLines(stripping) :

    lines=[]

    try:
    	path = os.path.join(os.environ['STRIPPINGDOCROOT'], 'python', 'StrippingDoc', stripping+'.py')
    except KeyError:
	print ' - DaVinci Version not compatible with StrippingDOC '
	print ' - TupleToolStripping will be configured with a reduced list of Stripping lines'
	print ""
	return lines
	
    ok=os.path.exists(path)
    print 'StrippingDoc : requested stripping',stripping

    if ok :
        from StrippingDoc import StrippingDoc
        strdoc = StrippingDoc(stripping)
        for linename, linedoc in strdoc.lines.items() :
            #print linename
            lines.append(linename)
    else :
        print 'WARNING : strippingDoc for '+stripping+' is unknown for this DV version'
	print 'List of Strippings allowed:'
	print '		stripping21,	stripping21r0p1,   stripping21r1,   stripping21r1p1'
	print '		stripping24,	stripping24r0p1,   stripping24r1,   stripping24r1p1'
	print '		stripping25,	stripping27,	   stripping28,	    stripping29'
        print ' - TupleToolStripping will be configured with a reduced list of Stripping lines'
    return lines


def tupStripping(tool, list,verbose=True,append=False) :
    if verbose and tool.StrippingList != [] and not append:
        print ' ... %-20s  : removed %i stripping lines  to %s' % ( 'TupleToolStripping' , len(tool.StrippingList), tool.getName() )
    k=0
    if append :
        for l in list :
            if l not in tool.StrippingList : 
                tool.StrippingList.append(l)
                k=k+1
    else :
        tool.StrippingList=list
        k=len(list)
    if k != 0 :
        if verbose :print ' ... %-20s  : added %i/%i stripping lines  to %s' % ( 'TupleToolStripping' , k ,len(tool.StrippingList), tool.getName() )
    


def addStrippingList(tuple, list,verbose=True,append=False) :        
    tuple.addTool(TupleToolStripping,name='TupleToolStripping')
    tuple.ToolList+=['TupleToolStripping']
    tupStripping(tuple.TupleToolStripping,list,verbose,append)

def lines(stripping,restrip,tags,sim,verbose=False) :
    list=[]
    strLines=[]
    if len(strLines) == 0 :

	print "============================="
	print "building "+stripping+" lines "
	print "============================="


	ok = os.path.exists(dbase(stripping))

	if ok and sim and False:    
	    config  = strippingConfiguration(stripping)
	    archive = strippingArchive(stripping)

	    banish={}
	    if stripping.find('21') != -1 :
	        # -- banish some lines for forward compatibility (N4BodyDecays bug for instance)
	        banish={'DisplVerticesLines','DisplVertices'}
	        if restrip=='1' :
	            print "cannot reprocess stripping line Lb2L0Gamma and Bd2eeKstarBDT for Str21 due to obsolete Related Info syntax"
	            banish={'DisplVerticesLines','DisplVertices','Bd2eeKstarBDT','Lb2L0Gamma','BetaSBs2PhiPhi','BetaSBs2JpsieePhi'}

	    dconfig  = dict(config)
	    myconfig ={}
	    for d in config :
	        if not d in banish :
	            myconfig[d]=dconfig[d]
	            #	print d," line is added"
	        else :
	            print d," line is banished for forward compatibility"
	    streams = buildStreams(stripping=myconfig,archive=archive) 
        
        if  len(strippingLines()) != 0 :
            for line in  strippingLines() :
                strLines.append(line.name())
        else :
            # trying  StrippingDoc
            strLines=docLines(stripping)


    for line in  strLines :
        for tag in tags :
            if line.find(tag) != -1 :
                if verbose : print '   . stripping list : appending '+line
                item = line+"Decision"
                list.append(item)
    return list

def caloStrippingTags() :
    tags = ['LowMult','D02KPiPi0']
    return tags

def q2bStrippingTags() :
    tags = ['Q2B','BetaSBs','Buto5h']
#    tags = ['Q2B','BetaSBs']
    return tags

def radStrippingTags() :
    # Radiative stripping tags [v20(r1)(p1,2,3) & v21(r1)]
    tags = ['B2VG','B2VPG','B2VVG',"Lb2PPiGamma","Lb2PKGamma","eeKst","eePhi","Lb2LGamma","JpsiKGamma","Beauty2XGamma","B2XGamma","Lb2L0Gamma","B2HHPi0","B2HHpi0"]
    return tags

def radFakeStrippingTags() :
    tags=['StrippingFullDSTDiMuon']
    return tags

# define predefined lists here :
def caloStrippingList(stripping,restrip,verbose=False) :
    list=lines(stripping,restrip,caloStrippingTags(),verbose)
    return list

def q2bStrippingList(stripping,restrip,verbose=False) :
    list=lines(stripping,restrip,q2bStrippingTags(),verbose)
    return list

def radStrippingList(stripping,restrip,sim,verbose=False) :
    list=lines(stripping,restrip,radStrippingTags(),sim,verbose)
    list.append('StrippingStreamRadiativeDecision')
    list.append('StrippingStreamLeptonicDecision')
    list.append('StrippingStreamBhadronDecision')
    return list

def radFakeStrippingList(stripping,restrip,verbose=False) :
    list= lines(stripping,restrip,radFakeStrippingTags(),verbose)
    return list

def strippingAlgs(name,stripping,restrip,tags,verbose=False,filter=False) :

    strLines=[]
    strLines2=[]
    listing = []

    if  len(strippingLines()) == 0 :

	print "============================="
	print "building "+stripping+" lines "
	print "============================="


	ok = os.path.exists(dbase(stripping))

	if ok:    
	    config  = strippingConfiguration(stripping)
	    archive = strippingArchive(stripping)

	    banish={}
	    if stripping.find('21') != -1 :
	        # -- banish some lines for forward compatibility (N4BodyDecays bug for instance)
	        banish={'DisplVerticesLines','DisplVertices'}
	        if restrip=='1' :
	            print "cannot reprocess stripping line Lb2L0Gamma and Bd2eeKstarBDT for Str21 due to obsolete Related Info syntax"
	            banish={'DisplVerticesLines','DisplVertices','Bd2eeKstarBDT','Lb2L0Gamma','BetaSBs2PhiPhi','BetaSBs2JpsieePhi'}

	    dconfig  = dict(config)
	    myconfig ={}
	    for d in config :
	        if not d in banish :
	            myconfig[d]=dconfig[d]
	            #	print d," line is added"
	        else :
	            print d," line is banished for forward compatibility"
	    streams = buildStreams(stripping=myconfig,archive=archive) 

    for line in strippingLines() :
        strLines.append(line)
	strLines2.append(line.name())
    print len(strLines), "stripping lines have been registered" 

    for line in  strLines2 :
        for tag in tags :
            if line.find(tag) != -1 :
                if verbose : print '   . stripping list : appending '+line
                item = line+"Decision"
                listing.append(item)
    
    listing.append('StrippingStreamRadiativeDecision')
    listing.append('StrippingStreamLeptonicDecision')
    listing.append('StrippingStreamBhadronDecision')

    #== kill strip report
    from Configurables import EventNodeKiller
    killer = EventNodeKiller('Stripkiller')
    killer.Nodes = [ '/Event/AllStreams', '/Event/Strip' ]

    # == build stream 
    print 'Building StrippingStream('+name+')'
    stream = StrippingStream(name)
    trash    = StrippingStream('TrashStream')
    n=0
    for line in  strLines :
        push=False
        for tag in tags :
            if line.name().find(tag) != -1 :
                push=True
        if push :
            stream.appendLines(  [line]  ) 
            if verbose : print '--->  adding '+line.name() 
            n=n+1
        else :
            trash.appendLines( [line] )  # to avoid warnings
    print str(n)+' lines have been added to the stream '+name
    from Configurables import ProcStatusCheck  
    filterBadEvents = ProcStatusCheck()  
    strip = StrippingConf(Streams = [ stream ],
                       MaxCandidates = -1,
                       AcceptBadEvents = False,
                       BadEventSelection = filterBadEvents,
                       TESPrefix='Strip'
                       )
    if not filter : strip.sequence().IgnoreFilterPassed=True  

    from Configurables import StrippingReport
    report = StrippingReport(Selections = strip.selections())

    from Configurables import AlgorithmCorrelationsAlg
    correl = AlgorithmCorrelationsAlg(Algorithms = strip.selections())

    return killer,strip,report,correl,listing
