"""
All possible radiative decays inside Dec diccionary
"""

global Dec
Dec={}
# == basic ==
Dec['pi0']           = ' ^( pi0 -> ^gamma ^gamma)'
Dec['pi0:Merged']          = ' ^pi0'
Dec['KS0']           = ' ^( KS0 -> ^pi+ ^pi- )'  
Dec['KS~0']          = ' ^( KS0 -> ^pi- ^pi+ )'  
Dec['Conversion']    = '^( gamma -> ^e+ ^e-)'

# == 2-body ==
# unflavored
Dec['rho(770)0']      = '^(rho(770)0 -> ^pi+ ^pi- )'
Dec['rho(770)0:Kpi']  = '^(rho(770)0 -> ^K+ ^pi- )'
Dec['rho(770)0:piK']  = '^(rho(770)0 -> ^K- ^pi+ )'
Dec['rho(770)+:wrong']= '[^(rho(770)+ -> '+Dec['KS0']+' ^pi+ )]CC'
Dec['phi(1020)']      = ' ^(phi(1020) -> ^K+  ^K-  )'
Dec['J/psi(1S):mm']   = '^(J/psi(1S) -> ^mu+ ^mu-)'
Dec['J/psi(1S):ee']   = '^(J/psi(1S) -> ^e+ ^e-)'
Dec['phi(1020):WS']   = '[^(phi(1020) -> ^K+  ^K+  )]CC' 
Dec['K*(892)0:WS']    = '[^(K*(892)0   -> ^K+  ^pi+ )]CC' 
Dec['a_2(1320)+']     = '[^(a_2(1320)+ -> ^K+ '+Dec['KS0']+' )]CC'
# strange
Dec['K*(892)+']      = '[^(K*(892)+  -> '+Dec['KS0']+' ^pi+ )]CC'  
Dec['K*(892)0']      = '[^(K*(892)0  -> ^K+  ^pi- )]CC'  
Dec['K*(892)~0']     = '[^(K*(892)~0 -> ^K-  ^pi+ )]CC'  
# baryons
Dec['Lambda0']       = '[^(Lambda0 -> ^p+  ^pi-  )]CC'  
Dec['Lambda(1520)0'] = '[^(Lambda(1520)0 -> ^p+  ^K-  )]CC'  
Dec['N(1440)0']      = '[^(N(1440)0-> ^p+  ^pi-  )]CC'

# == 3-body
# unflavored
Dec['omega(782):rhopi']        = ' ^(omega(782) -> ' +Dec['rho(770)0']+' '+Dec['pi0']+' )'
Dec['omega(782):rhopi:Merged'] = ' ^(omega(782) -> ' +Dec['rho(770)0']+' '+Dec['pi0:Merged']+' )'
Dec['omega(782)']              = ' ^(omega(782) -> ^pi+ ^pi-  '+Dec['pi0']+' )'
Dec['omega(782):Merged']       = ' ^(omega(782) -> ^pi+ ^pi-  '+Dec['pi0:Merged']+' )'
Dec['3:a_1(1260)0']    = ' ^(a_1(1260)0 -> ' +Dec['rho(770)0']+' '+Dec['pi0']+' )'
Dec['3M:a_1(1260)0']    = ' ^(a_1(1260)0 -> ' +Dec['rho(770)0']+' '+Dec['pi0:Merged']+' )'
Dec['3:a_1(1260)+']    = ' ^(a_1(1260)+ -> ^pi+ ^pi- ^pi+ )'
Dec['3:rho_3(1690)+']  = ' ^(rho_3(1690)+ -> ^K+ ^K- ^pi+ )'
# strange
Dec['3:K_1(1270)+']    = '[^(K_1(1270)+ -> ^K+  ^pi- ^pi+ )]CC'
Dec['3:K_2(1770)+']    = '[^(K_2(1770)+ -> ^K+ ^K- ^K+ )]CC'

Dec['3:K*_2(1430)0']   = '^(K*_2(1430)0  -> ' +Dec['rho(770)0']    +' '+Dec['KS0']+')'    # noCC !!
Dec['3Kpi:K*_2(1430)0']= '^(K*_2(1430)0  -> ' +Dec['rho(770)0:Kpi']+' '+Dec['KS0']+')'  # noCC!!
Dec['3piK:K*_2(1430)~0']= '^(K*_2(1430)~0  -> ' +Dec['rho(770)0:piK']+' '+Dec['KS~0']+')'  # noCC!!
Dec['3:K_2(1770)0']    = '^(K_2(1770)0   -> ' +Dec['phi(1020)']    +' '+Dec['KS0']+')'    # noCC !!
Dec['3:K_2(1820)0']    = '[^(K_2(1820)0  -> ' +Dec['K*(892)0']     +' '+Dec['KS0']+')]CC' # WARNING CC !!
Dec['3:Xi(1820)0']     = '[^(Xi(1820)0   ->'  +Dec['Lambda(1520)0']+' '+Dec['KS0']+')]CC'

Dec['3:K_1(1270)0']    = '[^(K_1(1270)0  -> ' +Dec['K*(892)0' ]+' '+Dec['pi0']+' )]CC' 
Dec['3M:K_1(1270)0']   = '[^(K_1(1270)0  -> ' +Dec['K*(892)0' ]+' '+Dec['pi0:Merged']+' )]CC' 
Dec['3:phi(1680)']     = '^(phi(1680)  -> ' +Dec['phi(1020)' ]+' '+Dec['pi0']+' )' 
Dec['3M:phi(1680)']    = '^(phi(1680)  -> ' +Dec['phi(1020)' ]+' '+Dec['pi0:Merged']+' )' 
Dec['3:Sigma(1660)0']  = '[^(Sigma(1660)0  -> ' +Dec['Lambda(1520)0' ]+' '+Dec['pi0']+' )]CC' 
Dec['3M:Sigma(1660)0'] = '[^(Sigma(1660)0  -> ' +Dec['Lambda(1520)0' ]+' '+Dec['pi0:Merged']+' )]CC' 


Dec['33:K*_2(1430)0']   = '^(K*_2(1430)0 -> ^pi+ ^pi- '+Dec['KS0']+')'    # no CC 
Dec['33:K_2(1770)0']    = '^(K_2(1770)0 -> ^K+  ^K-   '+Dec['KS0']+')'    # no CC 
Dec['33:K_2(1820)0']    = '[^(K_2(1820)0 -> ^K+  ^pi- '+Dec['KS0']+')]CC'  # 
Dec['33:Xi(1820)0']     = '[^(Xi(1820)0  -> ^p+ ^K-   '+Dec['KS0']+')]CC'


# (hhpi0)
Dec['33:K_1(1270)0']    = '[^(K_1(1270)0   -> ^K+  ^pi-'+Dec['pi0']+' )]CC' # (N'importe wak)**2
Dec['33M:K_1(1270)0']   = '[^(K_1(1270)0   -> ^K+  ^pi-'+Dec['pi0:Merged']+' )]CC' # (N'importe wak)**2
Dec['33:phi(1680)']     = '^(phi(1680)     -> ^K+  ^K- '+Dec['pi0']+' )'
Dec['33M:phi(1680)']    = '^(phi(1680)     -> ^K+  ^K- '+Dec['pi0:Merged']+' )' 
Dec['33:Sigma(1660)0']  = '[^(Sigma(1660)0 -> ^p+  ^K- '+Dec['pi0']+' )]CC'
Dec['33M:Sigma(1660)0'] = '[^(Sigma(1660)0 -> ^p+  ^K- '+Dec['pi0:Merged']+' )]CC' 
Dec['33:a_1(1260)0']    = ' ^(a_1(1260)0 -> ^pi+ ^pi- '+Dec['pi0']+' )'
Dec['33M:a_1(1260)0']    = ' ^(a_1(1260)0 -> ^pi+ ^pi- '+Dec['pi0:Merged']+' )'


# baryons
Dec['3:Xi(1820)-']     = '[^(Xi(1820)-  -> ^p+ ^K-   ^K- )]CC'
Dec['3:Xu+']           = '[^(Xu+ -> ^p~- ^K+ ^p+)]CC'

# == 4-body ==
# unflavored
Dec['4:f_2(1270)']     = '^(f_2(1270) -> ^pi+ ^pi- ^pi+ ^pi-)'
Dec['4:f_2(2010)']     = '^(f_2(2010) -> ^K+ ^K- ^K+ ^K-)'
Dec['4:f_2(1950)']     = '^(f_2(1950) -> ^K+ ^pi- ^pi+ ^K-)'
# strange
Dec['4:K*_2(1430)0']   = '^[(K*_2(1430)0  -> ^K+ ^pi- ^pi+ ^pi-)]CC'
Dec['4:K*_2(1430)~0']  = '^[(K*_2(1430)~0 -> ^pi+ ^K- ^pi+ ^pi-)]CC'
Dec['4:K*_4(2045)0']   = '^[(K*_4(2045)0  -> ^K+ ^pi- ^K+ ^K-)]CC'
Dec['4:K*_4(2045)~0']  = '^[(K*_4(2045)~0 -> ^pi+ ^K- ^K+ ^K-)]CC'

def decDesc(tag) :
    try:
        Dec[tag]
    except KeyError:
        print "unknown decay descriptor tag "+tag
    return Dec[tag]

#== create multi-body decay descriptors
def doDec(d1,d2,CC='auto') :
    if d1.find('^') != -1 :
        dd1=d1
    else :
        dd1=Dec[d1]
    if d2.find('^') != -1 :
        dd2=d2
    else :
        dd2=Dec[d2]
    cc=0
    if dd1.find(']CC') != -1 :
        dd1=dd1.replace(']CC','')
        dd1=dd1.replace('[','')
        cc=1
    if dd2.find(']CC') != -1 :
        dd2=dd2.replace(']CC','')
        dd2=dd2.replace('[','')
        cc=1
    if CC == 'noCC' :
        cc=0      # force NO []CC  
    if CC == 'CC' :
        cc=1      # force []CC         
    d = dd1+' '+dd2
    if cc == 1 :
        d = '['+d+']CC'
    return d

