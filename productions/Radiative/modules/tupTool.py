"""
Function to add tupletools the the DecayTreeTuple
"""
from Configurables import DecayTreeTuple
from Configurables import TupleToolDecay
import sys

def  add(tuple,name,branch='', verbose=True , Code='' ) :

    if name.find('/') != -1 :
        typ,instance = name.split('/')
    else :
        typ = name
        instance=name

    if typ == '' :
        print 'TupleTool type is missing'
        sys.exit(-1)        

    try: 
        exec('from Configurables import '+typ)
    except ImportError :
        print 'Fail to import TupleTool '+typ
        sys.exit(-1)

    if branch != '' :    
        tuple.addTool(TupleToolDecay,name=branch)
        exec('b = tuple.'+branch)
        b.ToolList+=[name]
        exec('b.addTool('+typ+",name='"+instance+"')")
        exec('tool=tuple.'+branch+'.'+instance)            
    else :
        exec('tuple.addTool('+typ+",name='"+instance+"')")
        tuple.ToolList+=[name]
        exec('tool=tuple.'+instance)

    _code=Code
    if str == type(_code) : 
        _code=[Code]
    for c in _code :
        if c != '' :
            try:
                exec('tool.'+c)
            except AttributeError :
                print 'WARNING : failed to configure '+tool.getName()+' with '+c
            if verbose : print '   . Configure '+tool.getName()+' with '+c

    if verbose : print ' ... %-20s  : added as %s' % ( typ , tool.getName() )
    return tool



