"""
Substitutions funcions for tupleTools
"""

from Configurables import DecayTreeTuple
from Configurables import TupleToolDecay
import sys
import tupTool

def  addSubstitution(tuple,name,branch='',verbose=True , Substitution=['all'] , DoubleSubstitution=['all'],Code='') :
    tool=tupTool.add(tuple,name,branch,verbose ,Code)
    if Substitution[0] == 'all' :
        tool.Substitution       += [ 'pi+ => K+'    , 'pi+ => p+'   ,
                                     'K+  => pi+'   , 'K+  => p+'   ,
                                     'p+  => pi+'   , 'p+  => K+'   ,
                                     'gamma => pi0' , 'pi0 =>gamma' ]
        if verbose : print '   . Configured with all predefined Substitutions'
    elif Substitution != [] :
        tool.Substitution = Substitution
        if verbose : print '   . Configured with Substitutions ['+','.join(Substitution)+']'
    else :
        if verbose : print '   . No substitution defined'
           
    if DoubleSubstitution[0] == 'all' :
        tool.DoubleSubstitution += [ 'pi+/pi- => K+/K-  ' , 'pi+/pi- => p+/p~- ' ,
                                     'pi+/pi- => p+/K-  ' , 'pi+/pi- => K+/p~- ' ,
                                     'K+/K-   => pi+/pi-' , 'K+/K-   => p+/p~- ' ,
                                     'K+/K-   => p+/pi- ' , 'K+/K-   => pi+/p~-' ,
                                     'K+/pi-  => pi+/K- ' , 'K+/pi-  => p+/K-  ' , 'K+/pi- => pi+/p~-' , 'K+/pi- => p+/p~-' ,
                                     'p+/pi-  => pi+/p~-' , 'p+/pi-  => K+/p~- ' , 'p+/pi- => pi+/K- ' , 'p+/pi- => K+/K- ' ,
                                     'p+/K-   => K+/p~- ' , 'p+/K-   => pi+/p~-' , 'p+/K- => K+/pi-  ' , 'p+/K- => pi+/pi-' , 
                                     'pi+/gamma => K+/pi0', 'K+/gamma => pi+/pi0','p+/gamma => pi+/pi0','p+/gamma => K+/pi0',
                                     'pi+/pi0 => K+/gamma', 'K+/pi0 => pi+/gamma','p+/pi0 => pi+/gamma','p+/pi0 => K+/gamma']
        if verbose : print '   . Configured with all predefined DoubleSubstitutions'
    elif DoubleSubstitution != [] :
        tool.DoubleSubstitution=DoubleSubstitution
        if verbose : print '   . Configured with DoubleSubstitutions ['+','.join(DoubleSubstitution)+']'
    else :
        if verbose : print '   . No Doublesubstitution defined'

    return tool
