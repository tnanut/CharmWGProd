"""
Creating of a basic Tuple 
"""
from Configurables import DecayTreeTuple
import tupTool,tupSubMass,tupDTF
import os

def defTuple(name='tuple', branch='' , Simulation=False, Propertime=True, DTF=True, PrivateTools=True,verbose=True) :
    # ------------------------------------ #
    # ----- Prepare generic tupling ------ # 
    # ------------------------------------ #

    tuple = DecayTreeTuple(name)

    print ' '
    print ' ==  Preparing a default DecayTreeTuple('+name+') =='
    v=verbose
    # Event data
    tupTool.add(tuple,"TupleToolEventInfo"        , verbose=v )
    tupTool.add(tuple,"TupleToolPrimaries"        , verbose=v )  
    tupTool.add(tuple,"TupleToolRecoStats"        , verbose=v , Code='Verbose=True' )
    tupTool.add(tuple,"TupleToolCPU"              , verbose=v )
    # Decay data
    tupTool.add(tuple,"TupleToolGeometry"         , verbose=v , Code="Verbose=True" )  
    tupTool.add(tuple,"TupleToolKinematic"        , verbose=v )
    tupTool.add(tuple,"TupleToolPid"              , verbose=v )         
    tupTool.add(tuple,"TupleToolTrackInfo"        , verbose=v , Code='Verbose=True' )                  
    tupTool.add(tuple,"TupleToolAngles"           , verbose=v )
#    if PrivateTools : tupTool.add(tuple,"TupleToolHelicity" , verbose=v )   # Warning this is a private tool
    #  dedicated tupSubMass :
    if "2hksG_ExclTDCPV" not in os.getenv("CATEGORY"):
       tupSubMass.addSubstitution(tuple,"TupleToolSubMass", verbose =v , Code='SubVertexFit=True' ) # TupleToolSubMass with default (double)subtitutions
    else:
       tupSubMass.addSubstitution(tuple,"TupleToolSubMass", branch='B', verbose =v , Code='SubVertexFit=True' ) # TupleToolSubMass with default (double)subtitutions
       
    if branch != '' :
        if DTF or Propertime or PrivateTools :
            print " ==  Adding a branch '"+branch+"' =="
        if PrivateTools : tupTool.add(tuple,"TupleToolDira"  , branch, verbose=v ) # private tool (from Fatima)
        if Propertime :
            from Configurables import PropertimeFitter
            tupTool.add(tuple,"TupleToolPropertime/DefaultPropertime"             , branch, verbose=v ) # default LifetimeFitter
            tupPT=tupTool.add(tuple,"TupleToolPropertime/NoMassConstPropertime"   , branch, verbose=v , Code="ExtraName='noBMassConst'" )
            #tupPT.ToolName='PropertimeFitter' # use (obsolete) PropertimeFitter WITHOUT B mass Constraint
            tupTime=tupTool.add(tuple,"TupleToolPropertime/MassConstPropertime"   , branch, verbose=v , Code="ExtraName='BMassConst'" )
            #tupTime.ToolName='PropertimeFitter' # use (obsolete) PropertimeFitter WITH B mass Constraint
            tupTime.addTool(PropertimeFitter,name='PropertimeFitter')
            tupTime.PropertimeFitter.applyBMassConstraint=True

        if DTF :
            tupDTF.addInstanceDTF(tuple,'Fit'          , branch , verbose=v ) # TupleToolDTF instances on the 'B' branch
            tupDTF.addInstanceDTF(tuple,'PVFit'        , branch , verbose=v , ConstVtx=True )

    if Simulation :
        print " ==  Adding simulation tools  == "
        tupTool.add(tuple, 'TupleToolMCTruth'          , verbose=v )
        tupTool.add(tuple, 'MCTupleToolEventType'      , verbose=v )
        tupTool.add(tuple, 'MCTupleToolPrimaries'      , verbose=v )
        tupTool.add(tuple, 'TupleToolMCBackgroundInfo' , verbose=v ).IBackgroundCategoryTypes=['BackgroundCategory']
    print ' ==  Default DecayTreeTuple completed  =='
    print ''
    return tuple
