"""
Trigger lines to be added to the Ntuple
"""

from Configurables import DecayTreeTuple
from Configurables import TupleToolTISTOS
from Configurables import TupleToolDecay

def tupTisTos(tistos, list,verbose=True) :
    if verbose and tistos.TriggerList != [] :
        print ' ... %-20s  : removed %i trigger lines  to %s' % ( 'TupleToolTISTOS' , len(tistos.TriggerList), tistos.getName() )
    tistos.TriggerList = list
    tistos.VerboseHlt1 = True
    tistos.VerboseHlt2 = True
    tistos.VerboseL0   = True
    if verbose : print ' ... %-20s  : added %i trigger lines  to %s' % ( 'TupleToolTISTOS' , len(tistos.TriggerList), tistos.getName() )

def addTriggerList( tuple , list , branch = '', verbose=True) :

    down=False
    if branch.find('->') != -1 :
        branch=branch.replace('->','') # add on both branch and ground (useful to overwrite branch setting)
        down=True
        
    if branch != '' :    
        tuple.addTool(TupleToolDecay,name=branch)
        exec('b = tuple.'+branch)
        b.ToolList+=['TupleToolTISTOS']
        b.addTool(TupleToolTISTOS,name='TupleToolTISTOS')
        tupTisTos( b.TupleToolTISTOS, list, verbose)

    if branch == '' or down :
        tuple.addTool(TupleToolTISTOS,name='TupleToolTISTOS')
        tuple.ToolList+=['TupleToolTISTOS']
        tupTisTos( tuple.TupleToolTISTOS, list,verbose)


def getTriggerList( tuple, branch = '' ) :
    tup=DecayTreeTuple(tuple.getName())
    if branch != '' :    
        tuple.addTool(TupleToolDecay,name=branch)
        exec('b = tuple.'+branch)
        b.addTool(TupleToolTISTOS,name='TupleToolTISTOS')
        tistos=b.TupleToolTISTOS
    else :
        tistos=tup.addTool(TupleToolTISTOS,name='TupleToolTISTOS')
    return tistos.TriggerList

# == (incomplete) list of relevant triggers :
def triggers( tag ) :
    trig={}

    trig['L0:Calo'      ]=['L0CALO','L0Photon','L0Electron','L0Hadron']
    trig['L0:Calo:Hi'   ]=['L0PhotonHi','L0ElectronHi']
    trig['L0:Calo:NoSPD']=['L0PhotonNoSPD','L0ElectronNoSPD','L0HadronNoSPD','L0MuonNoSPD']
    trig['L0:gamma'     ]=['L0Photon'  ,'L0Electron']
    trig['L0:Hadron'    ]=['L0Hadron']
    trig['L0:Muon'      ]=['L0Muon','L0DiMuon']
    trig['HLT1:def'     ]=['Hlt1TrackAllL0','Hlt1TrackPhoton','Hlt1TrackMuon']
    trig['HLT1:dimu'    ]=['Hlt1DiMuonHighMass','Hlt1DiMuonLowMass','Hlt1SingleMuonNoIP','Hlt1SingleMuonHighPT']
    trig['HLT1:e'       ]=['Hlt1SingleElectronNoIP']
    trig['HLT1:pass'    ]=['Hlt1TrackForwardPassThrough','Hlt1TrackForwardPassThroughLoose','Hlt1NoPVPassThrough']
    trig['HLT2:Rad:Excl']=['Hlt2Bs2PhiGamma','Hlt2Bd2KstGamma']
    trig['HLT2:Rad:Wide']=['Hlt2Bs2PhiGammaWideBMass','Hlt2Bd2KstGammaWideKMass','Hlt2Bd2KstGammaWideBMass']
    trig['HLT2:Rad:BDT' ]=['Hlt2TopoRad2BodyBBDT','Hlt2TopoRad2plus1BodyBBDT'] # inclusive BBDT lines (July 2011->beg. 2012)
    trig['HLT2:Rad:TOS' ]=['Hlt2RadiativeTopoTrackTOS','Hlt2RadiativeTopoPhotonL0'] # Sept 2011 -> end 2011
    trig['HLT2:Rad:Incl']=['Hlt2RadiativeTopoTrack'   ,'Hlt2RadiativeTopoPhoton'] # 2012
    trig['HLT2:Phi'     ]=['Hlt2IncPhi','Hlt2IncPhiSidebands']
    trig['HLT2:e'       ]=['Hlt2SingleTFElectron','Hlt2SingleElectronTFLowPt','Hlt2SingleElectronTFHighPt','Hlt2SingleTFVHighPtElectron']
    trig['HLT2:die'     ]=['Hlt2DiElectronHighMass','Hlt2DiElectronB']
    trig['HLT2:Topo'    ]=['Hlt2Topo2BodySimple','Hlt2Topo3BodySimple','Hlt2Topo4BodySimple']
    trig['HLT2:Topo:BDT']=['Hlt2Topo2BodyBBDT'  ,'Hlt2Topo3BodyBBDT'  ,'Hlt2Topo4BodyBBDT']
    trig['HLT2:TopoMu'  ]=['Hlt2TopoMu2BodyBBDT','Hlt2TopoMu3BodyBBDT','Hlt2TopoMu4BodyBBDT']
    trig['HLT2:TopoE'   ]=['Hlt2TopoE2BodyBBDT' ,'Hlt2TopoE3BodyBBDT' ,'Hlt2TopoE4BodyBBDT']
    trig['HLT2:LowMult' ]=['Hlt2LowMultMuon'      ,'Hlt2LowMultHadron' ,'Hlt2LowMultHadron_nofilter'
                           ,'Hlt2LowMultPhoton'    ,'Hlt2LowMultElectron','Hlt2LowMultElectron_nofilter']
    trig['HLT2:DisplVtx']=['Hlt2DisplVerticesHighMassSingle'   ,'Hlt2DisplVerticesDouble'
                           ,'Hlt2DisplVerticesHighFDSingle'     ,'Hlt2DisplVerticesSingle'
                           ,'Hlt2DisplVerticesSinglePostScaled' ,'Hlt2DisplVerticesDoublePostScaled'
                           ,'Hlt2DisplVerticesSingleHighMassPostScaled','Hlt2DisplVerticesSingleHighFDPostScaled'
                       ,'Hlt2DisplVerticesSingleMVPostScaled'      ,'Hlt2DisplVerticesSingleDown']
    trig['HLT2:HHPi0']   =['Hlt2B2HHPi0_Merged','Hlt2B2HHPi0_Resolved']
    trig['HLT2:mu'   ]   =['Hlt2MuonFromHLT1','Hlt2SingleMuon','Hlt2SingleMuonHighPT'
                           ,'Hlt2SingleMuonVHighPT','Hlt2SingleMuonLowPT','Hlt2diPhotonDiMuon']
    trig['HLT2:dimu' ]   =['Hlt2DiMuon'              ,'Hlt2DiMuonLowMass'
                           ,'Hlt2DiMuonJPsi'          ,'Hlt2DiMuonJPsiHighPT'
                           ,'Hlt2DiMuonB'             ,'Hlt2DiMuonDetached'
                           ,'Hlt2DiMuonDetachedHeavy' ,'Hlt2DiMuonDetachedJPsi'
                           ,'Hlt2DoubleDiMuon'        ,'Hlt2DiMuonAndGamma'
                           ]
    # ==  run2 - relevant trigger lines
    trig['HLT1:Run2:def']=['Hlt1L0Any','Hlt1TrackMVA','Hlt1TwoTrackMVA','Hlt1B2PhiGamma_LTUNB','Hlt1B2GammaGamma','Hlt1TrackMuon']
    trig['HLT2:Run2:Rad:Excl']=['Hlt2RadiativeBs2PhiGamma','Hlt2RadiativeBs2PhiGammaUnbiased','Hlt2RadiativeBd2KstGamma'
                               ,'Hlt2RadiativeBd2KstGammaULUnbiased','Hlt2RadiativeLb2L0GammaLL','Hlt2RadiativeB2GammaGamma'
                               ,'Hlt2RadiativeB2GammaGammaLL','Hlt2RadiativeB2GammaGammaDD'
                               ,'Hlt2RadiativeB2GammaGammaDouble'] 
    trig['HLT2:Run2:Rad:Incl']=['Hlt2RadiativeIncHHGamma','Hlt2RadiativeIncHHHGamma'] 
    trig['HLT2:Run2:Rad:Conv']=['Hlt2RadiativeIncHHGammaEE','Hlt2RadiativeIncHHHGammaEE','Hlt2RadiativeLb2L0GammaEELL'] 
    trig['HLT2:Run2:e'       ]=['Hlt2EWSingleElectronLowPt','Hlt2EWSingleElectronHighPt','Hlt2EWSingleElectronVHighPt'] 
    trig['HLT2:Run2:die'     ]=['Hlt2EWDiElectronHighMass','Hlt2EWDiElectronDY'] 
    trig['HLT2:Run2:Topo'    ]=['Hlt2Topo2Body','Hlt2Topo3Body','Hlt2Topo4Body']


    # == run2 (2016 add)
    trig['HLT1:Run2:2016:MVA']=['Hlt1TrackMVALoose','Hlt1TwoTrackMVALoose']
    trig['HLT2:Run2:2016:L0HG']=['Hlt2RadiativeHypb2L0HGammaOm','Hlt2RadiativeHypb2L0HGammaOmEE',
                                 'Hlt2RadiativeHypb2L0HGammaXi','Hlt2RadiativeHypb2L0HGammaXiEE']
    trig['HlT2:Run2:2016:Topo']=['Hlt2TopoE2Body','Hlt2TopoE3Body','Hlt2TopoE4Body',
                                 'Hlt2TopoEE2Body','Hlt2TopoEE3Body','Hlt2TopoEE4Body']

    
    try:
        trig[tag]
    except KeyError:
        return [tag] # assume the tag is a trigger line
    return trig[tag]

def triggerList(tags,verbose=False) :
    list = []
    if tags == [] :
        print 'WARNING : triggerList : empty tags will return empty list !'
    for tag in tags :
        tagList = triggers(tag)
        if verbose :
            print "   . trigger list : appending  ['"+tag+"'] = ["+','.join(tagList)+']'

        for line in tagList:
            list.append(line+'Decision')        
    return list

# === Specific trigger list defined here :
def caloTriggerList(verbose=False) :    
    print 'Predefined trigger list for LowMult calo tuples : '
    tags= ['L0:Calo','L0:Muon']
    tags+=['HLT1:def']         # HLT1 OneTrack (run1)
    tags+=['HLT1:Run2:def']    # HLT1 : default lines (run2)
    tags+=['HLT1:Run2:2016:MVA'] # (run2)
    tags+=['HLT2:LowMult']
    return triggerList(tags,verbose)

def q2bTriggerList(verbose=False) :    
    print 'Predefined trigger list for charmless Q2B tuples : '
    tags= ['L0:Calo','L0:Muon']
    tags+=['HLT1:def']         # HLT1 OneTrack (run1)
    tags+=['HLT1:Run2:def']    # HLT1 : default lines (run2)
    tags+=['HLT2:Topo','HLT2:Topo:BDT','HLT2:Phi','HLT2:LowMult' ]    # HLT2  topo  (run1)
    tags+=['HLT2:Run2:Topo'            ]    # HLT2 : topo (run2)
    tags+=['HLT1:Run2:2016:MVA','HlT2:Run2:2016:Topo'] # (run2)
    return triggerList(tags,verbose)

def radTriggerList(stripping,verbose=False) :    
    print 'Predefined trigger list for radiative tuples : '

    # -- common lines
    tags=['L0:Calo'       # L0e/g/h
          ,'HLT1:e'       # HLT1 single e
          ,'HLT2:Phi'     # HLT1 phi line          
          ,'HLT2:HHPi0'   # HLT2 : hhpi0
          ] 

    # -- run1 only !!
    if stripping.find('21') != -1 :
        tags+=['L0:Calo:Hi'                      # L0   : Calo (highEt)
               ,'HLT1:def'                       # HLT1 : 1Track/1TrackPhoton
               ,'HLT2:Rad:Excl','HLT2:Rad:Wide'  # HLT2 : exclusive radiative
               ,'HLT2:Rad:BDT','HLT2:Rad:TOS','HLT2:Rad:Incl' # HLT2 : inclusive radiative 
               ,'HLT2:e','HLT2:die'              # HLT2 : inclusive phi/e/die
               ,'HLT2:Topo','HLT2:Topo:BDT'      # HLT2 : topo
               ]
    else :  # run2
        tags+=['HLT1:Run2:def'                            # HLT1 : default lines 
               ,'HLT2:Run2:Rad:Excl'                      # HLT2 : exclusive radiative 
               ,'HLT2:Run2:Rad:Incl'                      # HLT2 : inclusive radiative 
               ,'HLT2:Run2:Rad:Conv'                      # HLT2 : lines with converted photons 
               ,'HLT2:Run2:e','HLT2:Run2:die'             # HLT2 : inclusive e/die 
               ,'HLT2:Run2:Topo'                          # HLT2 : topo 
               ]
        if stripping.find('26') != -1 or  stripping.find('28') != -1 or  stripping.find('29') != -1 :  # 2016 lines
            tags+=['HLT1:Run2:2016:MVA','HLT2:Run2:2016:L0HG','HlT2:Run2:2016:Topo']

        
    return triggerList(tags,verbose)
    
def radFakeTriggerList(verbose=False ) :
    tags=['L0:Hadron','L0:Muon','HLT1:def','HLT1:dimu','HLT2:dimu','HLT1:Run2:def'] # for fake category (TO BE CHECKED FOR RUN2 !!)
    return triggerList(tags,verbose)
    
