from DaVinci.Configuration import *
from Gaudi.Configuration import *
from Configurables import GaudiSequencer, DecayTreeTuple, TupleToolDecay, TupleToolTISTOS, TupleToolVeloTrackMatch, FilterDesktop, TupleToolDecayTreeFitter, TupleToolDira, TupleToolGeometry, TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolKinematic, TupleToolParticleReFit, TupleToolPid, TupleToolPropertime, TupleToolRICHPid, TupleToolTrackInfo, TupleToolTrackIsolation, TupleToolTrackPosition, TupleToolTrigger, TriggerTisTos, TupleToolPhotonInfo, TupleToolPi0Info, TupleToolAngles, TupleToolVeto, TupleToolProtoPData, PhysConf, CombineParticles,TupleToolCaloHypo

from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence
from Configurables import TupleToolDecayTreeFitter


######  SETTINGS ###### DaVinci v44r4
#
rootfilename = "CHARM_D2ETAPPGH_DVNTUPLE.root" 
myEvents = -1

#
#######################

from Configurables import PhysConf 
PhysConf().CaloReProcessing=True

tlist = [
    "TupleToolAngles",
    "TupleToolEventInfo",
    "TupleToolDecayTreeFitter",
    "TupleToolDira",
    "TupleToolGeometry",
    "TupleToolKinematic",
    "TupleToolParticleReFit",
    "TupleToolPid",
    "TupleToolPhotonInfo",
    "TupleToolPi0Info",
    "TupleToolPrimaries",
    "TupleToolPropertime",
    "TupleToolTrackInfo",
    "TupleToolRecoStats"
    ]

###### STORE TRIGGERS ######


L0Triggers = [  "L0HadronDecision",
                "L0MuonDecision",
                "L0DiMuonDecision",
                "L0ElectronDecision",
                "L0PhotonDecision"
                ]

Hlt1Triggers = [ "Hlt1TrackMVADecision",
                 "Hlt1TwoTrackMVADecision" ]

#add: https://twiki.cern.ch/twiki/bin/view/LHCb/Hlt2RatesAlpha
Hlt2Triggers = [
      "Hlt2CharmHadDp2EtaPip_Eta2PimPipGDecision",
      "Hlt2CharmHadDp2EtaKp_Eta2PimPipGDecision",
      "Hlt2CharmHadDp2EtapPip_Etap2PimPipGDecision",
      "Hlt2CharmHadDp2EtapKp_Etap2PimPipGDecision",
      "Hlt2Topo2BodyDecision",
      "Hlt2Topo3BodyDecision",
      "Hlt2Topo4BodyDecision"
      ]

#      "Hlt2GlobalDecision" ]

triggerListF = L0Triggers + Hlt1Triggers + Hlt2Triggers


####------------

MyTupleEtaPi = DecayTreeTuple("MyTupleEtaPi")

MyTupleEtaPi.Inputs = ["Phys/D2EtaHD2PiEtaPPGLine/Particles"]
MyTupleEtaPi.Decay = "[ D+ -> ^( eta -> ^pi+ ^pi- ^gamma ) ^pi+ ]CC"

MyTupleEtaPi.Branches = {
      "eta" : "[ D+ -> ^( eta -> pi+ pi- gamma ) pi+ ]CC",
      "gamma" : "[ D+ -> ( eta -> pi+ pi- ^gamma ) pi+ ]CC",
      "pip" : "[ D+ -> ( eta -> ^pi+ pi- gamma ) pi+ ]CC",
      "pim" : "[ D+ -> ( eta -> pi+ ^pi- gamma ) pi+ ]CC",
      "oddpi" : "[ D+ -> ( eta -> pi+ pi- gamma ) ^pi+ ]CC",
      "D" : "[ D+ -> ( eta -> pi+ pi- gamma ) pi+ ]CC"
      }


MyTupleEtaPi.ToolList += tlist

MyTupleEtaPi.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtaPi.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtaPi.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtaPi.addTool(TupleToolDecay, name="D")
MyTupleEtaPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtaPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtaPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtaPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtaPi.D.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtaPi.D.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtaPi.D.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtaPi.D.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtaPi.D.PVFitFull.Verbose = True
MyTupleEtaPi.D.PVFitFull.constrainToOriginVertex = True
MyTupleEtaPi.D.PVFitFull.daughtersToConstrain = ["eta"]
MyTupleEtaPi.D.PVFitFull.UpdateDaughters = True
MyTupleEtaPi.D.PVFitEtaP.Verbose = True
MyTupleEtaPi.D.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtaPi.D.PVFitEtaP.daughtersToConstrain = ["eta"]
MyTupleEtaPi.D.PVFitEtaP.UpdateDaughters = True
MyTupleEtaPi.D.PVFitBeam.Verbose = True
MyTupleEtaPi.D.PVFitBeam.constrainToOriginVertex = True
MyTupleEtaPi.D.PVFitBeam.UpdateDaughters = True
MyTupleEtaPi.D.PVFit.Verbose = True
MyTupleEtaPi.D.PVFit.constrainToOriginVertex = False
MyTupleEtaPi.D.PVFit.UpdateDaughters = True

MyTupleEtaPi.addTool(TupleToolDecay, name="gamma")
MyTupleEtaPi.gamma.ToolList += [ "TupleToolCaloHypo/TupleToolCaloHypoEtapPi" ]
MyTupleEtaPi.gamma.addTool(TupleToolCaloHypo("TupleToolCaloHypoEtapPi"))


MyTupleEtaPi.ToolList += ["TupleToolTrigger/TupleToolTriggerEta","TupleToolTISTOS/TupleToolTISTOSEta"]
MyTupleEtaPi.addTool(TupleToolTrigger("TupleToolTriggerEta"))
MyTupleEtaPi.addTool(TupleToolTISTOS("TupleToolTISTOSEta"))
MyTupleEtaPi.TupleToolTriggerEta.Verbose=True
MyTupleEtaPi.TupleToolTriggerEta.VerboseL0=True
MyTupleEtaPi.TupleToolTriggerEta.VerboseHlt1=True
MyTupleEtaPi.TupleToolTriggerEta.VerboseHlt2=True
MyTupleEtaPi.TupleToolTriggerEta.FillL0=True
MyTupleEtaPi.TupleToolTriggerEta.FillHlt1=True
MyTupleEtaPi.TupleToolTriggerEta.FillHlt2=True
MyTupleEtaPi.TupleToolTriggerEta.TriggerList= triggerListF
MyTupleEtaPi.TupleToolTISTOSEta.Verbose=True
MyTupleEtaPi.TupleToolTISTOSEta.VerboseL0=True
MyTupleEtaPi.TupleToolTISTOSEta.VerboseHlt1=True
MyTupleEtaPi.TupleToolTISTOSEta.VerboseHlt2=True
MyTupleEtaPi.TupleToolTISTOSEta.FillL0=True
MyTupleEtaPi.TupleToolTISTOSEta.FillHlt1=True
MyTupleEtaPi.TupleToolTISTOSEta.FillHlt2=True
MyTupleEtaPi.TupleToolTISTOSEta.TriggerList= triggerListF

####------------

####------------

MyTupleEtaK = DecayTreeTuple("MyTupleEtaK")

MyTupleEtaK.Inputs = ["Phys/D2EtaHD2KEtaPPGLine/Particles"]
MyTupleEtaK.Decay = "[ D+ -> ^( eta -> ^pi+ ^pi- ^gamma ) ^K+ ]CC"

MyTupleEtaK.Branches = {
      "eta" : "[ D+ -> ^( eta -> pi+ pi- gamma ) K+ ]CC",
      "gamma" : "[ D+ -> ( eta -> pi+ pi- ^gamma ) K+ ]CC",
      "pip" : "[ D+ -> ( eta -> ^pi+ pi- gamma ) K+ ]CC",
      "pim" : "[ D+ -> ( eta -> pi+ ^pi- gamma ) K+ ]CC",
      "oddK" : "[ D+ -> ( eta -> pi+ pi- gamma ) ^K+ ]CC",
      "D" : "[ D+ -> ( eta -> pi+ pi- gamma ) K+ ]CC"
      }


MyTupleEtaK.ToolList += tlist
MyTupleEtaK.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtaK.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtaK.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtaK.addTool(TupleToolDecay, name="D")
MyTupleEtaK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtaK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtaK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtaK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtaK.D.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtaK.D.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtaK.D.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtaK.D.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtaK.D.PVFitFull.Verbose = True
MyTupleEtaK.D.PVFitFull.constrainToOriginVertex = True
MyTupleEtaK.D.PVFitFull.daughtersToConstrain = ["eta"]
MyTupleEtaK.D.PVFitFull.UpdateDaughters = True
MyTupleEtaK.D.PVFitEtaP.Verbose = True
MyTupleEtaK.D.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtaK.D.PVFitEtaP.daughtersToConstrain = ["eta"]
MyTupleEtaK.D.PVFitEtaP.UpdateDaughters = True
MyTupleEtaK.D.PVFitBeam.Verbose = True
MyTupleEtaK.D.PVFitBeam.constrainToOriginVertex = True
MyTupleEtaK.D.PVFitBeam.UpdateDaughters = True
MyTupleEtaK.D.PVFit.Verbose = True
MyTupleEtaK.D.PVFit.constrainToOriginVertex = False
MyTupleEtaK.D.PVFit.UpdateDaughters = True


MyTupleEtaK.ToolList += ["TupleToolTrigger/TupleToolTriggerEta","TupleToolTISTOS/TupleToolTISTOSEta"]
MyTupleEtaK.addTool(TupleToolTrigger("TupleToolTriggerEta"))
MyTupleEtaK.addTool(TupleToolTISTOS("TupleToolTISTOSEta"))
MyTupleEtaK.TupleToolTriggerEta.Verbose=True
MyTupleEtaK.TupleToolTriggerEta.VerboseL0=True
MyTupleEtaK.TupleToolTriggerEta.VerboseHlt1=True
MyTupleEtaK.TupleToolTriggerEta.VerboseHlt2=True
MyTupleEtaK.TupleToolTriggerEta.FillL0=True
MyTupleEtaK.TupleToolTriggerEta.FillHlt1=True
MyTupleEtaK.TupleToolTriggerEta.FillHlt2=True
MyTupleEtaK.TupleToolTriggerEta.TriggerList= triggerListF
MyTupleEtaK.TupleToolTISTOSEta.Verbose=True
MyTupleEtaK.TupleToolTISTOSEta.VerboseL0=True
MyTupleEtaK.TupleToolTISTOSEta.VerboseHlt1=True
MyTupleEtaK.TupleToolTISTOSEta.VerboseHlt2=True
MyTupleEtaK.TupleToolTISTOSEta.FillL0=True
MyTupleEtaK.TupleToolTISTOSEta.FillHlt1=True
MyTupleEtaK.TupleToolTISTOSEta.FillHlt2=True
MyTupleEtaK.TupleToolTISTOSEta.TriggerList= triggerListF

####------------

####------------


from Configurables import SubstitutePID

subsEta = SubstitutePID(
      'MakeTausEta',
      Code = "DECTREE('[ D+ -> ( eta -> pi+ pi- gamma ) pi+ ]CC')",
      # note that SubstitutePID can't handle automatic CC
      Substitutions = {
      'D+ -> ( eta -> pi+ pi- gamma ) ^pi+':'mu+',   
      'D- -> ( eta -> pi+ pi- gamma ) ^pi-':'mu-',
      }
      )

# get the selection(s) created by the stripping
inputListEta = "Phys/D2EtaHD2PiEtaPPGLine/Particles"
strippingSelsEta = [DataOnDemand(Location=inputListEta)]

# create a selection using the substitution algorithm
selSubEta = Selection(
          'TauselEta',
          Algorithm=subsEta,
          RequiredSelections=strippingSelsEta
          )

selSubEta.Verbose = True
selSeqEta = SelectionSequence('SelSeqEta', TopSelection=selSubEta)

MyTupleEtaMu = DecayTreeTuple("MyTupleEtaMu")
MyTupleEtaMu.Inputs = [selSeqEta.outputLocation()] #Inputs = ["Phys/D2EtaHD2PiEtaPPGLine/Particles"]
MyTupleEtaMu.Decay = "[ D+ -> ^( eta -> ^pi+ ^pi- ^gamma ) ^mu+ ]CC"

MyTupleEtaMu.Branches = {
      "eta" : "[ D+ -> ^( eta -> pi+ pi- gamma ) mu+ ]CC",
      "gamma" : "[ D+ -> ( eta -> pi+ pi- ^gamma ) mu+ ]CC",
      "pip" : "[ D+ -> ( eta -> ^pi+ pi- gamma ) mu+ ]CC",
      "pim" : "[ D+ -> ( eta -> pi+ ^pi- gamma ) mu+ ]CC",
      "mu" : "[ D+ -> ( eta -> pi+ pi- gamma ) ^mu+ ]CC",
      "tau" : "[ D+ -> ( eta -> pi+ pi- gamma ) mu+ ]CC"
      }


MyTupleEtaMu.ToolList += tlist

MyTupleEtaMu.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtaMu.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtaMu.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtaMu.addTool(TupleToolDecay, name="tau")
MyTupleEtaMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtaMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtaMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtaMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtaMu.tau.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtaMu.tau.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtaMu.tau.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtaMu.tau.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtaMu.tau.PVFitFull.Verbose = True
MyTupleEtaMu.tau.PVFitFull.constrainToOriginVertex = True
MyTupleEtaMu.tau.PVFitFull.daughtersToConstrain = ["eta"]
MyTupleEtaMu.tau.PVFitFull.UpdateDaughters = True
MyTupleEtaMu.tau.PVFitEtaP.Verbose = True
MyTupleEtaMu.tau.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtaMu.tau.PVFitEtaP.daughtersToConstrain = ["eta"]
MyTupleEtaMu.tau.PVFitEtaP.UpdateDaughters = True
MyTupleEtaMu.tau.PVFitBeam.Verbose = True
MyTupleEtaMu.tau.PVFitBeam.constrainToOriginVertex = True
MyTupleEtaMu.tau.PVFitBeam.UpdateDaughters = True
MyTupleEtaMu.tau.PVFit.Verbose = True
MyTupleEtaMu.tau.PVFit.constrainToOriginVertex = False
MyTupleEtaMu.tau.PVFit.UpdateDaughters = True


MyTupleEtaMu.ToolList += ["TupleToolTrigger/TupleToolTriggerEta","TupleToolTISTOS/TupleToolTISTOSEta"]
MyTupleEtaMu.addTool(TupleToolTrigger("TupleToolTriggerEta"))
MyTupleEtaMu.addTool(TupleToolTISTOS("TupleToolTISTOSEta"))
MyTupleEtaMu.TupleToolTriggerEta.Verbose=True
MyTupleEtaMu.TupleToolTriggerEta.VerboseL0=True
MyTupleEtaMu.TupleToolTriggerEta.VerboseHlt1=True
MyTupleEtaMu.TupleToolTriggerEta.VerboseHlt2=True
MyTupleEtaMu.TupleToolTriggerEta.FillL0=True
MyTupleEtaMu.TupleToolTriggerEta.FillHlt1=True
MyTupleEtaMu.TupleToolTriggerEta.FillHlt2=True
MyTupleEtaMu.TupleToolTriggerEta.TriggerList= triggerListF
MyTupleEtaMu.TupleToolTISTOSEta.Verbose=True
MyTupleEtaMu.TupleToolTISTOSEta.VerboseL0=True
MyTupleEtaMu.TupleToolTISTOSEta.VerboseHlt1=True
MyTupleEtaMu.TupleToolTISTOSEta.VerboseHlt2=True
MyTupleEtaMu.TupleToolTISTOSEta.FillL0=True
MyTupleEtaMu.TupleToolTISTOSEta.FillHlt1=True
MyTupleEtaMu.TupleToolTISTOSEta.FillHlt2=True
MyTupleEtaMu.TupleToolTISTOSEta.TriggerList= triggerListF

####------------

####------------ start Etaprime

MyTupleEtapPi = DecayTreeTuple("MyTupleEtapPi")

MyTupleEtapPi.Inputs = ["Phys/D2EtaPrimeHD2PiEtaPrimePPGLine/Particles"]
MyTupleEtapPi.Decay = "[ D+ -> ^( eta_prime -> ^pi+ ^pi- ^gamma ) ^pi+ ]CC"

MyTupleEtapPi.Branches = {
      "eta" : "[ D+ -> ^( eta_prime -> pi+ pi- gamma ) pi+ ]CC",
      "gamma" : "[ D+ -> ( eta_prime -> pi+ pi- ^gamma ) pi+ ]CC",
      "pip" : "[ D+ -> ( eta_prime -> ^pi+ pi- gamma ) pi+ ]CC",
      "pim" : "[ D+ -> ( eta_prime -> pi+ ^pi- gamma ) pi+ ]CC",
      "oddpi" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) ^pi+ ]CC",
      "D" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) pi+ ]CC"
      }


MyTupleEtapPi.ToolList += tlist

MyTupleEtapPi.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtapPi.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtapPi.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtapPi.addTool(TupleToolDecay, name="D")
MyTupleEtapPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtapPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtapPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtapPi.D.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtapPi.D.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtapPi.D.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtapPi.D.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtapPi.D.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtapPi.D.PVFitFull.Verbose = True
MyTupleEtapPi.D.PVFitFull.constrainToOriginVertex = True
MyTupleEtapPi.D.PVFitFull.daughtersToConstrain = ["eta_prime"]
MyTupleEtapPi.D.PVFitFull.UpdateDaughters = True
MyTupleEtapPi.D.PVFitEtaP.Verbose = True
MyTupleEtapPi.D.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtapPi.D.PVFitEtaP.daughtersToConstrain = ["eta_prime"]
MyTupleEtapPi.D.PVFitEtaP.UpdateDaughters = True
MyTupleEtapPi.D.PVFitBeam.Verbose = True
MyTupleEtapPi.D.PVFitBeam.constrainToOriginVertex = True
MyTupleEtapPi.D.PVFitBeam.UpdateDaughters = True
MyTupleEtapPi.D.PVFit.Verbose = True
MyTupleEtapPi.D.PVFit.constrainToOriginVertex = False
MyTupleEtapPi.D.PVFit.UpdateDaughters = True

MyTupleEtapPi.addTool(TupleToolDecay, name="gamma")
MyTupleEtapPi.gamma.ToolList += [ "TupleToolCaloHypo/TupleToolCaloHypoEtapPi" ]
MyTupleEtapPi.gamma.addTool(TupleToolCaloHypo("TupleToolCaloHypoEtapPi"))


MyTupleEtapPi.ToolList += ["TupleToolTrigger/TupleToolTriggerEtaP","TupleToolTISTOS/TupleToolTISTOSEtaP"]
MyTupleEtapPi.addTool(TupleToolTrigger("TupleToolTriggerEtaP"))
MyTupleEtapPi.addTool(TupleToolTISTOS("TupleToolTISTOSEtaP"))
MyTupleEtapPi.TupleToolTriggerEtaP.Verbose=True
MyTupleEtapPi.TupleToolTriggerEtaP.VerboseL0=True
MyTupleEtapPi.TupleToolTriggerEtaP.VerboseHlt1=True
MyTupleEtapPi.TupleToolTriggerEtaP.VerboseHlt2=True
MyTupleEtapPi.TupleToolTriggerEtaP.FillL0=True
MyTupleEtapPi.TupleToolTriggerEtaP.FillHlt1=True
MyTupleEtapPi.TupleToolTriggerEtaP.FillHlt2=True
MyTupleEtapPi.TupleToolTriggerEtaP.TriggerList= triggerListF
MyTupleEtapPi.TupleToolTISTOSEtaP.Verbose=True
MyTupleEtapPi.TupleToolTISTOSEtaP.VerboseL0=True
MyTupleEtapPi.TupleToolTISTOSEtaP.VerboseHlt1=True
MyTupleEtapPi.TupleToolTISTOSEtaP.VerboseHlt2=True
MyTupleEtapPi.TupleToolTISTOSEtaP.FillL0=True
MyTupleEtapPi.TupleToolTISTOSEtaP.FillHlt1=True
MyTupleEtapPi.TupleToolTISTOSEtaP.FillHlt2=True
MyTupleEtapPi.TupleToolTISTOSEtaP.TriggerList= triggerListF

####------------

####------------

MyTupleEtapK = DecayTreeTuple("MyTupleEtapK")

MyTupleEtapK.Inputs = ["Phys/D2EtaPrimeHD2KEtaPrimePPGLine/Particles"]
MyTupleEtapK.Decay = "[ D+ -> ^( eta_prime -> ^pi+ ^pi- ^gamma ) ^K+ ]CC"

MyTupleEtapK.Branches = {
      "eta" : "[ D+ -> ^( eta_prime -> pi+ pi- gamma ) K+ ]CC",
      "gamma" : "[ D+ -> ( eta_prime -> pi+ pi- ^gamma ) K+ ]CC",
      "pip" : "[ D+ -> ( eta_prime -> ^pi+ pi- gamma ) K+ ]CC",
      "pim" : "[ D+ -> ( eta_prime -> pi+ ^pi- gamma ) K+ ]CC",
      "oddK" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) ^K+ ]CC",
      "D" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) K+ ]CC"
      }


MyTupleEtapK.ToolList += tlist
MyTupleEtapK.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtapK.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtapK.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtapK.addTool(TupleToolDecay, name="D")
MyTupleEtapK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtapK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtapK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtapK.D.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtapK.D.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtapK.D.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtapK.D.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtapK.D.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtapK.D.PVFitFull.Verbose = True
MyTupleEtapK.D.PVFitFull.constrainToOriginVertex = True
MyTupleEtapK.D.PVFitFull.daughtersToConstrain = ["eta_prime"]
MyTupleEtapK.D.PVFitFull.UpdateDaughters = True
MyTupleEtapK.D.PVFitEtaP.Verbose = True
MyTupleEtapK.D.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtapK.D.PVFitEtaP.daughtersToConstrain = ["eta_prime"]
MyTupleEtapK.D.PVFitEtaP.UpdateDaughters = True
MyTupleEtapK.D.PVFitBeam.Verbose = True
MyTupleEtapK.D.PVFitBeam.constrainToOriginVertex = True
MyTupleEtapK.D.PVFitBeam.UpdateDaughters = True
MyTupleEtapK.D.PVFit.Verbose = True
MyTupleEtapK.D.PVFit.constrainToOriginVertex = False
MyTupleEtapK.D.PVFit.UpdateDaughters = True


MyTupleEtapK.ToolList += ["TupleToolTrigger/TupleToolTriggerEtaP","TupleToolTISTOS/TupleToolTISTOSEtaP"]
MyTupleEtapK.addTool(TupleToolTrigger("TupleToolTriggerEtaP"))
MyTupleEtapK.addTool(TupleToolTISTOS("TupleToolTISTOSEtaP"))
MyTupleEtapK.TupleToolTriggerEtaP.Verbose=True
MyTupleEtapK.TupleToolTriggerEtaP.VerboseL0=True
MyTupleEtapK.TupleToolTriggerEtaP.VerboseHlt1=True
MyTupleEtapK.TupleToolTriggerEtaP.VerboseHlt2=True
MyTupleEtapK.TupleToolTriggerEtaP.FillL0=True
MyTupleEtapK.TupleToolTriggerEtaP.FillHlt1=True
MyTupleEtapK.TupleToolTriggerEtaP.FillHlt2=True
MyTupleEtapK.TupleToolTriggerEtaP.TriggerList= triggerListF
MyTupleEtapK.TupleToolTISTOSEtaP.Verbose=True
MyTupleEtapK.TupleToolTISTOSEtaP.VerboseL0=True
MyTupleEtapK.TupleToolTISTOSEtaP.VerboseHlt1=True
MyTupleEtapK.TupleToolTISTOSEtaP.VerboseHlt2=True
MyTupleEtapK.TupleToolTISTOSEtaP.FillL0=True
MyTupleEtapK.TupleToolTISTOSEtaP.FillHlt1=True
MyTupleEtapK.TupleToolTISTOSEtaP.FillHlt2=True
MyTupleEtapK.TupleToolTISTOSEtaP.TriggerList= triggerListF

####------------

####------------


from Configurables import SubstitutePID

subs = SubstitutePID(
      'MakeTaus',
      Code = "DECTREE('[ D+ -> ( eta_prime -> pi+ pi- gamma ) pi+ ]CC')",
      # note that SubstitutePID can't handle automatic CC
      Substitutions = {
      'D+ -> ( eta_prime -> pi+ pi- gamma ) ^pi+':'mu+',   
      'D- -> ( eta_prime -> pi+ pi- gamma ) ^pi-':'mu-',
      }
      )

# get the selection(s) created by the stripping
inputList = "Phys/D2EtaPrimeHD2PiEtaPrimePPGLine/Particles"
strippingSels = [DataOnDemand(Location=inputList)]

# create a selection using the substitution algorithm
selSub = Selection(
          'Tausel',
          Algorithm=subs,
          RequiredSelections=strippingSels
          )

selSub.Verbose = True
selSeq = SelectionSequence('SelSeq', TopSelection=selSub)

MyTupleEtapMu = DecayTreeTuple("MyTupleEtapMu")
MyTupleEtapMu.Inputs = [selSeq.outputLocation()] #Inputs = ["Phys/D2EtaPrimeHD2PiEtaPrimePPGLine/Particles"]
MyTupleEtapMu.Decay = "[ D+ -> ^( eta_prime -> ^pi+ ^pi- ^gamma ) ^mu+ ]CC"

MyTupleEtapMu.Branches = {
      "eta" : "[ D+ -> ^( eta_prime -> pi+ pi- gamma ) mu+ ]CC",
      "gamma" : "[ D+ -> ( eta_prime -> pi+ pi- ^gamma ) mu+ ]CC",
      "pip" : "[ D+ -> ( eta_prime -> ^pi+ pi- gamma ) mu+ ]CC",
      "pim" : "[ D+ -> ( eta_prime -> pi+ ^pi- gamma ) mu+ ]CC",
      "mu" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) ^mu+ ]CC",
      "tau" : "[ D+ -> ( eta_prime -> pi+ pi- gamma ) mu+ ]CC"
      }


MyTupleEtapMu.ToolList += tlist

MyTupleEtapMu.ToolList += [ "TupleToolProtoPData/TupleToolProtoPDataEtaP"]
MyTupleEtapMu.addTool(TupleToolProtoPData,name="TupleToolProtoPDataEtaP")
MyTupleEtapMu.TupleToolProtoPDataEtaP.DataList = ["IsPhoton","CaloTrMatch","CaloDepositID","ShowerShape","ClusterMass",
                                                "CaloNeutralSpd","CaloNeutralPrs","CaloNeutralEcal","CaloNeutralHcal2Ecal",
                                                "CaloNeutralE49","CaloNeutralID","PhotonID"]

MyTupleEtapMu.addTool(TupleToolDecay, name="tau")
MyTupleEtapMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitFull" ]
MyTupleEtapMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitEtaP" ]
MyTupleEtapMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFitBeam" ]
MyTupleEtapMu.tau.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
MyTupleEtapMu.tau.addTool(TupleToolDecayTreeFitter("PVFitFull"))
MyTupleEtapMu.tau.addTool(TupleToolDecayTreeFitter("PVFitEtaP"))
MyTupleEtapMu.tau.addTool(TupleToolDecayTreeFitter("PVFitBeam"))
MyTupleEtapMu.tau.addTool(TupleToolDecayTreeFitter("PVFit"))
MyTupleEtapMu.tau.PVFitFull.Verbose = True
MyTupleEtapMu.tau.PVFitFull.constrainToOriginVertex = True
MyTupleEtapMu.tau.PVFitFull.daughtersToConstrain = ["eta_prime"]
MyTupleEtapMu.tau.PVFitFull.UpdateDaughters = True
MyTupleEtapMu.tau.PVFitEtaP.Verbose = True
MyTupleEtapMu.tau.PVFitEtaP.constrainToOriginVertex = False
MyTupleEtapMu.tau.PVFitEtaP.daughtersToConstrain = ["eta_prime"]
MyTupleEtapMu.tau.PVFitEtaP.UpdateDaughters = True
MyTupleEtapMu.tau.PVFitBeam.Verbose = True
MyTupleEtapMu.tau.PVFitBeam.constrainToOriginVertex = True
MyTupleEtapMu.tau.PVFitBeam.UpdateDaughters = True
MyTupleEtapMu.tau.PVFit.Verbose = True
MyTupleEtapMu.tau.PVFit.constrainToOriginVertex = False
MyTupleEtapMu.tau.PVFit.UpdateDaughters = True


MyTupleEtapMu.ToolList += ["TupleToolTrigger/TupleToolTriggerEtaP","TupleToolTISTOS/TupleToolTISTOSEtaP"]
MyTupleEtapMu.addTool(TupleToolTrigger("TupleToolTriggerEtaP"))
MyTupleEtapMu.addTool(TupleToolTISTOS("TupleToolTISTOSEtaP"))
MyTupleEtapMu.TupleToolTriggerEtaP.Verbose=True
MyTupleEtapMu.TupleToolTriggerEtaP.VerboseL0=True
MyTupleEtapMu.TupleToolTriggerEtaP.VerboseHlt1=True
MyTupleEtapMu.TupleToolTriggerEtaP.VerboseHlt2=True
MyTupleEtapMu.TupleToolTriggerEtaP.FillL0=True
MyTupleEtapMu.TupleToolTriggerEtaP.FillHlt1=True
MyTupleEtapMu.TupleToolTriggerEtaP.FillHlt2=True
MyTupleEtapMu.TupleToolTriggerEtaP.TriggerList= triggerListF
MyTupleEtapMu.TupleToolTISTOSEtaP.Verbose=True
MyTupleEtapMu.TupleToolTISTOSEtaP.VerboseL0=True
MyTupleEtapMu.TupleToolTISTOSEtaP.VerboseHlt1=True
MyTupleEtapMu.TupleToolTISTOSEtaP.VerboseHlt2=True
MyTupleEtapMu.TupleToolTISTOSEtaP.FillL0=True
MyTupleEtapMu.TupleToolTISTOSEtaP.FillHlt1=True
MyTupleEtapMu.TupleToolTISTOSEtaP.FillHlt2=True
MyTupleEtapMu.TupleToolTISTOSEtaP.TriggerList= triggerListF

####------------

MessageSvc().OutputLevel = INFO 

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().InputType = 'MDST'
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 10000
DaVinci().Lumi   = True
DaVinci().EvtMax = myEvents

# Create a filter for events that passed the 
# job will only run over events passing the line.
from Configurables import LoKi__HDRFilter   as StripFilter
filter = StripFilter( 'StripPassFilter',
                      Code="HLT_PASS('StrippingD2EtaPrimeHD2PiEtaPrimePPGLineDecision') | HLT_PASS('StrippingD2EtaPrimeHD2KEtaPrimePPGLineDecision') | HLT_PASS('StrippingD2EtaHD2PiEtaPPGLineDecision') | HLT_PASS('StrippingD2EtaHD2KEtaPPGLineDecision')", 
                      Location="/Event/Strip/Phys/DecReports" )
                      

DaVinci().EventPreFilters += [filter]

DaVinci().RootInTES = '/Event/Charm'
DaVinci().TupleFile = rootfilename

      
# add our new selection and the tuple into the sequencer
seq = GaudiSequencer('MyTupleSeq')
seq.Members += [MyTupleEtaK, MyTupleEtaPi]
seq.Members += [selSeqEta.sequence(), MyTupleEtaMu]
seq.Members += [MyTupleEtapK, MyTupleEtapPi]
seq.Members += [selSeq.sequence(), MyTupleEtapMu]
seq.IgnoreFilterPassed=True
DaVinci().appendToMainSequence([seq])
