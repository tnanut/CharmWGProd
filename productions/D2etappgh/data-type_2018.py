########################################
# User configuration for 2018 data tuple
########################################
from Configurables import DaVinci
# Data-taking year
DaVinci().DataType = '2018'
# data or MC?
DaVinci().Simulation = False
from Configurables import CondDB
CondDB ( LatestGlobalTagByDataType = '2018' )
