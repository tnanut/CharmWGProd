"""
    Due to the use of the D2DVVDCHI2 functor, the decay descriptor given to
    the instantiation of this class *must* follow a specific ordering.
    The three-body final state must list the KS0 object last (in the third
    position), for example:

        decay = '[D0 -> pi+ pi- KS0]cc'
    This follows what describes on https://gitlab.cern.ch/lhcb/Hlt/blob/2018-patches/Hlt/Hlt2Lines/python/Hlt2Lines/CharmHad/Stages.py#L1098

    Note on a cut which must be done before analysing data:
      1.) KS_D2DVVDCHI2 > math.exp(D_KS_lnD2DVVDCHI2_MIN) # Cut introduced in 2017
         # D_KS_lnD2DVVDCHI2_MIN = 5.0 
      2.) Dst_DTF_VCHI2NDOF > 0 # to avoid failed DTF - cannot find how to add manually to TupleTool
          Dst_DTFD0KS_VCHI2NDOF>0

    There are differences between decay descriptor of Turbo line and Stripping line

        1.) Turbo line, when calling D0's childs the order should be 

            D0 -> h1h2KS
                   1 2 3

           e.g.  "DTF_D0_h1_KS_M": "CHILD(MASS(1, 3), 'D0'==ABSID)",

        2.) Stripping line, when calling D0's childs the order should be 

            D0 -> KSh1h2
                   1 2 3

           e.g.  "DTF_D0_h1_KS_M": "CHILD(MASS(2, 1), 'D0'==ABSID)",

"""
from PhysConf.Selections import (
    AutomaticData,
    CombineSelection,
    MomentumScaling,
    RebuildSelection,
    SelectionSequence,
    TupleSelection,
)
from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci, TupleToolTrigger
from GaudiKernel.SystemOfUnits import MeV
from StandardParticles import StdAllLooseMuons
from Configurables import CondDB
from Configurables import DstConf, TurboConf  # necessary for DaVinci v40r1 onwards
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import MessageSvc

from Configurables import TupleToolMCTruth, TupleToolMCBackgroundInfo, MCTupleToolHierarchy 

triggerList = [
    'L0DiMuonDecision',
    'L0ElectronDecision',
    'L0HadronDecision',
    'L0MuonDecision',
    'L0PhotonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMVATightDecision',
    'Hlt1TwoTrackMVATightDecision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackMuonMVADecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2CharmHadInclDst2PiD02HHXBDTDecision'
]

# Prefix "Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_",
triggerHlt2Head = [
    "_KS0LLTurboDecision",
    "_KS0DDTurboDecision",
    "_KS0LL_LTUNBTurboDecision",
    "_KS0DD_LTUNBTurboDecision",
]

default_variables = {
    'ID':     'ID',
    'P':      'P',
    'PX':     'PX',
    'PY':     'PY',
    'PZ':     'PZ',
    'E':      'E',
    'PT':     'PT',
    'ETA':    'ETA',
    'PHI':    'PHI',
    'IPCHI2': 'BPVIPCHI2()',
    'IP':     'BPVIP()',
}

track_variables = {
    'TRCHI2DOF':   'TRCHI2DOF',
    'GHOSTPROB':   'TRGHOSTPROB',
    'CLONEDIST':   'CLONEDIST',
    'ProbNNmu':    'PROBNNmu',
    'ProbNNk':     'PROBNNk',
    'ProbNNpi':    'PROBNNpi',
    'ProbNNghost': 'PROBNNghost',
    'ProbNNp':     'PROBNNp',
    'PIDpi':       'PIDpi',
    'PIDe':        'PIDe',
    'PIDp':        'PIDp',
    'PIDK':        'PIDK',
    'PIDmu':       'PIDmu',
    'IsMuon':      'switch(ISMUON,1,0)',
}

composite_variables = {
    'M':         'M',
    'VX':        'VFASPF(VX)',
    'VY':        'VFASPF(VY)',
    'VZ':        'VFASPF(VZ)',
    'BPVDIRA':   'BPVDIRA',
    'BPVVDCHI2': 'BPVVDCHI2',
    'BPVX':      'BPV(VX)',
    'BPVY':      'BPV(VY)',
    'BPVZ':      'BPV(VZ)',
    'VCHI2NDOF': 'CHI2VXNDOF',
}

DTF_variables_Dst = {
    # D*+
    "DTF_M":          "M",
    "DTF_DM":         "M-CHILD(M, 'D0'==ABSID)",
    "DTF_VCHI2NDOF":  "CHI2VXNDOF",
    "DTF_Dst_ID":     "ID",
    "DTF_Dst_PX":     "PX",
    "DTF_Dst_PY":     "PY",
    "DTF_Dst_PZ":     "PZ",
    "DTF_Dst_PT":     "PT",
    "DTF_Dst_E":      "E",
    "DTF_Dst_ETA":    "ETA",
    "DTF_Dst_PHI":    "PHI",
    # D0
    "DTF_D0_M":       "CHILD(M,    'D0'==ABSID)",
    "DTF_D0_PX":      "CHILD(PX,   'D0'==ABSID)",
    "DTF_D0_PY":      "CHILD(PY,   'D0'==ABSID)",
    "DTF_D0_PZ":      "CHILD(PZ,   'D0'==ABSID)",
    "DTF_D0_PT":      "CHILD(PT,   'D0'==ABSID)",
    "DTF_D0_E":       "CHILD(E,    'D0'==ABSID)",
    "DTF_D0_ETA":     "CHILD(ETA,  'D0'==ABSID)",
    "DTF_D0_PHI":     "CHILD(PHI,  'D0'==ABSID)",
    # Slow pion
    "DTF_pis_ID":     "CHILD(ID,  2)",
    "DTF_pis_PX":     "CHILD(PX,  2)",
    "DTF_pis_PY":     "CHILD(PY,  2)",
    "DTF_pis_PZ":     "CHILD(PZ,  2)",
    "DTF_pis_PT":     "CHILD(PT,  2)",
    "DTF_pis_E":      "CHILD(E,   2)",
    "DTF_pis_ETA":    "CHILD(ETA, 2)",
    "DTF_pis_PHI":    "CHILD(PHI, 2)",
    # KS
    "DTF_D0_KS_M":    "CHILD(CHILD(M,    'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_ID":   "CHILD(CHILD(ID,   'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_PX":   "CHILD(CHILD(PX,   'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_PY":   "CHILD(CHILD(PY,   'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_PZ":   "CHILD(CHILD(PZ,   'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_PT":   "CHILD(CHILD(PT,   'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_E":    "CHILD(CHILD(E,    'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_ETA":  "CHILD(CHILD(ETA,  'KS0'==ABSID), 'D0'==ABSID)",
    "DTF_D0_KS_PHI":  "CHILD(CHILD(PHI,  'KS0'==ABSID), 'D0'==ABSID)",
    # H1
    "DTF_D0_h1_E":    "CHILD(CHILD(E,   1), 'D0'==ABSID)",
    "DTF_D0_h1_ETA":  "CHILD(CHILD(ETA, 1), 'D0'==ABSID)",
    "DTF_D0_h1_ID":   "CHILD(CHILD(ID,  1), 'D0'==ABSID)",
    "DTF_D0_h1_PHI":  "CHILD(CHILD(PHI, 1), 'D0'==ABSID)",
    "DTF_D0_h1_PT":   "CHILD(CHILD(PT,  1), 'D0'==ABSID)",
    "DTF_D0_h1_PX":   "CHILD(CHILD(PX,  1), 'D0'==ABSID)",
    "DTF_D0_h1_PY":   "CHILD(CHILD(PY,  1), 'D0'==ABSID)",
    "DTF_D0_h1_PZ":   "CHILD(CHILD(PZ,  1), 'D0'==ABSID)",
    # H2
    "DTF_D0_h2_E":    "CHILD(CHILD(E,   2), 'D0'==ABSID)",
    "DTF_D0_h2_ETA":  "CHILD(CHILD(ETA, 2), 'D0'==ABSID)",
    "DTF_D0_h2_ID":   "CHILD(CHILD(ID,  2), 'D0'==ABSID)",
    "DTF_D0_h2_PHI":  "CHILD(CHILD(PHI, 2), 'D0'==ABSID)",
    "DTF_D0_h2_PT":   "CHILD(CHILD(PT,  2), 'D0'==ABSID)",
    "DTF_D0_h2_PX":   "CHILD(CHILD(PX,  2), 'D0'==ABSID)",
    "DTF_D0_h2_PY":   "CHILD(CHILD(PY,  2), 'D0'==ABSID)",
    "DTF_D0_h2_PZ":   "CHILD(CHILD(PZ,  2), 'D0'==ABSID)",
    # Others
    "DTF_D0_h1_KS_M": "CHILD(MASS(1, 3), 'D0'==ABSID)",
    "DTF_D0_h1_h2_M": "CHILD(MASS(1, 2), 'D0'==ABSID)",
    "DTF_D0_h2_KS_M": "CHILD(MASS(2, 3), 'D0'==ABSID)",
}

# DoubleTag from the Stripping
DTF_variable_Dst_Stripping = dict(DTF_variables_Dst.items() + {
                                                                # H1
                                                                "DTF_D0_h1_ID"      : "CHILD(CHILD(ID,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_PX"      : "CHILD(CHILD(PX,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_PY"      : "CHILD(CHILD(PY,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_PZ"      : "CHILD(CHILD(PZ,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_PT"      : "CHILD(CHILD(PT,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_E"       : "CHILD(CHILD(E ,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_ETA"     : "CHILD(CHILD(ETA ,2),'D0'==ABSID)",
                                                                "DTF_D0_h1_PHI"     : "CHILD(CHILD(PHI ,2),'D0'==ABSID)",
                                                                #H2
                                                                "DTF_D0_h2_ID"      : "CHILD(CHILD(ID,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_PX"      : "CHILD(CHILD(PX,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_PY"      : "CHILD(CHILD(PY,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_PZ"      : "CHILD(CHILD(PZ,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_PT"      : "CHILD(CHILD(PT,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_E"       : "CHILD(CHILD(E ,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_ETA"     : "CHILD(CHILD(ETA ,3),'D0'==ABSID)",
                                                                "DTF_D0_h2_PHI"     : "CHILD(CHILD(PHI ,3),'D0'==ABSID)",
                                                                # Others
                                                                "DTF_D0_h1_h2_M"    : "CHILD(MASS(2,3),'D0'==ABSID)",
                                                                "DTF_D0_h1_KS_M"    : "CHILD(MASS(2,1),'D0'==ABSID)",
                                                                "DTF_D0_h2_KS_M"    : "CHILD(MASS(3,1),'D0'==ABSID)",
                                                                }.items())
# Only SingleTag 
DTF_variables_B = {
    # B
    "DTF_VCHI2NDOF"     : "CHI2VXNDOF",
    # D0
    "DTF_DM"            : "M-CHILD(M,'D0'==ABSID)",  
    "DTF_D0_M"          : "CHILD(M,'D0'==ABSID)",
    "DTF_D0_PX"         : "CHILD(PX,'D0'==ABSID)",
    "DTF_D0_PY"         : "CHILD(PY,'D0'==ABSID)",
    "DTF_D0_PZ"         : "CHILD(PZ,'D0'==ABSID)",
    "DTF_D0_PT"         : "CHILD(PT,'D0'==ABSID)",
    "DTF_D0_E"          : "CHILD(E,'D0'==ABSID)",
    "DTF_D0_ETA"        : "CHILD(ETA,'D0'==ABSID)",
    "DTF_D0_PHI"        : "CHILD(PHI,'D0'==ABSID)",
    # Mu
    "DTF_Mu_ID"         : "CHILD(ID , 2)",
    "DTF_Mu_PX"         : "CHILD(PX , 2)",
    "DTF_Mu_PY"         : "CHILD(PY , 2)",
    "DTF_Mu_PZ"         : "CHILD(PZ , 2)",
    "DTF_Mu_PT"         : "CHILD(PT , 2)",
    "DTF_Mu_E"          : "CHILD(E  , 2)",
    "DTF_Mu_ETA"        : "CHILD(ETA, 2)",
    "DTF_Mu_PHI"        : "CHILD(PHI, 2)",
    # KS0
    "DTF_D0_KS_M"       : "CHILD(CHILD(M,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_ID"      : "CHILD(CHILD(ID,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_PX"      : "CHILD(CHILD(PX,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_PY"      : "CHILD(CHILD(PY,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_PZ"      : "CHILD(CHILD(PZ,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_PT"      : "CHILD(CHILD(PT,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_E"       : "CHILD(CHILD(E ,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_ETA"     : "CHILD(CHILD(ETA ,'KS0'==ABSID),'D0'==ABSID)",
    "DTF_D0_KS_PHI"     : "CHILD(CHILD(PHI ,'KS0'==ABSID),'D0'==ABSID)",
    # H1
    "DTF_D0_h1_ID"      : "CHILD(CHILD(ID,2),'D0'==ABSID)",
    "DTF_D0_h1_PX"      : "CHILD(CHILD(PX,2),'D0'==ABSID)",
    "DTF_D0_h1_PY"      : "CHILD(CHILD(PY,2),'D0'==ABSID)",
    "DTF_D0_h1_PZ"      : "CHILD(CHILD(PZ,2),'D0'==ABSID)",
    "DTF_D0_h1_PT"      : "CHILD(CHILD(PT,2),'D0'==ABSID)",
    "DTF_D0_h1_E"       : "CHILD(CHILD(E ,2),'D0'==ABSID)",
    "DTF_D0_h1_ETA"     : "CHILD(CHILD(ETA ,2),'D0'==ABSID)",
    "DTF_D0_h1_PHI"     : "CHILD(CHILD(PHI ,2),'D0'==ABSID)",
    #H2
    "DTF_D0_h2_ID"      : "CHILD(CHILD(ID,3),'D0'==ABSID)",
    "DTF_D0_h2_PX"      : "CHILD(CHILD(PX,3),'D0'==ABSID)",
    "DTF_D0_h2_PY"      : "CHILD(CHILD(PY,3),'D0'==ABSID)",
    "DTF_D0_h2_PZ"      : "CHILD(CHILD(PZ,3),'D0'==ABSID)",
    "DTF_D0_h2_PT"      : "CHILD(CHILD(PT,3),'D0'==ABSID)",
    "DTF_D0_h2_E"       : "CHILD(CHILD(E ,3),'D0'==ABSID)",
    "DTF_D0_h2_ETA"     : "CHILD(CHILD(ETA ,3),'D0'==ABSID)",
    "DTF_D0_h2_PHI"     : "CHILD(CHILD(PHI ,3),'D0'==ABSID)",
    # Others
    "DTF_D0_h1_h2_M"    : "CHILD(MASS(2,3),'D0'==ABSID)",
    "DTF_D0_h1_KS_M"    : "CHILD(MASS(2,1),'D0'==ABSID)",
    "DTF_D0_h2_KS_M"    : "CHILD(MASS(3,1),'D0'==ABSID)",
}

decay_descriptors = {
    # Prompt
    'Prompt_D0ToKsKK':   '${Dst}[D*(2010)+ -> ${D0}(X -> ${D0_daup}K+  ${D0_daum}K-  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+]CC',
    'Prompt_D0ToKsKPi':  '${Dst}[D*(2010)+ -> ${D0}(X -> ${D0_daup}K-  ${D0_daum}pi+ ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+]CC',
    'Prompt_D0ToKsPiK':  '${Dst}[D*(2010)+ -> ${D0}(X -> ${D0_daup}pi- ${D0_daum}K+  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+]CC',
    'Prompt_D0ToKsPiPi': '${Dst}[D*(2010)+ -> ${D0}(X -> ${D0_daup}pi+ ${D0_daum}pi- ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+]CC',
    # Single tagged
    'SingleTag_D0ToKsKK':   '${B}[B- -> ${D0}(X -> ${D0_daup}K+  ${D0_daum}K-  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${mu}mu-]CC',
    'SingleTag_D0ToKsKPi':  '${B}[B- -> ${D0}(X -> ${D0_daup}K-  ${D0_daum}pi+ ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${mu}mu-]CC',
    'SingleTag_D0ToKsPiK':  '${B}[B- -> ${D0}(X -> ${D0_daup}pi- ${D0_daum}K+  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${mu}mu-]CC',
    'SingleTag_D0ToKsPiPi': '${B}[B- -> ${D0}(X -> ${D0_daup}pi+ ${D0_daum}pi- ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${mu}mu-]CC',
    # Double tagged
    'DoubleTag_D0ToKsKK':   '${B}[Beauty -> ${Dst}(D*(2010)+ -> ${D0}(X -> ${D0_daup}K+  ${D0_daum}K-  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+) ${mu}mu-]CC',
    'DoubleTag_D0ToKsKPi':  '${B}[Beauty -> ${Dst}(D*(2010)+ -> ${D0}(X -> ${D0_daup}K-  ${D0_daum}pi+ ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+) ${mu}mu-]CC',
    'DoubleTag_D0ToKsPiK':  '${B}[Beauty -> ${Dst}(D*(2010)+ -> ${D0}(X -> ${D0_daup}pi- ${D0_daum}K+  ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+) ${mu}mu-]CC',
    'DoubleTag_D0ToKsPiPi': '${B}[Beauty -> ${Dst}(D*(2010)+ -> ${D0}(X -> ${D0_daup}pi+ ${D0_daum}pi- ${KS}(KS0 -> ${KS_pip}pi+ ${KS_pim}pi-)) ${pis}pi+) ${mu}mu-]CC',
}

input_containers = {
    # Dstar tagged
    'DstarTag_D0ToKsKK':   'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0{}Turbo/Particles',
    'DstarTag_D0ToKsKPi':  'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0{}Turbo/Particles',
    'DstarTag_D0ToKsPiK':  'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0{}Turbo/Particles',
    'DstarTag_D0ToKsPiPi': 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0{}Turbo/Particles',

    # Dstar tagged with Lifetime unbias line
    'DstarLTUNBTag_D0ToKsKK':   'Hlt2CharmHadDstp2D0Pip_D02KS0KmKp_KS0{}_LTUNBTurbo/Particles',
    'DstarLTUNBTag_D0ToKsKPi':  'Hlt2CharmHadDstp2D0Pip_D02KS0KmPip_KS0{}_LTUNBTurbo/Particles',
    'DstarLTUNBTag_D0ToKsPiK':  'Hlt2CharmHadDstp2D0Pip_D02KS0KpPim_KS0{}_LTUNBTurbo/Particles',
    'DstarLTUNBTag_D0ToKsPiPi': 'Hlt2CharmHadDstp2D0Pip_D02KS0PimPip_KS0{}_LTUNBTurbo/Particles',

    # Secondary tagged
    'SecondaryTag_D0ToKsKK':   'Phys/b2{}MuXKsKK{}CharmFromBSemiLine/Particles',
    'SecondaryTag_D0ToKsKPi':  'Phys/b2{}MuXKsKPi{}CharmFromBSemiLine/Particles',
    'SecondaryTag_D0ToKsPiK':  'Phys/b2{}MuXKsKPi{}CharmFromBSemiLine/Particles',
    'SecondaryTag_D0ToKsPiPi': 'Phys/b2{}MuXKsPiPi{}CharmFromBSemiLine/Particles',
}


def parseDescriptorTemplate(template):
    from string import Template
    # The argument 'template' is a Python string template
    # e.g. "${D}[D0 -> ${kaon}K- ${pion}pi+]CC"
    # Here ["D", "kaon", "pion"] are the branch names you want
    dd = Template(template)

    # This parses the temlate to get the list of branch names,
    # i.e. ["D", "kaon", "pion"]
    particles = [y[1] if len(y[1]) else y[2] for y in dd.pattern.findall(dd.template) if len(y[1]) or len(y[2])]

    # To form the decay descriptor, we need to mark all the particles
    # except for the top-level particle, which is included by default
    mapping = {p: '^' for p in particles}
    mapping[particles[0]] = ''

    # Make the descriptor
    # "[D0 -> ^K- ^pi+]CC"
    decay = dd.substitute(mapping)

    # Now make the branches
    branches = {}
    for p in particles:
        # Need a version of the descriptor where particle 'p' is marked but nothing else is.
        mapping = {q: '^' if p == q else '' for q in particles}
        branches[p] = dd.substitute(mapping)

    # Finally, add the branches to the DecayTreeTuple
    return decay, branches


def build_secondary_decay(name, input_data, muons):
    parent_cuts = [
        '(VFASPF(VCHI2)<25.0)',
        '(in_range({}, BPVCORRM, {}))'.format(2800*MeV, 9000*MeV),
        '(in_range({}, M, {}))'.format(2400*MeV, 5400*MeV),
    ]
    child_cuts = {
        'mu+': '(PT>1000*MeV) & (PROBNNmu >  0.1 ) & (BPVIPCHI2() > 4.0)'
    }

    if name.startswith('Dstar'):
        decay = ['[B0 -> D*(2010)+ mu-]cc']
    else:
        raise NotImplementedError(name)

    return CombineSelection(
        'BSequence'+name,
        inputs=[input_data, muons],
        DaughtersCuts=child_cuts,
        CombinationCut='APT>0',
        MotherCut=' & '.join(parent_cuts),
        DecayDescriptors=decay,
    )


def make_tuple(name, input_data, line = 'Turbo'):
    # Set up the momentum scaling
    if not DaVinci().Simulation:
        input_data = MomentumScaling(input_data, Turbo=DaVinci().Turbo, Year=DaVinci().DataType)

    # Create the DecayTreeTuple
    decay, branches = parseDescriptorTemplate(decay_descriptors[name[:-2]])
    dtt = TupleSelection(name, [input_data], Decay=decay, Branches=branches, ToolList=[
        'TupleToolEventInfo',
        'TupleToolRecoStats',
        'TupleToolL0Data',
        'TupleToolGeometry',
        'TupleToolPrimaries',
    ])
    constrain_vertex = not hasattr(dtt, 'B')

        
    # Add trigger information
    # Add Hlt2 Charm lines only to head
    trigTool = dtt.addTupleTool("TupleToolTrigger", name="TupleToolTrigger")
    Hlt2_prefix = input_containers['DstarTag'+name[name.find('_'):-2]] 
    Hlt2_prefix = Hlt2_prefix[:Hlt2_prefix.find('_KS0')]
    trigTool.VerboseHlt2 = True
    trigTool.TriggerList = [Hlt2_prefix + line_suf for line_suf in triggerHlt2Head] 

    # TISTOS
    tistosTool = dtt.addTupleTool("TupleToolTISTOS", name="TupleToolTISTOS")
    tistosTool.VerboseL0 = True
    tistosTool.VerboseHlt1 = True
    tistosTool.VerboseHlt2 = True
    tistosTool.TriggerList = triggerList
    tistosTool.TUS = True
    tistosTool.TPS = True

    ## Add Isolation variables
    if line == "Stripping":
        location = '/Event/Charm/'
        if hasattr(dtt,'Dst'):
            d_meson = 'Dstar'
        else:
            d_meson = 'D0'
        lineRelInfo = 'b2{}MuXKsPiPi{}CharmFromBSemiLine'.format(d_meson, track_type)

        relateTool = dtt.B.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone')
        relateTool.Variables= {}
        for coneAngle in [0.5,0.8,1.0,1.3,1.5,1.7,2.0]:
            conestr = str(coneAngle).replace('.','')

            relateTool.Variables.update({
                        "CONEPTASYM_B_{}".format(conestr) : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_B\',\'CONEPTASYM\',-1.)".format(conestr),
                        "CONEMULT_B_{}".format(conestr)   : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_B\',\'CONEMULT\',-1.)".format(conestr)  ,
                        "CONEANGLE_B_{}".format(conestr)  : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_B\',\'CONEANGLE\',-1.)".format(conestr) ,

                        "CONEPTASYM_Mu_{}".format(conestr) : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_Mu\',\'CONEPTASYM\',-1.)".format(conestr),
                        "CONEMULT_Mu_{}".format(conestr)   : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_Mu\',\'CONEMULT\',-1.)".format(conestr)  ,
                        "CONEANGLE_Mu_{}".format(conestr)  : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_Mu\',\'CONEANGLE\',-1.)".format(conestr) ,

                        "CONEPTASYM_D0_{}".format(conestr) : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_C\',\'CONEPTASYM\',-1.)".format(conestr),
                        "CONEMULT_D0_{}".format(conestr)   : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_C\',\'CONEMULT\',-1.)".format(conestr)  ,
                        "CONEANGLE_D0_{}".format(conestr)  : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/P2ConeVar{}_C\',\'CONEANGLE\',-1.)".format(conestr) ,

            })

        relateTool.Variables.update({
                    "smallestDeltaChi2OneTrack_B"      : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/VertexIsoInfo_B\',\'VTXISODCHI2ONETRACK\',-1.)",
                    "smallestDeltaChi2TwoTracks_B"     : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/VertexIsoInfo_B\',\'VTXISODCHI2TWOTRACK\',-1.)",
                    "smallestDeltaChi2MassOneTrack_B"  : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/VertexIsoInfo_B\',\'VTXISODCHI2MASSONETRACK\',-1.)",
                    "smallestDeltaChi2MassTwoTracks_B" : "RELINFO(\'"+location+"Phys/"+lineRelInfo+"/VertexIsoInfo_B\',\'VTXISODCHI2MASSTWOTRACK\',-1.)",
        })

    # Add LoKi variables
    dtt.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Variables').Variables = default_variables

    # Add DTF corrected mass for secondary sample  
    # DTF below does not have these varibles - consider adding separately 
    if hasattr(dtt, 'B'): 
        dtt.B.addTupleTool('LoKi::Hybrid::TupleTool/B').Variables = {
            'BPVCORRM'          : 'BPVCORRM',

            'DTF_CHI2VXNDOF'    : "DTF_FUN(CHI2VXNDOF, "+str(constrain_vertex)+")",
            'DTFKS_CHI2VXNDOF'  : "DTF_FUN(CHI2VXNDOF, "+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_CHI2VXNDOF': "DTF_FUN(CHI2VXNDOF, "+str(constrain_vertex)+", strings('D0','KS0'))",

            'DTF_CHI2'        : "DTF_CHI2("+str(constrain_vertex)+")",
            'DTFKS_CHI2'      : "DTF_CHI2("+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_CHI2'    : "DTF_CHI2("+str(constrain_vertex)+", strings('D0','KS0'))",
            
            'DTF_NDOF'        : "DTF_NDOF("+str(constrain_vertex)+")",
            'DTFKS_NDOF'      : "DTF_NDOF("+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_NDOF'    : "DTF_NDOF("+str(constrain_vertex)+", strings('D0','KS0'))",
            
            'DTF_CHI2NDOF'    : "DTF_CHI2NDOF("+str(constrain_vertex)+")",
            'DTFKS_CHI2NDOF'  : "DTF_CHI2NDOF("+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_CHI2NDOF': "DTF_CHI2NDOF("+str(constrain_vertex)+", strings('D0','KS0'))",
            
            'DTF_PROB'        : "DTF_PROB("+str(constrain_vertex)+")",
            'DTFKS_PROB'      : "DTF_PROB("+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_PROB'    : "DTF_PROB("+str(constrain_vertex)+", strings('D0','KS0'))",

            'DTF_M'           : "DTF_FUN(M, "+str(constrain_vertex)+")",
            'DTFKS_M'         : "DTF_FUN(M, "+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_M'       : "DTF_FUN(M, "+str(constrain_vertex)+", strings('D0','KS0'))",

            'DTF_BPVCORRM'    : "DTF_FUN(BPVCORRM, "+str(constrain_vertex)+")",
            'DTFKS_BPVCORRM'  : "DTF_FUN(BPVCORRM, "+str(constrain_vertex)+", strings('KS0'))",
            'DTFD0KS_BPVCORRM': "DTF_FUN(BPVCORRM, "+str(constrain_vertex)+", strings('D0','KS0'))",

            # Mu -> this line is only reasonable for DoubleTag stripping
            "DTF_MuDouble_PX"   : "DTF_FUN(CHILD(PX , 2), "+str(constrain_vertex)+")",
            "DTF_MuDouble_PY"   : "DTF_FUN(CHILD(PY , 2), "+str(constrain_vertex)+")",
            "DTF_MuDouble_PZ"   : "DTF_FUN(CHILD(PZ , 2), "+str(constrain_vertex)+")",
            "DTF_MuDouble_PT"   : "DTF_FUN(CHILD(PT , 2), "+str(constrain_vertex)+")",
            "DTF_MuDouble_E"    : "DTF_FUN(CHILD(E  , 2), "+str(constrain_vertex)+")",

            "DTFKS_MuDouble_PX"   : "DTF_FUN(CHILD(PX , 2), "+str(constrain_vertex)+", strings('KS0'))",
            "DTFKS_MuDouble_PY"   : "DTF_FUN(CHILD(PY , 2), "+str(constrain_vertex)+", strings('KS0'))",
            "DTFKS_MuDouble_PZ"   : "DTF_FUN(CHILD(PZ , 2), "+str(constrain_vertex)+", strings('KS0'))",
            "DTFKS_MuDouble_PT"   : "DTF_FUN(CHILD(PT , 2), "+str(constrain_vertex)+", strings('KS0'))",
            "DTFKS_MuDouble_E"    : "DTF_FUN(CHILD(E  , 2), "+str(constrain_vertex)+", strings('KS0'))",

            "DTFD0KS_MuDouble_PX"   : "DTF_FUN(CHILD(PX , 2), "+str(constrain_vertex)+", strings('D0','KS0'))",
            "DTFD0KS_MuDouble_PY"   : "DTF_FUN(CHILD(PY , 2), "+str(constrain_vertex)+", strings('D0','KS0'))",
            "DTFD0KS_MuDouble_PZ"   : "DTF_FUN(CHILD(PZ , 2), "+str(constrain_vertex)+", strings('D0','KS0'))",
            "DTFD0KS_MuDouble_PT"   : "DTF_FUN(CHILD(PT , 2), "+str(constrain_vertex)+", strings('D0','KS0'))",
            "DTFD0KS_MuDouble_E"    : "DTF_FUN(CHILD(E  , 2), "+str(constrain_vertex)+", strings('D0','KS0'))",


        }
        # Add ReFit variables
        refit = dtt.B.addTupleTool('TupleToolDecayTreeFitter/ReFit')
        refit.Verbose= True
        refit.constrainToOriginVertex = False
        refit.UpdateDaughters = True


    for particle in ['Dst']:
        if not hasattr(dtt, particle):
            continue
        getattr(dtt, particle).addTupleTool('LoKi::Hybrid::TupleTool/'+particle).Variables = {
            'BPVCORRM'          : 'BPVCORRM',
        }

    dtt.D0.addTupleTool('LoKi::Hybrid::TupleTool/D0').Variables = {
        'BPVLTIME':      'BPVLTIME()',
        'KS_D2DVVDCHI2': 'D2DVVDCHI2(3)',  # Cut introduced in 2017 data
    }

    dtt.D0.addTupleTool('TupleToolPropertime') # Add D0 decay time.

    for particle in ['B', 'Dst', 'D0', 'KS']:
        if not hasattr(dtt, particle):
            assert particle in ['B', 'Dst'], 'Only ever skip B or Dst'
            continue
        getattr(dtt, particle).addTupleTool('LoKi::Hybrid::TupleTool/CompositeParticle').Variables = composite_variables

    for particle in ['pis', 'D0_daum', 'D0_daup', 'KS_pim', 'KS_pip', 'mu']:
        if not hasattr(dtt, particle):
            assert particle in ['mu', 'pis'], 'Only ever skip mu or pis'
            continue
        getattr(dtt, particle).addTupleTool('LoKi::Hybrid::TupleTool/ChargedParticle').Variables = track_variables

    # Add decay tree fitter variables if we have a D*+ -> D0 pi decay
    if line == 'Turbo': 
        dtf_particle = dtt.Dst
        dtf_var = DTF_variables_Dst
    elif line == 'Stripping':
        if hasattr(dtt, 'Dst'): # DoubleTag 
            dtf_particle = dtt.Dst
            dtf_var = DTF_variable_Dst_Stripping
        else: # Only SingleTag sample - does not have Dst
            dtf_particle = dtt.B
            dtf_var = DTF_variables_B
    else:
        print "TESTTTT "+line
        print dtf_particle

    dtf_particle.addTupleTool('LoKi::Hybrid::TupleTool/LoKiDTF').Variables = {
        'DTF_KS_CTAU'    : "DTF_CTAU('KS0'==ABSID, "+str(constrain_vertex)+")",
        'DTFKS_KS_CTAU'  : "DTF_CTAU('KS0'==ABSID, "+str(constrain_vertex)+", strings(['KS0']))",
        'DTFD0KS_D0_CTAU': "DTF_CTAU('D0'==ABSID, "+str(constrain_vertex)+", strings(['D0', 'KS0']))",

        'DTF_D0_CTAU'    : "DTF_CTAU('D0'==ABSID, "+str(constrain_vertex)+")",
        'DTFKS_D0_CTAU'  : "DTF_CTAU('D0'==ABSID, "+str(constrain_vertex)+", strings(['KS0']))",
        'DTFD0KS_KS_CTAU': "DTF_CTAU('KS0'==ABSID, "+str(constrain_vertex)+", strings(['D0', 'KS0']))",
    }

    add_dtf(dtf_particle, '', constrain_vertex, [], dtf_var)
    add_dtf(dtf_particle, 'KS', constrain_vertex, ['KS0'], dtf_var)
    add_dtf(dtf_particle, 'D0KS', constrain_vertex, ['D0', 'KS0'], dtf_var)


    # Add MC TupleTool 
    if DaVinci().Simulation:
        # Add Bkg info
        dtt.addTool(TupleToolMCBackgroundInfo, name="TupleToolMCBackgroundInfo")
        dtt.TupleToolMCBackgroundInfo.Verbose=True

        # Add Truth matching
        dtt.addTool(TupleToolMCTruth, name="truth")
        dtt.truth.ToolList+=["MCTupleToolHierarchy"]
        dtt.ToolList+=["TupleToolMCTruth/truth"]


        # Keep for future work
        #from Configurables import MCDecayTreeTuple, MCTupleToolKinematic
        #MCTupTmp = MCDecayTreeTuple()
        #decay_MCDTT = ''
        #if "SingleTag" in name:
        #    decay_MCDTT = '[B- => (^X -> ^pi+ ^pi- ^(KS0 -> ^pi+ ^pi-)) ^mu-]CC'
        #elif "DoubleTag" in name:
        #    decay_MCDTT = '[Beauty => ^(D*(2010)+ -> ^(X -> ^pi+ ^pi- ^(KS0 -> ^pi+ ^pi-)) ^pi+) ^mu+]CC'
        #else : 
        #    decay_MCDTT =  decay
        #MCTupTmp.Decay = decay_MCDTT 
        #MCTupTmp.Branches = { "MCDTT_"+Key_branches : Val_branches for Key_branches, Val_branches in branches.items()} 


        #MCTupTmp.ToolList += ['MCTupleToolHierarchy']
        #MCTupTmp.ToolList += ['MCTupleToolKinematic']
        #MCTupTmp.ToolList += ['MCTupleToolReconstructed']
        #MCTupTmp.ToolList += ['MCTupleToolPID']


    

    DaVinci().UserAlgorithms.append(
        SelectionSequence("Seq"+dtt.name(), dtt).sequence()
    )

    #if DaVinci().Simulation and MCTupTmp != None : 
    #    DaVinci().UserAlgorithms.append(MCTupTmp)  

    return dtt


def add_dtf(particle, suffix, constrain_vertex, constrain_masses, DTF_variables):
    dtf = particle.addTupleTool(LoKi__Hybrid__Dict2Tuple, 'DTFTuple'+suffix)
    dtf.addTool(DTFDict, 'DTF')
    dtf.Source = 'LoKi::Hybrid::DTFDict/DTF'
    dtf.NumVar = 70
    dtf.DTF.constrainToOriginVertex = constrain_vertex
    dtf.DTF.daughtersToConstrain = constrain_masses
    dtf.DTF.addTool(LoKi__Hybrid__DictOfFunctors, 'dict')
    dtf.DTF.Source = 'LoKi::Hybrid::DictOfFunctors/dict'
    dtf.DTF.dict.Variables = {k.replace('DTF', 'DTF'+suffix): v for k, v in DTF_variables.items()}


# Use the TupleFile to figure out which kind of data we are processing
assert DaVinci().TupleFile.lower().endswith('.root'), 'TupleFile must end with .root'
name = DaVinci().TupleFile[:-len('.root')]

# Configure generic DaVinci options
DaVinci().InputType = 'MDST'
DaVinci().Lumi = not DaVinci().Simulation
MessageSvc().Format = '% F%40W%S%7W%R%T %0W%M'
if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType=DaVinci().DataType)

# Configure channel specific DaVinci options
if name.startswith('SecondaryTag'):
    # Secondary tagged uses the stripping
    # Contain Single tag + double tag
    DaVinci().Turbo = False
    DaVinci().RootInTES = '/Event/Charm' if not DaVinci().Simulation else 'D02KSPiPi.Strip'
    DaVinci().EventPreFilters = [LoKi_Filters(
        STRIP_Code="HLT_PASS_RE('Stripping"+input_containers[name].format('(D0|Dstar)' , '(DD|LL)').split('/')[1]+"Decision')"
    ).sequencer('PreFilter')]
else:
    # Prompt and double tagged uses turbo
    # Contain Dst and double tag
    DaVinci().Turbo = True
    if DaVinci().DataType == '2016':
        DaVinci().RootInTES = 'Turbo'
    else:
        DaVinci().RootInTES = 'Charmmultibody/Turbo'
        DaVinci().EventPreFilters = [LoKi_Filters(
            HLT2_Code="HLT_PASS_RE('"+input_containers[name].format('(DD|LL)').split('/')[0]+"Decision')",
        ).sequencer('PreFilter')]

# Add the DecayTreeTuples
muons = RebuildSelection(StdAllLooseMuons)
for track_type in ['LL', 'DD']:
    tuple_name = name+track_type

    if name.startswith('DstarTag'):
        input_data = AutomaticData(input_containers[name].format(track_type))
        make_tuple('Prompt'+tuple_name[len('DstarTag'):], input_data, "Turbo")
        # Use PersistReco to add a muon when requesting double tagged secondary candidates
        input_data = build_secondary_decay(tuple_name, input_data, muons)
        make_tuple('DoubleTag'+tuple_name[len('DstarTag'):], input_data, "Turbo")
    elif name.startswith('DstarLTUNBTag'):
        input_data = AutomaticData(input_containers[name].format(track_type))
        make_tuple('Prompt'+tuple_name[len('DstarLTUNBTag'):], input_data, "Turbo")
        # Use PersistReco to add a muon when requesting double tagged secondary candidates
        input_data = build_secondary_decay(tuple_name, input_data, muons)
        make_tuple('DoubleTag'+tuple_name[len('DstarLTUNBTag'):], input_data, "Turbo")
    else:
        assert name.startswith('SecondaryTag'), name
        input_data = AutomaticData(input_containers[name].format('D0', track_type))
        make_tuple('SingleTag'+tuple_name[len('SecondaryTag'):], input_data, "Stripping")
        input_data = AutomaticData(input_containers[name].format('Dstar', track_type))
        make_tuple('DoubleTag'+tuple_name[len('SecondaryTag'):], input_data, "Stripping")
