#!/usr/bin/env python
import argparse
import json
from os.path import dirname, join


all_years = [2015, 2016, 2017, 2018]
all_polarities = ['MagDown', 'MagUp']
channels = ['D0ToKsKK', 'D0ToKsKPi', 'D0ToKsPiK', 'D0ToKsPiPi']
davinci_versions = {
    2015: 'v44r7',
    2016: 'v44r7',
    2017: 'v44r7',
    2018: 'v44r7',
}
bk_paths = {
    'DstarLTUNBTag': {
        2016: '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo03a/94000000/CHARMKSHH.MDST',
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo04/94000000/CHARMMULTIBODY.MDST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo05/94000000/CHARMMULTIBODY.MDST',
    },
    'DstarTag': {
        2016: '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo03a/94000000/CHARMKSHH.MDST',
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo04/94000000/CHARMMULTIBODY.MDST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo05/94000000/CHARMMULTIBODY.MDST',
    },
    'SecondaryTag': {
        2015: '/LHCb/Collision15/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco15a/Stripping24r1/90000000/CHARM.MDST',
        2016: '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST',
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco17/Stripping29r2/90000000/CHARM.MDST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Reco18/Stripping34r0p1/90000000/CHARM.MDST' ,
    },
    'SecondaryTagBPMC': {
        2016: '/MC/2016/Beam6500GeV-2016-{polarity}-Nu1.6-25ns-Pythia8/Sim09f-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/12875523/D02KSPIPI.STRIP.MDST',
        2017: '/MC/2017/Beam6500GeV-2017-{polarity}-Nu1.6-25ns-Pythia8/Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/12875523/D02KSPIPI.STRIP.MDST',
        2018: '/MC/2018/Beam6500GeV-2018-{polarity}-Nu1.6-25ns-Pythia8/Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34r0p1Filtered/12875523/D02KSPIPI.STRIP.MDST',
    },
    'SecondaryTagB0MC': {
        2016: '/MC/2016/Beam6500GeV-2016-{polarity}-Nu1.6-25ns-Pythia8/Sim09f-ReDecay01/Trig0x6138160F/Reco16/Turbo03/Stripping28r1Filtered/11876125/D02KSPIPI.STRIP.MDST',
        2017: '/MC/2017/Beam6500GeV-2017-{polarity}-Nu1.6-25ns-Pythia8/Sim09h-ReDecay01/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2Filtered/11876125/D02KSPIPI.STRIP.MDST',
        2018: '/MC/2018/Beam6500GeV-2018-{polarity}-Nu1.6-25ns-Pythia8/Sim09h-ReDecay01/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34r0p1Filtered/11876125/D02KSPIPI.STRIP.MDST'
    },
    'SecondaryTagBPMC_nocuts': {
        2016: '/MC/2016/12875500/Beam6500GeV-2016-MagDown-Nu1.6-25ns-Pythia8/Sim09d/Trig0x6139160F/Reco16/Turbo03/Stripping28r1p1Filtered/Merge14/12875500/D02KSPIPI.STRIP.MDST'
    }
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
    parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
    parser.add_argument('--tag-types', nargs='+', choices=list(bk_paths.keys()), default=list(bk_paths.keys()))
    parser.add_argument('--channels', nargs='+', choices=channels, default=channels)
    parser.add_argument('--data-types', type=str, choices=['RealData', 'MC'], default='RealData')
    parser.add_argument('--force-full-lfn', action='store_false', help='Force the merge request testing to use a full LFN')
    args = parser.parse_args()

    results = {}

    if args.data_types == 'RealData':
        ChainOption = [     
                        'DataTypes/real_data.py',
                        ]
    elif args.data_types == 'MC':  
        ChainOption = [     
                        'DataTypes/mc.py',
                        ]
    else:
        raise NotImplementedError(args.data_types)

    for tag_type in args.tag_types:
        for channel in args.channels:
            for year in args.years:
                if year == 2015 and tag_type.startswith('Dstar'): 
                    continue
                for polarity in args.polarities:
                    key = '_'.join([str(year), polarity, tag_type, channel])
                    assert key not in results, 'Duplicate keys are not possible'
                    results[key] = {
                        'options': ChainOption + ['DataTypes/'+str(year)+'.py',
                                                 'TupleFiles/'+tag_type[:tag_type.find('Tag')+3]+'_'+channel+'.py']
                                               + (['DataTypes/{}.py'.format(polarity)] if args.data_types == 'MC' else []) 
                                               + [ 'main_options.py'],
                        'bookkeeping_path': bk_paths[tag_type][year].format(polarity=polarity),
                        'dq_flag': 'OK',
                        'application': 'DaVinci',
                        'application_version': davinci_versions[year],
                        'output_type': '_'.join(['CHARM', tag_type, channel]).upper()+'.ROOT'
                    }
                    if args.force_full_lfn:
                        results[key]['n_events'] = -1
                        results[key]['n_lfns'] = 2

    with open(join(dirname(__file__), 'info.json'), 'wt') as fp:
        json.dump(results, fp, indent=4, sort_keys=True)


if __name__ == '__main__':
    parse_args()
