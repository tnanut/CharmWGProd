# get the year
from Configurables import DaVinci
from copy import copy
the_year = DaVinci().DataType

# get the latest conditions for momenum scale etc.
from Configurables import CondDB, TrackScaleState
CondDB ( LatestGlobalTagByDataType = the_year )

# Set the input particle locations and apply Momentum Scaling
# ==============================================================================

if the_year == '2015' or the_year == '2016':
    rootInTES = '/Event/Turbo'
elif the_year == '2017' or the_year == '2018':
    rootInTES = '/Event/Charmtwobody/Turbo'

# particles
# ---------

# prompt
hh_keys = { 'KK' : 'KmKp'
           ,'PP' : 'PimPip'
           ,'RS' : 'KmPip'
           ,'WS' : 'KpPim'
}
# LTUNB
hh_keys_LTUNB = { 'KK_LTUNB' : 'KmKp'
                 ,'PP_LTUNB' : 'PimPip'
                 ,'RS_LTUNB' : 'KmPip'
                 ,'WS_LTUNB' : 'KpPim'
}
# semi-leptonic
hh_keys_SL = { 'KK_SL' : 'KmKp'
              ,'PP_SL' : 'PimPip'
              ,'KP_SL' : 'KmPip'
}
# doubly-tagged
hh_keys_DT = { 'KK_DT' : 'KmKp'
              ,'PP_DT' : 'PimPip'
              ,'KP_DT' : 'KmPip'
}

# read particles from TES
hlt2_line_names      = { k : 'Hlt2CharmHadDstp2D0Pip_D02{0}Turbo'.format(v)       for k,v in hh_keys.iteritems()}
hlt2_line_names.update({ k : 'Hlt2CharmHadDstp2D0Pip_D02{0}_LTUNBTurbo'.format(v) for k,v in hh_keys_LTUNB.iteritems()})
if the_year != '2015':
    hlt2_line_names.update({ k : 'Hlt2SLB_B2D0Mu_D02{0}Turbo'.format(v)  for k,v in hh_keys_SL.iteritems()})
    hlt2_line_names.update({ k : 'Hlt2SLB_B2DstMu_D02{0}Turbo'.format(v) for k,v in hh_keys_DT.iteritems()})

particle_locations = { k : '{0}/Particles'.format(v) for k,v in hlt2_line_names.iteritems() }

from PhysConf.Selections import AutomaticData
particles = { k : AutomaticData(v) for k,v in particle_locations.iteritems() }

# momentum scaling
from PhysConf.Selections import MomentumScaling
for k in particles.keys():
    particles[k] = MomentumScaling ( particles[k] , Turbo = True , Year = the_year )

# muons for prompt analysis
from PhysConf.Selections import RebuildSelection
from StandardParticles import StdAllLooseMuons
muons = RebuildSelection(StdAllLooseMuons)

# make the (Dst->RS, mu) combinations for the prompt NTuples (proxy of secondary decays)
# --------------------------------------------------------------------------------------
from PhysSelPython.Wrappers import Selection, SelectionSequence
from Configurables import CombineParticles

_Dstmu = CombineParticles("DstMu")
if the_year == '2015':
    _Dstmu.WriteP2PVRelations = False
    _Dstmu.InputPrimaryVertices = 'Primary'
if the_year == '2016':
    _Dstmu.CheckOverlapTool = 'LoKi::CheckOverlap'  # avoid that the same track is used twice in the same decay
_Dstmu.DecayDescriptors = [ '[B0 -> D*(2010)+ mu-]cc' , '[B0 -> D*(2010)+ mu+]cc' ]
_Dstmu.DaughtersCuts = {
      'D*(2010)+' : 'ALL'
    , 'D*(2010)-' : 'ALL'
    , 'mu-'       : '(PT > 800.0*MeV) & (P > 3.0*GeV) & (TRGHOSTPROB < 0.5) & (TRCHI2DOF < 5.0) & (MIPCHI2DV(PRIMARY) > 4.0)'
    , 'mu+'       : '(PT > 800.0*MeV) & (P > 3.0*GeV) & (TRGHOSTPROB < 0.5) & (TRCHI2DOF < 5.0) & (MIPCHI2DV(PRIMARY) > 4.0)'
}
_Dstmu.CombinationCut = "(AM > 2.0*GeV) & (AM < 10.0*GeV)"
_Dstmu.MotherCut = "(VFASPF(VCHI2/VDOF) < 25) & (BPVDIRA> 0.999) & (M > 3.0*GeV) & (M < 8.0*GeV)"
LooseDstMu = Selection("SelLooseDstMu", Algorithm = _Dstmu, RequiredSelections = [particles['RS'], muons])
DstMuSeq = SelectionSequence('DstMuSeq', TopSelection = LooseDstMu)
particles['Mu'] = DstMuSeq

# map from tuple to keys of input particles
tuple_keys = { 'Mu' : 'Mu'
              ,'KK' : 'KK'
              ,'PP' : 'PP'
              ,'RS' : 'RS'
              ,'WS' : 'WS'
              ,'KK_LTUNB' : 'KK_LTUNB'
              ,'PP_LTUNB' : 'PP_LTUNB'
              ,'RS_LTUNB' : 'RS_LTUNB'
              ,'WS_LTUNB' : 'WS_LTUNB'
}
if the_year != '2015':
    tuple_keys.update({ 'KK_SL' : 'KK_SL'
                       ,'PP_SL' : 'PP_SL'
                       ,'RS_SL' : 'KP_SL'
                       ,'WS_SL' : 'KP_SL'
                       ,'KK_DT' : 'KK_DT'
                       ,'PP_DT' : 'PP_DT'
                       ,'RS_DT' : 'KP_DT'
                       ,'WS_DT' : 'KP_DT'
    })


# inputs to make the tuples
inputs = { k : particles[tuple_keys[k]] for k in tuple_keys.iterkeys() }

# ensure ProbNN is calculated
# https://twiki.cern.ch/twiki/bin/view/LHCb/EnsureProbNNsCalculated
# ----------------------------------------------------------------------
from Configurables import GaudiSequencer, ChargedProtoANNPIDConf

probnn_sequence = {}
for k,v in hlt2_line_names.iteritems():
    probnn_sequence[k] = GaudiSequencer("{0}_ProbNNSeq".format(k))
    annpid = ChargedProtoANNPIDConf(
        k + "ProbNNalg",
        RecoSequencer=probnn_sequence[k],
        ProtoParticlesLocation="{0}/Protos".format(v)
    )

# DecayTreeTuples
# ==============================================================================

from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *

# K- pi+, K+ K-, pi+ pi- are reconstructed only as D0
# https://gitlab.cern.ch/lhcb/Hlt/blob/2018-patches/Hlt/Hlt2Lines/python/Hlt2Lines/CharmHad/D02HHLines.py
# https://gitlab.cern.ch/lhcb/Hlt/blob/2018-patches/Hlt/Hlt2Lines/python/Hlt2Lines/SLB/Lines.py
decay_descriptors = {
      "KK_LTUNB" : "           ${Dst}[D*(2010)+ -> ${D0}([D0]cc -> ${P1}K+  ${P2}K- ) ${sPi}pi+]CC"
    , "PP_LTUNB" : "           ${Dst}[D*(2010)+ -> ${D0}([D0]cc -> ${P1}pi+ ${P2}pi-) ${sPi}pi+]CC"
    , "RS_LTUNB" : "           ${Dst}[D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+]CC"
    , "WS_LTUNB" : "           ${Dst}[D*(2010)- -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi-]CC"
    , "KK"       : "           ${Dst}[D*(2010)+ -> ${D0}([D0]cc -> ${P1}K+  ${P2}K- ) ${sPi}pi+]CC"
    , "PP"       : "           ${Dst}[D*(2010)+ -> ${D0}([D0]cc -> ${P1}pi+ ${P2}pi-) ${sPi}pi+]CC"
    , "RS"       : "           ${Dst}[D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+]CC"
    , "WS"       : "           ${Dst}[D*(2010)- -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi-]CC"
    , "Mu"       : "${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+) ${Mu}[mu-]cc]CC"
    , "KK_DT"    : "${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}([D0]cc -> ${P1}K+  ${P2}K- ) ${sPi}pi+) ${Mu}[mu-]cc]CC"
    , "PP_DT"    : "${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}([D0]cc -> ${P1}pi+ ${P2}pi-) ${sPi}pi+) ${Mu}[mu-]cc]CC"
    , "RS_DT"    : "${B}[Xb -> ${Dst}(D*(2010)+ -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi+) ${Mu}[mu-]cc]CC"
    , "WS_DT"    : "${B}[Xb -> ${Dst}(D*(2010)- -> ${D0}(D0     -> ${P1}K-  ${P2}pi+) ${sPi}pi-) ${Mu}[mu+]cc]CC"
    , "KK_SL"    : "${B}[Xb ->                     ${D0}([D0]cc -> ${P1}K+  ${P2}K- )            ${Mu}mu-    ]CC"
    , "PP_SL"    : "${B}[Xb ->                     ${D0}([D0]cc -> ${P1}pi+ ${P2}pi-)            ${Mu}mu-    ]CC"
    , "RS_SL"    : "${B}[Xb ->                     ${D0}(D0     -> ${P1}K-  ${P2}pi+)            ${Mu}mu-    ]CC"
    , "WS_SL"    : "${B}[Xb ->                     ${D0}(D0     -> ${P1}K-  ${P2}pi+)            ${Mu}mu+    ]CC"
}

# Parser of the decay descriptor
# ------------------------------------------------------------------------------

def ParseDescriptorTemplate(template):
    """
        Input 'template' is a Python string template
        e.g. "${D0}[D0 -> ${P1}K- ${P2}pi+]CC"
        ["D0", "P1", "P2"] are the branch names you want

        Returned values:
           - Decay field of the ddt
           - branches to be added to ddt with addBranches
    """
    
    # parse the template to get the list of branch names,
    # e.g. ["D0", "P1", "P2"]
    import re
    particles = re.findall("\${(\w+)}", template)
    
    # To form the decay descriptor, we need to mark all the particles
    # except for the top-level particle, which is included by default
    mapping = {p: '^' for p in particles}
    mapping[particles[0]] = ''

    from string import Template
    dd = Template(template)

    # Make the descriptor
    # "[D0 -> ^K- ^pi+]CC"
    decay = dd.substitute(mapping)

    # Now make the branches
    branches = {}
    for p in particles:
        # Need a version of the descriptor where particle 'p' is marked but nothing else is.
        mapping = {q: '^' if p == q else '' for q in particles}
        branches[p] = dd.substitute(mapping)

    return decay, branches

# Variables to put in the tuple
# ------------------------------------------------------------------------------

# List of TupleTools
# path of header files starts with https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/
tools = [ 
     "TupleToolPropertime"  # DecayTreeTuple/src/TupleToolPropertime.h
    ,"TupleToolPid"         # DecayTreeTuple/src/TupleToolPid.h
    ,"TupleToolEventInfo"   # DecayTreeTupleTrigger/src/TupleToolEventInfo.h
    ,"TupleToolBeamSpot"    # DecayTreeTuple/src/TupleToolBeamSpot.h
    ,"TupleToolRecoStats"   # DecayTreeTupleReco/src/TupleToolRecoStats.h
]

# List of Triggers
trigger_list = [
     "L0B1gasDecision"
    ,"L0B2gasDecision"
    ,"L0DiEMDecision,lowMultDecision"
    ,"L0DiHadron,lowMultDecision"
    ,"L0DiMuonDecision"
    ,"L0DiMuon,lowMultDecision"
    ,"L0ElectronDecision"
    ,"L0Electron,lowMultDecision"
    ,"L0HadronDecision"
    ,"L0JetElDecision"
    ,"L0JetPhDecision"
    ,"L0MuonDecision"
    ,"L0Muon,lowMultDecision"
    ,"L0MuonEWDecision"
    ,"L0PhotonDecision"
    ,"L0Photon,lowMultDecision"
    ,"Hlt1TrackMVADecision"
    ,"Hlt1TrackMVALooseDecision"
    ,"Hlt1TrackMVATightDecision"
    ,"Hlt1TwoTrackMVADecision"
    ,"Hlt1TwoTrackMVALooseDecision"
    ,"Hlt1TwoTrackMVATightDecision"
    ,"Hlt1CalibTrackingKPiDecision"
    ,"Hlt1CalibTrackingKKDecision"
    ,"Hlt1CalibTrackingPiPiDecision"
]
trigger_list_SL_DT = [
     "Hlt1TrackMuonDecision"
    ,"Hlt1TrackMuonMVADecision"
    ,"Hlt1L0AnyDecision"
    ,"Hlt1MBNoBiasDecision"
    ,"Hlt2TopoMu2BodyDecision"
    ,"Hlt2TopoMu3BodyDecision"
    ,"Hlt2TopoMu4BodyDecision"
    ,"Hlt2Topo2BodyDecision"
    ,"Hlt2Topo3BodyDecision"
    ,"Hlt2Topo4BodyDecision"
    ,"Hlt2CharmHadInclDst2PiD02HHXBDTDecision"
    ,"Hlt2SingleMuonDecision"
]

#Loki Variables
lokiVarDst = {
     "DOCACHI2"     : "DOCACHI2(1,2)"
    ,"DOCA"         : "DOCA(1,2)"
}
lokiVarD = {
     "DOCACHI2"  : "DOCACHI2(1,2)"
    ,"DOCA"      : "DOCA(1,2)"
    ,"BPVLTIME"  : "BPVLTIME()"
}
lokiVar = {
     "ETA" : "ETA"
    ,"PHI" : "PHI"
}

#Configure TupleTools
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DTFDict as DTFDict

# Add DTF to the tuples
# from https://twiki.cern.ch/twiki/bin/view/LHCb/DaVinciTutorial9b
# ----------------------------------------------------------------
def AddDTF(dtt, constrain_PV, constrain_D0M):
    name = 'DTF{0}{1}'.format('_PV'  if constrain_PV  else '',
                              '_D0M' if constrain_D0M else '' )
    DictTuple = dtt.Dst.addTupleTool(LoKi__Hybrid__Dict2Tuple, "{0}_DictTuple".format(name))
    DictTuple.addTool(DTFDict, "DTFfoo")
    DictTuple.Source = "LoKi::Hybrid::DTFDict/DTFfoo"
    DictTuple.NumVar = 40 
    DictTuple.DTFfoo.constrainToOriginVertex = constrain_PV
    if constrain_D0M:
        DictTuple.DTFfoo.daughtersToConstrain = ["D0", "D~0"]
    DictTuple.DTFfoo.addTool(LoKi__Hybrid__DictOfFunctors, "dict")
    DictTuple.DTFfoo.Source = "LoKi::Hybrid::DictOfFunctors/dict"
    DictTuple.DTFfoo.dict.Variables = {
          "{0}_Dst_M".format(name)    : "M"
        , "{0}_Dst_PX".format(name)   : "PX"
        , "{0}_Dst_PY".format(name)   : "PY"
        , "{0}_Dst_PZ".format(name)   : "PZ"
        , "{0}_Dst_PT".format(name)   : "PT"
        , "{0}_Dst_P".format(name)    : "P"
        , "{0}_Dst_ETA".format(name)  : "ETA"
        , "{0}_Dst_PHI".format(name)  : "PHI"
        , "{0}_D0_M".format(name)     : "CHILD(1, M)"
        , "{0}_D0_PX".format(name)    : "CHILD(1, PX)"
        , "{0}_D0_PY".format(name)    : "CHILD(1, PY)"
        , "{0}_D0_PZ".format(name)    : "CHILD(1, PZ)"
        , "{0}_D0_PT".format(name)    : "CHILD(1, PT)"
        , "{0}_D0_P".format(name)     : "CHILD(1, P)"
        , "{0}_D0_ETA".format(name)   : "CHILD(1, ETA)"
        , "{0}_D0_PHI".format(name)   : "CHILD(1, PHI)"
        , "{0}_P1_PX".format(name)    : "CHILD(1, CHILD(2, PX))"
        , "{0}_P1_PY".format(name)    : "CHILD(1, CHILD(2, PY))"
        , "{0}_P1_PZ".format(name)    : "CHILD(1, CHILD(2, PZ))"
        , "{0}_P1_PT".format(name)    : "CHILD(1, CHILD(2, PT))"
        , "{0}_P1_P".format(name)     : "CHILD(1, CHILD(2, P))"
        , "{0}_P1_ETA".format(name)   : "CHILD(1, CHILD(2, ETA))"
        , "{0}_P1_PHI".format(name)   : "CHILD(1, CHILD(2, PHI))"
        , "{0}_P2_PX".format(name)    : "CHILD(1, CHILD(1, PX))"
        , "{0}_P2_PY".format(name)    : "CHILD(1, CHILD(1, PY))"
        , "{0}_P2_PZ".format(name)    : "CHILD(1, CHILD(1, PZ))"
        , "{0}_P2_PT".format(name)    : "CHILD(1, CHILD(1, PT))"
        , "{0}_P2_P".format(name)     : "CHILD(1, CHILD(1, P))"
        , "{0}_P2_ETA".format(name)   : "CHILD(1, CHILD(1, ETA))"
        , "{0}_P2_PHI".format(name)   : "CHILD(1, CHILD(1, PHI))"
        , "{0}_sPi_PX".format(name)   : "CHILD(2, PX)"
        , "{0}_sPi_PY".format(name)   : "CHILD(2, PY)"
        , "{0}_sPi_PZ".format(name)   : "CHILD(2, PZ)"
        , "{0}_sPi_PT".format(name)   : "CHILD(2, PT)"
        , "{0}_sPi_P".format(name)    : "CHILD(2, P)"
        , "{0}_sPi_ETA".format(name)  : "CHILD(2, ETA)"
        , "{0}_sPi_PHI".format(name)  : "CHILD(2, PHI)"
    }
    # the DTF is run once for each of the following variable
    Loki_DTF = dtt.Dst.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_{0}'.format(name))
    Loki_DTF.Variables = {
          "{0}_D0_CTAU".format(name)    :     "DTF_CTAU(1, {0}{1})".format(True if constrain_PV else False, ", strings(['D0', 'D~0'])" if constrain_D0M else "")
        , "{0}_D0_CTAUERR".format(name) : "DTF_CTAUERR( 1, {0}{1})".format(True if constrain_PV else False, ", strings(['D0', 'D~0'])" if constrain_D0M else "")
        , "{0}_CHI2".format(name)       :        "DTF_CHI2({0}{1})".format(True if constrain_PV else False, ", strings(['D0', 'D~0'])" if constrain_D0M else "")
        , "{0}_NDOF".format(name)       :        "DTF_NDOF({0}{1})".format(True if constrain_PV else False, ", strings(['D0', 'D~0'])" if constrain_D0M else "")
        , "{0}_CHI2NDOF".format(name)   :    "DTF_CHI2NDOF({0}{1})".format(True if constrain_PV else False, ", strings(['D0', 'D~0'])" if constrain_D0M else "")
    }

# Make the DecayTreeTuple
# key = KK, PP, RS, WS, Mu
# ------------------------------------------------------------------------------

def MakeTuple(key):

    decay, branches = ParseDescriptorTemplate(decay_descriptors[key])
    
    dtt = DecayTreeTuple("{0}_Tuple".format(key)) 
    dtt.TupleName = key
    dtt.Decay = decay
    dtt.addBranches( branches )
    dtt.Inputs = [ inputs[key].outputLocation() ]

    if the_year == '2015':
        dtt.WriteP2PVRelations = False
        dtt.InputPrimaryVertices = 'Primary'

    # use ReFitted PVs (not tested)
#    dtt.WriteP2PVRelations = False
#    dtt.InputPrimaryVertices = '_ReFitPVs'
#    dtt.ReFitPVs = True
    
    dtt.ToolList = copy(tools)

    # path of header files starts with https://gitlab.cern.ch/lhcb/Analysis/blob/master/Phys/
    dtt.addTupleTool("TupleToolTrackInfo").Verbose = True               # DecayTreeTupleReco/src/TupleToolTrackInfo.h
    dtt.addTupleTool("TupleToolGeometry" ).Verbose = True               # DecayTreeTuple/src/TupleToolGeometry.h
    dtt.addTupleTool("TupleToolKinematic").Verbose = True               # DecayTreeTuple/src/TupleToolKinematic.h
    dtt.addTupleTool("TupleToolANNPID").ANNPIDTunes = ["MC15TuneV1"]    # DecayTreeTupleANNPID/src/TupleToolANNPID.h
    primTool = dtt.addTupleTool("TupleToolPrimaries")                   # DecayTreeTuple/src/TupleToolPrimaries.h
    primTool.Verbose = True
    if the_year == '2015':
        primTool.InputLocation = 'Primary'

    # trigger global info for the event - DecayTreeTupleTrigger/src/TupleToolTrigger.h
    triggerTool = dtt.addTupleTool("TupleToolTrigger")
    triggerTool.VerboseL0   = True
    triggerTool.VerboseHlt1 = True
    triggerTool.VerboseHlt2 = True
    triggerTool.TriggerList = copy(trigger_list)
    
    # TISTOS - DecayTreeTupleTrigger/src/TupleToolTISTOS.h
    tistosTool = dtt.addTupleTool("TupleToolTISTOS")
    tistosTool.VerboseL0   = True
    tistosTool.VerboseHlt1 = True
    tistosTool.VerboseHlt2 = True
    tistosTool.FillHlt2    = True
    tistosTool.TriggerList = copy(trigger_list)
    
    # additional variables
    LoKi_All   = dtt.addTupleTool("LoKi::Hybrid::TupleTool/Loki_ETA_PHI")
    LoKi_D0    = dtt.D0.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_D0")
    LoKi_All.Variables   = lokiVar 
    LoKi_D0.Variables    = lokiVarD
    
    if not ('SL' in key):
        LoKi_DStar = dtt.Dst.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_DStar")
        LoKi_DStar.Variables = lokiVarDst

    # additional info for Mu, SL and DT samples only
    if key == 'Mu' or ('SL' in key) or ('DT' in key):
        dtt.B.addTupleTool("TupleToolCorrectedMass")    # DecayTreeTuple/src/TupleToolCorrectedMass.h
        loki_B = dtt.B.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B")
        loki_B.Variables={ "BPVMCORR" : "BPVCORRM"
                          ,"DOCACHI2" : "DOCACHI2(1,2)"
                          ,"DOCA"     : "DOCA(1,2)"
        }

    # additional info for SL and DT samples only
    if ('SL' in key) or ('DT' in key):
        triggerTool.TriggerList += trigger_list_SL_DT
        tistosTool.TriggerList  += trigger_list_SL_DT
        neutrinoTool = dtt.B.addTupleTool("TupleToolNeutrinoReco")
        neutrinoTool.Verbose = True
        neutrinoTool.MotherMass = 5279.63
    
        # Decay Tree Fitter
        dtt.B.addTupleTool('TupleToolDecayTreeFitter/DTF')
        dtt.B.DTF.constrainToOriginVertex = False
        dtt.B.DTF.Verbose = True
        dtt.B.DTF.daughtersToConstrain = []
        dtt.B.DTF.UpdateDaughters = False
    else:
        # info for prompt and LTUNB tuples only
        AddDTF(dtt, False, False)
        AddDTF(dtt, True,  False)
        AddDTF(dtt, False, True )
        AddDTF(dtt, True,  True )
        if ('TUNB' in key):
            # add prompt Hlt2 line
            triggerTool.TriggerList += [ hlt2_line_names[key[:2]] + 'Decision' ]
            tistosTool.TriggerList  += [ hlt2_line_names[key[:2]] + 'Decision' ]
        else:
            # add LTUNB Hlt2 line
            if key == 'Mu':
                triggerTool.TriggerList += [hlt2_line_names['RS_LTUNB'] + 'Decision' ]
                tistosTool.TriggerList  += [hlt2_line_names['RS_LTUNB'] + 'Decision' ]
            else:
                triggerTool.TriggerList += [hlt2_line_names[key+'_LTUNB'] + 'Decision' ]
                tistosTool.TriggerList  += [hlt2_line_names[key+'_LTUNB'] + 'Decision' ]

    return dtt

# make the tuples
# ---------------
tuples = [ MakeTuple(key) for key in tuple_keys.iterkeys() ]

# DaVinci parameters
# ==============================================================================

if the_year == '2015':
    from Configurables import DstConf, TurboConf
    DstConf().Turbo = True
    TurboConf().PersistReco = True

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters(
    HLT1_Code = "  HLT_PASS_RE('Hlt1.*TrackMVA.*Decision')"
                "| HLT_PASS_RE('Hlt1TrackMuon.*Decision')"
                "| HLT_PASS_RE('Hlt1CalibTracking.*Decision')",
    HLT2_Code =    "  HLT_PASS_RE('"+hlt2_line_names['KK']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['PP']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['RS']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['WS']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['KK_LTUNB']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['PP_LTUNB']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['RS_LTUNB']+"Decision') "
                   "| HLT_PASS_RE('"+hlt2_line_names['WS_LTUNB']+"Decision') "
                + ("| HLT_PASS_RE('"+hlt2_line_names['KK_SL']+"Decision')"
                   "| HLT_PASS_RE('"+hlt2_line_names['PP_SL']+"Decision')"
                   "| HLT_PASS_RE('"+hlt2_line_names['KP_SL']+"Decision')"
                   "| HLT_PASS_RE('"+hlt2_line_names['KK_DT']+"Decision')"
                   "| HLT_PASS_RE('"+hlt2_line_names['PP_DT']+"Decision')"
                   "| HLT_PASS_RE('"+hlt2_line_names['KP_DT']+"Decision')" if (the_year != "2015") else "")
    )
fltrSeq = fltrs.sequence('MyFilters')
DaVinci().EventPreFilters = [fltrSeq]
DaVinci().Simulation = False
DaVinci().SkipEvents = 0
DaVinci().EvtMax     = -1
DaVinci().PrintFreq  = 1000
DaVinci().Lumi       = not DaVinci().Simulation
DaVinci().TupleFile  = 'CHARM_D02HH_DVNTUPLE.ROOT'
DaVinci().appendToMainSequence([v for v in probnn_sequence.itervalues()])
DaVinci().UserAlgorithms.append(DstMuSeq.sequence())
DaVinci().UserAlgorithms.append(muons)
DaVinci().UserAlgorithms += [v for k,v in particles.iteritems() ]
DaVinci().UserAlgorithms += tuples
DaVinci().InputType      = 'MDST'   
DaVinci().RootInTES      = rootInTES
DaVinci().Turbo          = True     
