# WG production options for $` D^{0}\rightarrow h^+ h^- `$

The options contained in this directory generate the NTuples for:

1. prompt $` D^{\ast+} \rightarrow (D^{0}\rightarrow h^+ h^-) \pi^{+}_{s} `$ decays (for both standard and LTUNB triggers);
2. semileptonic $` \bar{B} \rightarrow (D^{0}\rightarrow h^+ h^-) \mu^{-} `$ decays;
3. doubly-tagged $` \bar{B}^{0} \rightarrow [D^{\ast+} \rightarrow  (D^{0}\rightarrow h^+ h^-) \pi^{+}_{s}] \mu^{-} `$ decays;

with $` h^+ h^- = K^- \pi^+,\; K^+ K^-,\; \pi^+ \pi^-,\; K^+ \pi^- `$.

General features:
  * Turbo data for 2015-2018 (prompt decays), 2016-2018 (semileptonic and doubly-tagged decays);
  * `DecayTreeFitter` algorithm applied to the:
    1. $` D^{\ast+} `$ decay chain, w/ and w/o the PV constraint and the $` D^{0} `$ mass constraint, for the prompt decays;
    2. $` \bar{B} \rightarrow ... `$ decay chain without constraints for the semileptonic and doubly-tagged decays;
  * `TupleToolNeutrinoReco` used for semileptonic and doubly-tagged decays;
  * combination of the RS prompt sample with a `PersistReco` muon to be used as proxy for secondary decays;
  * momentum scaling of all particles, except the persisted muons of the prompt sample.

Warnings:
  * the `MC15TuneV1_ProbNN` variables should always be used for PID (not the `ProbNN` ones, which have a worse tuning and are not even saved for 2015 muons).

## Generating the `info.json` file
The `info.json` file can be used using the `make_info_file.py` script.
Examples of valid arguments:

```bash
./make_info_file.py
./make_info_file.py --help
./make_info_file.py --years 2016 2017 --polarities MagDown
```

where the call without arguments generates an `info.json` file for all the available data.
