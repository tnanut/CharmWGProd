# Settings only for local testing
# ==============================================================================

from Configurables import DaVinci

the_year = DaVinci().DataType

DaVinci().SkipEvents = 10
DaVinci().EvtMax = 30000
DaVinci().PrintFreq = 1000
DaVinci().TupleFile = the_year + '.root'
from Gaudi.Configuration import *
if the_year == '2015':
    EventSelector().Input = [
            "DATAFILE='root://eoslhcb.cern.ch://eos/lhcb/user/t/tpajero/D0-timeDepCPV/MDST/2015/00051289_00000085_1.turbo.mdst'"
    ]
elif the_year == '2016':
    EventSelector().Input = [
            "DATAFILE='root://eoslhcb.cern.ch://eos/lhcb/user/t/tpajero/D0-timeDepCPV/MDST/2016/00076443_00000022_1.charmtwobody.mdst'"
    ]
elif the_year == '2017':
    EventSelector().Input = [
            "DATAFILE='root://eoslhcb.cern.ch://eos/lhcb/user/t/tpajero/D0-timeDepCPV/MDST/2017/00066595_00000092_1.charmtwobody.mdst'"
    ]
elif the_year == '2018':
    EventSelector().Input = [
            "DATAFILE='root://eoslhcb.cern.ch://eos/lhcb/user/t/tpajero/D0-timeDepCPV/MDST/2018/00077432_00000053_1.charmtwobody.mdst'"
    ]
