#!/usr/bin/env python
import argparse
import json
from os.path import dirname, join

all_years = [  
               2015
             , 2016
             , 2017
             , 2018 
            ]

all_polarities = [
                    'MagUp'
                  , 'MagDown'
                 ]

davinci_versions = {
                      2015 : 'v44r7'
                    , 2016 : 'v44r7'
                    , 2017 : 'v44r7'
                    , 2018 : 'v44r7'
                   }

bk_paths = {
               2015 : '/LHCb/Collision15/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo02/RawRemoved/94000000/TURBO.MDST'
             , 2016 : '/LHCb/Collision16/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo03a/94000000/CHARMTWOBODY.MDST'
             , 2017 : '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo04/94000000/CHARMTWOBODY.MDST'
             , 2018 : '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo05/94000000/CHARMTWOBODY.MDST'
            }

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
    parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
    parser.add_argument('--force-full-lfn', action='store_true', help='Force the merge request testing to use a full LFN')
    args = parser.parse_args()

    results = {}
    for year in args.years:
        for polarity in args.polarities:
            key = '_'.join([str(year), polarity])
            assert key not in results, 'Duplicate keys are not possible'
            results[key] = {
                'options': [
                    'DataTypes/'+str(year)+'.py',
                    'make_decay_tree_tuples.py'
                ],
                'bookkeeping_path': bk_paths[year].format(polarity=polarity),
                'dq_flag': 'OK',
                'application': 'DaVinci',
                'application_version': davinci_versions[year],
                'output_type': 'CHARM_D02HH_DVNTUPLE.ROOT'
            }
            if args.force_full_lfn:
                results[key]['n_events'] = -1
                results[key]['n_lfns'] = 2

    with open(join(dirname(__file__), 'info.json'), 'wt') as fp:
        json.dump(results, fp, indent=4, sort_keys=True)

if __name__ == '__main__':
    parse_args()
