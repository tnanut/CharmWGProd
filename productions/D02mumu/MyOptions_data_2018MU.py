import sys, os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/D02mumu')


import D0mumu_options as opts
from DV_Config import ConfigDaVinci

#######################To set#######################
DataType = 'CL' #CL or MC
DataYear = '18'
isTest = False
Mag = 'MU'

whichMC =['mu+', 'mu-']  # ['mu+', 'mu-'], ['pi+', 'pi-'], ['K-, 'pi+']

####################################################
 
opts.setalgs(DataType,whichMC)
ConfigDaVinci("CHARM_DATA_D02MUMU_2018_MU.root",DataType, DataYear, opts.algs,isTest=isTest, Mag=Mag )
