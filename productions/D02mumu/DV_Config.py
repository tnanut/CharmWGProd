from Gaudi.Configuration import *

MC_tags = {"15" : {    "DDDB"  :   "dddb-20170721-3",
                        "CONDDB_MU"    :   "sim-20161124-vc-mu100",
                        "CONDDB_MD"    :   "sim-20161124-vc-md100"
           },
           "16" : {    "DDDB"  :   "dddb-20170721-3",
                       "CONDDB_MU"    :   "sim-20170721-2-vc-mu100",
                       "CONDDB_MD"    :   "sim-20170721-2-vc-md100"
           },
           "17" : {    "DDDB"  :   "dddb-20170721-3",
                       "CONDDB_MU"    :   "sim-20180411-vc-mu100",
                       "CONDDB_MD"    :   "sim-20180411-vc-md100"
           },
           "18" : {    "DDDB"  :   "dddb-20170721-3",
                       "CONDDB_MU"    :   "sim-20190128-vc-mu100",
                       "CONDDB_MD"    :   "sim-20190128-vc-md100"
           }
        }

def ConfigDaVinci(Filename,DataType,DataYear,UserAlgs=[],isTest=False,Mag="") :
    from Configurables import DaVinci
    from Configurables import CondDB

    DaVinci().UserAlgorithms  += UserAlgs
    DaVinci().EvtMax    = -1
    DaVinci().PrintFreq = 10000
    if isTest :
        DaVinci().EvtMax     = 1000
        if DataType == "CL" :
            DaVinci().EvtMax = 100000
        DaVinci().PrintFreq  = 100

    DaVinci().TupleFile = Filename

    DaVinci().DataType = "20"+DataYear

    DaVinci().InputType = "MDST"

    if DataType == "CL" :
        DaVinci().RootInTES  = "/Event/Charm/" 
        DaVinci().Simulation = False
        DaVinci().Lumi       = True
        CondDB( LatestGlobalTagByDataType = "20" + DataYear)

    if DataType == "MC" :
#        DaVinci().RootInTES  = "/Event/AllStreams"
        DaVinci().RootInTES  = "/Event/D02MUMU"
#        if DataYear == "16" :
#            from PhysConf.Filters import LoKi_Filters
#            fltrs = LoKi_Filters (
#                VOID_Code = """                                                                                                                                                                                                                                      
#                EXISTS( '/Event/AllStreams/pMC/Particles' )                                                                                                                                                                                                          
#                """
#                )
#            DaVinci().EventPreFilters = fltrs.filters('MC')
        DaVinci().Simulation = True
        DaVinci().Lumi       = False
        DaVinci().DDDBtag =MC_tags[DataYear][ "DDDB"]
        DaVinci().CondDBtag=MC_tags[DataYear][ "CONDDB_"+Mag]
        

    from Configurables import EventTuple       
    DaVinci().UserAlgorithms += [ EventTuple("EventTuple") ]

	



