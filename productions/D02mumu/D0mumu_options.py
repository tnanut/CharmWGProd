from Gaudi.Configuration       import *
from GaudiKernel.SystemOfUnits import *
from Configurables import TupleToolDecay

from DecayTupleCreator import TupTmp, TupTmpMC
from DV_Relinfo import getLokiTool

#####################################################################
#
# Define DecayTreeTuple tuple
#
######################################################################
algs = []

def setalgs(DataType, whichMC) :

    if DataType == "CL" :
        isMC = False
    elif DataType == "MC" :
        isMC = True

    global TupTmp, TupTmpMC, algs
    if isMC :
        TupTmp = TupTmpMC

    D0mumuTuple = TupTmp.clone("D02mumu_tuple")
    D0mumuTuple.Inputs = ['Phys/DstarD02xxDst2PiD02mumuBox/Particles'] 
    D0mumuTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^mu+ ^mu-) ^pi+]CC'
    D0mumuTuple.Branches = {'Dst'  	: '[D*(2010)+ -> ([D0]cc -> mu+ mu-) pi+]CC',
                    		'D0'    	: '[D*(2010)+ -> ^([D0]cc -> mu+ mu-) pi+]CC',
                    		'lab1' 	: '[D*(2010)+ -> ([D0]cc -> ^mu+ mu-) pi+]CC',
                    		'lab2' 	: '[D*(2010)+ -> ([D0]cc -> mu+ ^mu-) pi+]CC',
                    		'pi'    : '[D*(2010)+ -> ([D0]cc -> mu+ mu-) ^pi+]CC'}
    D0mumuTuple.D0.TupleToolSubMass.Substitution       += [ "mu+ => pi+" ]
    D0mumuTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "mu+/mu- => pi+/pi-" ]
    getLokiTool(D0mumuTuple, "DstarD02xxDst2PiD02mumuBox", isMC)

    D0pipiTuple = TupTmp.clone("D02pipi_tuple")
    D0pipiTuple.Inputs = ['Phys/DstarD02xxDst2PiD02pipiBox/Particles'] 
    D0pipiTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^pi+ ^pi-) ^pi+]CC'
    D0pipiTuple.Branches = {'Dst'  : '[D*(2010)+ -> ([D0]cc -> pi+ pi-) pi+]CC',
                    		'D0'    : '[D*(2010)+ -> ^([D0]cc -> pi+ pi-) pi+]CC',
                    		'lab1' : '[D*(2010)+ -> ([D0]cc -> ^pi+ pi-) pi+]CC',
                    		'lab2' : '[D*(2010)+ -> ([D0]cc -> pi+ ^pi-) pi+]CC',
                    		'pi'    : '[D*(2010)+ -> ([D0]cc -> pi+ pi-) ^pi+]CC'}
    D0pipiTuple.D0.TupleToolSubMass.Substitution       += [ "pi+ => mu+" ]
    D0pipiTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "pi+/pi- => mu+/mu-" ]
    getLokiTool(D0pipiTuple, "DstarD02xxDst2PiD02pipiBox", isMC)

    D0kpiTuple = TupTmp.clone("D02Kpi_tuple")
    D0kpiTuple.Inputs = ['Phys/DstarD02xxDst2PiD02KpiBox/Particles'] 
    D0kpiTuple.Decay = '[D*(2010)+ -> ^([D0]cc -> ^K- ^pi+) ^pi+]CC'
    D0kpiTuple.Branches = {'Dst'  : '[D*(2010)+ -> ([D0]cc -> K- pi+) pi+]CC',
                    		'D0'    : '[D*(2010)+ -> ^([D0]cc -> K- pi+) pi+]CC',
                    		'lab1' : '[D*(2010)+ -> ([D0]cc -> ^K- pi+) pi+]CC',
                    		'lab2' : '[D*(2010)+ -> ([D0]cc -> K- ^pi+) pi+]CC',
                    		'pi': '[D*(2010)+ -> ([D0]cc -> K- pi+) ^pi+]CC'}
    D0kpiTuple.D0.TupleToolSubMass.Substitution       += [ "K- => mu-" ]
    # D0kpiTuple.D0.TupleToolSubMass.Substitution       += [ "pi+ => mu+" ]
    D0kpiTuple.D0.TupleToolSubMass.DoubleSubstitution += [ "K-/pi+ => mu-/mu+" ]
    getLokiTool(D0kpiTuple, "DstarD02xxDst2PiD02KpiBox", isMC)

    algs += [D0mumuTuple,D0pipiTuple,D0kpiTuple]


##################################################################################################

    #Add MCtuple

##################################################################################################
    if not isMC:
        return

    print "Adding MCDecayTreeTuple"

    from Configurables import MCDecayTreeTuple, MCTupleToolKinematic
    MCTupTmp = MCDecayTreeTuple()
    MCTupTmp.Decay = '[D*(2010)+ => ^([D0]cc -> ^' + whichMC[0] + ' ^' + whichMC[1] + ') ^pi+]CC'
    MCTupTmp.Branches = {'Dst'      : '[D*(2010)+ -> ([D0]cc -> ' + whichMC[0] +  whichMC[1] + ') pi+]CC',
                             'D0'        : '[D*(2010)+ -> ^([D0]cc -> ' + whichMC[0] +  whichMC[1] + ') pi+]CC',
                             'lab1'  : '[D*(2010)+ -> ([D0]cc -> ^' + whichMC[0] +  whichMC[1] +') pi+]CC',
                             'lab2'  : '[D*(2010)+ -> ([D0]cc -> ' + whichMC[0] + ' ^' +  whichMC[1] +') pi+]CC',
                             'pi'    : '[D*(2010)+ -> ([D0]cc -> ' + whichMC[0] + whichMC[1] + ') ^pi+]CC'}

    
    MCTupTmp.ToolList += ['MCTupleToolHierarchy']
    MCTupTmp.ToolList += ['MCTupleToolKinematic']
#    MCTupTmp.ToolList += ['MCTupleToolReconstructed']
    MCTupTmp.ToolList += ['MCTupleToolPID']
    
    algs += [MCTupTmp]
    print algs
