import sys, os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/D02mumu')

#cwd=os.getcwd()
#sys.path.append(cwd)


import D0mumu_options as opts
from DV_Config import ConfigDaVinci

#######################To set#######################
DataType = 'MC' #CL or MC
DataYear = '18'
isTest = False
Mag = 'MD'

whichMC =['K-', 'pi+'] # ['mu+', 'mu-'], ['pi+', 'pi-'], ['K-, 'pi+']

####################################################
 
opts.setalgs(DataType,whichMC)
ConfigDaVinci("CHARM_MC_D02KPI_2018_MD.root",DataType, DataYear, opts.algs,isTest=isTest, Mag=Mag )
