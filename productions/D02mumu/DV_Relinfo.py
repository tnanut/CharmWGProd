from Configurables import LoKi__Hybrid__TupleTool

def getLokiTool(Tuple, line, isMC) :
	if isMC :
#		location = "/Event/AllStreams/"
               location = "/Event/D02MUMU/"
	else : location = '/Event/Charm/' 
	my_LoKiCone_tool = Tuple.Dst.addTupleTool('LoKi__Hybrid__TupleTool/LoKi_Cone')
	my_LoKiCone_tool.Variables={
                "CONEANGLE_D_08"      : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD008\',\'CONEANGLE\',-1.)",
                "CONEMULT_D_08"       : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD008\',\'CONEMULT\', -1.)",
                "CONEPTASYM_D_08"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD008\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_Dstar_08"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst08\',\'CONEANGLE\',-1.)",
                "CONEMULT_Dstar_08"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst08\',\'CONEMULT\',-1.)",
                "CONEPTASYM_Dstar_08" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst08\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_plus_08"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus08\',\'CONEANGLE\',-1.)",
                "CONEMULT_plus_08"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus08\',\'CONEMULT\', -1.)",
                "CONEPTASYM_plus_08"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus08\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_minus_08"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus08\',\'CONEANGLE\',-1.)",
                "CONEMULT_minus_08"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus08\',\'CONEMULT\',-1.)",
                "CONEPTASYM_minus_08" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus08\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_pis_08"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis08\',\'CONEANGLE\',-1.)",
                "CONEMULT_pis_08"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis08\',\'CONEMULT\',-1.)",
                "CONEPTASYM_pis_08"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis08\',\'CONEPTASYM\',-1.)",

                "CONEANGLE_D_10"      : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD010\',\'CONEANGLE\',-1.)",
                "CONEMULT_D_10"       : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD010\',\'CONEMULT\', -1.)",
                "CONEPTASYM_D_10"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD010\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_Dstar_10"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst10\',\'CONEANGLE\',-1.)",
                "CONEMULT_Dstar_10"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst10\',\'CONEMULT\',-1.)",
                "CONEPTASYM_Dstar_10" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst10\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_plus_10"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus10\',\'CONEANGLE\',-1.)",
                "CONEMULT_plus_10"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus10\',\'CONEMULT\', -1.)",
                "CONEPTASYM_plus_10"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus10\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_minus_10"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus10\',\'CONEANGLE\',-1.)",
                "CONEMULT_minus_10"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus10\',\'CONEMULT\',-1.)",
                "CONEPTASYM_minus_10" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus10\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_pis_10"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis10\',\'CONEANGLE\',-1.)",
                "CONEMULT_pis_10"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis10\',\'CONEMULT\',-1.)",
                "CONEPTASYM_pis_10"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis10\',\'CONEPTASYM\',-1.)",

                "CONEANGLE_D_12"      : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD012\',\'CONEANGLE\',-1.)",
                "CONEMULT_D_12"       : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD012\',\'CONEMULT\', -1.)",
                "CONEPTASYM_D_12"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD012\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_Dstar_12"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst12\',\'CONEANGLE\',-1.)",
                "CONEMULT_Dstar_12"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst12\',\'CONEMULT\',-1.)",
                "CONEPTASYM_Dstar_12" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst12\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_plus_12"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus12\',\'CONEANGLE\',-1.)",
                "CONEMULT_plus_12"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus12\',\'CONEMULT\', -1.)",
                "CONEPTASYM_plus_12"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus12\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_minus_12"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus12\',\'CONEANGLE\',-1.)",
                "CONEMULT_minus_12"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus12\',\'CONEMULT\',-1.)",
                "CONEPTASYM_minus_12" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus12\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_pis_12"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis12\',\'CONEANGLE\',-1.)",
                "CONEMULT_pis_12"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis12\',\'CONEMULT\',-1.)",
                "CONEPTASYM_pis_12"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis12\',\'CONEPTASYM\',-1.)",

                "CONEANGLE_D_14"      : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD014\',\'CONEANGLE\',-1.)",
                "CONEMULT_D_14"       : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD014\',\'CONEMULT\', -1.)",
                "CONEPTASYM_D_14"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVD014\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_Dstar_14"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst14\',\'CONEANGLE\',-1.)",
                "CONEMULT_Dstar_14"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst14\',\'CONEMULT\',-1.)",
                "CONEPTASYM_Dstar_14" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVDst14\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_plus_14"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus14\',\'CONEANGLE\',-1.)",
                "CONEMULT_plus_14"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus14\',\'CONEMULT\', -1.)",
                "CONEPTASYM_plus_14"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVplus14\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_minus_14"  : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus14\',\'CONEANGLE\',-1.)",
                "CONEMULT_minus_14"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus14\',\'CONEMULT\',-1.)",
                "CONEPTASYM_minus_14" : "RELINFO(\'"+location+"Phys/"+line+"/P2CVminus14\',\'CONEPTASYM\',-1.)",
                "CONEANGLE_pis_14"    : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis14\',\'CONEANGLE\',-1.)",
                "CONEMULT_pis_14"     : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis14\',\'CONEMULT\',-1.)",
                "CONEPTASYM_pis_14"   : "RELINFO(\'"+location+"Phys/"+line+"/P2CVpis14\',\'CONEPTASYM\',-1.)"
	}
