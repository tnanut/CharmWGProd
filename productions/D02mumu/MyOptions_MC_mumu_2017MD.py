import sys, os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/D02mumu')


import D0mumu_options as opts
from DV_Config import ConfigDaVinci

#######################To set#######################
DataType = 'MC' #CL or MC
DataYear = '17'
isTest = False
Mag = 'MD'

whichMC =['mu+', 'mu-']  # ['mu+', 'mu-'], ['pi+', 'pi-'], ['K-, 'pi+']

####################################################
 
opts.setalgs(DataType,whichMC)
ConfigDaVinci("CHARM_MC_D02MUMU_2017_MD.root",DataType, DataYear, opts.algs,isTest=isTest, Mag=Mag )
