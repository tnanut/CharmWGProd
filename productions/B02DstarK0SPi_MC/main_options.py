#options for B0->D*K0SPi, D*->D0Pi, K0S->PiPi

from Gaudi.Configuration import *
from GaudiConf import IOHelper
from Configurables import DaVinci, DecayTreeTuple, CombineParticles, FilterDesktop, TupleToolVeto, TupleToolDalitz, TupleToolPhotonInfo, LoKi__Hybrid__TupleTool, TupleToolDecayTreeFitter
from Configurables import TupleToolTrigger, TupleToolTISTOS
from DecayTreeTuple.Configuration import *
from Configurables import TrackSmearState

# Stream and stripping line
stream = 'Bhadron' 
line_names = ['B02DstarKsPiDDDst2D0PiBeauty2CharmLine','B02DstarKsPiLLDst2D0PiBeauty2CharmLine','B02DstarKsPiDDWSDst2D0PiBeauty2CharmLine','B02DstarKsPiLLWSDst2D0PiBeauty2CharmLine']

######################### TRIGGER LINES ####################

commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
commonTriggerList += ['Hlt1'+x+'Decision' for x in ['TrackMVA','TwoTrackMVA']]

additionalD0TriggerList = ['Hlt2'+x+'Decision' for x in ['Hlt2CharmHadD02KmPipTurbo']];
additionalDstTriggerList = ['Hlt2'+x+'Decision' for x in ['Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo', 'Hlt2CharmHadDstp2D0Pip_D02KmPip_LTUNBTurbo']];
additionalBTriggerList = ['Hlt2'+x+'Decision' for x in ['Topo2Body', 'Topo3Body', 'Topo4Body']];

TriggerLists = {'lab0'     : commonTriggerList+additionalBTriggerList,
                'lab1'     : commonTriggerList+additionalDstTriggerList,    
                'lab2'     : commonTriggerList+additionalD0TriggerList,                           
                'lab3'     : commonTriggerList,
                'lab4'     : commonTriggerList,
                'lab5'     : commonTriggerList,
                'lab6'     : commonTriggerList,
                'lab7'     : commonTriggerList,
                'lab8'     : commonTriggerList,
                'lab9'     : commonTriggerList
                }


######################### NO SELECTIONS #########################
                      
############################ nTUPLEs ###########################

# Create ntuples to store B0 decays
ntuple_names = [ 'TupleB02DstarKS0DDPi','TupleB02DstarKS0LLPi','TupleB02DstarKS0WSDDPi','TupleB02DstarKS0WSLLPi'   ]


def CreateTree(intuple,idecay):

    ntuple = ntuple_names[intuple]
    line = line_names[intuple]
    
    B0Tree = DecayTreeTuple(ntuple)

    B0Tree.Inputs = ['/Event/AllStreams/Phys/{0}/Particles'.format(line)]
    if idecay == 0:
        B0Tree.Decay = '[B0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+) ^(KS0 -> ^pi+ ^pi-) ^pi-]CC'
    if idecay == 1:
        B0Tree.Decay = '[B0 -> ^(D*(2010)+ -> ^(D0 -> ^K- ^pi+) ^pi+) ^(KS0 -> ^pi+ ^pi-) ^pi+]CC'

    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
    B0Tree.ToolList = ['TupleTool'+tool for tool in GeneralTools]

    if idecay == 0:
        B0Tree.addBranches({'lab0': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi-]CC',
                            'lab1': '[B0 -> ^(D*(2010)+ ->  (D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi-]CC', #D*
                            'lab2': '[B0 -> (D*(2010)+ ->  ^(D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi-]CC', #D0 (D*)
                            'lab3': '[B0 -> (D*(2010)+ ->  (D0 -> ^K- pi+) pi+) (KS0 -> pi+ pi-) pi-]CC', #K- (D0)
                            'lab4': '[B0 -> (D*(2010)+ ->  (D0 -> K- ^pi+) pi+) (KS0 -> pi+ pi-) pi-]CC', #pi+ (D0)
                            'lab5': '[B0 -> (D*(2010)+ ->  (D0 -> K- pi+) ^pi+) (KS0 -> pi+ pi-) pi-]CC', #pi+ (D*)
                            'lab6': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  ^(KS0 -> pi+ pi-) pi-]CC', #K0S
                            'lab7': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> ^pi+ pi-) pi-]CC', #pi+ (K0S)
                            'lab8': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> pi+ ^pi-) pi-]CC', #pi- (K0S)
                            'lab9': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> pi+ pi-) ^pi-]CC'  #pi-
                            })

    if idecay == 1:
        B0Tree.addBranches({'lab0': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi+]CC',
                            'lab1': '[B0 -> ^(D*(2010)+ ->  (D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi+]CC', #D*
                            'lab2': '[B0 -> (D*(2010)+ ->  ^(D0 -> K- pi+) pi+) (KS0 -> pi+ pi-) pi+]CC', #D0 (D*)
                            'lab3': '[B0 -> (D*(2010)+ ->  (D0 -> ^K- pi+) pi+) (KS0 -> pi+ pi-) pi+]CC', #K- (D0)
                            'lab4': '[B0 -> (D*(2010)+ ->  (D0 -> K- ^pi+) pi+) (KS0 -> pi+ pi-) pi+]CC', #pi+ (D0)
                            'lab5': '[B0 -> (D*(2010)+ ->  (D0 -> K- pi+) ^pi+) (KS0 -> pi+ pi-) pi+]CC', #pi+ (D*)
                            'lab6': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  ^(KS0 -> pi+ pi-) pi+]CC', #K0S
                            'lab7': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> ^pi+ pi-) pi+]CC', #pi+ (K0S)
                            'lab8': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> pi+ ^pi-) pi+]CC', #pi- (K0S)
                            'lab9': '[B0 -> (D*(2010)+ -> (D0 -> K- pi+) pi+)  (KS0 -> pi+ pi-) ^pi+]CC'  #pi-
                            })
        
    B0Tree.UseLabXSyntax = True
    B0Tree.RevertToPositiveID = False
    
#DFT no mass constraint
    B0_Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), 'lab0') ]
    fit = B0_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose = True
    fit.constrainToOriginVertex = True
    fit.UpdateDaughters = True
    
#DTF with mass constraints
    B0_Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), 'lab0') ]
    fit2 = B0_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit2')
    fit2.Verbose = True
    fit2.constrainToOriginVertex = True
    fit2.daughtersToConstrain += ['KS0', 'D0', 'D*(2010)+']
    fit2.UpdateDaughters = True
    
    for nodeName, tList in TriggerLists.iteritems():    
        Node = B0Tree.allConfigurables['%s.%s' % ( B0Tree.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList

    return B0Tree
 #end function CreateTree

Tree = [CreateTree(intuple,0) for intuple in [0,1]]
Tree2 = [CreateTree(intuple,1) for intuple in [2,3]]
       
#algos are executed ACCORDING TO the sequence specified below, sequence of operation has to be kept
smear = TrackSmearState ('Smear' )
DaVinci().InputType = 'DST'
#DaVinci().RootInTES = '/Event/{0}'.format(stream) #to be used for micro-DSTs
DaVinci().UserAlgorithms += Tree
DaVinci().UserAlgorithms += Tree2
DaVinci().TupleFile = 'MCntuple.root'
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().appendToMainSequence( [smear] )
