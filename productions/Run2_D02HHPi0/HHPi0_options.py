triglist=[
    'L0HadronDecision',
    'L0PhotonDecision',
    #          'L0MuonDecision',
#          'Hlt1TrackMuonDecision',
    'Hlt1TrackAllL0Decision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
# 	  'Hlt2CharmHadDstp2D0Pip_D02.*Pi0_Pi0.*Decision',
    'Hlt2CharmHadInclDst2PiD02HHXBDTDecision',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0RDecision',
    'Hlt2CharmHadDstp2D0Pip_D02PimPipPi0_Pi0MDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0RDecision',
    'Hlt2CharmHadDstp2D0Pip_D02KmPipPi0_Pi0MDecision',]

from DecayTreeTuple.Configuration import  GaudiSequencer
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import  DaVinci, DecayTreeTuple
from Configurables import  GaudiSequencer
from Configurables import  CombineParticles,  OfflineVertexFitter, TupleToolPid, TupleToolTrackInfo, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolEventInfo, TupleToolGeometry, TupleToolRecoStats, TupleToolTrackPosition
from Configurables import LoKi__Hybrid__EvtTupleTool as MyTool 
from Configurables import TupleToolDecay, TupleToolTrigger, TupleToolTISTOS
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import FilterDesktop
from Configurables import TupleToolDecayTreeFitter

from Configurables import TupleToolProtoPData
from Configurables import TupleToolPhotonInfo
from Configurables import TupleToolPi0Info
from Configurables import TupleToolCaloHypo
#from Configurables import TupleToolGammaPi0

from Configurables import CondDB, LHCbApp

from Configurables import DstConf
DstConf().EnableUnpack = ["Reconstruction",
                          "Stripping"]

line_infos = [
# D from prompt D*
    ['Phys/DstarD0ToHHPi0_pipipi0_R_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^pi- ^pi+) ^(pi0 -> ^gamma ^gamma)) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DPiPiPi0R'],
    ['Phys/DstarD0ToHHPi0_pipipi0_M_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^pi- ^pi+) ^pi0) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DPiPiPi0M'],
    ['Phys/DstarD0ToHHPi0_Kpipi0_R_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K- ^pi+) ^(pi0 -> ^gamma ^gamma)) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKPiPi0R'],
    ['Phys/DstarD0ToHHPi0_Kpipi0_M_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K- ^pi+) ^pi0) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKPiPi0M'],
    ['Phys/DstarD0ToHHPi0_Kpipi0_R_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K+ ^pi-) ^(pi0 -> ^gamma ^gamma)) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKPiPi0WSR'],
    ['Phys/DstarD0ToHHPi0_Kpipi0_M_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K+ ^pi-) ^pi0) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKPiPi0WSM'],
    ['Phys/DstarD0ToHHPi0_KKpi0_R_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K- ^K+) ^(pi0 -> ^gamma ^gamma)) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKKPi0R'],
    ['Phys/DstarD0ToHHPi0_KKpi0_M_Line/Particles', '[D*(2010)+ -> ^(D0 -> ^(K*(892)0 -> ^K- ^K+) ^pi0) ^pi+]CC', 'DfromPromptDstr', 'Dstr2DKKPi0M'],
    ]


def makeDecayTreeTuple(location, decay, decay_family, tuple_name ):
    tuple = DecayTreeTuple( tuple_name )
    
    # RootInTES needed for MDST
    tuple.RootInTES = "/Event/Charm/"
    tuple.Inputs = [ location ]
    
    tuple.Decay = decay

    LoKi_mu=LoKi__Hybrid__TupleTool("LoKi_mu")
    LoKi_mu.Variables =  {
         "LOKI_ETA" : "ETA"
        , "LOKI_PHI" : "PHI"
         }

    if 'DfromPromptDstr' == decay_family:
        if 'Dstr2DPiPiPi0R' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) (pi0 -> gamma gamma)) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> pi- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) (pi0 -> gamma gamma)) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> pi- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^pi- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- ^pi+) (pi0 -> gamma gamma)) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) ^(pi0 -> gamma gamma)) pi+]CC",
                "gamma1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) (pi0 -> ^gamma gamma)) pi+]CC",
                "gamma2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) (pi0 -> gamma ^gamma)) pi+]CC"
                }
        elif 'Dstr2DPiPiPi0M' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) pi0) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> pi- pi+) pi0) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) pi0) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> pi- pi+) pi0) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^pi- pi+) pi0) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- ^pi+) pi0) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> pi- pi+) ^pi0) pi+]CC",
                }
        elif 'Dstr2DKKPi0R' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) (pi0 -> gamma gamma)) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K- K+) (pi0 -> gamma gamma)) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) (pi0 -> gamma gamma)) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K- K+) (pi0 -> gamma gamma)) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K- K+) (pi0 -> gamma gamma)) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- ^K+) (pi0 -> gamma gamma)) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) ^(pi0 -> gamma gamma)) pi+]CC",
                "gamma1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) (pi0 -> ^gamma gamma)) pi+]CC",
                "gamma2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) (pi0 -> gamma ^gamma)) pi+]CC"
                }
        elif 'Dstr2DKKPi0M' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) pi0) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K- K+) pi0) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) pi0) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K- K+) pi0) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K- K+) pi0) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- ^K+) pi0) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- K+) ^pi0) pi+]CC",
                }
        elif 'Dstr2DKPiPi0R' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) (pi0 -> gamma gamma)) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) (pi0 -> gamma gamma)) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K- pi+) (pi0 -> gamma gamma)) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- ^pi+) (pi0 -> gamma gamma)) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) ^(pi0 -> gamma gamma)) pi+]CC",
                "gamma1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) (pi0 -> ^gamma gamma)) pi+]CC",
                "gamma2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) (pi0 -> gamma ^gamma)) pi+]CC"
                }
        elif 'Dstr2DKPiPi0M' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) pi0) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K- pi+) pi0) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) pi0) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K- pi+) pi0) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K- pi+) pi0) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- ^pi+) pi0) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K- pi+) ^pi0) pi+]CC",
                }
        elif 'Dstr2DKPiPi0WSR' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) (pi0 -> gamma gamma)) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K+ pi-) (pi0 -> gamma gamma)) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) (pi0 -> gamma gamma)) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K+ pi-) (pi0 -> gamma gamma)) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K+ pi-) (pi0 -> gamma gamma)) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ ^pi-) (pi0 -> gamma gamma)) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) ^(pi0 -> gamma gamma)) pi+]CC",
                "gamma1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) (pi0 -> ^gamma gamma)) pi+]CC",
                "gamma2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) (pi0 -> gamma ^gamma)) pi+]CC"
                }
        elif 'Dstr2DKPiPi0WSM' == tuple_name:
            tuple.Branches = {
                "Dstr" : "^([D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) pi0) pi+]CC)",
                "D" : "[D*(2010)+ -> ^(D0 -> (K*(892)0 -> K+ pi-) pi0) pi+]CC",
                "piSoft" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) pi0) ^pi+]CC",
                "Kstr" : "[D*(2010)+ -> (D0 -> ^(K*(892)0 -> K+ pi-) pi0) pi+]CC",
                "H1" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> ^K+ pi-) pi0) pi+]CC",
                "H2" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ ^pi-) pi0) pi+]CC",
                "pi0" : "[D*(2010)+ -> (D0 -> (K*(892)0 -> K+ pi-) ^pi0) pi+]CC",
                }
            
        tuple.addTool(TupleToolDecay, name = 'Dstr')
        tuple.addTool(TupleToolDecay, name = 'D')
        tuple.addTool(TupleToolDecay, name = 'piSoft')
        tuple.addTool(TupleToolDecay, name = 'Kstr')
        tuple.addTool(TupleToolDecay, name = 'H1')
        tuple.addTool(TupleToolDecay, name = 'H2')
        tuple.addTool(TupleToolDecay, name = 'pi0')

    if 'Pi0R' in tuple_name:
        tuple.addTool(TupleToolDecay, name = 'gamma1')
        tuple.addTool(TupleToolDecay, name = 'gamma2')

        tuple.H1.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_mu"]
        tuple.H1.addTool(LoKi_mu)
        tuple.H2.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_mu"]
        tuple.H2.addTool(LoKi_mu)
        tuple.piSoft.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_mu"]
        tuple.piSoft.addTool(LoKi_mu)
        tuple.D.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_mu"]
        tuple.D.addTool(LoKi_mu)


    tuple.ToolList = [
        "TupleToolAngles",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        "TupleToolProtoPData",
        "TupleToolTrigger",
        ]


    # RecoStats to filling SpdMult, etc
    tuple.addTool(TupleToolRecoStats, name="TupleToolRecoStats")
    tuple.TupleToolRecoStats.Verbose=False
    
    #tuple.Dstr.ToolList += [ "TupleToolTISTOS" ]
    tuple.addTupleTool( "TupleToolTISTOS" )
    tuple.TupleToolTISTOS.Verbose=True
    tuple.TupleToolTISTOS.VerboseL0=True
    tuple.TupleToolTISTOS.VerboseHlt1=True
    tuple.TupleToolTISTOS.VerboseHlt2=True
    tuple.TupleToolTISTOS.TriggerList = triglist
    
    tuple.addTool(TupleToolProtoPData,name="TupleToolProtoPData") 
    tuple.TupleToolProtoPData.DataList = ["CaloEcalE","CaloHcalE","ProtoPNeutral","IsPhoton"] 

    LoKi_DTFMASS = LoKi__Hybrid__TupleTool("LoKi_DTFMASS")

    if 'Pi0R' in tuple_name:
        LoKi_DTFMASS.Variables = {
        ############ with D0 & pi0 mass constraint
        #
	#
	#
        # mass of D* aft Mass constraints
        "FIT_M"              : "DTF_FUN ( M ,           True, 'pi0')"
        , "FIT_M1"           : "DTF_FUN ( M ,           True, 'D0')"
        , "FIT_M2"           : "DTF_FUN ( M ,           True, strings(['D0','pi0']) )"
        , "FIT_M3"           : "DTF_FUN ( M ,           True )"
        # chi^2 of fit
        , "FIT_CHI2"      : "DTF_CHI2(               True, strings(['D0','pi0']) )"
        # ndof of fit
        , "FIT_NDOF"      : "DTF_NDOF(               True, strings(['D0','pi0']) )"
        # Momentum of D*
        , "FIT_P"         : "DTF_FUN ( P ,           True, strings(['D0','pi0']) )"
        # Eta of D*
        , "FIT_ETA"         : "DTF_FUN ( ETA ,           True, strings(['D0','pi0']) )"
        # Transerve Momentum of D
        , "FIT_PT"        : "DTF_FUN ( PT ,          True, strings(['D0','pi0']) )"
        # Lifetime of D*
        , "FIT_LT"        : "DTF_FUN (BPVLTIME('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        # VertexChi2 of D*
        , "FIT_VTXCHI2DOF": "DTF_FUN (VFASPF(VCHI2/VDOF),True, strings(['D0','pi0']))"
        # Min IP Chi2 of D*
        , "FIT_MIPCHI2"   : "DTF_FUN (MIPCHI2DV(PRIMARY),True, strings(['D0','pi0']))"
        # DOCA of D*
        , "FIT_MAXDOCA"   : "DTF_FUN (DOCAMAX,True, strings(['D0','pi0']))"
        # DIRA of D*
        , "FIT_DIRABPV"   : "DTF_FUN(BPVDIRA,True, strings(['D0','pi0']))"
        #  
        ####new added wrt to v21
        # IPCHI2 of D*
        , "FIT_IPCHI2BPV"   : "DTF_FUN(BPVIPCHI2(),True, strings(['D0','pi0']))"
        # DVCHI2 of D*
        , "FIT_DVCHI2BPV"   : "DTF_FUN(BPVVDCHI2,True, strings(['D0','pi0']))"
        # LifeTime CHI2 of D*
        , "FIT_LTCHI2BPV"   : "DTF_FUN(BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        # Fitted LifeTime CHI2 of D*
        , "FIT_LTFITCHI2BPV"   : "DTF_FUN(BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        #
        , "FIT_PX"         : "DTF_FUN ( PX ,           True, strings(['D0','pi0']) )"
        , "FIT_PY"         : "DTF_FUN ( PY ,           True, strings(['D0','pi0']) )"
        , "FIT_PZ"         : "DTF_FUN ( PZ ,           True, strings(['D0','pi0']) )"
        ####
        #
        #
        # Mass of D0
        , "FIT_DM"       : "DTF_FUN ( CHILD(M, 1),  True, 'pi0' )"
        , "FIT_DM1"       : "DTF_FUN ( CHILD(M, 1),  True, 'D0')"
        , "FIT_DM2"       : "DTF_FUN ( CHILD(M, 1),  True, strings(['D0','pi0']) )"
        , "FIT_DM3"       : "DTF_FUN ( CHILD(M, 1),  True )"
        # Momentum of D0
        , "FIT_DP"       : "DTF_FUN ( CHILD(P, 1),  True, strings(['D0','pi0']) )"
        # Eta of D0
        , "FIT_DETA"       : "DTF_FUN ( CHILD(ETA, 1),  True, strings(['D0','pi0']) )"
        # Transverve Momenutm of D0
        , "FIT_DPT"      : "DTF_FUN ( CHILD(PT, 1), True, strings(['D0','pi0']) )"
        # D0 DIRA
        , "FIT_DDIRABPV"    : "DTF_FUN ( CHILD(BPVDIRA, 1), True, strings(['D0','pi0']))"
        # D0 Min IP Chi2
        , "FIT_DMIPCHI2"   : "DTF_FUN ( CHILD(MIPCHI2DV(PRIMARY), 1), True, strings(['D0','pi0']))"
        # D0 VertexChi2
        , "FIT_DVTXCHI2DOF" : "DTF_FUN ( CHILD(VFASPF(VCHI2/VDOF), 1), True, strings(['D0','pi0']))"
        # D0 Lifetime
        , "FIT_DLT" : "DTF_FUN ( CHILD(BPVLTIME('PropertimeFitter/ProperTime::PUBLIC'),1),True,strings(['D0','pi0']))"
        #
        ####new added wrt to v21 (=new1)
        # IPCHI2 of D0
        , "FIT_DIPCHI2BPV"   : "DTF_FUN( CHILD(BPVIPCHI2(), 1), True, strings(['D0','pi0']))"
        # DVCHI2 of D0
        , "FIT_DDVCHI2BPV"   : "DTF_FUN( CHILD(BPVVDCHI2, 1), True, strings(['D0','pi0']))"
        # LifeTime CHI2 of D0
        , "FIT_DLTCHI2BPV"   : "DTF_FUN( CHILD(BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC'), 1), True, strings(['D0','pi0']))"
        # Fitted LifeTime CHI2 of D0
        , "FIT_DLTFITCHI2BPV"   : "DTF_FUN( CHILD(BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC'), 1), True, strings(['D0','pi0']))"
        #
        , "FIT_DPX"         : "DTF_FUN (  CHILD(PX , 1),            True, strings(['D0','pi0']) )"
        , "FIT_DPY"         : "DTF_FUN (  CHILD(PY , 1),            True, strings(['D0','pi0']) )"
        , "FIT_DPZ"         : "DTF_FUN (  CHILD(PZ , 1),            True, strings(['D0','pi0']) )"
        ####
        #
        #
        # Momentum of Soft Pi
        , "FIT_PisP"       : "DTF_FUN ( CHILD(P, 2),  True, strings(['D0','pi0']) )"
        # Transverve Momenutm of Soft Pi
        , "FIT_PisPT"      : "DTF_FUN ( CHILD(PT, 2), True, strings(['D0','pi0']) )"
        # Soft Pi DIRA
#        , "FIT_PisDIRABPV"    : "DTF_FUN ( CHILD(BPVDIRA, 2), True, strings(['D0','pi0']))"
        # Soft Pi Min IP Chi2
        , "FIT_PisMIPCHI2"   : "DTF_FUN ( CHILD(MIPCHI2DV(PRIMARY), 2), True, strings(['D0','pi0']))"
        #
        , "FIT_PisPX"         : "DTF_FUN ( CHILD(PX, 2) ,           True, strings(['D0','pi0']) )"
        , "FIT_PisPY"         : "DTF_FUN ( CHILD(PY, 2) ,           True, strings(['D0','pi0']) )"
        , "FIT_PisPZ"         : "DTF_FUN ( CHILD(PZ, 2) ,           True, strings(['D0','pi0']) )"
        ####
        #
        #
        #
        # Pi0 variables
        , "FIT_Pi0P"       : "DTF_FUN ( CHILD(CHILD(P, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PT"	: "DTF_FUN ( CHILD(CHILD(PT, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0M"       : "DTF_FUN ( CHILD(CHILD(M, 2), 1),  True, 'D0' )"
        , "FIT_Pi0ETA"	 : "DTF_FUN ( CHILD(CHILD(ETA, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PX"         : "DTF_FUN ( CHILD(CHILD(PX , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PY"         : "DTF_FUN ( CHILD(CHILD(PY , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PZ"         : "DTF_FUN ( CHILD(CHILD(PZ , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0E"         : "DTF_FUN ( CHILD(CHILD(E , 2), 1),  True, strings(['D0','pi0']) )"
        #
        #
        #
        # H1 variables
        , "FIT_H1P"       : "DTF_FUN ( CHILD(CHILD(CHILD(P, 1), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H1PT"       : "DTF_FUN ( CHILD(CHILD(CHILD(PT, 1), 1), 1),  True, strings(['D0','pi0']) )"
#        , "FIT_H1ETA"	: "DTF_FUN ( CHILD(CHILD(CHILD(ETA, 1), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H1PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX ,1), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H1PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY ,1), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H1PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ ,1), 1), 1), True, strings(['D0','pi0']) )"
        # H2 variables
        , "FIT_H2P"       : "DTF_FUN ( CHILD(CHILD(CHILD(P, 2), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H2PT"       : "DTF_FUN ( CHILD(CHILD(CHILD(PT, 2), 1), 1),  True, strings(['D0','pi0']) )"
#        , "FIT_H2ETA"	: "DTF_FUN ( CHILD(CHILD(CHILD(ETA, 2), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H2PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX , 2), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H2PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY , 2), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H2PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ , 2), 1), 1), True, strings(['D0','pi0']) )"       
        , "FIT_gamma1PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX , 1), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma1PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY , 1), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma1PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ , 1), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma1E"          : "DTF_FUN ( CHILD(CHILD(CHILD(E , 1), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma2PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX , 2), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma2PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY , 2), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma2PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ , 2), 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_gamma2E"          : "DTF_FUN ( CHILD(CHILD(CHILD(E , 2), 2), 1),  True, strings(['D0','pi0']) )"
        #
	#
###############FIT0 without D0 mass constraint new added wrt to v21(=new1)
#removed from version 33
        # chi^2 of fit
        , "FIT0_CHI2"      : "DTF_CHI2(               True, 'pi0' )"
        # ndof of fit
        , "FIT0_NDOF"      : "DTF_NDOF(               True, 'pi0' )"
        }        
    elif 'Pi0M' in tuple_name:
        LoKi_DTFMASS.Variables = {
        ############ with D0 & pi0 mass constraint
        #
	#
	#
        # mass of D* aft Mass constraints
        "FIT_M"              : "DTF_FUN ( M ,           True, 'pi0')"
        , "FIT_M1"           : "DTF_FUN ( M ,           True, 'D0')"
        , "FIT_M2"           : "DTF_FUN ( M ,           True, strings(['D0','pi0']) )"
        , "FIT_M3"           : "DTF_FUN ( M ,           True )"
        # chi^2 of fit
        , "FIT_CHI2"      : "DTF_CHI2(               True, strings(['D0','pi0']) )"
        # ndof of fit
        , "FIT_NDOF"      : "DTF_NDOF(               True, strings(['D0','pi0']) )"
        # Momentum of D*
        , "FIT_P"         : "DTF_FUN ( P ,           True, strings(['D0','pi0']) )"
        # Eta of D*
        , "FIT_ETA"         : "DTF_FUN ( ETA ,           True, strings(['D0','pi0']) )"
        # Transerve Momentum of D
        , "FIT_PT"        : "DTF_FUN ( PT ,          True, strings(['D0','pi0']) )"
        # Lifetime of D*
        , "FIT_LT"        : "DTF_FUN (BPVLTIME('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        # VertexChi2 of D*
        , "FIT_VTXCHI2DOF": "DTF_FUN (VFASPF(VCHI2/VDOF),True, strings(['D0','pi0']))"
        # Min IP Chi2 of D*
        , "FIT_MIPCHI2"   : "DTF_FUN (MIPCHI2DV(PRIMARY),True, strings(['D0','pi0']))"
        # DOCA of D*
        , "FIT_MAXDOCA"   : "DTF_FUN (DOCAMAX,True, strings(['D0','pi0']))"
        # DIRA of D*
        , "FIT_DIRABPV"   : "DTF_FUN(BPVDIRA,True, strings(['D0','pi0']))"
        #  
        ####new added wrt to v21
        # IPCHI2 of D*
        , "FIT_IPCHI2BPV"   : "DTF_FUN(BPVIPCHI2(),True, strings(['D0','pi0']))"
        # DVCHI2 of D*
        , "FIT_DVCHI2BPV"   : "DTF_FUN(BPVVDCHI2,True, strings(['D0','pi0']))"
        # LifeTime CHI2 of D*
        , "FIT_LTCHI2BPV"   : "DTF_FUN(BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        # Fitted LifeTime CHI2 of D*
        , "FIT_LTFITCHI2BPV"   : "DTF_FUN(BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC'),True, strings(['D0','pi0']))"
        #
        , "FIT_PX"         : "DTF_FUN ( PX ,           True, strings(['D0','pi0']) )"
        , "FIT_PY"         : "DTF_FUN ( PY ,           True, strings(['D0','pi0']) )"
        , "FIT_PZ"         : "DTF_FUN ( PZ ,           True, strings(['D0','pi0']) )"
        ####
        #
        #
        # Mass of D0
        , "FIT_DM"       : "DTF_FUN ( CHILD(M, 1),  True, 'pi0' )"
        , "FIT_DM1"       : "DTF_FUN ( CHILD(M, 1),  True, 'D0')"
        , "FIT_DM2"       : "DTF_FUN ( CHILD(M, 1),  True, strings(['D0','pi0']) )"
        , "FIT_DM3"       : "DTF_FUN ( CHILD(M, 1),  True )"
        # Momentum of D0
        , "FIT_DP"       : "DTF_FUN ( CHILD(P, 1),  True, strings(['D0','pi0']) )"
        # Eta of D0
        , "FIT_DETA"       : "DTF_FUN ( CHILD(ETA, 1),  True, strings(['D0','pi0']) )"
        # Transverve Momenutm of D0
        , "FIT_DPT"      : "DTF_FUN ( CHILD(PT, 1), True, strings(['D0','pi0']) )"
        # D0 DIRA
        , "FIT_DDIRABPV"    : "DTF_FUN ( CHILD(BPVDIRA, 1), True, strings(['D0','pi0']))"
        # D0 Min IP Chi2
        , "FIT_DMIPCHI2"   : "DTF_FUN ( CHILD(MIPCHI2DV(PRIMARY), 1), True, strings(['D0','pi0']))"
        # D0 VertexChi2
        , "FIT_DVTXCHI2DOF" : "DTF_FUN ( CHILD(VFASPF(VCHI2/VDOF), 1), True, strings(['D0','pi0']))"
        # D0 Lifetime
        , "FIT_DLT" : "DTF_FUN ( CHILD(BPVLTIME('PropertimeFitter/ProperTime::PUBLIC'),1),True,strings(['D0','pi0']))"
        #
        ####new added wrt to v21 (=new1)
        # IPCHI2 of D0
        , "FIT_DIPCHI2BPV"   : "DTF_FUN( CHILD(BPVIPCHI2(), 1), True, strings(['D0','pi0']))"
        # DVCHI2 of D0
        , "FIT_DDVCHI2BPV"   : "DTF_FUN( CHILD(BPVVDCHI2, 1), True, strings(['D0','pi0']))"
        # LifeTime CHI2 of D0
        , "FIT_DLTCHI2BPV"   : "DTF_FUN( CHILD(BPVLTCHI2('PropertimeFitter/ProperTime::PUBLIC'), 1), True, strings(['D0','pi0']))"
        # Fitted LifeTime CHI2 of D0
        , "FIT_DLTFITCHI2BPV"   : "DTF_FUN( CHILD(BPVLTFITCHI2('PropertimeFitter/ProperTime::PUBLIC'), 1), True, strings(['D0','pi0']))"
        #
        , "FIT_DPX"         : "DTF_FUN (  CHILD(PX , 1),            True, strings(['D0','pi0']) )"
        , "FIT_DPY"         : "DTF_FUN (  CHILD(PY , 1),            True, strings(['D0','pi0']) )"
        , "FIT_DPZ"         : "DTF_FUN (  CHILD(PZ , 1),            True, strings(['D0','pi0']) )"
        ####
        #
        #
        # Momentum of Soft Pi
        , "FIT_PisP"       : "DTF_FUN ( CHILD(P, 2),  True, strings(['D0','pi0']) )"
        # Transverve Momenutm of Soft Pi
        , "FIT_PisPT"      : "DTF_FUN ( CHILD(PT, 2), True, strings(['D0','pi0']) )"
        # Soft Pi DIRA
#        , "FIT_PisDIRABPV"    : "DTF_FUN ( CHILD(BPVDIRA, 2), True, strings(['D0','pi0']))"
        # Soft Pi Min IP Chi2
        , "FIT_PisMIPCHI2"   : "DTF_FUN ( CHILD(MIPCHI2DV(PRIMARY), 2), True, strings(['D0','pi0']))"
        #
        , "FIT_PisPX"         : "DTF_FUN ( CHILD(PX, 2) ,           True, strings(['D0','pi0']) )"
        , "FIT_PisPY"         : "DTF_FUN ( CHILD(PY, 2) ,           True, strings(['D0','pi0']) )"
        , "FIT_PisPZ"         : "DTF_FUN ( CHILD(PZ, 2) ,           True, strings(['D0','pi0']) )"
        ####
        #
        #
        #
        # Pi0 variables
        , "FIT_Pi0P"       : "DTF_FUN ( CHILD(CHILD(P, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PT"	: "DTF_FUN ( CHILD(CHILD(PT, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0M"       : "DTF_FUN ( CHILD(CHILD(M, 2), 1),  True, 'D0' )"
        , "FIT_Pi0ETA"	 : "DTF_FUN ( CHILD(CHILD(ETA, 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PX"         : "DTF_FUN ( CHILD(CHILD(PX , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PY"         : "DTF_FUN ( CHILD(CHILD(PY , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0PZ"         : "DTF_FUN ( CHILD(CHILD(PZ , 2), 1),  True, strings(['D0','pi0']) )"
        , "FIT_Pi0E"         : "DTF_FUN ( CHILD(CHILD(E , 2), 1),  True, strings(['D0','pi0']) )"
        #
        #
        #
        # H1 variables
        , "FIT_H1P"       : "DTF_FUN ( CHILD(CHILD(CHILD(P, 1), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H1PT"       : "DTF_FUN ( CHILD(CHILD(CHILD(PT, 1), 1), 1),  True, strings(['D0','pi0']) )"
#        , "FIT_H1ETA"	: "DTF_FUN ( CHILD(CHILD(CHILD(ETA, 1), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H1PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX ,1), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H1PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY ,1), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H1PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ ,1), 1), 1), True, strings(['D0','pi0']) )"
        # H2 variables
        , "FIT_H2P"       : "DTF_FUN ( CHILD(CHILD(CHILD(P, 2), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H2PT"       : "DTF_FUN ( CHILD(CHILD(CHILD(PT, 2), 1), 1),  True, strings(['D0','pi0']) )"
#        , "FIT_H2ETA"	: "DTF_FUN ( CHILD(CHILD(CHILD(ETA, 2), 1), 1),  True, strings(['D0','pi0']) )"
        , "FIT_H2PX"         : "DTF_FUN ( CHILD(CHILD(CHILD(PX , 2), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H2PY"         : "DTF_FUN ( CHILD(CHILD(CHILD(PY , 2), 1), 1), True, strings(['D0','pi0']) )"
        , "FIT_H2PZ"         : "DTF_FUN ( CHILD(CHILD(CHILD(PZ , 2), 1), 1), True, strings(['D0','pi0']) )"       
        #
	#
###############FIT0 without D0 mass constraint new added wrt to v21(=new1)
        # chi^2 of fit
        , "FIT0_CHI2"      : "DTF_CHI2(               True, 'pi0' )"
        # ndof of fit
        , "FIT0_NDOF"      : "DTF_NDOF(               True, 'pi0' )"
        }        
    tuple.Dstr.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_DTFMASS"]
    tuple.Dstr.addTool(LoKi_DTFMASS)
        
    tuple.pi0.ToolList +=  ["TupleToolPi0Info"]
    if 'Pi0R' in tuple_name:
        tuple.gamma1.ToolList +=  ["TupleToolPhotonInfo"]
        tuple.gamma2.ToolList +=  ["TupleToolPhotonInfo"]
    
    return tuple



# Kill DAQ, temporary fix
from Configurables import EventNodeKiller
eventNodeKiller = EventNodeKiller('DAQkiller')
eventNodeKiller.Nodes = ['/Event/DAQ','/Event/pRec']

DaVinci().PrintFreq = 1000
DaVinci().EvtMax = -1                          # Number of events
DaVinci().SkipEvents = 0                       # Events to skip
#DaVinci().DataType = "2015"                    # necessary
DaVinci().InputType = "MDST"
for line_info in line_infos:
    print line_info
    tuple = makeDecayTreeTuple( line_info[0], line_info[1], line_info[2], line_info[3] )
    DaVinci().UserAlgorithms += [ tuple ]   
    
DaVinci().Simulation   = False
db = CondDB(LatestGlobalTagByDataType=DaVinci().DataType)

DaVinci().Lumi = True
DaVinci().TupleFile = "DVNtuples.root"


