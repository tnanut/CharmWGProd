# WG productions options for D0->KSKS

The options contained in this directory generate ntuples for real data and MC.

The info.json file is generated using a script. Examples of valid arguments:
```bash
./make_info_file.py --help
./make_info_file.py
./make_info_file.py --years 2017 2018 --channels D02KsKsMC --data-types MC
./make_info_file.py --polarities MagDown
```