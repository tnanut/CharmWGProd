#!/usr/bin/env python
import argparse
import json
from os.path import dirname, join


all_years = [2017, 2018]
all_polarities = ['MagDown', 'MagUp']
davinci_versions = {
    2017: 'v44r7',
    2018: 'v44r7',
}
bk_paths = {
    'D02KsKs': {
        2017: '/LHCb/Collision17/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo04/94000000/CHARMTWOBODY.MDST',
        2018: '/LHCb/Collision18/Beam6500GeV-VeloClosed-{polarity}/Real Data/Turbo05/94000000/CHARMTWOBODY.MDST',
    },
    'D02KsKsMC': {
        2017: '/MC/2017/Beam6500GeV-2017-{polarity}-Nu1.6-25ns-Pythia8/Sim09h/Trig0x62661709/Reco17/Turbo04a-WithTurcal/Stripping29r2NoPrescalingFlagged/27165900/ALLSTREAMS.MDST',
        2018: '/MC/2018/Beam6500GeV-2018-{polarity}-Nu1.6-25ns-Pythia8/Sim09h/Trig0x617d18a4/Reco18/Turbo05-WithTurcal/Stripping34NoPrescalingFlagged/27165900/ALLSTREAMS.MDST',
    },
}



def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--years', type=int, nargs='+', choices=all_years, default=all_years)
    parser.add_argument('--polarities', nargs='+', choices=all_polarities, default=all_polarities)
    parser.add_argument('--channels', nargs='+', choices=list(bk_paths.keys()), default=list(bk_paths.keys()))
    parser.add_argument('--data-types', type=str, choices=['RealData', 'MC'], default='RealData')
    parser.add_argument('--force-full-lfn', action='store_false', help='Force the merge request testing to use a full LFN')
    args = parser.parse_args()

    results = {}

    if args.data_types == 'RealData':
        ChainOption = [     
                        'real_data.py',
                        ]
    elif args.data_types == 'MC':  
        ChainOption = [     
                        'mc.py',
                        ]
    else:
        raise NotImplementedError(args.data_types)

    for channel in args.channels:
        for year in args.years:
            for polarity in args.polarities:
                key = '_'.join([str(year), polarity, channel])
                assert key not in results, 'Duplicate keys are not possible'
                results[key] = {
                    'options': ChainOption + [str(year)+'.py']
                                           + (['{}.py'.format(polarity)] if args.data_types == 'MC' else []) 
                                           + [ 'make_tuples.py'],
                    'bookkeeping_path': bk_paths[channel][year].format(polarity=polarity),
                    'dq_flag': 'OK',
                    'application': 'DaVinci',
                    'application_version': davinci_versions[year],
                    'output_type': '_'.join(['CHARM', channel]).upper()+'_DVNTUPLE.ROOT'
                }
                if args.force_full_lfn:
                    results[key]['n_events'] = -1
                    results[key]['n_lfns'] = 2

    with open(join(dirname(__file__), 'info.json'), 'wt') as fp:
        json.dump(results, fp, indent=4, sort_keys=True)


if __name__ == '__main__':
    parse_args()
