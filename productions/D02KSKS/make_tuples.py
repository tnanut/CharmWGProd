from PhysConf.Selections import (
    AutomaticData,
    CombineSelection,
    MomentumScaling,
    RebuildSelection,
    SelectionSequence,
    TupleSelection,
)
from PhysConf.Filters import LoKi_Filters
from Configurables import DaVinci, TupleToolTrigger
from GaudiKernel.SystemOfUnits import MeV
from StandardParticles import StdAllLooseMuons
from Configurables import CondDB, TrackScaleState
from Configurables import DstConf, TurboConf  # necessary for DaVinci v40r1 onwards
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
from Configurables import LoKi__Hybrid__Dict2Tuple
from Configurables import LoKi__Hybrid__DictOfFunctors
from Configurables import LoKi__Hybrid__DTFDict as DTFDict
from Configurables import MessageSvc
from copy import copy
from Configurables import DecayTreeTuple
import DecayTreeTuple.Configuration
from DecayTreeTuple.Configuration import *

from Configurables import TupleToolDecay, TupleToolTISTOS, TupleToolVeloTrackMatch, FilterDesktop, TupleToolDecayTreeFitter, TupleToolDira, TupleToolGeometry, TupleToolMCTruth, TupleToolMCBackgroundInfo, TupleToolKinematic, TupleToolParticleReFit, TupleToolPid, TupleToolPropertime, TupleToolRICHPid, TupleToolTrackInfo, TupleToolTrackIsolation, TupleToolTrackPosition, TupleToolTrigger, TriggerTisTos, TupleToolPhotonInfo, TupleToolPi0Info, TupleToolAngles, TupleToolVeto, TupleToolProtoPData, PhysConf, CombineParticles, L0TriggerTisTos, TupleToolPrimaries 

from PhysSelPython.Wrappers import AutomaticData, DataOnDemand, Selection, SelectionSequence
from Configurables import TupleToolDecayTreeFitter


# get the year
the_year = DaVinci().DataType

# get the latest conditions for momenum scale etc.
if not DaVinci().Simulation:
    CondDB(LatestGlobalTagByDataType = the_year)

# Set the input particle locations and apply Momentum Scaling
# ==============================================================================

#if the_year == '2015' or the_year == '2016':
#    rootInTES = '/Event/Turbo'
#elif the_year == '2017' or the_year == '2018':
if DaVinci().Simulation:
    rootInTES = '/Event/Turbo'
else:
    rootInTES = '/Event/Charmtwobody/Turbo'


#particles

hh_keys = { 'LL' : '_KS0LL',
            'LD' : '_KS0LL_KS0DD',
	    'DD' : '_KS0DD'}



triggerList = [
    'L0DiMuonDecision',
    'L0ElectronDecision',
    'L0HadronDecision',
    'L0MuonDecision',
    'L0PhotonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackMVATightDecision',
    'Hlt1TwoTrackMVATightDecision',
    'Hlt1TrackMuonMVADecision',
    'Hlt1CalibTrackingKKDecision',
    'Hlt1CalibTrackingKPiDecision',
    'Hlt1CalibTrackingKPiDetachedDecision',
    'Hlt1CalibTrackingPiPiDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision'
]


#'Hlt1SingleElectronNoIPDecision',
#'Hlt1SingleMuonNoIPDecision'

#Hlt1SingleMuonHighPTDecision
#Hlt1DiMuonNoIPDecision
#Hlt1DiElectronLowMassNoIPDecision
#Hlt1CalibHighPTLowMultTrksDecision
#Hlt1DiElectronLowMassDecision
#Hlt1DiElectronHighMassDecision

# Prefix "Hlt2CharmHadDstp2D0Pip_D02KS0KS0",
#triggerHlt2Head = [
#    "_KS0LLTurbo",
#    "_KS0DDTurbo",
#    "_KS0LL_KS0DD_Turbo"
#]

default_variables = {
    'ID':     'ID',
    'P':      'P',
    'PX':     'PX',
    'PY':     'PY',
    'PZ':     'PZ',
    'E':      'E',
    'PT':     'PT',
    'ETA':    'ETA',
    'PHI':    'PHI',
    'IPCHI2': 'BPVIPCHI2()',
    'IP':     'BPVIP()',
}

track_variables = {
    'TRCHI2DOF':   'TRCHI2DOF',
    'GHOSTPROB':   'TRGHOSTPROB',
    'CLONEDIST':   'CLONEDIST',
    'ProbNNmu':    'PROBNNmu',
    'ProbNNk':     'PROBNNk',
    'ProbNNpi':    'PROBNNpi',
    'ProbNNghost': 'PROBNNghost',
    'ProbNNp':     'PROBNNp',
    'PIDpi':       'PIDpi',
    'PIDe':        'PIDe',
    'PIDp':        'PIDp',
    'PIDK':        'PIDK',
    'PIDmu':       'PIDmu',
    'IsMuon':      'switch(ISMUON,1,0)',
}

composite_variables = {
    'M':         'M',
    'VX':        'VFASPF(VX)',
    'VY':        'VFASPF(VY)',
    'VZ':        'VFASPF(VZ)',
    'BPVDIRA':   'BPVDIRA',
    'BPVVDCHI2': 'BPVVDCHI2',
    'BPVX':      'BPV(VX)',
    'BPVY':      'BPV(VY)',
    'BPVZ':      'BPV(VZ)',
    'VCHI2NDOF': 'CHI2VXNDOF',
}

tools = [
        "TupleToolPropertime",
        "TupleToolEventInfo",
        "TupleToolRecoStats",
        "TupleToolAngles",
        "TupleToolDira",
        "TupleToolGeometry"
        ]

decay_descriptors = {
    'LL' : "${DS}[D*(2010)+ -> ${D0}([D0]cc -> ${K1}(KS0 -> ${Pip1}pi+ ${Pim1}pi-  )  ${K2}(KS0 -> ${Pip2}pi+ ${Pim2}pi-  )  ) ${Pis}pi+]CC",
    'LD' : "${DS}[D*(2010)+ -> ${D0}([D0]cc -> ${K1}(KS0 -> ${Pip1}pi+ ${Pim1}pi-  )  ${K2}(KS0 -> ${Pip2}pi+ ${Pim2}pi-  )  ) ${Pis}pi+]CC",
    'DD' : "${DS}[D*(2010)+ -> ${D0}([D0]cc -> ${K1}(KS0 -> ${Pip1}pi+ ${Pim1}pi-  )  ${K2}(KS0 -> ${Pip2}pi+ ${Pim2}pi-  )  ) ${Pis}pi+]CC"

}


# Parser of the decay descriptor
# ------------------------------------------------------------------------------

def ParseDescriptorTemplate(template):
    """
        Input 'template' is a Python string template
        e.g. "${D0}[D0 -> ${P1}K- ${P2}pi+]CC"
        ["D0", "P1", "P2"] are the branch names you want

        Returned values:
           - Decay field of the ddt
           - branches to be added to ddt with addBranches
    """
    
    # parse the template to get the list of branch names,
    # e.g. ["D0", "P1", "P2"]
    import re
    particles = re.findall("\${(\w+)}", template)
    
    # To form the decay descriptor, we need to mark all the particles
    # except for the top-level particle, which is included by default
    mapping = {p: '^' for p in particles}
    mapping[particles[0]] = ''

    from string import Template
    dd = Template(template)

    # Make the descriptor
    # "[D0 -> ^K- ^pi+]CC"
    decay = dd.substitute(mapping)

    # Now make the branches
    branches = {}
    for p in particles:
        # Need a version of the descriptor where particle 'p' is marked but nothing else is.
        mapping = {q: '^' if p == q else '' for q in particles}
        branches[p] = dd.substitute(mapping)

    return decay, branches


tuple_keys = { 'LL' : 'LL'
              ,'LD' : 'LD'
              ,'DD' : 'DD'
}

# read particles from TES
hlt2_line_names      = { k : 'Hlt2CharmHadDstp2D0Pip_D02KS0KS0{0}Turbo'.format(v) for k,v in hh_keys.iteritems()}

particle_locations = { k : '{0}/Particles'.format(v) for k,v in hlt2_line_names.iteritems() }

from PhysConf.Selections import AutomaticData
particles = { k : AutomaticData(v) for k,v in particle_locations.iteritems() }

# inputs to make the tuples
inputs = { k : particles[tuple_keys[k]] for k in tuple_keys.iterkeys() }



def MakeTuple(key):


   decay, branches = ParseDescriptorTemplate(decay_descriptors[key])
    
   dtt = DecayTreeTuple("{0}tuple".format(key)) 
   dtt.TupleName = key
   dtt.Decay = decay
   dtt.addBranches( branches )
   dtt.Inputs = [ inputs[key].outputLocation() ]

   
   dtt.WriteP2PVRelations = False
   dtt.InputPrimaryVertices = 'Primary'

    
   dtt.ToolList = copy(tools)

   primTool = dtt.addTupleTool("TupleToolPrimaries")
   primTool.InputLocation = 'Primary'
   primTool.Verbose = True
   dtt.addTupleTool("TupleToolTrackInfo").Verbose = True
   #dtt.TupleToolTrackInfo.Verbose = True
   #dtt.addTupleTool("TupleToolGeometry").Verbose = True
   #dtt.TupleToolGeometry.Verbose = True
   dtt.addTupleTool("TupleToolKinematic").Verbose = True
   #dtt.TupleToolKinematic.Verbose = True
   dtt.addTupleTool("TupleToolPid").Verbose = False
   #dtt.TupleToolPid.Verbose = False
    
   triggerTool = dtt.addTupleTool("TupleToolTrigger")
   triggerTool.TriggerList= triggerList
   triggerTool.VerboseL0=True
   triggerTool.VerboseHlt1=True
   triggerTool.VerboseHlt2=False
        
   tistosTool = dtt.addTupleTool("TupleToolTISTOS")
   tistosTool.addTool(L0TriggerTisTos())
   tistosTool.addTool(TriggerTisTos())
   tistosTool.TriggerList= triggerList    

   dtt.TupleToolTISTOS.Verbose=True
   dtt.TupleToolTISTOS.VerboseL0=True
   dtt.TupleToolTISTOS.VerboseHlt1=True
   dtt.TupleToolTISTOS.VerboseHlt2=False
   dtt.TupleToolTISTOS.FillHlt2=False

   dtt.addTool(TupleToolDecay, name="DS")
   dtt.addTool(TupleToolDecay, name="D0")

   from Configurables import LoKi__Hybrid__TupleTool
   LoKi = LoKi__Hybrid__TupleTool("LoKi")
   LoKi.Variables = { "BPV_nTracks" : "BPV( NTRACKS )" }
   dtt.ToolList += ["LoKi::Hybrid::TupleTool/LoKi"]
   LoKi = dtt.DS.addTool(LoKi)


   dtt.DS.ToolList += [ "TupleToolDecayTreeFitter/FitDs" ]
   dtt.DS.addTool(TupleToolDecayTreeFitter("FitDs"))
   dtt.DS.FitDs.Verbose = True
   dtt.DS.FitDs.constrainToOriginVertex = False
   dtt.DS.FitDs.UpdateDaughters = True


   dtt.DS.ToolList += [ "TupleToolDecayTreeFitter/PVFitDs" ]
   dtt.DS.addTool(TupleToolDecayTreeFitter("PVFitDs"))
   dtt.DS.PVFitDs.Verbose = True
   dtt.DS.PVFitDs.constrainToOriginVertex = True
   dtt.DS.PVFitDs.UpdateDaughters = True


   dtt.DS.ToolList += [ "TupleToolDecayTreeFitter/KSMassPVFitDs" ]
   dtt.DS.addTool(TupleToolDecayTreeFitter("KSMassPVFitDs"))
   dtt.DS.KSMassPVFitDs.Verbose = True
   dtt.DS.KSMassPVFitDs.constrainToOriginVertex = True
   dtt.DS.KSMassPVFitDs.daughtersToConstrain = [ "KS0" ]
   dtt.DS.KSMassPVFitDs.UpdateDaughters = True

   dtt.DS.ToolList += [ "TupleToolDecayTreeFitter/KSMassFitDs" ]
   dtt.DS.addTool(TupleToolDecayTreeFitter("KSMassFitDs"))
   dtt.DS.KSMassFitDs.Verbose = True
   dtt.DS.KSMassFitDs.constrainToOriginVertex = False
   dtt.DS.KSMassFitDs.daughtersToConstrain = [ "KS0" ]
   dtt.DS.KSMassFitDs.UpdateDaughters = True

   dtt.D0.ToolList += [ "TupleToolDecayTreeFitter/PVFit" ]
   dtt.D0.addTool(TupleToolDecayTreeFitter("PVFit"))
   dtt.D0.PVFit.Verbose = True
   dtt.D0.PVFit.constrainToOriginVertex = True
   dtt.D0.PVFit.UpdateDaughters = True


   dtt.D0.ToolList += [ "TupleToolDecayTreeFitter/KSMassPVFit" ]
   dtt.D0.addTool(TupleToolDecayTreeFitter("KSMassPVFit"))
   dtt.D0.KSMassPVFit.Verbose = True
   dtt.D0.KSMassPVFit.constrainToOriginVertex = True
   dtt.D0.KSMassPVFit.daughtersToConstrain = [ "KS0" ]
   dtt.D0.KSMassPVFit.UpdateDaughters = True

   dtt.D0.ToolList += [ "TupleToolDecayTreeFitter/KSMassFit" ]
   dtt.D0.addTool(TupleToolDecayTreeFitter("KSMassFit"))
   dtt.D0.KSMassFit.Verbose = True
   dtt.D0.KSMassFit.constrainToOriginVertex = False
   dtt.D0.KSMassFit.daughtersToConstrain = [ "KS0" ]
   dtt.D0.KSMassFit.UpdateDaughters = True

   dtt.D0.ToolList += [ "TupleToolDecayTreeFitter/Fit" ]
   dtt.D0.addTool(TupleToolDecayTreeFitter("Fit"))
   dtt.D0.KSMassFit.Verbose = True
   dtt.D0.KSMassFit.constrainToOriginVertex = False
   dtt.D0.KSMassFit.UpdateDaughters = True

   if DaVinci().Simulation:
        # Add Bkg info
        #dtt.addTool(TupleToolMCBackgroundInfo, name="TupleToolMCBackgroundInfo")
        #dtt.TupleToolMCBackgroundInfo.Verbose=True

        # Add Truth matching
        #dtt.addTool(TupleToolMCTruth, name="truth")
        #dtt.truth.ToolList+=["MCTupleToolHierarchy"]
        #dtt.ToolList+=["TupleToolMCTruth/truth"]

        from TeslaTools import TeslaTruthUtils

        #relations = [ TeslaTruthUtils.getRelLoc('') ]
        #relations.append('/Event/Turbo/Relations/Hlt2/Protos/Charged')
        #TeslaTruthUtils.makeTruth(dtt, relations, ['MCTupleToolKinematic','MCTupleToolHierarchy'])
        
        dtt.ToolList += [
            'TupleToolMCBackgroundInfo',
            'TupleToolMCTruth',
        ]


        relations = TeslaTruthUtils.getRelLocs() + [
            TeslaTruthUtils.getRelLoc(''),
            # Location of the truth tables for PersistReco objects
            'Relations/Hlt2/Protos/Charged'
        ]
        mc_tools = [
            'MCTupleToolKinematic',
            'MCTupleToolHierarchy',
            # ...and any other tools you'd like to use
        ]

        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools);

   return dtt

tuples = [ MakeTuple(key) for key in tuple_keys.iterkeys() ]


from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
#HLT1_Code = "HLT_PASS_RE('Hlt1TrackMVADecision') | HLT_PASS_RE('Hlt1TwoTrackMVADecision')",

fltrs = LoKi_Filters( HLT2_Code = "HLT_PASS_RE('"+hlt2_line_names['LL']+"Decision') | HLT_PASS_RE('"+hlt2_line_names['LD']+"Decision') | HLT_PASS_RE('"+hlt2_line_names['DD']+"Decision')")

DaVinci().EventPreFilters = fltrs.filters('TriggerFilter')
#DaVinci().Simulation = False
DaVinci().SkipEvents = 0
DaVinci().EvtMax = -1
DaVinci().Lumi = not DaVinci().Simulation
DaVinci().TupleFile = 'CHARM_D02KSKS_DVNTUPLE.ROOT'
DaVinci().PrintFreq = 10000
DaVinci().UserAlgorithms += tuples
DaVinci().UserAlgorithms += [v for k,v in particles.iteritems() ]
DaVinci().InputType = 'MDST'
DaVinci().RootInTES      = rootInTES
DaVinci().Turbo = True
#from Configurables import DstConf, TurboConf
#DstConf().Turbo = True
#TurboConf().PersistReco = True 
