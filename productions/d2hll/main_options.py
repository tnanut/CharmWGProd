from collections import namedtuple
import os
from os.path import normpath, join
import sys

from Configurables import DaVinci, CondDB, MCDecayTreeTuple

if "CHARMWGPRODROOT" in os.environ:
    # We are running a released version
    sys.path.append(join(os.environ["CHARMWGPRODROOT"], 'productions/d2hll'))
elif "CI_PROJECT_DIR" in os.environ:
    # We are running inside the CI
    sys.path.append(join(os.environ["CI_PROJECT_DIR"], 'productions/d2hll'))
else:
    # We are running with a local version
    sys.path.append(normpath(os.getcwd()))

try:
    from make_decay_tree_tuples import make_decay_tree_tuple
    from decay_parser import parse_decay
    from decay_parser import utils as decay_utils
except ImportError as e:
    raise Exception(str(os.listdir(os.getcwd())) + '\n' + repr(e))

# Keep the linter happy
if False:
    unicode = str


Decay = namedtuple('Decay', ['name', 'decay_descriptor', 'stripping_line', 'dp_event_type', 'dsp_event_type'])

decays = [
    Decay('DpToKmumu_OS', '[D+ -> K+ mu+ mu-]CC', 'D2XMuMuSS_KOSLine', '21113001', '23113001'),
    Decay('DpToKmumu_SS', '[D+ -> K- mu+ mu+]CC', 'D2XMuMuSS_KSSLine', '21113003', '23113004'),
    Decay('DpToKmue_OS', '[D+ -> K+ mu+ e-]CC', 'D2XMuMuSS_K_MuE_OSLine', '21113015', '23113015'),
    Decay('DpToKmue_SS', '[D+ -> K- mu+ e+]CC', 'D2XMuMuSS_K_MuE_SSLine', '21113035', '23113035'),
    Decay('DpToKemu_OS', '[D+ -> K+ e+ mu-]CC', 'D2XMuMuSS_K_EMu_OSLine', '21113005', '23113010'),
    Decay('DpToKee_OS', '[D+ -> K+ e+ e-]CC', 'D2XMuMuSS_K_EE_OSLine', '21123000', '23123000'),
    Decay('DpToKee_SS', '[D+ -> K- e+ e+]CC', 'D2XMuMuSS_K_EE_SSLine', '21123010', '23123010'),
    Decay('DpTopimumu_OS', '[D+ -> pi+ mu+ mu-]CC', 'D2XMuMuSS_PiOSLine', '21113000', '23113000'),
    Decay('DpTopimumu_SS', '[D+ -> pi- mu+ mu+]CC', 'D2XMuMuSS_PiSSLine', '21113002', '23113003'),
    Decay('DpTopimue_OS', '[D+ -> pi+ mu+ e-]CC', 'D2XMuMuSS_Pi_MuE_OSLine', '21113016', '23113016'),
    Decay('DpTopimue_SS', '[D+ -> pi- mu+ e+]CC', 'D2XMuMuSS_Pi_MuE_SSLine', '21113036', '23113036'),
    Decay('DpTopiemu_OS', '[D+ -> pi+ e+ mu-]CC', 'D2XMuMuSS_Pi_EMu_OSLine', '21113006', '23113011'),
    Decay('DpTopiee_OS', '[D+ -> pi+ e+ e-]CC', 'D2XMuMuSS_Pi_EE_OSLine', '21123001', '23123001'),
    Decay('DpTopiee_SS', '[D+ -> pi- e+ e+]CC', 'D2XMuMuSS_Pi_EE_SSLine', '21123011', '23123011'),
    # Unphysical channels
    Decay('DpTopimumu_WS', '[D+ -> pi+ mu+ mu+]CC', 'D2XMuMuSS_PiMuMuCalLine', None, None),
    Decay('DpTopimue_WS', '[D+ -> pi+ mu+ e+]CC', 'D2XMuMuSS_PiEMuCalLine', None, None),
    Decay('DpTopiee_WS', '[D+ -> pi+ e+ e+]CC', 'D2XMuMuSS_PiEECalLine', None, None),
    Decay('DpToKmumu_WS', '[D+ -> K+ mu+ mu+]CC', 'D2XMuMuSS_KMuMuCalLine', None, None),
    Decay('DpToKmue_WS', '[D+ -> K+ mu+ e+]CC', 'D2XMuMuSS_KEMuCalLine', None, None),
    Decay('DpToKee_WS', '[D+ -> K+ e+ e+]CC', 'D2XMuMuSS_KEECalLine', None, None),
    # Calibration channels
    Decay('DpToKKpi_SS', '[D+ -> K- K+ pi+]CC', 'D2XMuMuSS_2KPiLine', None, None),
    Decay('DpToKpipi_SS', '[D+ -> K- pi+ pi+]CC', 'D2XMuMuSS_K2PiLine', None, None),
    Decay('DpTopipipi_SS', '[D+ -> pi- pi+ pi+]CC', 'D2XMuMuSS_PiCalLine', None, None),
]

norms = [
    Decay('DpToKphi_phiTomumu_OS', '[D+ -> K+ mu+ mu-]CC', 'D2XMuMuSS_KOSLine', None, '23113002'),
    Decay('DpTopiphi_phiTomumu_OS', '[D+ -> pi+ mu+ mu-]CC', 'D2XMuMuSS_PiOSLine', '21173001', '23173001'),
    Decay('DpTopiphi_phiToemu_OS', '[D+ -> pi+ e+ mu-]CC', 'D2XMuMuSS_Pi_EMu_OSLine', '21313000', None),
    Decay('DpTopiphi_phiToee_OS', '[D+ -> pi+ e+ e-]CC', 'D2XMuMuSS_Pi_EE_OSLine', '21123020', '23123022'),

    Decay('DspToXphi_phiToemu_OS', '[D+ -> pi+ e+ mu-]CC', 'D2XMuMuSS_Pi_EMu_OSLine', None, '23712012'),
    Decay('DspToXphi_phiTomumu_OS', '[D+ -> pi+ mu+ mu-]CC', 'D2XMuMuSS_PiOSLine', None, '23712020'),
    Decay('DspToXphi_phiToee_OS', '[D+ -> pi+ e+ e-]CC', 'D2XMuMuSS_Pi_EE_OSLine', None, '23722012'),
]

bkg_event_types = ['21263010', '23263020', '23513071', '23523071', '27163001', '27163002', '27163003']

for decay in decays[:]:
    if 'mumu' in decay.name or 'ee' in decay.name:
        new_name = decay.name.replace('ToK', 'ToKphi_phiTo').replace('Topi', 'Topiphi_phiTo')
        if decay.name == new_name:
            raise Exception('Something went very wrong', decay.name, new_name)
        decays.append(Decay(new_name, decay.decay_descriptor, decay.stripping_line, decay.dp_event_type, decay.dsp_event_type))

# Finally configure DaVinci
if DaVinci().Simulation:
    event_type = DaVinci().TupleFile
DaVinci().TupleFile = 'Charm_D2hll_DVntuple.root'
DaVinci().PrintFreq = 1000
# Only ask for luminosity information when not using simulated data
DaVinci().Lumi = not DaVinci().Simulation


# Setup the needed DecayTreeTuples
decays_to_consider = decays[:]

dtts = []
for decay in decays_to_consider:
    dtt_name = decay_descriptor = None

    if DaVinci().Simulation and str(event_type) not in bkg_event_types:
        if event_type == decay.dp_event_type:
            dtt_name = decay.name
            decay_descriptor = decay.decay_descriptor
        elif event_type == decay.dsp_event_type:
            dtt_name = decay.name.replace('Dp', 'Dsp')
            decay_descriptor = decay.decay_descriptor.replace('D+', 'D_s+')
        else:
            matches = [norm_decay for norm_decay in norms
                       if event_type == norm_decay.dp_event_type or
                       event_type == norm_decay.dsp_event_type]
            if matches:
                assert len(matches) == 1, matches
                if event_type == matches[0].dp_event_type:
                    dtt_name = decay.name
                    decay_descriptor = decay.decay_descriptor
                elif event_type == matches[0].dsp_event_type:
                    dtt_name = decay.name.replace('Dp', 'Dsp')
                    decay_descriptor = decay.decay_descriptor.replace('D+', 'D_s+')
                else:
                    raise RuntimeError(event_type)
            else:
                # Nothing should be added to dtts
                continue
    else:
        dtt_name = decay.name
        decay_descriptor = decay.decay_descriptor

    assert dtt_name and decay_descriptor
    dtts.append(make_decay_tree_tuple(
        name=dtt_name,
        line=decay.stripping_line,
        decay_descriptor=decay_descriptor,
        input_type=DaVinci().InputType,
        year=DaVinci().DataType,
        mc=DaVinci().Simulation
    ))

    # Add MCDecayTreeTuple so we can get the Generator+Reconstruction+Stripping efficiency
    if DaVinci().Simulation:
        mctuple = MCDecayTreeTuple("MC"+dtt_name)
        assert decay_descriptor.count('->') == 1, decay_descriptor
        _decay = parse_decay(decay_descriptor)
        mctuple.Decay = str(decay_utils.mark_all(_decay)).replace('->', '==>')
        mctuple.addBranches({k: v.replace('->', '==>')
                             for k, v in decay_utils.get_branches(_decay).items()})

        mctuple.ToolList = [
            "MCTupleToolHierarchy",
            "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
        ]
        # Add a 'number of photons' branch
        mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
        mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
            "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
        }
        dtts.append(mctuple)

assert dtts
DaVinci().UserAlgorithms += dtts
