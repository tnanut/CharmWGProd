#!/usr/bin/env python3
import argparse
import json
from os.path import join, dirname


ALL_YEARS = [2016, 2017]
STRIPPING_VERSION = {
    2016: '28NoPrescalingFlagged',
    2017: '29r2Filtered',
    2018: '34Filtered',
}
SIMULATION_VERSION = {
    2016: '09c',
    2017: '09h-ReDecay01',
    2018: '09h-ReDecay01',
}
TRIGGER_VERSION = {
    2016: '6138160F',
    2017: '62661709',
    2018: '617d18a4',
}
TURBO_VERSION = {
    2016: '03',
    2017: '04a-WithTurcal',
    2018: '05-WithTurcal',
}
DAVINCI_VERSION= {
    2016: 'v45r3',
    2017: 'v45r3',
    2018: 'v45r3',
}

FILTERED_EVENT_TYPES = set()
INFO_FN = join(dirname(__file__), 'info.json')


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('years', type=int, nargs='*', choices=[[]]+ALL_YEARS)

    parser.add_argument('--data', dest='data', action='store_true')
    parser.add_argument('--no-data', dest='data', action='store_false')
    parser.set_defaults(data=True)

    parser.add_argument('--event-types', nargs='+', default=[])

    args = parser.parse_args()
    #if args.event_types:
        #raise NotImplementedError()

    if args.years:
        years = set(args.years)
    else:
        years = set(ALL_YEARS)

    result = {}
    for year in years:
        for polarity in ['MagDown', 'MagUp']:
            if args.data:
                result[f'{year}_{polarity}'] = generate_production(year, polarity)
            for event_type in args.event_types:
                if (year== 2017 or year== 2018) and (event_type == '23722012' or event_type == '23712020'):
                      continue  
                elif year== 2016 and (event_type == '23123022' or event_type == '23173001'):
                      continue  
                else:
                    result[f'{year}_{polarity}_{event_type}'] = generate_production(year, polarity, event_type)

    result = json.dumps(result, indent=4, sort_keys=False)
    # print(INFO_FN, result)
    with open(INFO_FN, 'wt') as fp:
        fp.write(result)


def generate_production(year, polarity, event_type=None):
    #file_type = f'CHARM.MDST'
    #print(type(year))
    options = [f'options/{year}.py']
    if event_type is None:
        file_type = f'CHARM.MDST'
        dq_flag = 'OK'
        options += [f'options/data.py']
        options += [f'options/tes_charm.py']
        options += [f'options/{file_type.split(".")[-1]}.py']
    else:
        if year == 2016:
            if event_type == '23712020' or event_type == '23722012' or event_type == '21173001' or event_type == '21123020':
                file_type = f'ALLSTREAMS.DST'
                options += [f'options/{year}norm{polarity}.py']
                options += [f'options/tes_allstreams.py']
            else:
                file_type = f'ALLSTREAMS.MDST'
                options += [f'options/{year}{polarity}.py']
                options += [f'options/tes_allstreams.py']
        else: 
            file_type = f'D2HLL.STRIP.MDST'
            options += [f'options/{year}{polarity}.py']
            options += [f'options/tes_stripped.py']
            
        
        dq_flag = 'ALL'
        options += [f'options/mc.py']
        options += [f'options/{file_type.split(".")[-1]}.py']
        options += [f'options/{event_type}.py']
        #if event_type in FILTERED_EVENT_TYPES:
            #options += [f'options/tes_filtered.py']
        #else:
            #options += [f'options/tes_allstreams.py']
    options += [f'main_options.py']

    # Construct bookkeeping path
    short_year = year-2000

    if event_type is None:
        config = 'LHCb'
        config_version = f'Collision{short_year}'
        conditions = f'Beam6500GeV-VeloClosed-{polarity}'
    else:
        config = 'MC'
        config_version = f'{year}'
        conditions = f'Beam6500GeV-{year}-{polarity}-Nu1.6-25ns-Pythia8'

   
    if event_type:
        processing_pass = '/'.join([
            f'Sim{SIMULATION_VERSION[year]}',
            f'Trig0x{TRIGGER_VERSION[year]}'
            
        ])
    else:
        processing_pass = '/'.join([
            f'Real Data'
        ])

    reconstruction_version = f'Reco{short_year}'
    turbo_version = f'Turbo{TURBO_VERSION[year]}'
    
    if (event_type == '21123020' or event_type == '21173001') and year == 2016:
        stripping_version = f'Stripping{STRIPPING_VERSION[year]}'
        processing_pass = '/'.join([f'Sim09b',f'Trig0x{TRIGGER_VERSION[year]}'])
    elif (event_type == '23712020' or event_type == '23722012') and year == 2016:
        stripping_version = f'Stripping26NoPrescalingFlagged'
        processing_pass = '/'.join([f'Sim09b',f'Trig0x{TRIGGER_VERSION[year]}'])
    else:
        stripping_version = f'Stripping{STRIPPING_VERSION[year]}'
    
    
    if event_type is None:
        bookkeeping_path = '/'.join([
            '',
            config,
            config_version,
            conditions,
            processing_pass,
            reconstruction_version,
            stripping_version,
            f'90000000',
            file_type,
        ])
    else: 
        bookkeeping_path = '/'.join([
            '',
            config,
            config_version,
            conditions,
            processing_pass,
            reconstruction_version,
            turbo_version,
            stripping_version,
            event_type,
            file_type,
        ])

    return {
        'options': options,
        'bookkeeping_path': bookkeeping_path,
        'dq_flag': dq_flag ,
        'application': 'DaVinci',
        'application_version': 'v45r3' ,
        'output_type': 'D2HLL_DVNTUPLE.ROOT'
    }


if __name__ == '__main__':
    parse_args()

