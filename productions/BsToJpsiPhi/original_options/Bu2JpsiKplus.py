from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter, TupleToolGeometry
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS, FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln, TupleToolPid, EventCountHisto, TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto, TupleToolTrackPosition, TupleToolTrackInfo
from Configurables import TupleToolTagging, TupleToolL0Data, TupleToolL0Calo, TupleToolP2VV
from DecayTreeTuple.Configuration import *

name = 'Bu2JpsiKplus'
is_mc = DaVinci().Simulation
is_data = not is_mc
input_type = DaVinci().InputType.upper()
is_dst = input_type == 'DST'
is_ldst = input_type == 'LDST'
is_mdst = input_type == 'MDST'
with_ft = not is_mdst
f_type = DaVinci().InputType

l0_lines = [
     'L0MuonDecision'
    ,'L0DiMuonDecision'
    ,'L0GlobalDecision'
]

hlt1_lines = [
     'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1TrackMVALooseDecision'
    ,'Hlt1TwoTrackMVALooseDecision'
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1TrackMuonMVADecision'
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1DiMuonNoL0Decision'
    ,'Hlt1L0AnyDecision'
    ,'Hlt1GlobalDecision'
]

hlt2_lines = [
     'Hlt2Topo2BodyDecision'
    ,'Hlt2Topo3BodyDecision'
    ,'Hlt2Topo4BodyDecision'
    ,'Hlt2TopoMu2BodyDecision'
    ,'Hlt2TopoMu3BodyDecision'
    ,'Hlt2TopoMu4BodyDecision'
    ,'Hlt2TopoMuMu2BodyDecision'
    ,'Hlt2TopoMuMu3BodyDecision'
    ,'Hlt2TopoMuMu4BodyDecision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2SingleMuonRareDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2DiMuonDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2DiMuonDetachedPsi2SDecision'
]

mtl = l0_lines + hlt1_lines + hlt2_lines

location  = '/Event/AllStreams/Phys/BetaSBu2JpsiKDetachedLine/Particles'
if is_data:
    location  = '/Event/Dimuon/Phys/BetaSBu2JpsiKDetachedLine/Particles'
if is_mdst:
    location  = 'Phys/BetaSBu2JpsiKDetachedLine/Particles'

from PhysSelPython.Wrappers import AutomaticData
b2jpsik_selection = AutomaticData(location)

tl = [
     'TupleToolKinematic'
    ,'TupleToolPropertime'
    ,'TupleToolPrimaries'
    ,'TupleToolEventInfo'
    ,'TupleToolPid'
    ,'TupleToolANNPID'
    ,'TupleToolL0Calo'
    ,'TupleToolL0Data'
    ,'TupleToolTISTOS'
]
if is_mc:
    from Configurables import TupleToolMCBackgroundInfo
    tl += ['TupleToolMCBackgroundInfo']

from GaudiConfUtils.ConfigurableGenerators import DecayTreeTuple as TUPLE
from PhysSelPython.Wrappers                import SimpleSelection
rd_selection = SimpleSelection(
     '{}_Tuple'.format(name)
    , TUPLE
    , [b2jpsik_selection]
    ## Properties:
    , Decay    = '[B+ -> ^(J/psi(1S) -> ^mu+ ^mu-) ^K+]CC'
    , ToolList = tl
    , Branches = {
         'B'       : '[B+ ->  (J/psi(1S) ->  mu+  mu-)  K+]CC'
        ,'Jpsi'    : '[B+ -> ^(J/psi(1S) ->  mu+  mu-)  K+]CC'
        ,'muplus'  : '[B+ ->  (J/psi(1S) -> ^mu+  mu-)  K+]CC'
        ,'muminus' : '[B+ ->  (J/psi(1S) ->  mu+ ^mu-)  K+]CC'
        ,'hplus'   : '[B+ ->  (J/psi(1S) ->  mu+  mu-) ^K+]CC'
    }
)

tuple_B2jpsik = rd_selection.algorithm()

if is_mc:
    mctruth = tuple_B2jpsik.addTupleTool('TupleToolMCTruth')
    mctruth.ToolList = [
         'MCTupleToolKinematic'
        ,'MCTupleToolHierarchy'
        ,'MCTupleToolReconstructed'
        ,'MCTupleToolPID'
    ]

tuples = [tuple_B2jpsik]

for particle in ['B', 'Jpsi', 'muplus', 'muminus', 'hplus']:
    tuple_B2jpsik.addTool(TupleToolDecay, name = particle)

tuple_B2jpsik.ReFitPVs = True

geom = tuple_B2jpsik.addTupleTool('TupleToolGeometry')
geom.RefitPVs = True
geom.Verbose = True

reco_stats = tuple_B2jpsik.addTupleTool('TupleToolRecoStats')
reco_stats.Verbose = True

track_inf = tuple_B2jpsik.addTupleTool('TupleToolTrackInfo')
track_inf.Verbose = True

track_pos = tuple_B2jpsik.addTupleTool('TupleToolTrackPosition')
track_pos.Z = 7500.

trig_tool = tuple_B2jpsik.addTupleTool('TupleToolTrigger')
trig_tool.Verbose = True
trig_tool.TriggerList = tl
trig_tool.OutputLevel = 6

### TISTOS
tuple_B2jpsik.B.ToolList += ['TupleToolTISTOS']
tuple_B2jpsik.B.addTool(TupleToolTISTOS, name = 'TupleToolTISTOS')
tuple_B2jpsik.B.TupleToolTISTOS.Verbose = True
tuple_B2jpsik.B.TupleToolTISTOS.TriggerList = mtl

# # change TOSFracMu for B
# from Configurables import TriggerTisTos
# tuple_B2jpsik.B.TupleToolTISTOS.addTool(TriggerTisTos())
# tuple_B2jpsik.B.TupleToolTISTOS.TriggerTisTos.TOSFracMuon = 0.
# tuple_B2jpsik.B.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
# tuple_B2jpsik.B.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.

tuple_B2jpsik.Jpsi.ToolList += ['TupleToolTISTOS']
tuple_B2jpsik.Jpsi.addTool(TupleToolTISTOS, name = 'TupleToolTISTOS')
tuple_B2jpsik.Jpsi.TupleToolTISTOS.Verbose = True
tuple_B2jpsik.Jpsi.TupleToolTISTOS.TriggerList = mtl

# # Change TOSFracMu for Jpsi
# from Configurables import TriggerTisTos
# tuple_B2jpsik.Jpsi.TupleToolTISTOS.addTool(TriggerTisTos())
# tuple_B2jpsik.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracMuon = 0.
# tuple_B2jpsik.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
# tuple_B2jpsik.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.

LoKi_B = LoKi__Hybrid__TupleTool('LoKi_B')
LoKi_B.Variables = {
     'JpsiKMass'            : 'WM(\'J/psi(1S)\',\'K+\')'
    ,'ETA'                  : 'ETA'
    ,'DOCA'                 : 'DOCA(1,2)'
    ,'Y'                    : 'Y'
    ,'LV01'                 : 'LV01'
    ,'LV02'                 : 'LV02'
    ,'LOKI_FDCHI2'          : 'BPVVDCHI2'
    ,'LOKI_FDS'             : 'BPVDLS'
    ,'LOKI_DIRA'            : 'BPVDIRA'
    ,'LOKI_DTF_CTAU'        : 'DTF_CTAU(0, True)'
    ,'LOKI_DTF_CTAUS'       : 'DTF_CTAUSIGNIFICANCE(0, True)'
    ,'LOKI_DTF_CHI2NDOF'    : 'DTF_CHI2NDOF(True)'
    ,'LOKI_DTF_CTAUERR'     : 'DTF_CTAUERR(0, True)'
    ,'LOKI_MASS_JpsiConstr' : 'DTF_FUN(M, True, \'J/psi(1S)\')'
    ,'LOKI_DTF_VCHI2NDOF'   : 'DTF_FUN(VFASPF(VCHI2/VDOF), True)'
}

tuple_B2jpsik.B.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_B']
tuple_B2jpsik.B.addTool(LoKi_B)

# Requires SemileptonicCommonTools->TupleToolDocas, out of tree atm : https://gitlab.cern.ch/lhcb-slb/SemileptonicCommonTools
# doca = tuple_B2jpsik.B.addTupleTool('TupleToolDocas')
# doca.Name = ['muplus_muminus', 'hplus_muplus', 'hplus_muminus',]
# doca.Location1 = [
#      '[B+ ->  (J/psi(1S) ->  ^mu+   mu-)   K+]CC'
#     ,'[B+ ->  (J/psi(1S) ->   mu+   mu-)  ^K+]CC'
#     ,'[B+ ->  (J/psi(1S) ->   mu+   mu-)  ^K+]CC'
# ]
# doca.Location2 = [
#      '[B+ ->  (J/psi(1S) ->   mu+  ^mu-)   K+]CC'
#     ,'[B+ ->  (J/psi(1S) ->  ^mu+   mu-)   K+]CC'
#     ,'[B+ ->  (J/psi(1S) ->   mu+  ^mu-)   K+]CC'
# ]

LoKi_Jpsi = LoKi__Hybrid__TupleTool('LoKi_Jpsi')
LoKi_Jpsi.Variables = {
     'ETA'         : 'ETA'
    ,'Y'           : 'Y'
    ,'LV01'        : 'LV01'
    ,'LV02'        : 'LV02'
    ,'LOKI_FDCHI2' : 'BPVVDCHI2'
    ,'LOKI_FDS'    : 'BPVDLS'
    ,'LOKI_DIRA'   : 'BPVDIRA'
}

tuple_B2jpsik.Jpsi.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_Jpsi']
tuple_B2jpsik.Jpsi.addTool(LoKi_Jpsi)

LoKi_muon = LoKi__Hybrid__TupleTool('LoKi_muon')
LoKi_muon.Variables = {
     'LOKI_ETA': 'ETA'
    ,'LOKI_Y'  : 'Y'
}
tuple_B2jpsik.muplus.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_muon']
tuple_B2jpsik.muminus.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_muon']
tuple_B2jpsik.muplus.addTool(LoKi_muon)
tuple_B2jpsik.muminus.addTool(LoKi_muon)

# refit with mass constraint
from Configurables import TupleToolDecayTreeFitter

tuple_B2jpsik.B.ToolList += ['TupleToolDecayTreeFitter/ConstJpsi']
tuple_B2jpsik.B.addTool(TupleToolDecayTreeFitter('ConstJpsi'))
tuple_B2jpsik.B.ConstJpsi.Verbose = True
tuple_B2jpsik.B.ConstJpsi.UpdateDaughters = True
tuple_B2jpsik.B.ConstJpsi.constrainToOriginVertex = True
tuple_B2jpsik.B.ConstJpsi.daughtersToConstrain = ['J/psi(1S)']

tuple_B2jpsik.B.ToolList += ['TupleToolDecayTreeFitter/ConstJpsiNoPV']
tuple_B2jpsik.B.addTool(TupleToolDecayTreeFitter('ConstJpsiNoPV'))
tuple_B2jpsik.B.ConstJpsiNoPV.Verbose = True
tuple_B2jpsik.B.ConstJpsiNoPV.UpdateDaughters = True
tuple_B2jpsik.B.ConstJpsiNoPV.constrainToOriginVertex = False
tuple_B2jpsik.B.ConstJpsiNoPV.daughtersToConstrain = ['J/psi(1S)']

tuple_B2jpsik.B.ToolList += ['TupleToolDecayTreeFitter/ConstBJpsi']
tuple_B2jpsik.B.addTool(TupleToolDecayTreeFitter('ConstBJpsi'))
tuple_B2jpsik.B.ConstBJpsi.Verbose = True
tuple_B2jpsik.B.ConstBJpsi.UpdateDaughters = True
tuple_B2jpsik.B.ConstBJpsi.constrainToOriginVertex = True
tuple_B2jpsik.B.ConstBJpsi.daughtersToConstrain = ['B+', 'J/psi(1S)']

tuple_B2jpsik.B.ToolList += ['TupleToolDecayTreeFitter/ConstOnlyPV']
tuple_B2jpsik.B.addTool(TupleToolDecayTreeFitter('ConstOnlyPV'))
tuple_B2jpsik.B.ConstOnlyPV.Verbose = True
tuple_B2jpsik.B.ConstOnlyPV.UpdateDaughters = True
tuple_B2jpsik.B.ConstOnlyPV.constrainToOriginVertex = True

### Backgrounds
# B -> KplusPiMuMu
tuple_B2jpsik.B.ToolList +=  ['TupleToolDecayTreeFitter/B2PiJpsi']
B2PiJpsi = TupleToolDecayTreeFitter(
    'B2PiJpsi'
    ,Verbose = True
    ,Substitutions = {
         'Beauty -> Meson ^K+': 'pi+'
        ,'Beauty -> Meson ^K-': 'pi-'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsik.B.addTool(B2PiJpsi)

# Lb -> pKMuMu (Kplus)
tuple_B2jpsik.B.ToolList +=  ['TupleToolDecayTreeFitter/B2pJpsi']
B2pJpsi = TupleToolDecayTreeFitter(
    'B2pJpsi'
    ,Verbose = True
    ,Substitutions = {
         'Beauty -> Meson ^K+': 'p+'
        ,'Beauty -> Meson ^K-': 'p~-'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsik.B.addTool(B2pJpsi)

if with_ft:
    # specific tagging tuple tool
    btag = tuple_B2jpsik.addTupleTool('TupleToolTagging')
    if is_dst or is_ldst:
        btag.Verbose = True
        btag.AddMVAFeatureInfo = True

        from Configurables import BTaggingTool
        btagtool = btag.addTool(BTaggingTool, name = 'MyBTaggingTool')

        from FlavourTagging.Tunings import applyTuning as applyFTTuning
        applyFTTuning(btagtool, tuning_version = 'Summer2017Optimisation_v4_Run2')
        btag.TaggingToolName = btagtool.getFullName()
    else:
        btag.UseFTfromDST = True

# event tuple
from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool('LoKi_EvtTuple')
LoKi_EvtTuple.VOID_Variables = {
     'LoKi_nPVs'              : 'CONTAINS(\'Rec/Vertex/Primary\')'
    ,'LoKi_nSpdMult'          : 'CONTAINS(\'Raw/Spd/Digits\')'
    ,'LoKi_nVeloClusters'     : 'CONTAINS(\'Raw/Velo/Clusters\')'
    ,'LoKi_nVeloLiteClusters' : 'CONTAINS(\'Raw/Velo/LiteClusters\')'
    ,'LoKi_nITClusters'       : 'CONTAINS(\'Raw/IT/Clusters\')'
    ,'LoKi_nTTClusters'       : 'CONTAINS(\'Raw/TT/Clusters\')'
    ,'LoKi_nOThits'           : 'CONTAINS(\'Raw/OT/Times\')'
}

tuple_B2jpsik.ToolList += ['LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple']
tuple_B2jpsik.addTool(LoKi_EvtTuple)

# ## Force update of CondDB to use proper momentum scaling
# from Configurables import CondDB
# CondDB(LatestGlobalTagByDataType = '2016')

from PhysSelPython.Wrappers import SelectionSequence
rd_SEQ = SelectionSequence('{}_SelSeq'.format(name), rd_selection)

mc_alg = None
if not is_data:
    from Configurables import MCDecayTreeTuple
    mc_alg = MCDecayTreeTuple('{}_MCTuple'.format(name))
    mc_alg.Decay = '[[B+]cc ==> ^K+ ^(J/psi(1S) ==> ^mu+ ^mu-)]CC'
    mc_alg.TupleName = 'MCTuple'
    mc_alg.ToolList += [
         'MCTupleToolKinematic'
        ,'TupleToolEventInfo'
        ,'MCTupleToolHierarchy'
        ,'MCTupleToolPrimaries'
        ,'MCTupleToolReconstructed'
        ,'MCTupleToolPID'
    ]
    mc_alg.Branches = {
         'B'       : '[[B+]cc ==>  K+  (J/psi(1S) ==>  mu+  mu-)]CC'
        ,'Jpsi'    : '[[B+]cc ==>  K+ ^(J/psi(1S) ==>  mu+  mu-)]CC'
        ,'muplus'  : '[[B+]cc ==>  K+  (J/psi(1S) ==> ^mu+  mu-)]CC'
        ,'muminus' : '[[B+]cc ==>  K+  (J/psi(1S) ==>  mu+ ^mu-)]CC'
        ,'hplus'   : '[[B+]cc ==> ^K+  (J/psi(1S) ==>  mu+  mu-)]CC'
    }

########################################################################
from Configurables import DaVinci
if is_data:
    GaudiSequencer('TupleSeq').Members += [rd_SEQ.sequence()]
else:
    GaudiSequencer('TupleSeq').Members += [mc_alg, rd_SEQ.sequence()]
# DaVinci().DataType       = '2016'
if is_mdst and is_mc:
    DaVinci().RootInTES      = '/Event/AllStreams'
elif is_mdst:
    DaVinci().RootInTES      = '/Event/Dimoun'
DaVinci().EvtMax         = -1                                   # Number of events
DaVinci().InputType      = f_type
DaVinci().PrintFreq      = 1000
DaVinci().SkipEvents     = 0                                    # Events to skip
DaVinci().HistogramFile  = 'DVHistos.root'                      # Histogram file
# DaVinci().TupleFile      = 'BuJpsiK.root'                       # Ntuple
DaVinci().Simulation     = is_mc
DaVinci().Lumi           = is_data
