from GaudiKernel.SystemOfUnits import *
from Gaudi.Configuration import *
from Configurables import DaVinci
from Configurables import GaudiSequencer, CombineParticles, OfflineVertexFitter, TupleToolGeometry
from Configurables import DecayTreeTuple, EventTuple, TupleToolTrigger, TupleToolTISTOS, FilterDesktop
from Configurables import BackgroundCategory, TupleToolDecay, TupleToolVtxIsoln, TupleToolPid, EventCountHisto, TupleToolRecoStats
from Configurables import LoKi__Hybrid__TupleTool, TupleToolVeto, TupleToolTrackPosition, TupleToolTrackInfo
from Configurables import TupleToolTagging, TupleToolL0Data, TupleToolL0Calo, TupleToolP2VV
from DecayTreeTuple.Configuration import *

name = 'Bc2Bspi2JpsiPhi'
is_mc = DaVinci().Simulation
is_data = not is_mc
input_type = DaVinci().InputType.upper()
is_dst = input_type == 'DST'
is_ldst = input_type == 'LDST'
is_mdst = input_type == 'MDST'
with_ft = not is_mdst
f_type = DaVinci().InputType

l0_lines = [
     'L0MuonDecision'
    ,'L0DiMuonDecision'
    ,'L0GlobalDecision'
]

hlt1_lines = [
     'Hlt1TrackMVADecision'
    ,'Hlt1TwoTrackMVADecision'
    ,'Hlt1TrackMVALooseDecision'
    ,'Hlt1TwoTrackMVALooseDecision'
    ,'Hlt1TrackMuonDecision'
    ,'Hlt1TrackMuonMVADecision'
    ,'Hlt1DiMuonHighMassDecision'
    ,'Hlt1DiMuonLowMassDecision'
    ,'Hlt1SingleMuonHighPTDecision'
    ,'Hlt1DiMuonNoL0Decision'
    ,'Hlt1L0AnyDecision'
    ,'Hlt1GlobalDecision'
]

hlt2_lines = [
     'Hlt2Topo2BodyDecision'
    ,'Hlt2Topo3BodyDecision'
    ,'Hlt2Topo4BodyDecision'
    ,'Hlt2TopoMu2BodyDecision'
    ,'Hlt2TopoMu3BodyDecision'
    ,'Hlt2TopoMu4BodyDecision'
    ,'Hlt2TopoMuMu2BodyDecision'
    ,'Hlt2TopoMuMu3BodyDecision'
    ,'Hlt2TopoMuMu4BodyDecision'
    ,'Hlt2SingleMuonDecision'
    ,'Hlt2SingleMuonHighPTDecision'
    ,'Hlt2SingleMuonLowPTDecision'
    ,'Hlt2SingleMuonRareDecision'
    ,'Hlt2SingleMuonVHighPTDecision'
    ,'Hlt2DiMuonDecision'
    ,'Hlt2DiMuonJPsiDecision'
    ,'Hlt2DiMuonDetachedDecision'
    ,'Hlt2DiMuonDetachedHeavyDecision'
    ,'Hlt2DiMuonDetachedJPsiDecision'
    ,'Hlt2DiMuonDetachedPsi2SDecision'
]

mtl = l0_lines + hlt1_lines + hlt2_lines

location = '/Event/AllStreams/Phys/BetaSBs2JpsiPhiDetachedLine/Particles'
if is_data:
    location = '/Event/Dimuon/Phys/BetaSBs2JpsiPhiDetachedLine/Particles'
if is_mdst:
    location = 'Phys/BetaSBs2JpsiPhiDetachedLine/Particles'

from PhysSelPython.Wrappers import AutomaticData
b2jpsiphi_selection = AutomaticData(location)

tl = [
     'TupleToolKinematic'
    ,'TupleToolPropertime'
    ,'TupleToolPrimaries'
    ,'TupleToolEventInfo'
    ,'TupleToolPid'
    ,'TupleToolANNPID'
    ,'TupleToolL0Calo'
    ,'TupleToolL0Data'
    ,'TupleToolTISTOS'
]
if is_mc:
    from Configurables import TupleToolMCBackgroundInfo
    tl += ['TupleToolMCBackgroundInfo']

from GaudiConfUtils.ConfigurableGenerators import DecayTreeTuple as TUPLE
from PhysSelPython.Wrappers                import SimpleSelection
rd_selection = SimpleSelection(
     '{}_Tuple'.format(name)
    , TUPLE
    , [b2jpsiphi_selection]
    ## Properties:
    , Decay    = 'B_s0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)'
    , ToolList = tl
    , Branches = {
         'B'       : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+  K-)'
        ,'Jpsi'    : 'B_s0 -> ^(J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+  K-)'
        ,'muplus'  : 'B_s0 ->  (J/psi(1S) -> ^mu+  mu-)  (phi(1020) ->  K+  K-)'
        ,'muminus' : 'B_s0 ->  (J/psi(1S) ->  mu+ ^mu-)  (phi(1020) ->  K+  K-)'
        ,'X'       : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-) ^(phi(1020) ->  K+  K-)'
        ,'hplus'   : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) -> ^K+  K-)'
        ,'hminus'  : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+ ^K-)'
    }
)

tuple_B2jpsiphi = rd_selection.algorithm()

if is_mc:
    mctruth = tuple_B2jpsiphi.addTupleTool('TupleToolMCTruth')
    mctruth.ToolList = [
         'MCTupleToolKinematic'
        ,'MCTupleToolHierarchy'
        ,'MCTupleToolReconstructed'
        ,'MCTupleToolPID'
    ]

tuples = [tuple_B2jpsiphi]

for particle in ['B', 'Jpsi', 'muplus', 'muminus', 'X', 'hplus', 'hminus']:
    tuple_B2jpsiphi.addTool(TupleToolDecay, name = particle)

tuple_B2jpsiphi.ReFitPVs = True

geom = tuple_B2jpsiphi.addTupleTool('TupleToolGeometry')
geom.RefitPVs = True
geom.Verbose = True

reco_stats = tuple_B2jpsiphi.addTupleTool('TupleToolRecoStats')
reco_stats.Verbose = True

track_inf = tuple_B2jpsiphi.addTupleTool('TupleToolTrackInfo')
track_inf.Verbose = True

track_pos = tuple_B2jpsiphi.addTupleTool('TupleToolTrackPosition')
track_pos.Z = 7500.

trig_tool = tuple_B2jpsiphi.addTupleTool('TupleToolTrigger')
trig_tool.Verbose = True
trig_tool.TriggerList = mtl
trig_tool.OutputLevel = 6

### TISTOS
tuple_B2jpsiphi.B.ToolList += ['TupleToolTISTOS']
tuple_B2jpsiphi.B.addTool(TupleToolTISTOS, name = 'TupleToolTISTOS')
tuple_B2jpsiphi.B.TupleToolTISTOS.Verbose = True
tuple_B2jpsiphi.B.TupleToolTISTOS.TriggerList = mtl

# # change TOSFracMu for B
# from Configurables import TriggerTisTos
# tuple_B2jpsiphi.B.TupleToolTISTOS.addTool(TriggerTisTos())
# tuple_B2jpsiphi.B.TupleToolTISTOS.TriggerTisTos.TOSFracMuon = 0.
# tuple_B2jpsiphi.B.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
# tuple_B2jpsiphi.B.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.

tuple_B2jpsiphi.Jpsi.ToolList += ['TupleToolTISTOS']
tuple_B2jpsiphi.Jpsi.addTool(TupleToolTISTOS, name = 'TupleToolTISTOS')
tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.Verbose = True
tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.TriggerList = mtl

# # Change TOSFracMu for Jpsi
# from Configurables import TriggerTisTos
# tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.addTool(TriggerTisTos())
# tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracMuon = 0.
# tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracEcal = 0.
# tuple_B2jpsiphi.Jpsi.TupleToolTISTOS.TriggerTisTos.TOSFracHcal = 0.

LoKi_B = LoKi__Hybrid__TupleTool('LoKi_B')
LoKi_B.Variables = {
     'JpsiPhiMass'          : 'WM(\'J/psi(1S)\',\'phi(1020)\')'
    ,'ETA'                  : 'ETA'
    ,'DOCA'                 : 'DOCA(1,2)'
    ,'Y'                    : 'Y'
    ,'LV01'                 : 'LV01'
    ,'LV02'                 : 'LV02'
    ,'LOKI_FDCHI2'          : 'BPVVDCHI2'
    ,'LOKI_FDS'             : 'BPVDLS'
    ,'LOKI_DIRA'            : 'BPVDIRA'
    ,'LOKI_DTF_CTAU'        : 'DTF_CTAU(0, True)'
    ,'LOKI_DTF_CTAUS'       : 'DTF_CTAUSIGNIFICANCE(0, True)'
    ,'LOKI_DTF_CHI2NDOF'    : 'DTF_CHI2NDOF(True)'
    ,'LOKI_DTF_CTAUERR'     : 'DTF_CTAUERR(0, True)'
    ,'LOKI_MASS_JpsiConstr' : 'DTF_FUN(M, True, \'J/psi(1S)\')'
    ,'LOKI_DTF_VCHI2NDOF'   : 'DTF_FUN(VFASPF(VCHI2/VDOF), True)'
}

tuple_B2jpsiphi.B.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_B']
tuple_B2jpsiphi.B.addTool(LoKi_B)

# Requires SemileptonicCommonTools->TupleToolDocas, out of tree atm : https://gitlab.cern.ch/lhcb-slb/SemileptonicCommonTools
# doca = tuple_B2jpsiphi.B.addTupleTool('TupleToolDocas')
# doca.Name = ['hplus_hminus', 'muplus_muminus', 'hplus_muplus', 'hplus_muminus', 'hminus_muplus', 'hminus_muminus']
# doca.Location1 = [
#      'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)'
# ]
# doca.Location2 = [
#      'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)'
#     ,'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
# ]

LoKi_Jpsi = LoKi__Hybrid__TupleTool('LoKi_Jpsi')
LoKi_Jpsi.Variables = {
     'ETA'         : 'ETA'
    ,'Y'           : 'Y'
    ,'LV01'        : 'LV01'
    ,'LV02'        : 'LV02'
    ,'LOKI_FDCHI2' : 'BPVVDCHI2'
    ,'LOKI_FDS'    : 'BPVDLS'
    ,'LOKI_DIRA'   : 'BPVDIRA'
}

tuple_B2jpsiphi.Jpsi.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_Jpsi']
tuple_B2jpsiphi.Jpsi.addTool(LoKi_Jpsi)

LoKi_muon = LoKi__Hybrid__TupleTool('LoKi_muon')
LoKi_muon.Variables = {
     'LOKI_ETA': 'ETA'
    ,'LOKI_Y'  : 'Y'
}
tuple_B2jpsiphi.muplus.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_muon']
tuple_B2jpsiphi.muminus.ToolList += ['LoKi::Hybrid::TupleTool/LoKi_muon']
tuple_B2jpsiphi.muplus.addTool(LoKi_muon)
tuple_B2jpsiphi.muminus.addTool(LoKi_muon)

# refit with mass constraint
from Configurables import TupleToolDecayTreeFitter

tuple_B2jpsiphi.B.ToolList += ['TupleToolDecayTreeFitter/ConstJpsi']
tuple_B2jpsiphi.B.addTool(TupleToolDecayTreeFitter('ConstJpsi'))
tuple_B2jpsiphi.B.ConstJpsi.Verbose = True
tuple_B2jpsiphi.B.ConstJpsi.UpdateDaughters = True
tuple_B2jpsiphi.B.ConstJpsi.constrainToOriginVertex = True
tuple_B2jpsiphi.B.ConstJpsi.daughtersToConstrain = ['J/psi(1S)']

tuple_B2jpsiphi.B.ToolList += ['TupleToolDecayTreeFitter/ConstJpsiNoPV']
tuple_B2jpsiphi.B.addTool(TupleToolDecayTreeFitter('ConstJpsiNoPV'))
tuple_B2jpsiphi.B.ConstJpsiNoPV.Verbose = True
tuple_B2jpsiphi.B.ConstJpsiNoPV.UpdateDaughters = True
tuple_B2jpsiphi.B.ConstJpsiNoPV.constrainToOriginVertex = False
tuple_B2jpsiphi.B.ConstJpsiNoPV.daughtersToConstrain = ['J/psi(1S)']

tuple_B2jpsiphi.B.ToolList += ['TupleToolDecayTreeFitter/ConstBJpsi']
tuple_B2jpsiphi.B.addTool(TupleToolDecayTreeFitter('ConstBJpsi'))
tuple_B2jpsiphi.B.ConstBJpsi.Verbose = True
tuple_B2jpsiphi.B.ConstBJpsi.UpdateDaughters = True
tuple_B2jpsiphi.B.ConstBJpsi.constrainToOriginVertex = True
tuple_B2jpsiphi.B.ConstBJpsi.daughtersToConstrain = ['B_s0', 'J/psi(1S)']

tuple_B2jpsiphi.B.ToolList += ['TupleToolDecayTreeFitter/ConstOnlyPV']
tuple_B2jpsiphi.B.addTool(TupleToolDecayTreeFitter('ConstOnlyPV'))
tuple_B2jpsiphi.B.ConstOnlyPV.Verbose = True
tuple_B2jpsiphi.B.ConstOnlyPV.UpdateDaughters = True
tuple_B2jpsiphi.B.ConstOnlyPV.constrainToOriginVertex = True

### Backgrounds
# B -> KplusPiMuMu
tuple_B2jpsiphi.B.ToolList +=  ['TupleToolDecayTreeFitter/B2KpPiJpsi']
B2KpPiJpsi = TupleToolDecayTreeFitter(
    'B2KpPiJpsi'
    ,Verbose = True
    ,Substitutions = {
        'Beauty -> Meson (phi(1020) -> ^K+ X-)': 'pi+'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsiphi.B.addTool(B2KpPiJpsi)

# B -> KminusPiMuMu
tuple_B2jpsiphi.B.ToolList +=  ['TupleToolDecayTreeFitter/B2KmPiJpsi']
B2KmPiJpsi = TupleToolDecayTreeFitter(
    'B2KmPiJpsi'
    ,Verbose = True
    ,Substitutions = {
        'Beauty -> Meson (phi(1020) -> X+ ^K-)': 'pi-'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsiphi.B.addTool(B2KmPiJpsi)

# Lb -> pKMuMu (Kplus)
tuple_B2jpsiphi.B.ToolList +=  ['TupleToolDecayTreeFitter/pKMuMuKplus']
pKMuMuKplus = TupleToolDecayTreeFitter(
    'pKMuMuKplus'
    ,Verbose = True
    ,Substitutions = {
        'Beauty -> Meson (phi(1020) -> ^K+ X-)': 'p+'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsiphi.B.addTool(pKMuMuKplus)

# Lb -> pKMuMu (Kminus)
tuple_B2jpsiphi.B.ToolList +=  ['TupleToolDecayTreeFitter/pKMuMuKminus']
pKMuMuKminus = TupleToolDecayTreeFitter(
    'pKMuMuKminus'
    ,Verbose = True
    ,Substitutions = {
        'Beauty -> Meson (phi(1020) -> X+ ^K-)': 'p~-'
    }
    ,daughtersToConstrain = ['J/psi(1S)']
    ,constrainToOriginVertex = True
)
tuple_B2jpsiphi.B.addTool(pKMuMuKminus)

if with_ft:
    # specific tagging tuple tool
    btag = tuple_B2jpsiphi.addTupleTool('TupleToolTagging')
    if is_dst or is_ldst:
        btag.Verbose = True
        btag.AddMVAFeatureInfo = True

        from Configurables import BTaggingTool
        btagtool = btag.addTool(BTaggingTool, name = 'MyBTaggingTool')

        from FlavourTagging.Tunings import applyTuning as applyFTTuning
        applyFTTuning(btagtool, tuning_version = 'Summer2017Optimisation_v4_Run2')
        btag.TaggingToolName = btagtool.getFullName()
    else:
        btag.UseFTfromDST = True

# event tuple
from Configurables import LoKi__Hybrid__EvtTupleTool
LoKi_EvtTuple=LoKi__Hybrid__EvtTupleTool('LoKi_EvtTuple')
LoKi_EvtTuple.VOID_Variables = {
     'LoKi_nPVs'              : 'CONTAINS(\'Rec/Vertex/Primary\')'
    ,'LoKi_nSpdMult'          : 'CONTAINS(\'Raw/Spd/Digits\')'
    ,'LoKi_nVeloClusters'     : 'CONTAINS(\'Raw/Velo/Clusters\')'
    ,'LoKi_nVeloLiteClusters' : 'CONTAINS(\'Raw/Velo/LiteClusters\')'
    ,'LoKi_nITClusters'       : 'CONTAINS(\'Raw/IT/Clusters\')'
    ,'LoKi_nTTClusters'       : 'CONTAINS(\'Raw/TT/Clusters\')'
    ,'LoKi_nOThits'           : 'CONTAINS(\'Raw/OT/Times\')'
}

tuple_B2jpsiphi.ToolList += ['LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple']
tuple_B2jpsiphi.addTool(LoKi_EvtTuple)

### P2VV
if is_mc:
    from Configurables import MCTupleToolP2VV, TupleToolMCTruth
    tuple_B2jpsiphi.B.addTool(TupleToolMCTruth)
    tuple_B2jpsiphi.B.TupleToolMCTruth.ToolList += ['MCTupleToolP2VV']
    tuple_B2jpsiphi.B.TupleToolMCTruth.addTool(MCTupleToolP2VV)
    tuple_B2jpsiphi.B.TupleToolMCTruth.MCTupleToolP2VV.Calculator = 'MCBs2JpsiPhiAngleCalculator'

TupleToolP2VV_BsDetached             = TupleToolP2VV('TupleToolP2VV_Bs')
TupleToolP2VV_BsDetached.Calculator  = 'Bs2JpsiPhiAngleCalculator'
TupleToolP2VV_BsDetached.OutputLevel = 6
tuple_B2jpsiphi.B.addTool(TupleToolP2VV_BsDetached)
tuple_B2jpsiphi.B.ToolList          += ['TupleToolP2VV/TupleToolP2VV_Bs']

from PhysSelPython.Wrappers import SelectionSequence
rd_SEQ = SelectionSequence('{}_SelSeq'.format(name), rd_selection)

mc_alg = None
if not is_data:
    from Configurables import MCDecayTreeTuple
    mc_alg = MCDecayTreeTuple('{}_MCTuple'.format(name))
    mc_alg.Decay = '[B_c+ ==> ^(B_s0 ==> ^(J/psi(1S) ==> ^mu+ ^mu-) ^(phi(1020) ==> ^K+ ^K-)) ^pi+]CC'
    mc_alg.TupleName = 'MCTuple'
    mc_alg.ToolList += [
         'MCTupleToolKinematic'
        ,'TupleToolEventInfo'
        ,'MCTupleToolHierarchy'
        ,'MCTupleToolPrimaries'
        ,'MCTupleToolReconstructed'
        ,'MCTupleToolPID'
    ]
    mc_alg.Branches = {
         'Bc'      : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-))  pi+]CC'
        ,'B'       : '[B_c+ ==> ^(B_s0 ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-))  pi+]CC'
        ,'Jpsi'    : '[B_c+ ==>  (B_s0 ==> ^(J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-))  pi+]CC'
        ,'muplus'  : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==> ^mu+  mu-)  (phi(1020) ==>  K+  K-))  pi+]CC'
        ,'muminus' : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+ ^mu-)  (phi(1020) ==>  K+  K-))  pi+]CC'
        ,'X'       : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+  mu-) ^(phi(1020) ==>  K+  K-))  pi+]CC'
        ,'hplus'   : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==> ^K+  K-))  pi+]CC'
        ,'hminus'  : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+ ^K-))  pi+]CC'
        ,'piplus'  : '[B_c+ ==>  (B_s0 ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-)) ^pi+]CC'
    }

########################################################################
from Configurables import DaVinci
if is_data:
    GaudiSequencer('TupleSeq').Members += [rd_SEQ.sequence()]
else:
    GaudiSequencer('TupleSeq').Members += [mc_alg, rd_SEQ.sequence()]
# DaVinci().DataType       = '2012'
if is_mdst and is_mc:
    DaVinci().RootInTES      = '/Event/AllStreams'
elif is_mdst:
    DaVinci().RootInTES      = '/Event/Dimoun'
DaVinci().EvtMax         = -1                                   # Number of events
DaVinci().InputType      = f_type
DaVinci().PrintFreq      = 1000
DaVinci().SkipEvents     = 0                                    # Events to skip
DaVinci().HistogramFile  = 'DVHistos.root'                      # Histogram file
# DaVinci().TupleFile      = 'BsJpsiPhi.root'                     # Ntuple
DaVinci().Simulation     = is_mc
DaVinci().Lumi           = is_data
# DaVinci().CondDBtag      = 'Sim08-20130503-1-vc-md100'          # Sim08a
# DaVinci().DDDBtag        = 'Sim08-20130503-1'                   # Sim08a
