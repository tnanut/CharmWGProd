"""Use the latest CondDB tags for the current DaVinci().DataType.

TODO: do we want to use this? it's recommended when using the momentum scaling.
"""
from Configurables import CondDB, DaVinci


DaVinci().Simulation = False
DaVinci().Lumi = True

assert DaVinci().DataType in ['2015', '2016', '2017', '2018'], 'Unknown DataType'

CondDB(LatestGlobalTagByDataType=DaVinci().DataType)