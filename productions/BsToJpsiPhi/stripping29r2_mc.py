"""Run some lines from Stripping29r2 on MC.

Some simulated samples don't have the Stripping run on them, so we do it
manually here, running only the lines we need.

Options file modified from
`$APPCONFIGOPTS/DaVinci/DV-Stripping29r2-Stripping-MC-NoPrescaling-LDST.py`,
taken from production step ID 133718.
"""
LINES_TO_RUN = [
    'StrippingBetaSBu2JpsiKDetachedLine'
]

#use CommonParticlesArchive
stripping='stripping29r2'
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.2)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive


#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

# Fix Bu2LLK config for S29r2, which was not updated in Settings but is correct in Archive
from StrippingArchive.Stripping29r2.StrippingRD.StrippingBu2LLK import default_config as Bu2LLK_config
config = dict(config)
config['Bu2LLK']['CONFIG'] = Bu2LLK_config['CONFIG']

streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("AllStreams")

# These lines are needed to remove duplicates (everything goes to the same stream)
for stream in streams:
    if 'MiniBias' not in stream.name():
        for line in stream.lines:
            dup=False
            for line2 in AllStreams.lines:
                if line2.name()==line.name():
                    dup=True
                    break
            if not dup:
                if not line.name() in LINES_TO_RUN:
                    continue
                line._prescale = 1.0
                AllStreams.appendLines([line]) 

sc = StrippingConf( Streams = [AllStreams],
                    MaxCandidates = 2000,
                    MaxCombinations = 10000000,
                    TESPrefix = 'Strip' )

AllStreams.sequence().IgnoreFilterPassed = True

enablePacking = False

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import (SelDSTWriter,
                                      stripDSTStreamConf,
                                      stripDSTElements)

SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking)
    }

SelDSTWriterConf = {
    'default'               : stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=True,fileExtension='.ldst')
    }


dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='000000',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x42722920)

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60
