#
# DVTupleMaker
# A script to make tuples out of the stripping using DaVinci
#
import os

from Configurables import DaVinci, DecayTreeTuple, TupleToolDecay, LoKi__Hybrid__TupleTool
from DecayTreeTuple import Configuration
from PhysConf.Filters import LoKi_Filters
from LoKiPhys.decorators import *
from DecayTreeTuple.Configuration import *

########################################
# User configuration begins
########################################

# # Data-taking year
# data_type = '2017'
data_type = DaVinci().DataType
# data or MC?
IsMC=DaVinci().Simulation
# RootInTES
root_in_tes = '/Event/Bhadron/' if not IsMC else '/Event/AllStreams/'

# The full name of the HLT2 line
lnames = {'KSPimPipPip': 'PiPiPi',
          'KSKmPipPip': 'KPiPi', 'KSKpPimPip': 'KPiPi',
          'KSKmKpPip': 'KKPi', 'KSKpKpPim': 'KKPi',
          'KSKmKpKp': 'KKK'}
line_names = {}
for x in lnames.keys():
    line_names[x+'_DD']='B2KShhh_'+lnames[x]+'_DDLine'
    line_names[x+'_LL']='B2KShhh_'+lnames[x]+'_LLLine'

# The decay descriptor, including string template markers to indicate which
# particles to save information about, and what names the branches should be
# called
decay_descriptors = {
    'KSPimPipPip' : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}pi- ${P1}pi+ ${P2}pi+]CC',
    'KSKmPipPip'  : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K-  ${P1}pi+ ${P2}pi+]CC',
    'KSKpPimPip'  : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}pi- ${P2}pi+]CC',
    'KSKmKpPip'   : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K-  ${P1}K+  ${P2}pi+]CC',
    'KSKpKpPim'   : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}K+  ${P2}pi-]CC',
    'KSKmKpKp'    : '[${B}B+ -> ${KS}(KS0 -> ${KS_P0}pi- ${KS_P1}pi+) ${P0}K+  ${P1}K+  ${P2}K- ]CC',
}

# Probnn tunings
Probnn = []#["MC15TuneV1"] # uncomment if using DV < v40r0 or new tuning becomes available

########################################
# User configuration ends
########################################

# DecayTreeTuple input location template
particles_input = 'Phys/{0}/Particles'

# Define some trigger lines to check for TISTOS
commonTriggerList = ['L0'+x+'Decision' for x in ['Hadron','Muon','DiMuon','Electron','Photon']]
commonTriggerList += ['Hlt1'+x+'Decision' for x in ['TrackMVA','TwoTrackMVA']]
TopoBody = ['Hlt2'+x+'Decision' for x in ['Topo2Body','Topo3Body','Topo4Body']]
additionalBmesTriggerList = ['Hlt1'+x+'Decision' for x in ['TrackMuon','TrackMuonMVA']]
additionalBmesTriggerList += TopoBody

TriggerLists = {'B'     : commonTriggerList+additionalBmesTriggerList,
                'KS'    : commonTriggerList,
                'KS_P0' : commonTriggerList,
                'KS_P1' : commonTriggerList,
                'P0'    : commonTriggerList,
                'P1'    : commonTriggerList,
                'P2'    : commonTriggerList }

def SetupMCTools(dtt, IsTurbo):
    dtt.ToolList += [ "TupleToolMCTruth", "TupleToolMCBackgroundInfo" ]
    mc_tools = ['MCTupleToolPrompt', 'MCTupleToolKinematic'] #, "MCTupleToolHierarchy"]
    if IsTurbo:
        ## Set MC truth for Turbo
        from TeslaTools import TeslaTruthUtils
        relations = TeslaTruthUtils.getRelLocs() + [
        TeslaTruthUtils.getRelLoc(''),
        # Location of the truth tables for PersistReco objects
        'Relations/Hlt2/Protos/Charged'
        ]
        #relations = [TeslaTruthUtils.getRelLoc('')]
        #relations.append('/Event/Turbo/Relations/Hlt2/Protos/Charged')
        TeslaTruthUtils.makeTruth(dtt, relations, mc_tools)
    else:
        from Configurables import TupleToolMCTruth
        MCTruth = TupleToolMCTruth()
        MCTruth.ToolList =  mc_tools
        dtt.addTool(MCTruth)
    return

def CreateDTT(lname, IsMC=False):
    line_name = line_names[lname]
    decay_desc = decay_descriptors[lname[:lname.find('_')]]
    IsTurbo = 'Turbo' in line_name
    # Declare the tuple
    dtt = DecayTreeTuple('{0}_Tuple'.format(lname))
    dtt.Inputs = [particles_input.format(line_name)]
    dtt.setDescriptorTemplate(decay_desc)
    # fix to avoid bug in setDescriptorTemplate in DV v44r7
    dtt.Branches['B'] = dtt.Decay.replace('^','')
    # list of general tools
    GeneralTools = ["Geometry", "Primaries", "EventInfo", "Trigger", "Kinematic", "TrackInfo", "Propertime", "Pid", "RecoStats"]
    dtt.ToolList = ['TupleTool'+tool for tool in GeneralTools]
    # Setup MC tools if neeeded
    if IsMC: SetupMCTools(dtt, IsTurbo)
    # DTF Refit info
    B_Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), 'B') ]
    fit = B_Node.addTupleTool('TupleToolDecayTreeFitter/ReFit')
    fit.Verbose= False
    fit.constrainToOriginVertex = True
    fit.UpdateDaughters = True
    # DTF Mass-Constrained D0 fit
    fit_KS_MC = B_Node.addTupleTool('TupleToolDecayTreeFitter/KSFit')
    fit_KS_MC.Verbose= False
    fit_KS_MC.constrainToOriginVertex = True
    fit_KS_MC.daughtersToConstrain += ['KS0']
    fit_KS_MC.UpdateDaughters = True
    # Setup the long-lived particles branches
    IntermediateTools = [ 'Geometry', 'Kinematic', 'Propertime']
    for vtx in ['KS']:
        Vtx = dtt.allConfigurables['%s.%s' % ( dtt.name(), vtx) ]
        Vtx.InheritTools = False
        Vtx.ToolList = [  "TupleTool"+name for name in IntermediateTools ]
        if IsMC:
            Vtx.addTool( dtt.allConfigurables['ToolSvc.TupleToolMCTruth'] )
            Vtx.ToolList += [ "TupleToolMCTruth" ]
    # Setup the stable particles branches
    TrackTools = ['Geometry','Kinematic','Pid','TrackInfo']
    for trk in ['KS_P0', 'KS_P1', 'P0', 'P1', 'P2' ]:
        branch = dtt.allConfigurables['%s.%s' % ( dtt.name(), trk) ]
        # P info
        branch.InheritTools = False
        branch.ToolList = [ "TupleTool"+name for name in TrackTools ]
        if len(Probnn):
            from Configurables import TupleToolANNPID
            branch.ToolList += [ "TupleToolANNPID" ]
            branch.addTool(TupleToolANNPID, name="TupleToolANNPID" )
            branch.TupleToolANNPID.ANNPIDTunes = Probnn
        if IsMC:
            branch.addTool(dtt.allConfigurables['ToolSvc.TupleToolMCTruth'])
            branch.ToolList += [ "TupleToolMCTruth",'TupleToolL0Calo' ]
    # TISTOS
    from Configurables import TupleToolTISTOS
    for nodeName, tList in TriggerLists.iteritems():
        Node = dtt.allConfigurables['%s.%s' % ( dtt.name(), nodeName) ]
        Node.ToolList += [ "TupleToolTISTOS" ]
        Node.addTool(TupleToolTISTOS, name="TupleToolTISTOS" )
        Node.TupleToolTISTOS.Verbose=True
        Node.TupleToolTISTOS.TriggerList = tList
        if IsTurbo: Node.TupleToolTISTOS.FillHlt2 = False
    # LoKi Variables
    # loki_B = B_Node.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_B")
    # loki_B.Variables={ "BPVCORRM" : "BPVCORRM"}
    LoKiVars = LoKi__Hybrid__TupleTool('LoKi_Variables')
    lokivars = {}#"BPVCORRM" : "BPVCORRM"}
    conepath = root_in_tes+'Phys/'+line_name+'/P2ConeVar{0}_{1}'
    conemult = 'ConeMult{0}_{1}'
    coneptas = 'ConePtAsym{0}_{1}'
    for coneangle in ['08','10','13','17']:
        for par in ['1','2','3']:
            cpath = conepath.format(coneangle,par)
            lokivars.update({
                conemult.format(coneangle,par): "RELINFO('%s','CONEMULT',-999)"%(cpath),
                coneptas.format(coneangle,par): "RELINFO('%s','CONEPTASYM',-999)"%(cpath)
            })
    vtxisopath = root_in_tes+'Phys/'+line_name+'/VertexIsoInfo'
    for vtxisovar in ['VTXISONUMVTX','VTXISODCHI2ONETRACK','VTXISODCHI2MASSONETRACK','VTXISODCHI2TWOTRACK','VTXISODCHI2MASSTWOTRACK']:
        lokivars.update({vtxisovar: "RELINFO('%s','%s',-999)"%(vtxisopath,vtxisovar)})# FIXXX
    LoKiVars.Variables = lokivars
    dtt.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Variables"]
    dtt.addTool(LoKiVars)
    # Finishing
    return dtt
# Create a tuple for each line
ntuples = [CreateDTT(lname+x,IsMC) for lname in lnames for x in ['_DD','_LL']]

# Speed up processing by filtering out events with no positive signal decisions
# trigger_filter = LoKi_Filters(
#     HLT2_Code="HLT_PASS_RE('.*{0}.*')".format(hlt2_line_name)
# )

# Setup Momentum calibration
# --- Begin MomentumCorrection ---
def MomentumCorrection(IsMC=False):
    """
        Returns the momentum scale correction algorithm for data tracks or the momentum smearing algorithm for MC tracks
    """
    if not IsMC: ## Apply the momentum error correction (for data only)
        from Configurables import TrackScaleState as SCALE
        scaler = SCALE('StateScale')
        return scaler
    else: ## Apply the momentum smearing (for MC only)
        from Configurables import TrackSmearState as SMEAR
        smear = SMEAR('StateSmear')
        return smear
    return
# ---  End MomentumCorrection  ---

dv = DaVinci()
#dv.EventPreFilters = trigger_filter.filters('TriggerFilters')
dv.UserAlgorithms = [MomentumCorrection(IsMC)]+ntuples
dv.TupleFile = 'DVntuple.root'
dv.PrintFreq = 1000
dv.DataType = data_type
dv.Turbo = False
dv.InputType = 'MDST'
dv.RootInTES = root_in_tes
dv.Lumi = True


# # Testing
# files = {
#     '2015': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision15/BHADRON.MDST/00068487/0001/00068487_00012312_1.bhadron.mdst"],
#     '2016': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision16/BHADRON.MDST/00069593/0001/00069593_00014372_1.bhadron.mdst"],
#     '2017': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/BHADRON.MDST/00071571/0001/00071571_00012352_1.bhadron.mdst"],
#     '2018': ["root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision18/BHADRON.MDST/00080044/0001/00080044_00012126_1.bhadron.mdst"],
# }
# dv.Input = files[data_type]
# dv.EvtMax=1000 # testing
