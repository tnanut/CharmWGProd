########################################
# User configuration for 2017 data tuple
########################################
from Configurables import DaVinci
# Data-taking year
DaVinci().DataType = '2018'
# data or MC?
DaVinci().Simulation = False
