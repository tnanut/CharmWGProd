import sys
import os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')
from Configurables import DaVinci, GaudiSequencer
from helpers import tuple_maker

# ONLY CONFIG NEEDED
tuple_name     = 'Lb2JpsiL_mmTuple' # BECAUSE THIS RECONSTRUCTS Lb2Lee with Jpsi resonance

tuple_seq = tuple_maker.tuple_maker( tuple_name, upstream_electrons=False )

