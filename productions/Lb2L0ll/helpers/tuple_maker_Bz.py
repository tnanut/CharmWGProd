import os

from Configurables import (
    DaVinci,
    GaudiSequencer,
    MCDecayTreeTuple,
    TupleToolDecay
)

from PhysSelPython.Wrappers import (
    AutomaticData,
    FilterSelection,
    SelectionSequence,
    TupleSelection,
)

from related_info import getLoKiTool
import sys
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')
import tags

def tuple_maker(name, decay, branches, stripping_line, is_mc, input_type, year, upstream_e = False):
    """Return a sequence for producing ntuples.

    Assumes the decay structure is like:

        B0 -> (J/Psi -> l- l+) (KS0 -> pi+ pi-)

    """
    # TODO: consider moving this block out of this method
    is_mdst = input_type.upper() == 'MDST'
    tes_root = ''
    if is_mc:
        #tes_root = '/Event/AllStreams'
        tes_root = '/Event/' + tags.tags[name[:-5]]['StrippingStream'] +'/'
    else:
        #tes_root = '/Event/BhadronCompleteEvent'
        tes_root = '/Event/Leptonic'
    if is_mdst:
        DaVinci().RootInTES = tes_root
    location = 'Phys/{}/Particles'.format(stripping_line)
    if upstream_e:
        location_upstream = 'Phys/Bu2LLK_eeLine4/Particles'
    if not is_mdst:
        location = os.path.join(tes_root, location)
        if upstream_e:
            location_upstream = os.path.join(tes_root, location_upstream)

    tuple_input = AutomaticData(location)

    ee_mode = 'eeLine' in stripping_line
    mm_mode = 'mmLine' in stripping_line
    emu_mode = 'meLine' in stripping_line

    # Tool list
    tools = [
        "TupleToolANNPID",
        "TupleToolEventInfo",
        "TupleToolGeometry",
        "TupleToolKinematic",
        "TupleToolPid",
        "TupleToolPrimaries",
        "TupleToolPropertime",
        "TupleToolRecoStats",
        "TupleToolTrackInfo",
        "TupleToolL0Data"
        ]
    if ee_mode or emu_mode:
        tools += ["TupleToolBremInfo"]
    if is_mc:
        tools += [
            'TupleToolMCBackgroundInfo'
        ]
    input_list = [tuple_input]

    tuple_selection = TupleSelection(
        name,
        input_list,
        decay,
        ToolList=tools,
    )

    # Define the tuple
    dtt = tuple_selection.algorithm()
    # Add upstream electrons if necessary
    if upstream_e:
        dtt.Inputs += [location_upstream]
    dtt.addBranches(branches)

    # L0 lines
    from Configurables import TupleToolTISTOS
    for particle in [dtt.B,dtt.JPs,dtt.K0,dtt.L1,dtt.L2,dtt.Pi1,dtt.Pi2]:
        particle.addTool( TupleToolTISTOS, name = "L0TISTOS" )
        particle.ToolList += [ "TupleToolTISTOS/L0TISTOS" ]

        particle.L0TISTOS.TriggerList = ["L0HadronDecision"]
        if ee_mode or emu_mode:
            particle.L0TISTOS.TriggerList += ["L0ElectronDecision"]
        if mm_mode or emu_mode:
            particle.L0TISTOS.TriggerList += ["L0MuonDecision"]
        if emu_mode:
            particle.L0TISTOS.TriggerList += ["L0DiMuonDecision"]

        particle.L0TISTOS.Verbose = True


    # Hlt1 info for each particle to calibrate TrackMVA
    for particle in [dtt.B, dtt.JPs, dtt.K0, dtt.L1, dtt.L2, dtt.Pi1, dtt.Pi2]:
        particle.addTool( TupleToolTISTOS, name = "HltTISTOS" )
        particle.ToolList += [ "TupleToolTISTOS/HltTISTOS" ]

        # Start with empty list
        particle.HltTISTOS.TriggerList = []

        if mm_mode or emu_mode:
            particle.HltTISTOS.TriggerList += [
                "Hlt1TrackMuonDecision",
                "Hlt1SingleMuonDecision"
                ]

        # Add the correct trigger lines for the year and channel
        if year in ['2011', '2012']:
            particle.HltTISTOS.TriggerList += [
                    "Hlt1TrackAllL0Decision",
                    "Hlt1TrackAllL0TightDecision"
                    ]

        else:
            particle.HltTISTOS.TriggerList += [
                "Hlt1TrackMVADecision",
                "Hlt1TrackMVALooseDecision",
                "Hlt1TwoTrackMVADecision",
                "Hlt1TwoTrackMVALooseDecision"
                ]
            # Only for 2016 or higher
            if year in ['2016', '2017', '2018']:
                particle.HltTISTOS.TriggerList += ["Hlt1TrackMuonMVADecision"]
        particle.HltTISTOS.Verbose = True

    #HLT for Lb
    dtt.B.HltTISTOS.TriggerList = []

    # For all years
    if mm_mode or emu_mode:
        dtt.B.HltTISTOS.TriggerList += [
            "Hlt1TrackMuonDecision",
            "Hlt1SingleMuonDecision"
            "Hlt1SingleMuonHighPTDecision",
            "Hlt1DiMuonHighMassDecision"
            ]

    if mm_mode:
        dtt.B.HltTISTOS.TriggerList += [
            "Hlt2DiMuonDetachedDecision",
            "Hlt2DiMuonDetachedHeavyDecision",
            "Hlt2DiMuonDetachedJPsiDecision",
            "Hlt2DiMuonDetachedPsi2SDecision",
            "Hlt2DiMuonJPsiHighPTDecision",
            "Hlt2DiMuonPsi2SHighPTDecision"
            ]

    # Add the correct trigger lines based per year and decay channel
    if year in ['2011', '2012']:
        dtt.B.HltTISTOS.TriggerList += [
                "Hlt1TrackAllL0Decision",
                "Hlt1TrackAllL0TightDecision",
                "Hlt2Topo2BodyBBDTDecision",
                "Hlt2Topo3BodyBBDTDecision",
                "Hlt2Topo4BodyBBDTDecision",
                ]

        if mm_mode or emu_mode:
            dtt.B.HltTISTOS.TriggerList += [
                "Hlt2TopoMu2BodyBBDTDecision",
                "Hlt2TopoMu3BodyBBDTDecision",
                "Hlt2TopoMu4BodyBBDTDecision",
                ]
        if ee_mode or emu_mode:
            dtt.B.HltTISTOS.TriggerList += [
                "Hlt2TopoE2BodyBBDTDecision",
                "Hlt2TopoE3BodyBBDTDecision",
                "Hlt2TopoE4BodyBBDTDecision"
                ]

    else:
        dtt.B.HltTISTOS.TriggerList += [
            "Hlt1TrackMVADecision",
            "Hlt1TrackMVALooseDecision",
            "Hlt1TwoTrackMVALooseDecision",
            "Hlt1TwoTrackMVADecision",
            ]

        # 2015 specific
        if year == '2015':
            dtt.B.HltTISTOS.TriggerList += [
                "Hlt2Topo2BodyDecision",
                "Hlt2Topo3BodyDecision",
                "Hlt2Topo4BodyDecision"
                ]

        # 2016, 2017, 2018 specific
        else:
            dtt.B.HltTISTOS.TriggerList += [
                "Hlt1TrackMuonMVADecision",
                ]

            if mm_mode:
                dtt.B.HltTISTOS.TriggerList += [
                "Hlt2TopoMuMu2BodyDecision",
                "Hlt2TopoMuMu3BodyDecision",
                "Hlt2TopoMuMu4BodyDecision"
                ]

            # ee, mm and emu specific
            if mm_mode or emu_mode:
                dtt.B.HltTISTOS.TriggerList += [
                    "Hlt1DiMuonHighMassDecision",
                    "Hlt2SingleMuonDecision",
                    "Hlt2DiMuonDetachedDecision",
                    "Hlt2TopoMu2BodyDecision",
                    "Hlt2TopoMu3BodyDecision",
                    "Hlt2TopoMu4BodyDecision",
                    ]

            # e mu modes get both electron and muon trigger
            if emu_mode:
                dtt.B.HltTISTOS.TriggerList += [
                    "Hlt2SingleElectronTFHighPtDecision",
                    "Hlt2SingleElectronTFLowPtDecision",
                    "Hlt2TopoE2BodyDecision",
                    "Hlt2TopoE3BodyDecision",
                    "Hlt2TopoE4BodyDecision",
                    "Hlt2TopoMuE2BodyDecision",
                    "Hlt2TopoMuE3BodyDecision",
                    "Hlt2TopoMuE4BodyDecision",
                    ]

            if ee_mode:
                dtt.B.HltTISTOS.TriggerList += [
                    "Hlt2SingleElectronTFHighPtDecision",
                    "Hlt2SingleElectronTFLowPtDecision",
                    "Hlt2TopoE2BodyDecision",
                    "Hlt2TopoE3BodyDecision",
                    "Hlt2TopoE4BodyDecision",
                    "Hlt2TopoEE2BodyDecision",
                    "Hlt2TopoEE3BodyDecision",
                    "Hlt2TopoEE4BodyDecision"
                    ]

    dtt.B.HltTISTOS.Verbose = True

    # Hlt2 info for JPsi
    dtt.JPs.addTool( TupleToolTISTOS, name = "HltTISTOS" )
    dtt.JPs.ToolList += [ "TupleToolTISTOS/HltTISTOS" ]
    dtt.JPs.HltTISTOS.TriggerList = []

    # Run 1 specific
    if year in ['2011', '2012']:
        dtt.JPs.HltTISTOS.TriggerList += ["Hlt2Topo2BodyBBDTDecision"]

        # e or mu?
        if ee_mode or emu_mode:
            dtt.JPs.HltTISTOS.TriggerList += ["Hlt2TopoE2BodyBBDTDecision"]
        if mm_mode or emu_mode:
            dtt.JPs.HltTISTOS.TriggerList += ["Hlt2TopoMu2BodyBBDTDecision"]

    # Run 2 specific:
    else:
        dtt.JPs.HltTISTOS.TriggerList += ["Hlt2Topo2BodyDecision"]

        # e or mu?
        if ee_mode:
            dtt.JPs.HltTISTOS.TriggerList += [
                "Hlt2TopoE2BodyDecision",
                "Hlt2TopoEE2BodyDecision"
                ]
        elif mm_mode:
            dtt.JPs.HltTISTOS.TriggerList += ["Hlt2TopoMu2BodyDecision"]
        else:
            dtt.JPs.HltTISTOS.TriggerList += [
                "Hlt2TopoMu2BodyDecision",
                "Hlt2TopoMuE2BodyDecision",
                "Hlt2TopoE2BodyDecision"
                ]

    dtt.JPs.HltTISTOS.Verbose = True

    # TupleToolDecay
    names = ['B', 'K0', 'JPsi', 'Pi1', 'Pi2', 'L1', 'L2']
    for particle in names:
        dtt.addTool(TupleToolDecay, name=particle)

    # Tool list
    dtt.ToolList += tools

    # Include TupleToolL0Calo for correction of ET cut in 2016 samples + possibility to bin in CaloRegions
    from Configurables import TupleToolL0Calo
    for particle in [dtt.L1,dtt.L2,dtt.Pi1,dtt.Pi2]:
        particle.addTool(TupleToolL0Calo,name='L0Calo')
        particle.ToolList += [ "TupleToolL0Calo/L0Calo"]
        particle.L0Calo.WhichCalo = "ECAL"

    for particle in [dtt.L1,dtt.L2]:
        particle.addTool(TupleToolL0Calo,name='L0Calo')
        particle.ToolList += [ "TupleToolL0Calo/L0Calo"]
        particle.L0Calo.WhichCalo = "ECAL"

    # Different DecayTreeFitter setups
    from Configurables import TupleToolDecayTreeFitter
    dtt.B.addTool( TupleToolDecayTreeFitter, name = "DTF" )
    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF" ]
    dtt.B.addTool( dtt.B.DTF.clone( "DTF_PV",
                                          Verbose = True,
                                          constrainToOriginVertex = True ) )
    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_PV" ]
    dtt.B.addTool( dtt.B.DTF.clone( "DTF_K0_PV",
                                          Verbose = True,
                                          constrainToOriginVertex = True,
                                          daughtersToConstrain = [ "KS0" ] ) )
    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_K0_PV" ]
    dtt.B.addTool( dtt.B.DTF.clone( "DTF_B0_PV",
                                          Verbose = True,
                                          constrainToOriginVertex = True,
                                          daughtersToConstrain = [ "B0" ] ) )
    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_B0_PV" ]
    dtt.B.addTool( dtt.B.DTF.clone( "DTF_B0_K0_PV",
                                          Verbose = True,
                                          constrainToOriginVertex = True,
                                          daughtersToConstrain = [ "B0","KS0" ] ) )
    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_B0_K0_PV" ]
    dtt.B.addTool( dtt.B.DTF.clone( "DTF_K0_JPs_PV",
                                          Verbose = True,
                                          constrainToOriginVertex = True,
                                          daughtersToConstrain = [ "KS0","J/psi(1S)" ] ) )

    dtt.B.ToolList += [ "TupleToolDecayTreeFitter/DTF_K0_Jps_PV" ]

    dtt.B.addTool( TupleToolTISTOS, name = "HltTISTOS" )
    dtt.B.ToolList += [ "TupleToolTISTOS/HltTISTOS" ]

    # Adding hop from loki functor
    from Configurables import  LoKi__Hybrid__TupleTool
    LoKi_HOP = LoKi__Hybrid__TupleTool("LoKi_HOP")
    LoKi_HOP.Variables ={
        'hop_LoKi_mass_bv': 'BPVHOPM()',
        'hop_LoKi_mass': 'HOPM(0,0,0)',
                }

    if ee_mode or emu_mode:
        dtt.B.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_HOP" ]
        dtt.B.addTool(LoKi_HOP)

    # Adding MIPCHI2 variable to p and pi
    LoKi_MIPCHI2 = LoKi__Hybrid__TupleTool("LoKi_MIPCHI2DV")
    LoKi_MIPCHI2.Variables ={
        "MIPCHI2" : "MIPCHI2DV(PRIMARY)",
                }

    # Adding Track momenta to l1 and l2
    LoKi_Track_mom = LoKi__Hybrid__TupleTool("LoKi_Track_mom")
    LoKi_Track_mom.Variables ={
        "TRACK_P"  : "PPINFO(504,-100,-200)",
        "TRACK_PT" : "PPINFO(505,-100,-200)",
        "TRACK_PX" : "PPINFO(505,-1e7,-1e7) * cos( PHI )",
        "TRACK_PY" : "PPINFO(505,-1e7,-1e7) * sin( PHI )",
        "TRACK_PZ" : "PPINFO(505,-1e7,-1e7) * sinh(ETA )",
                }

    #adding DOCA variables
    LoKi_DOCA = LoKi__Hybrid__TupleTool("LoKi_DOCA")
    LoKi_DOCA.Variables = {
        "DOCA01"  :  "DOCA(0,1)",
        "DOCA02"  :  "DOCA(0,2)",
        "DOCA12"  :  "DOCA(1,2)",
        "DOCA_MAX":  "DOCAMAX",
        }

    LoKi_ACC = LoKi__Hybrid__TupleTool("LoKi_ACC")
    LoKi_ACC.Variables = {
        "InAccSpd"  : "PPINFO( LHCb.ProtoParticle.InAccSpd,  -1 )",
        "InAccPrs"  : "PPINFO( LHCb.ProtoParticle.InAccPrs,  -1 )",
        "InAccEcal" : "PPINFO( LHCb.ProtoParticle.InAccEcal, -1 )",
        "InAccHcal" : "PPINFO( LHCb.ProtoParticle.InAccHcal, -1 )",
        "InAccMuon" : "PPINFO( LHCb.ProtoParticle.InAccMuon, -1 )",
        "ETA" : "ETA",
        "PHI" : "PHI"
        }

    dtt.L1.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_Track_mom" ]
    dtt.L1.addTool(LoKi_Track_mom)
    dtt.L2.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_Track_mom" ]
    dtt.L2.addTool(LoKi_Track_mom)

    dtt.Pi1.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_MIPCHI2DV" ]
    dtt.Pi1.addTool(LoKi_MIPCHI2)
    dtt.Pi2.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_MIPCHI2DV" ]
    dtt.Pi2.addTool(LoKi_MIPCHI2)

    dtt.B.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_DOCA"]
    dtt.B.addTool(LoKi_DOCA)
    dtt.JPs.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_DOCA"]
    dtt.JPs.addTool(LoKi_DOCA)
    dtt.K0.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_DOCA"]
    dtt.K0.addTool(LoKi_DOCA)

    dtt.L1.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_ACC" ]
    dtt.L1.addTool(LoKi_ACC)
    dtt.L2.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_ACC" ]
    dtt.L2.addTool(LoKi_ACC)
    dtt.Pi1.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_ACC" ]
    dtt.Pi1.addTool(LoKi_ACC)
    dtt.Pi2.ToolList += [ "LoKi::Hybrid::TupleTool/LoKi_ACC" ]
    dtt.Pi2.addTool(LoKi_ACC)

    # Mass substitutions
    from Configurables import TupleToolSubMass
    dtt.B.addTool( TupleToolSubMass )
    dtt.B.ToolList += ["TupleToolSubMass"]
    dtt.B.TupleToolSubMass.EndTreePIDs = [22]

    dtt.B.TupleToolSubMass.Substitution       += [ "p+ => pi+" ]
    dtt.B.TupleToolSubMass.Substitution       += [ "p+ => K+" ]
    dtt.B.TupleToolSubMass.Substitution       += [ "pi- => p~-" ]
    dtt.B.TupleToolSubMass.Substitution       += [ "pi- => K-" ]
    #e/mu still relevant?
    dtt.B.TupleToolSubMass.DoubleSubstitution += [ "mu+/e- => e+/mu-" ]
    dtt.B.TupleToolSubMass.DoubleSubstitution += [ "e+/e- => mu+/mu-" ]
    dtt.B.TupleToolSubMass.DoubleSubstitution += [ "mu+/mu- => e+/e-" ]
    dtt.B.TupleToolSubMass.DoubleSubstitution += [ "p+/pi- => pi+/p~-" ]

    # Adding hop from tuple tool
    from Configurables import TupleToolHOP
    dtt.B.addTool(TupleToolHOP, name = "LbHOP")
    dtt.B.ToolList += [ "TupleToolHOP/LbHOP"]

    # Add isolation variables
    from Configurables import TupleToolTrackIsolation,TupleToolConeIsolation
    if not is_mdst:
        dtt.addTool(TupleToolTrackIsolation, name = "TrackIsoInfo")
        dtt.addTool(TupleToolConeIsolation, name = "ConeIsoInfo")

        dtt.ToolList += ["TupleToolTrackIsolation/TrackIsoInfo","TupleToolConeIsolation/ConeIsoInfo"]
        dtt.TrackIsoInfo.MaxConeAngle = 0.5
        dtt.TrackIsoInfo.Verbose = True
        dtt.ConeIsoInfo.MinConeSize = 0.5 # ONLY USE ANGLE OF 0.5
        dtt.ConeIsoInfo.MaxConeSize = 0.5 # ONLY USE ANGLE OF 0.5
        dtt.ConeIsoInfo.Verbose = True

    # Set correct name depending on ee or mumu
    if 'ee' in stripping_line:
        loki_name = 'BuJPsKsEE'
    else:
        loki_name = 'BuJPsKsMM'
    if is_mc: 
        Filter_loc = tags.tags[name[:-5]]['StrippingStream']
    else: 
        Filter_loc = None
    LoKi_Tool = getLoKiTool(loki_name, stripping_line, is_mc, is_mc, Filter_loc )
    dtt.B.ToolList += ["LoKi::Hybrid::TupleTool/LoKi_Tool{}".format(loki_name)]
    dtt.B.addTool(LoKi_Tool)

    tuple_seq = SelectionSequence('{}_SelSeq'.format(name), tuple_selection)

    if is_mc:
        # Add MC tuple tools
        mctruth = dtt.addTupleTool('TupleToolMCTruth')
        mctruth.ToolList = [
            'MCTupleToolKinematic',
            'MCTupleToolHierarchy',
            'MCTupleToolPID',
            'MCTupleToolReconstructed'
        ]

    return tuple_seq.sequence()


def tuple_sequence():
    """Return the sequencer used for running tuple algorithms.

    The sequencer is configured such that all members are run, as their filter
    flags are ignored.
    """
    seq = GaudiSequencer('TupleSeq')
    seq.IgnoreFilterPassed = True

    return seq
