# FILE CONTAINING ALL DECAY DESCRIPTORS

dict_rec = {
    'Lb2JpsiL_mm'  :'[Lambda_b0 ->  ^(J/psi(1S) -> ^mu+  ^mu-)    ^(Lambda0 -> ^p+ ^pi-)]CC',
    'Lb2JpsiL_ee'  :'[Lambda_b0 ->  ^(J/psi(1S) -> ^e+  ^e- )    ^(Lambda0 -> ^p+ ^pi-)]CC',
    'Lb2Lmm_SS'    :'[Lambda_b0 ->  ^(J/psi(1S) -> ^mu+  ^mu+)    ^(Lambda0 -> ^p+ ^pi-)]CC',
    'Lb2Lee_SS'    :'[Lambda_b0 ->  ^(J/psi(1S) -> ^e+  ^e+ )    ^(Lambda0 -> ^p+ ^pi-)]CC',
    'Lb2Lemu'      :"[Lambda_b0 -> [^(J/psi(1S) -> ^e+  ^mu-)]CC ^(Lambda0 -> ^p+ ^pi-)]CC",
    'Lb2Lemu_SS'   :"[Lambda_b0 -> [^(J/psi(1S) -> ^e+  ^mu+)]CC ^(Lambda0 -> ^p+ ^pi-)]CC",
    'Bd2JpsiKs_mm' :'[       B0 ->  ^(J/psi(1S) -> ^mu+  ^mu-)    ^(KS0     -> ^pi+ ^pi-)]CC',
    'Bd2JpsiKs_ee' :'[       B0 ->  ^(J/psi(1S) -> ^e+  ^e- )    ^(KS0     -> ^pi+ ^pi-)]CC',
    'Bd2Ksmm_SS'   :'[       B0 ->  ^(J/psi(1S) -> ^mu+  ^mu+)    ^(KS0     -> ^pi+ ^pi-)]CC',
    'Bd2Ksee_SS'   :'[       B0 ->  ^(J/psi(1S) -> ^e+  ^e+ )    ^(KS0     -> ^pi+ ^pi-)]CC',
    'Bd2Ksem   '   :"[       B0 -> [^(J/psi(1S) -> ^e+  ^mu-)]CC ^(KS0     -> ^pi+ ^pi-)]CC",
    'Bd2Ksem_SS'   :"[       B0 -> [^(J/psi(1S) -> ^e+  ^mu+)]CC ^(KS0     -> ^pi+ ^pi-)]CC",
    'Bu2JpsiK_mm'  :'[       B+ ->  ^(J/psi(1S) -> ^mu+  ^mu-)    ^K+                   ]CC',
    'Bu2JpsiK_ee'  :'[       B+ ->  ^(J/psi(1S) -> ^e+  ^e- )    ^K+                   ]CC',
    'Bu2Kmm_SS'    :'[       B+ ->  ^(J/psi(1S) -> ^mu+  ^mu+)    ^K+                   ]CC',
    'Bu2Kee_SS'    :'[       B+ ->  ^(J/psi(1S) -> ^e+  ^e+ )    ^K+                   ]CC',
    'Bu2Kem   '    :"[       B+ -> [^(J/psi(1S) -> ^e+  ^mu-)]CC ^K+                   ]CC",
    'Bu2Kem_SS'    :"[       B+ -> [^(J/psi(1S) -> ^e+  ^mu+)]CC ^K+                   ]CC",
}

dict_mc = {
    'Lb2LJpsmm'    :'[Lambda_b0 =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^(Lambda0 => ^p+ ^pi-)]CC',
    'Lb2LJpsee'    :'[Lambda_b0 =>  ^(J/psi(1S) => ^e+  ^e- )    ^(Lambda0 => ^p+ ^pi-)]CC',
    'Lb2Lee'       :'[Lambda_b0 =>                 ^e+  ^e-      ^(Lambda0 => ^p+ ^pi-)]CC',
    'Lb2Lmm'       :'[Lambda_b0 =>                 ^mu+  ^mu-     ^(Lambda0 => ^p+ ^pi-)]CC',
    'Lb2Lem'       :"([Lambda_b0 =>                 ^e+  ^mu-      ^(Lambda0 => ^p+ ^pi-)]CC) || ([Lambda_b0 =>  ^e-  ^mu+ ^(Lambda0 => ^p+ ^pi-)]CC)",
    'Bd2Ksmm'      :'[       B0 =>                 ^mu+  ^mu-     ^(KS0     => ^pi+ ^pi-)]CC',
    'Bd2Ksee'      :'[       B0 =>                 ^e+  ^e-      ^(KS0     => ^pi+ ^pi-)]CC',
    'Bd2KsJpsmm'   :'[       B0 =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^(KS0     => ^pi+ ^pi-)]CC',
    'Bd2KsJpsee'   :'[       B0 =>  ^(J/psi(1S) => ^e+  ^e- )    ^(KS0     => ^pi+ ^pi-)]CC',
    'Bd2Ksem'      :"[       B0 =>               [ ^e+  ^mu- ]CC ^(KS0     => ^pi+ ^pi-)]CC",
    'Bd2Ksem'      :"([      B0 =>                  ^e+  ^mu-      ^(KS0   => ^pi+ ^pi-)]CC) || ([       B0 =>  ^e-  ^mu+ ^(KS0    => ^pi+ ^pi-)]CC)",
    'Bu2JpsiK_mm'  :'[       B+ =>  ^(J/psi(1S) => ^mu+  ^mu-)    ^K+                   ]CC',
    'Bu2JpsiK_ee'  :'[       B+ =>  ^(J/psi(1S) => ^e+  ^e- )    ^K+                   ]CC',
    'Bu2Kem'       :"[       B+ =>               [ ^e+  ^mu- ]CC ^K+                   ]CC",
    'Bu2Kem'       :"([      B+ =>                 ^e+  ^mu-     ^K+                   ]CC) || ([       B+ =>  ^e-  ^mu+ ^K+                   ]CC)",

}

dict_mc_noV0dec = {k : ( v if 'Bu' in k
                 else v.replace('=> ^pi+ ^pi-)','').replace('(KS0','KS0') if 'Bd' in k
                 else v.replace('=> ^p+ ^pi-)' ,'').replace('(Lam','Lam') )
                 for (k,v) in dict_mc.items() }
for keys,vals in dict_mc_noV0dec.iteritems():
    print keys,vals

# CODE TO GIVE TUPLE BRANCHES; THERE ARE THREE CONFIGURATIONS TIMES TWO WITHOUT JPSI
Lb_branches = ['Lb','L0','JPs','L1','L2']
B0_branches = [ 'B','K0','JPs','L1','L2']
Bp_branches = [ 'B','K' ,'JPs','L1','L2']

Lb_branches_nonres = ['Lb','L0','L1','L2']
B0_branches_nonres = [ 'B','K0','L1','L2']
Bp_branches_nonres = [ 'B','K' ,'L1','L2']

def tuple_branches(decay_desc_full,noV0dec=False):
    # Now add resonances for data/MC and different b hadrons
    tuple_branches = []
    particles      = []

    # First add the b and s hadrons
    if 'Lamb' in decay_desc_full:
        tuple_branches = ['Lb','L0']
        particles     += ['Lambda0']
    elif 'B0' in decay_desc_full:
        tuple_branches = ['B','K0'] 
        particles     += ['KS0']
    elif 'B+' in decay_desc_full:
        tuple_branches = ['B','K']  
        particles     += ['K+']

    # take leptons from decay descriptor
    # decay descriptor has first lepton after first or second ^ 
    # ['e+ ','mu+)    '] is an example string
    first_lepton = 1 

    # Add Jpsi if needed and appropriate leptons
    if 'J/psi(1S)' in decay_desc_full: # FOR DecayTree or MCTree with Jpsi
        tuple_branches += ['JPs']
        particles += ['J/psi(1S)']
        
        first_lepton += 1

    leptons = decay_desc_full.split('^')[first_lepton:first_lepton+2]
    print ('leptons',leptons)

    leptons[0] = leptons[0].replace(' ','')
    leptons[1] = leptons[1].split(')')[0].replace(' ','')

    print ('leptons',leptons)



    tuple_branches += ['L1','L2']    
    particles += leptons

    # Add hadrons if the Lambda decays
    if 'pi' in decay_desc_full:
        if 'Lamb' in decay_desc_full:
            tuple_branches += ['P'  ,'Pi' ]
            particles += ['p+','pi-']
        elif 'B0' in decay_desc_full:
            tuple_branches += ['Pi1','Pi2']
            particles += ['pi+','pi-']



    # Replace hats to get individual particles
    decay_desc = decay_desc_full.replace('^','') # remove hats because only one will be needed

    print particles
    print tuple_branches

    # add single hats
    branches = { tuple_branches[0] : decay_desc  }
    for i in range(len(tuple_branches)-1):
        print i
        part = particles[i]
        if part in ['Lambda0','KS0','J/psi(1S)']:
            branches[tuple_branches[i+1]] = decay_desc.replace('('+part,'^('+part,1)
        else:
            branches[tuple_branches[i+1]] = decay_desc.replace(' '+part,'^'+part,1)
    if ['e+','e+'] in tuple_branches or ['e+','mu+'] in tuple_branches or ['mu+','mu+'] in tuple_branches: # IF SAME SIGN IN TUPLE BRANCHES 
        # decay desc has ^e+   e+
        branches['L2'] = decay_desc.replace('^{0}+  {1}+'.format(leptons[0],leptons[1]),' {0}+ ^{1}+'.format(leptons[0],leptons[1]))

    return branches,tuple_branches

