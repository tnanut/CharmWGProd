import sys
import os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')
from Configurables import DaVinci, GaudiSequencer
from helpers import tuple_maker_Bz
import tags

# Set the name, stripping line, decay, and branches. Create a tuple sequence using the helper file.
year=DaVinci().DataType
name = '{0}_MagUp_MC_Bd2KsJpseeTuple'.format(year)
is_mc = DaVinci().Simulation
if is_mc:
    line = tags.tags[name[:-5]]['StrippingLine']
else:
    line = 'Bu2LLK_eeLine2'

tuple_seq = tuple_maker_Bz.tuple_maker(
    name,
    decay='[B0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(KS0 -> ^pi+ ^pi-)]CC',
    branches={
        'B' : '[B0 ->  (J/psi(1S) -> e+ e-) (KS0 -> pi+ pi-)]CC',
        'K0' : '[B0 ->  (J/psi(1S) -> e+ e-) ^(KS0 -> pi+ pi-)]CC',
        'JPs' : '[B0 ->  ^(J/psi(1S) -> e+ e-) (KS0 -> pi+ pi-)]CC',
        'Pi1' : '[B0 ->  (J/psi(1S) -> e+ e-) (KS0 -> ^pi+ pi-)]CC',
        'Pi2' : '[B0 ->  (J/psi(1S) -> e+ e-) (KS0 -> pi+ ^pi-)]CC',
        'L1' : '[B0 ->  (J/psi(1S) -> ^e+ e-) (KS0 -> pi+ pi-)]CC',
        'L2' : '[B0 ->  (J/psi(1S) -> e+ ^e-) (KS0 -> pi+ pi-)]CC',
    },
    stripping_line=line,
    is_mc=is_mc,
    input_type=DaVinci().InputType,
    upstream_e=True,
    year=DaVinci.DataType
)
dtt = tuple_seq.Members[-1]

# Add the tuple sequence to the main sequence
seq = tuple_maker_Bz.tuple_sequence()
seq.Members += [tuple_seq]
