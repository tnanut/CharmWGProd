import sys
import os
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')
from Configurables import DaVinci, GaudiSequencer
from helpers import tuple_maker
import tags

# Set the name, stripping line, decay, and branches. Create a tuple sequence using the helper file.
year = DaVinci().DataType
name = '{0}_MagUp_MC_Lb2LJpseeTuple'.format(year)
is_mc = DaVinci().Simulation

if is_mc:
    line = tags.tags[name[:-5]]['StrippingLine']
else:
    line = 'Bu2LLK_eeLine2'

tuple_seq = tuple_maker.tuple_maker(
    name,
    decay='[Lambda_b0 -> ^(J/psi(1S) -> ^e+ ^e-) ^(Lambda0 -> ^p+ ^pi-)]CC',
    branches={
        'Lb' : '[Lambda_b0 ->  (J/psi(1S) -> e+ e-) (Lambda0 -> p+ pi-)]CC',
        'L0' : '[Lambda_b0 ->  (J/psi(1S) -> e+ e-) ^(Lambda0 -> p+ pi-)]CC',
        'JPs' : '[Lambda_b0 ->  ^(J/psi(1S) -> e+ e-) (Lambda0 -> p+ pi-)]CC',
        'P' : '[Lambda_b0 ->  (J/psi(1S) -> e+ e-) (Lambda0 -> ^p+ pi-)]CC',
        'Pi' : '[Lambda_b0 ->  (J/psi(1S) -> e+ e-) (Lambda0 -> p+ ^pi-)]CC',
        'L1' : '[Lambda_b0 ->  (J/psi(1S) -> ^e+ e-) (Lambda0 -> p+ pi-)]CC',
        'L2' : '[Lambda_b0 ->  (J/psi(1S) -> e+ ^e-) (Lambda0 -> p+ pi-)]CC',
    },
    stripping_line=line,
    is_mc=is_mc,
    input_type=DaVinci().InputType,
    upstream_e=True,
    year=DaVinci.DataType
)
dtt = tuple_seq.Members[-1]

# Add the tuple sequence to the main sequence
seq = tuple_maker.tuple_sequence()
seq.Members += [tuple_seq]
