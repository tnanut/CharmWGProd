import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci, CondDB
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay


from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger

from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos

from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool

from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",   
   # "TupleToolPropertime",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',                                  
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)',
    }

################# Inputs and Momentum Scaling Correction  ######################

lineDs2PhiPi = "Hlt2CharmHadDspToKmKpPipTurbo"

year = "2016"

from PhysConf.Selections import AutomaticData
Ds2PhiPi = AutomaticData( Location = lineDs2PhiPi + '/Particles' )

from PhysConf.Selections import MomentumScaling
Ds2PhiPi = MomentumScaling ( Ds2PhiPi , Turbo = True , Year = year )

from PhysConf.Selections import TupleSelection

################# D -> Phi Pi #####################################

tuple_Ds2PhiPi = TupleSelection ( 
    'Dsp2KmKppip' , 
    [Ds2PhiPi], 
    Decay = "[D_s+ -> ^K+  ^K- ^pi+]CC",
    Branches = {
        "Dplus"   : "[D_s+ -> K+  K- pi+]CC",
        "Kplus"   : "[D_s+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D_s+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D_s+ -> K+  K- ^pi+]CC"
        },
    ToolList = [ ] 
    )

for dtt in [tuple_Ds2PhiPi]:
    
    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="Kplus")
    dtt.addTool(TupleToolDecay, name="Kminus")

#LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.Kplus,dtt.Kminus,dtt.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2PhiPi)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]
    
#DECAY TREE FITTER
#constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True

################# D -> KS0 Pi, KS0 K, Phi Pi #####################################
    dtt.TupleName = "ntp"

    dtt.ToolList =  TupleToolList
    
    dtt.TupleToolTrigger.VerboseL0 = True
    dtt.TupleToolTrigger.VerboseHlt1 = True
    dtt.TupleToolTrigger.VerboseHlt2 = False
    dtt.TupleToolTrigger.FillHlt2 = False
    dtt.TupleToolTrigger.TriggerList = TriggerList

    dtt.TupleToolTISTOS.addTool(L0TriggerTisTos())
    dtt.TupleToolTISTOS.addTool(TriggerTisTos())
 #   dtt.TupleToolTISTOS.Verbose = True
    dtt.TupleToolTISTOS.VerboseL0 = True
    dtt.TupleToolTISTOS.VerboseHlt1 = True
    dtt.TupleToolTISTOS.VerboseHlt2 = False
    dtt.TupleToolTISTOS.FillHlt2 = False
    dtt.TupleToolTISTOS.TriggerList = TriggerList

    # dtt.WriteP2PVRelations = False
    # dtt.InputPrimaryVertices = '/Event/Turbo/Primary'


from PhysConf.Selections import SelectionSequence
seq_Ds2PhiPi = SelectionSequence( 'Seq_Ds2PhiPi' , tuple_Ds2PhiPi ).sequence()  

############ DaVinci configuration ###############################

from Configurables import DstConf # necessary for DaVinci v40r1 onwards
DstConf().Turbo = True

# FIX for DaVinci mess
from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link ( 'LinkRawEvent1', 
                 What   =  '/Event/DAQ/RawEvent' , 
                 Target = '/Event/Trigger/RawEvent' )
dod.AlgMap [ rawEvt1  . Target ] = rawEvt1

from Configurables import LoKi__HDRFilter as Filter
hltfilter = Filter('HLT2Filter', Code = "HLT_PASS_RE('"+lineDs2PhiPi+"Decision')")

DaVinci(
    InputType       = 'MDST'          , 
    RootInTES       = "/Event/Turbo"       , 
    DataType        = year        ,
    Turbo           = True           ,
    Simulation      = False          ,
    EventPreFilters = [ hltfilter ]   ,
    UserAlgorithms  = [ seq_Ds2PhiPi ]         , 
    EvtMax          = -1             , 
    Lumi            = True            , 
    TupleFile       = "tuple_Ds2PhiPi_MS_2016.root" , 
    PrintFreq       = 5000
    )

CondDB  ( LatestGlobalTagByDataType = year )

################# READ FROM LOCAL FILE ###########################  

from GaudiConf import IOHelper
IOHelper().inputFiles([
        #'/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/CHARMSPECPARKED.MDST/00054257/0000/00054257_00005894_1.charmspecparked.mdst' 
        ], clear = True)
