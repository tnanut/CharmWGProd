import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci, CondDB
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay


from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger

from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos

from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool

from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",   
    # "TupleToolPropertime",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',                                  
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2KS0h=LoKi__Hybrid__TupleTool("LoKi_Dplus2KS0h")
LoKi_Dplus2KS0h.Variables =  {
    'DOCACHI2_KS0_hplus' : 'DOCACHI2(1,2)',
    'DOCA_KS0_hplus' : 'DOCA(1,2)',
  }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)',
 }
LoKi_KS0=LoKi__Hybrid__TupleTool("LoKi_KS0")
LoKi_KS0.Variables = {
   'DOCACHI2_piplus_piminus' : 'DOCACHI2(1,2)',
   'DOCA_piplus_piminus' : 'DOCA(1,2)',
  }


################# Inputs and Momentum Scaling Correction  ######################

lineKS0PiLL = "Hlt2CharmHadDp2KS0Pip_KS0LLTurbo"
lineKS0PiDD = "Hlt2CharmHadDp2KS0Pip_KS0DDTurbo"
lineKS0KLL = "Hlt2CharmHadDp2KS0Kp_KS0LLTurbo"
lineKS0KDD = "Hlt2CharmHadDp2KS0Kp_KS0DDTurbo"
lineD2PhiPi = "Hlt2CharmHadDpToKmKpPipTurbo"

year = "2017"

from PhysConf.Selections import AutomaticData
AD_KS0PiLL = AutomaticData( Location = lineKS0PiLL + '/Particles' )
AD_KS0PiDD = AutomaticData( Location = lineKS0PiDD + '/Particles' )
AD_KS0KLL = AutomaticData( Location = lineKS0KLL + '/Particles' )
AD_KS0KDD = AutomaticData( Location = lineKS0KDD + '/Particles' )
AD_D2PhiPi = AutomaticData( Location = lineD2PhiPi + '/Particles' )

from PhysConf.Selections import MomentumScaling
KS0PiLL = MomentumScaling ( AD_KS0PiLL , Turbo = True , Year = year )
KS0PiDD = MomentumScaling ( AD_KS0PiDD , Turbo = True , Year = year )
KS0KLL = MomentumScaling ( AD_KS0KLL , Turbo = True , Year = year )
KS0KDD = MomentumScaling ( AD_KS0KDD , Turbo = True , Year = year )
D2PhiPi = MomentumScaling ( AD_D2PhiPi , Turbo = True , Year = year )

from PhysConf.Selections import TupleSelection


################# D -> KS0 H #####################################

## D -> KS0 Pi

# tuple_KsPiLL = DecayTreeTuple( 'Dp2KS0pipLL'  )
# tuple_KsPiLL.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Pip_KS0LLTurbo/Particles' ]

# tuple_KsPiDD = DecayTreeTuple( 'Dp2KS0pipDD'  )
# tuple_KsPiDD.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Pip_KS0DDTurbo/Particles']

# for tuple in [tuple_KsPiLL,tuple_KsPiDD]:
#     tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC"
#     tuple.Branches = {
#         "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
#         "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
#         "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
#         "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
#         "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
#         }

tuple_KsPiLL = TupleSelection ( 
    'Dp2KS0pipLL' , 
    [KS0PiLL], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )

tuple_KsPiDD = TupleSelection ( 
    'Dp2KS0pipDD' , 
    [KS0PiDD], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )

     
## D -> KS0 K

# tuple_KsKLL = DecayTreeTuple( 'Dp2KS0KpLL'  )
# tuple_KsKLL.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Kp_KS0LLTurbo/Particles']

# tuple_KsKDD = DecayTreeTuple( 'Dp2KS0KpDD'  )
# tuple_KsKDD.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Kp_KS0DDTurbo/Particles']

# for tuple in [tuple_KsKLL,tuple_KsKDD]:

#     tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC"
#     tuple.Branches = {
#         "Dplus"       : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
#         "KS0"      : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
#         "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
#         "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
#         "hplus"       : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
#         }

tuple_KsKLL = TupleSelection ( 
    'Dp2KS0KpLL' , 
    [KS0KLL], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        },
    ToolList = [ ] 
    )

tuple_KsKDD = TupleSelection ( 
    'Dp2KS0KpDD' , 
    [KS0KDD], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        },
    ToolList = [ ] 
    )


## D -> KS0 H

for dtt in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD]:
    
    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="KS0")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="piminus")
    dtt.addTool(TupleToolDecay, name="hplus")
 #LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.KS0,dtt.piplus,dtt.piminus,dtt.hplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2KS0h)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
    dtt.KS0.addTool(LoKi_KS0)
    dtt.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

#DECAY TREE FITTER
#constrain PV and mass                                          
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')                     
    dtt.Dplus.DTF.Verbose = True                              
    dtt.Dplus.DTF.constrainToOriginVertex = True                
    dtt.Dplus.DTF.daughtersToConstrain = ['KS0']                         
    dtt.Dplus.DTF.UpdateDaughters = True                  
#constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True
        
################# D -> Phi Pi #####################################

# tuple_D2PhiPi = DecayTreeTuple('Dp2KmKppip')
# tuple_D2PhiPi.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDpToKmKpPipTurbo/Particles']

tuple_D2PhiPi = TupleSelection ( 
    'Dp2KmKppip' , 
    [D2PhiPi], 
    Decay = "[D+ -> ^K+  ^K- ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> K+  K- pi+]CC",
        "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D+ -> K+  K- ^pi+]CC"
        },
    ToolList = [ ] 
    )


for dtt in [tuple_D2PhiPi]:
    
    # dtt.Decay = "[D+ -> ^K+  ^K- ^pi+]CC"
    # dtt.Branches = {
    #     "Dplus"   : "[D+ -> K+  K- pi+]CC",
    #     "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
    #     "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
    #     "piplus"  : "[D+ -> K+  K- ^pi+]CC"
    #     }

    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="Kplus")
    dtt.addTool(TupleToolDecay, name="Kminus")

#LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.Kplus,dtt.Kminus,dtt.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2PhiPi)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]
   
#DECAY TREE FITTER
 #constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True

################# D -> KS0 H, D-> Phi Pi #####################################

for dtt in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD,tuple_D2PhiPi]:

    dtt.TupleName = "ntp"

    dtt.ToolList =  TupleToolList
 
    dtt.TupleToolTrigger.VerboseL0 = True
    dtt.TupleToolTrigger.VerboseHlt1 = True
    dtt.TupleToolTrigger.VerboseHlt2 = False
    dtt.TupleToolTrigger.FillHlt2 = False
    dtt.TupleToolTrigger.TriggerList = TriggerList

    dtt.TupleToolTISTOS.addTool(L0TriggerTisTos())
    dtt.TupleToolTISTOS.addTool(TriggerTisTos())
    dtt.TupleToolTISTOS.Verbose = True
    dtt.TupleToolTISTOS.VerboseL0 = True
    dtt.TupleToolTISTOS.VerboseHlt1 = True
    dtt.TupleToolTISTOS.VerboseHlt2 = False
    dtt.TupleToolTISTOS.FillHlt2 = False
    dtt.TupleToolTISTOS.TriggerList = TriggerList

    # dtt.WriteP2PVRelations = False
    # dtt.InputPrimaryVertices = '/Event/Turbo/Primary'


from PhysConf.Selections import SelectionSequence
seq_KsPiLL = SelectionSequence( 'Seq_KsPiLL' , tuple_KsPiLL ).sequence()    
seq_KsPiDD = SelectionSequence( 'Seq_KsPiDD' , tuple_KsPiDD ).sequence()    
seq_KsKLL = SelectionSequence( 'Seq_KsKLL' , tuple_KsKLL ).sequence()    
seq_KsKDD = SelectionSequence( 'Seq_KsKDD' , tuple_KsKDD ).sequence()    
seq_D2PhiPi = SelectionSequence( 'Seq_D2PhiPi' , tuple_D2PhiPi ).sequence()    


############ DaVinci configuration ###############################

from Configurables import DstConf # necessary for DaVinci v40r1 onwards
DstConf().Turbo = True

# FIX for DaVinci mess
from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link ( 'LinkRawEvent1', 
                 What   =  '/Event/DAQ/RawEvent' , 
                 Target = '/Event/Trigger/RawEvent' )
dod.AlgMap [ rawEvt1  . Target ] = rawEvt1

from Configurables import LoKi__HDRFilter as Filter
hltfilter = Filter('HLT2Filter', Code = "HLT_PASS_RE('"+lineKS0PiLL+"Decision') | HLT_PASS_RE('"+lineKS0PiDD+"Decision') | HLT_PASS_RE('"+lineKS0KLL+"Decision') | HLT_PASS_RE('"+lineKS0KDD+"Decision') | HLT_PASS_RE('"+lineD2PhiPi+"Decision')")

DaVinci(
    InputType       = 'MDST'          , 
    RootInTES       = "/Event/Charmcharged/Turbo"       , 
    DataType        = year        ,
    Turbo           = True     ,
    Simulation      = False          ,
    EventPreFilters = [ hltfilter ]   ,
    UserAlgorithms  = [ seq_KsPiLL , seq_KsPiDD , seq_KsKLL , seq_KsKDD , seq_D2PhiPi]         ,
    EvtMax          = -1             , 
    Lumi            = True            , 
    TupleFile       = "tuple_D2KS0H_D2PhiPi_MS_2017.root" , 
    PrintFreq       = 5000
    )


# DaVinci().Simulation   = False
# DaVinci().Lumi = True
# DaVinci().RootInTES = "/Event/Charmcharged/Turbo"
# DaVinci().Turbo = True
# DaVinci().appendToMainSequence([tuple_KsPiLL])
# DaVinci().appendToMainSequence([tuple_KsPiDD])
# DaVinci().appendToMainSequence([tuple_KsKLL])
# DaVinci().appendToMainSequence([tuple_KsKDD])
# DaVinci().appendToMainSequence([tuple_D2PhiPi])
# DaVinci().EvtMax = -1
# DaVinci().PrintFreq = 50000
# year = "2017"
# DaVinci().DataType = year
# DaVinci().TupleFile = "tuple_D2KS0H_D2PhiPi_2017.root"
# DaVinci().InputType='MDST'

CondDB  ( LatestGlobalTagByDataType = year )
################# READ FROM LOCAL FILE ###########################  
from GaudiConf import IOHelper
IOHelper().inputFiles([
        # '/eos/lhcb/user/s/smaccoli/00066595_00000482_1.charmcharged.mdst'
        ], clear = True)
