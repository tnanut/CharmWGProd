import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci, CondDB
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay


from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger

from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos

from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool

from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",
#    "TupleToolPropertime",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)'
 }

################# D -> Phi Pi #####################################

tuple_Ds2PhiPi = DecayTreeTuple('Dsp2KmKppip')
tuple_Ds2PhiPi.Inputs = ['/Event/Charmspec/Turbo/Hlt2CharmHadDspToKmKpPipTurbo/Particles'] #prescaled 0.1

for tuple in [tuple_Ds2PhiPi]:

    tuple.Decay = "[D_s+ -> ^K+  ^K- ^pi+]CC"
    tuple.Branches = {
        "Dplus"   : "[D_s+ -> K+  K- pi+]CC",
        "Kplus"   : "[D_s+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D_s+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D_s+ -> K+  K- ^pi+]CC"
        }
    tuple.addTool(TupleToolDecay, name="Dplus")
    tuple.addTool(TupleToolDecay, name="piplus")
    tuple.addTool(TupleToolDecay, name="Kplus")
    tuple.addTool(TupleToolDecay, name="Kminus")

#LoKi VARIABLES
    for particle in [tuple.Dplus,tuple.Kplus,tuple.Kminus,tuple.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    tuple.Dplus.addTool(LoKi_Dplus2PhiPi)
    tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]

#DECAY TREE FITTER
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
#constrain only PV
    tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tuple.Dplus.DTFonlyPV.Verbose = True
    tuple.Dplus.DTFonlyPV.UpdateDaughters = True

################# D -> KS0 Pi, KS0 K, Phi Pi #####################################

    tttt = TupleToolTISTOS()
    tttt.addTool(L0TriggerTisTos())
    tttt.addTool(TriggerTisTos())

    tuple.TupleName = "ntp"

    tuple.ToolList =  TupleToolList

    tuple.addTool(TupleToolTrigger())
    tuple.TupleToolTrigger.VerboseL0 = True
    tuple.TupleToolTrigger.VerboseHlt1 = True
    tuple.TupleToolTrigger.VerboseHlt2 = False
    tuple.TupleToolTrigger.FillHlt2 = False
    tuple.TupleToolTrigger.TriggerList = TriggerList

    tuple.addTool(tttt)
    tuple.TupleToolTISTOS.Verbose = True
    tuple.TupleToolTISTOS.VerboseL0 = True
    tuple.TupleToolTISTOS.VerboseHlt1 = True
    tuple.TupleToolTISTOS.VerboseHlt2 = False
    tuple.TupleToolTISTOS.FillHlt2 = False
    tuple.TupleToolTISTOS.TriggerList = TriggerList

    # tuple.WriteP2PVRelations = False
    # tuple.InputPrimaryVertices = '/Event/Charmspec/Turbo/Primary'

############ DaVinci configuration ###############################

from Configurables import DstConf # necessary for DaVinci v40r1 onwards
DstConf().Turbo = True

# from Configurables import DataOnDemandSvc
# dod = DataOnDemandSvc()
# from Configurables import Gaudi__DataLink as Link
# rawEvt1 = Link('LinkRawEvent1',What='/Event/DAQ/RawEvent',Target= "/Event/" + "Trigger/RawEvent")
# dod.AlgMap[rawEvt1 .Target] = rawEvt1


lineDs2PhiPi = "Hlt2CharmHadDspToKmKpPipTurbo"
#from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters(
    HLT2_Code = "HLT_PASS_RE('"+lineDs2PhiPi+"Decision')"
    )
DaVinci().EventPreFilters = fltrs.filters('TriggerFilters')

DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().RootInTES = "/Event/Charmspec/Turbo"
DaVinci().Turbo = True
DaVinci().appendToMainSequence([tuple_Ds2PhiPi])
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 50000
year = "2017"
DaVinci().DataType = year
DaVinci().TupleFile = "tuple_Ds2PhiPi_2017.root"
DaVinci().InputType='MDST'
CondDB  ( LatestGlobalTagByDataType = year )
