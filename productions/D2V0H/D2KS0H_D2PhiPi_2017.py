import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci, CondDB
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay


from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger

from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos

from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool

from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",
#    "TupleToolPropertime",
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TwoTrackMVADecision',
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2KS0h=LoKi__Hybrid__TupleTool("LoKi_Dplus2KS0h")
LoKi_Dplus2KS0h.Variables =  {
    'DOCACHI2_KS0_hplus' : 'DOCACHI2(1,2)',
    'DOCA_KS0_hplus' : 'DOCA(1,2)',
    }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)'
 }
LoKi_KS0=LoKi__Hybrid__TupleTool("LoKi_KS0")
LoKi_KS0.Variables = {
   'DOCACHI2_piplus_piminus' : 'DOCACHI2(1,2)',
   'DOCA_piplus_piminus' : 'DOCA(1,2)',
   }

################# D -> KS0 H #####################################

# D -> KS0 Pi

tuple_KsPiLL = DecayTreeTuple( 'Dp2KS0pipLL'  )
tuple_KsPiLL.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Pip_KS0LLTurbo/Particles' ]

tuple_KsPiDD = DecayTreeTuple( 'Dp2KS0pipDD'  )
tuple_KsPiDD.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Pip_KS0DDTurbo/Particles']

for tuple in [tuple_KsPiLL,tuple_KsPiDD]:
    tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC"
    tuple.Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        }

# D -> KS0 K

tuple_KsKLL = DecayTreeTuple( 'Dp2KS0KpLL'  )
tuple_KsKLL.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Kp_KS0LLTurbo/Particles']

tuple_KsKDD = DecayTreeTuple( 'Dp2KS0KpDD'  )
tuple_KsKDD.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDp2KS0Kp_KS0DDTurbo/Particles']

for tuple in [tuple_KsKLL,tuple_KsKDD]:

    tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC"
    tuple.Branches = {
        "Dplus"       : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"      : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus"       : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        }

# D -> KS0 H

for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD]:

    tuple.addTool(TupleToolDecay, name="Dplus")
    tuple.addTool(TupleToolDecay, name="KS0")
    tuple.addTool(TupleToolDecay, name="piplus")
    tuple.addTool(TupleToolDecay, name="piminus")
    tuple.addTool(TupleToolDecay, name="hplus")
 #LoKi VARIABLES
    for particle in [tuple.Dplus,tuple.KS0,tuple.piplus,tuple.piminus,tuple.hplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    tuple.Dplus.addTool(LoKi_Dplus2KS0h)
    tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
    tuple.KS0.addTool(LoKi_KS0)
    tuple.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

#DECAY TREE FITTER
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')
    # tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMass')
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
#constrain PV and mass
    tuple.Dplus.DTF.constrainToOriginVertex = True
    tuple.Dplus.DTF.Verbose = True
    tuple.Dplus.DTF.daughtersToConstrain = ['KS0']
    tuple.Dplus.DTF.UpdateDaughters = True
#constrain only PV
    tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tuple.Dplus.DTFonlyPV.Verbose = True
    tuple.Dplus.DTFonlyPV.UpdateDaughters = True
#constrain only mass
    # tuple.Dplus.DTFonlyMass.constrainToOriginVertex = False
    # tuple.Dplus.DTFonlyMass.Verbose = True
    # tuple.Dplus.DTFonlyMass.daughtersToConstrain = ['KS0']
    # tuple.Dplus.DTFonlyMass.UpdateDaughters = True

################# D -> Phi Pi #####################################

tuple_D2PhiPi = DecayTreeTuple('Dp2KmKppip')
tuple_D2PhiPi.Inputs = ['/Event/Charmcharged/Turbo/Hlt2CharmHadDpToKmKpPipTurbo/Particles']

for tuple in [tuple_D2PhiPi]:

    tuple.Decay = "[D+ -> ^K+  ^K- ^pi+]CC"
    tuple.Branches = {
        "Dplus"   : "[D+ -> K+  K- pi+]CC",
        "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D+ -> K+  K- ^pi+]CC"
        }
    tuple.addTool(TupleToolDecay, name="Dplus")
    tuple.addTool(TupleToolDecay, name="piplus")
    tuple.addTool(TupleToolDecay, name="Kplus")
    tuple.addTool(TupleToolDecay, name="Kminus")

#LoKi VARIABLES
    for particle in [tuple.Dplus,tuple.Kplus,tuple.Kminus,tuple.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    tuple.Dplus.addTool(LoKi_Dplus2PhiPi)
    tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]

#DECAY TREE FITTER
    tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
#constrain only PV
    tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
    tuple.Dplus.DTFonlyPV.Verbose = True
    tuple.Dplus.DTFonlyPV.UpdateDaughters = True

################# D -> KS0 H, D-> Phi Pi #####################################

tttt = TupleToolTISTOS()
tttt.addTool(L0TriggerTisTos())
tttt.addTool(TriggerTisTos())


for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD,tuple_D2PhiPi]:

    tuple.TupleName = "ntp"

    tuple.ToolList =  TupleToolList

    tuple.addTool(TupleToolTrigger())
    tuple.TupleToolTrigger.VerboseL0 = True
    tuple.TupleToolTrigger.VerboseHlt1 = True
    tuple.TupleToolTrigger.VerboseHlt2 = False
    tuple.TupleToolTrigger.FillHlt2 = False
    tuple.TupleToolTrigger.TriggerList = TriggerList

    tuple.addTool(tttt)
    tuple.TupleToolTISTOS.Verbose = True
    tuple.TupleToolTISTOS.VerboseL0 = True
    tuple.TupleToolTISTOS.VerboseHlt1 = True
    tuple.TupleToolTISTOS.VerboseHlt2 = False
    tuple.TupleToolTISTOS.FillHlt2 = False
    tuple.TupleToolTISTOS.TriggerList = TriggerList

    tuple.WriteP2PVRelations = False
    tuple.InputPrimaryVertices = '/Event/Charmcharged/Turbo/Primary'

############ DaVinci configuration ###############################

from Configurables import DstConf # necessary for DaVinci v40r1 onwards
DstConf().Turbo = True

from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link('LinkRawEvent1',What='/Event/DAQ/RawEvent',Target= "/Event/" + "Trigger/RawEvent")
dod.AlgMap[rawEvt1 .Target] = rawEvt1

lineKS0PiLL = "Hlt2CharmHadDp2KS0Pip_KS0LLTurbo"
lineKS0PiDD = "Hlt2CharmHadDp2KS0Pip_KS0DDTurbo"
lineKS0KLL = "Hlt2CharmHadDp2KS0Kp_KS0LLTurbo"
lineKS0KDD = "Hlt2CharmHadDp2KS0Kp_KS0DDTurbo"
lineD2PhiPi = "Hlt2CharmHadDpToKmKpPipTurbo"

##Serena
# from Configurables import LoKi__HDRFilter as StripFilter
# from PhysConf.Filters import LoKi_Filters
# fltrs = LoKi_Filters(
#     HLT2_Code = "HLT_PASS_RE('"+lineKS0PiLL+"Decision') | HLT_PASS_RE('"+lineKS0PiDD+"Decision') | HLT_PASS_RE('"+lineKS0KLL+"Decision') | HLT_PASS_RE('"+lineKS0KDD+"Decision') | HLT_PASS_RE('"+lineD2PhiPi+"Decision')"
#     )
# DaVinci().EventPreFilters = fltrs.filters('TriggerFilters')

##Laurent
from Configurables import LoKi__HDRFilter as Filter
hltfilter = Filter('HLT2Filter', Code = "HLT_PASS_RE('"+lineKS0PiLL+"Decision') | HLT_PASS_RE('"+lineKS0PiDD+"Decision') | HLT_PASS_RE('"+lineKS0KLL+"Decision') | HLT_PASS_RE('"+lineKS0KDD+"Decision') | HLT_PASS_RE('"+lineD2PhiPi+"Decision')")
DaVinci().EventPreFilters = [ hltfilter ]

# #Laurent prompt cut that modifies tuple_KsPiLL.Inputs()
# from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
# from Configurables import FilterDesktop
# _turboEvents = AutomaticData(Location =  'Hlt2CharmHadDp2KS0Pip_KS0'+'LL'+'Turbo/Particles')
# _promptFilter = FilterDesktop('promptFilter', Code = "(BPVIPCHI2()<20)")
# PromptFilterSel = Selection(name = 'PromptFilterSelection',
#                           Algorithm = _promptFilter,
#                           RequiredSelections = [ _turboEvents ])
# PromptFilterSelSeq = SelectionSequence( "promptSelSeq", TopSelection=PromptFilterSel
#                                         )
# tuple_KsPiLL.Inputs = [ PromptFilterSelSeq.outputLocation() ]
# DaVinci().appendToMainSequence( [ PromptFilterSelSeq, tuple_KsPiLL ] )

DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().RootInTES = "/Event/Charmcharged/Turbo"
DaVinci().Turbo = True
DaVinci().appendToMainSequence([tuple_KsPiLL])
DaVinci().appendToMainSequence([tuple_KsPiDD])
DaVinci().appendToMainSequence([tuple_KsKLL])
DaVinci().appendToMainSequence([tuple_KsKDD])
DaVinci().appendToMainSequence([tuple_D2PhiPi])
DaVinci().EvtMax = -1
DaVinci().PrintFreq = 50000
year = "2017"
DaVinci().DataType = year
DaVinci().TupleFile = "tuple_D2KS0H_D2PhiPi_2017.root"
DaVinci().InputType='MDST'
CondDB  ( LatestGlobalTagByDataType = year )
# ################# READ FROM LOCAL FILE ###########################
# from GaudiConf import IOHelper
# IOHelper().inputFiles([
#         '/eos/lhcb/user/s/smaccoli/00066595_00000482_1.charmcharged.mdst'
#         ], clear = True)
