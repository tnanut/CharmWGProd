import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from Configurables import DaVinci
from Configurables import DecayTreeTuple
from DecayTreeTuple.Configuration import *
from Configurables import TupleToolDecay
from Configurables import TupleToolGeometry, TupleToolKinematic, TupleToolPropertime, TupleToolPrimaries, TupleToolPid, TupleToolEventInfo, TupleToolTrackInfo, TupleToolRecoStats, TupleToolTrigger
from Configurables import TupleToolTISTOS, L0TriggerTisTos, TriggerTisTos
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKi__Hybrid__TupleTool as LoKiTupleTool
from Configurables import TupleToolDecayTreeFitter

TupleToolList = [
    "TupleToolGeometry",
    "TupleToolKinematic",   
   # "TupleToolPropertime", 
    "TupleToolPrimaries",
    "TupleToolPid",
    "TupleToolEventInfo",
    "TupleToolTrackInfo",
    "TupleToolRecoStats",
    "TupleToolTISTOS",
    "TupleToolTrigger"
    ]

TriggerList = [
    'L0HadronDecision',
    'L0MuonDecision',
    'Hlt1TrackAllL0Decision',
    'Hlt2CharmHadD2KS0H_D2KS0PiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDPiDecision',
    'Hlt2CharmHadD2KS0H_D2KS0KDecision',
    'Hlt2CharmHadD2KS0H_D2KS0DDKDecision',
    'Hlt2CharmHadD2HHHDecision'
    ]

LoKi_particle=LoKi__Hybrid__TupleTool("LoKi_particle")
LoKi_particle.Variables =  {
    "ETA" : "ETA",
    "PHI" : "PHI"
    }
LoKi_Dplus2KS0h=LoKi__Hybrid__TupleTool("LoKi_Dplus2KS0h")
LoKi_Dplus2KS0h.Variables =  {
    'DOCACHI2_KS0_hplus' : 'DOCACHI2(1,2)',
    'DOCA_KS0_hplus' : 'DOCA(1,2)',
    }
LoKi_Dplus2PhiPi=LoKi__Hybrid__TupleTool("LoKi_Dplus2PhiPi")
LoKi_Dplus2PhiPi.Variables =  {
    'DOCACHI2_Kplus_Kminus' : 'DOCACHI2(1,2)',
    'DOCA_Kplus_Kminus' : 'DOCA(1,2)',
    'DOCACHI2_Kplus_piplus' : 'DOCACHI2(1,3)',
    'DOCA_Kplus_plus' : 'DOCA(1,3)',
    'DOCACHI2_Kminus_piplus' : 'DOCACHI2(2,3)',
    'DOCA_Kminus_piplus' : 'DOCA(2,3)'
 }
LoKi_KS0=LoKi__Hybrid__TupleTool("LoKi_KS0")
LoKi_KS0.Variables = {
   'DOCACHI2_piplus_piminus' : 'DOCACHI2(1,2)',
   'DOCA_piplus_piminus' : 'DOCA(1,2)',
   }

################# Inputs and Momentum Scaling Correction  ######################

from PhysConf.Selections import AutomaticData
AD_KS0PiLL = AutomaticData( Location = 'Phys/D2KS0HPionLine/Particles' )
AD_KS0PiDD = AutomaticData( Location = 'Phys/D2KS0HPionLineDD/Particles' )
AD_KS0KLL = AutomaticData( Location = 'Phys/D2KS0HKaonLine/Particles' )
AD_KS0KDD = AutomaticData( Location = 'Phys/D2KS0HKaonLineDD/Particles' )
AD_D2PhiPi = AutomaticData( Location = 'Phys/D2hhh_KKPLine/Particles' )

year = "2012"

from PhysConf.Selections import MomentumScaling
KS0PiLL = MomentumScaling ( AD_KS0PiLL , Turbo = False , Year = year )
KS0PiDD = MomentumScaling ( AD_KS0PiDD , Turbo = False , Year = year )
KS0KLL = MomentumScaling ( AD_KS0KLL , Turbo = False , Year = year )
KS0KDD = MomentumScaling ( AD_KS0KDD , Turbo = False , Year = year )
D2PhiPi = MomentumScaling ( AD_D2PhiPi , Turbo = False , Year = year )

from PhysConf.Selections import TupleSelection

################# D -> KS0 H #####################################

# D -> KS0 Pi

# tuple_KsPiLL = DecayTreeTuple( 'Dp2KS0pipLL'  )
# tuple_KsPiLL.Inputs = ['Phys/D2KS0HPionLine/Particles' ]

# tuple_KsPiDD = DecayTreeTuple( 'Dp2KS0pipDD'  )
# tuple_KsPiDD.Inputs = ['Phys/D2KS0HPionLineDD/Particles']

# for tuple in [tuple_KsPiLL,tuple_KsPiDD]:
#     tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC"
#     tuple.Branches = {
#         "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
#         "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
#         "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
#         "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
#         "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
#         }

tuple_KsPiLL = TupleSelection ( 
    'Dp2KS0pipLL' , 
    [KS0PiLL], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )

tuple_KsPiDD = TupleSelection ( 
    'Dp2KS0pipDD' , 
    [KS0PiDD], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  pi+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  pi+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  pi+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  pi+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^pi+]CC"
        },
    ToolList = [ ] 
    )
     
# D -> KS0 K

# tuple_KsKLL = DecayTreeTuple( 'Dp2KS0KpLL'  )
# tuple_KsKLL.Inputs = ['Phys/D2KS0HKaonLine/Particles']

# tuple_KsKDD = DecayTreeTuple( 'Dp2KS0KpDD'  )
# tuple_KsKDD.Inputs = ['Phys/D2KS0HKaonLineDD/Particles']

# for tuple in [tuple_KsKLL,tuple_KsKDD]:

#     tuple.Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC"
#     tuple.Branches = {
#         "Dplus"       : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
#         "KS0"      : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
#         "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
#         "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
#         "hplus"       : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
#         }

# D -> KS0 H

tuple_KsKLL = TupleSelection ( 
    'Dp2KS0KpLL' , 
    [KS0KLL], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        },
    ToolList = [ ] 
    )

tuple_KsKDD = TupleSelection ( 
    'Dp2KS0KpDD' , 
    [KS0KDD], 
    Decay = "[D+ -> ^(KS0 -> ^pi+ ^pi-) ^K+]CC",
    Branches = {
        "Dplus"   : "[D+ -> (KS0 -> pi+ pi-)  K+]CC",
        "KS0"     : "[D+ -> ^(KS0 ->  pi+  pi-)  K+]CC",
        "piplus"  : "[D+ -> ( KS0 -> ^pi+  pi-)  K+]CC",
        "piminus" : "[D+ -> ( KS0 ->  pi+ ^pi-)  K+]CC",
        "hplus" : "[D+ -> ( KS0 ->  pi+  pi-) ^K+]CC"
        },
    ToolList = [ ] 
    )

## D -> KS0 H

# for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD]:
    
#     tuple.addTool(TupleToolDecay, name="Dplus")
#     tuple.addTool(TupleToolDecay, name="KS0")
#     tuple.addTool(TupleToolDecay, name="piplus")
#     tuple.addTool(TupleToolDecay, name="piminus")
#     tuple.addTool(TupleToolDecay, name="hplus")
#  #LoKi VARIABLES
#     for particle in [tuple.Dplus,tuple.KS0,tuple.piplus,tuple.piminus,tuple.hplus]:
#         particle.addTool(LoKi_particle)
#         particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
#     tuple.Dplus.addTool(LoKi_Dplus2KS0h)
#     tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
#     tuple.KS0.addTool(LoKi_KS0)
#     tuple.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

# #DECAY TREE FITTER
#     tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')                 
#     # tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMass')                   
#     tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
# #constrain PV and mass                                          
#     tuple.Dplus.DTF.constrainToOriginVertex = True                
#     tuple.Dplus.DTF.Verbose = True                              
#     tuple.Dplus.DTF.daughtersToConstrain = ['KS0']                         
#     tuple.Dplus.DTF.UpdateDaughters = True                  
# #constrain only PV                                              
#     tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
#     tuple.Dplus.DTFonlyPV.Verbose = True
#     tuple.Dplus.DTFonlyPV.UpdateDaughters = True
# #constrain only mass                                                      
#     # tuple.Dplus.DTFonlyMass.constrainToOriginVertex = False          
#     # tuple.Dplus.DTFonlyMass.Verbose = True                         
#     # tuple.Dplus.DTFonlyMass.daughtersToConstrain = ['KS0']                                
#     # tuple.Dplus.DTFonlyMass.UpdateDaughters = True 

## D -> KS0 H

for dtt in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD]:
    
    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="KS0")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="piminus")
    dtt.addTool(TupleToolDecay, name="hplus")
 #LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.KS0,dtt.piplus,dtt.piminus,dtt.hplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2KS0h)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2KS0h"]
    dtt.KS0.addTool(LoKi_KS0)
    dtt.KS0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_KS0"]

#DECAY TREE FITTER
#constrain PV and mass                                          
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTF')                     
    dtt.Dplus.DTF.Verbose = True                              
    dtt.Dplus.DTF.constrainToOriginVertex = True                
    dtt.Dplus.DTF.daughtersToConstrain = ['KS0']                         
    dtt.Dplus.DTF.UpdateDaughters = True                  
#constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True


################# D -> Phi Pi #####################################

# tuple_PhiPi = DecayTreeTuple('Dp2KmKppip')
# tuple_PhiPi.Inputs = ['Phys/D2hhh_KKPLine/Particles']

# for tuple in [tuple_PhiPi]:
#     tuple.Decay = "[D+ -> ^K+  ^K- ^pi+]CC"
#     tuple.Branches = {
#         "Dplus"   : "[D+ -> K+  K- pi+]CC",
#         "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
#         "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
#         "piplus"  : "[D+ -> K+  K- ^pi+]CC"
#         }
#     tuple.addTool(TupleToolDecay, name="Dplus")
#     tuple.addTool(TupleToolDecay, name="Kplus")
#     tuple.addTool(TupleToolDecay, name="Kminus")
#     tuple.addTool(TupleToolDecay, name="piplus")

# #LoKi VARIABLES
#     for particle in [tuple.Dplus,tuple.Kplus,tuple.Kminus,tuple.piplus]:
#         particle.addTool(LoKi_particle)
#         particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
#     tuple.Dplus.addTool(LoKi_Dplus2PhiPi)
#     tuple.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]
   
# #DECAY TREE FITTER
#     tuple.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
# #constrain only PV                                              
#     tuple.Dplus.DTFonlyPV.constrainToOriginVertex = True
#     tuple.Dplus.DTFonlyPV.Verbose = True
#     tuple.Dplus.DTFonlyPV.UpdateDaughters = True

tuple_D2PhiPi = TupleSelection ( 
    'Dp2KmKppip' , 
    [D2PhiPi], 
    Decay = "[D+ -> ^K+  ^K- ^pi+]CC",
    Branches = {
        "Dplus"   : "[D+ -> K+  K- pi+]CC",
        "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
        "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
        "piplus"  : "[D+ -> K+  K- ^pi+]CC"
        },
    ToolList = [ ] 
    )


for dtt in [tuple_D2PhiPi]:
    
    # dtt.Decay = "[D+ -> ^K+  ^K- ^pi+]CC"
    # dtt.Branches = {
    #     "Dplus"   : "[D+ -> K+  K- pi+]CC",
    #     "Kplus"   : "[D+ -> ^K+  K- pi+]CC",
    #     "Kminus"  : "[D+ -> K+  ^K- pi+]CC",
    #     "piplus"  : "[D+ -> K+  K- ^pi+]CC"
    #     }

    dtt.addTool(TupleToolDecay, name="Dplus")
    dtt.addTool(TupleToolDecay, name="piplus")
    dtt.addTool(TupleToolDecay, name="Kplus")
    dtt.addTool(TupleToolDecay, name="Kminus")

#LoKi VARIABLES
    for particle in [dtt.Dplus,dtt.Kplus,dtt.Kminus,dtt.piplus]:
        particle.addTool(LoKi_particle)
        particle.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_particle"]
    dtt.Dplus.addTool(LoKi_Dplus2PhiPi)
    dtt.Dplus.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus2PhiPi"]
   
#DECAY TREE FITTER
 #constrain only PV                                              
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyPV')
    dtt.Dplus.DTFonlyPV.constrainToOriginVertex = True
    dtt.Dplus.DTFonlyPV.Verbose = True
    dtt.Dplus.DTFonlyPV.UpdateDaughters = True
#DTF with only Momentum Scaling
    dtt.Dplus.addTupleTool('TupleToolDecayTreeFitter/DTFonlyMS') 
    dtt.Dplus.DTFonlyMS.Verbose = True
    dtt.Dplus.DTFonlyMS.UpdateDaughters = True


################# D -> KS0 H, D -> Phi Pi #####################################

# tttt = TupleToolTISTOS()
# tttt.addTool(L0TriggerTisTos())
# tttt.addTool(TriggerTisTos())
    
# for tuple in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD,tuple_PhiPi]:
  
#     tuple.TupleName = "ntp"    

#     tuple.ToolList =  TupleToolList

#     tuple.addTool(TupleToolTrigger())
#     tuple.TupleToolTrigger.VerboseL0 = True
#     tuple.TupleToolTrigger.VerboseHlt1 = True
#     tuple.TupleToolTrigger.VerboseHlt2 = True
#     tuple.TupleToolTrigger.TriggerList = TriggerList
   
#     tuple.addTool(tttt)
#     tuple.TupleToolTISTOS.Verbose = True
#     tuple.TupleToolTISTOS.VerboseL0 = True
#     tuple.TupleToolTISTOS.VerboseHlt1 = True
#     tuple.TupleToolTISTOS.VerboseHlt2 = True
#     tuple.TupleToolTISTOS.TriggerList = TriggerList

for dtt in [tuple_KsPiLL,tuple_KsPiDD,tuple_KsKLL,tuple_KsKDD,tuple_D2PhiPi]:

    dtt.TupleName = "ntp"

    dtt.ToolList =  TupleToolList
 
    dtt.TupleToolTrigger.VerboseL0 = True
    dtt.TupleToolTrigger.VerboseHlt1 = True
    dtt.TupleToolTrigger.VerboseHlt2 = False
    dtt.TupleToolTrigger.FillHlt2 = False
    dtt.TupleToolTrigger.TriggerList = TriggerList

    dtt.TupleToolTISTOS.addTool(L0TriggerTisTos())
    dtt.TupleToolTISTOS.addTool(TriggerTisTos())
    dtt.TupleToolTISTOS.Verbose = True
    dtt.TupleToolTISTOS.VerboseL0 = True
    dtt.TupleToolTISTOS.VerboseHlt1 = True
    dtt.TupleToolTISTOS.VerboseHlt2 = False
    dtt.TupleToolTISTOS.FillHlt2 = False
    dtt.TupleToolTISTOS.TriggerList = TriggerList

from PhysConf.Selections import SelectionSequence
seq_KsPiLL = SelectionSequence( 'Seq_KsPiLL' , tuple_KsPiLL ).sequence()    
seq_KsPiDD = SelectionSequence( 'Seq_KsPiDD' , tuple_KsPiDD ).sequence()    
seq_KsKLL = SelectionSequence( 'Seq_KsKLL' , tuple_KsKLL ).sequence()    
seq_KsKDD = SelectionSequence( 'Seq_KsKDD' , tuple_KsKDD ).sequence()    
seq_D2PhiPi = SelectionSequence( 'Seq_D2PhiPi' , tuple_D2PhiPi ).sequence()          
############ DaVinci configuration ###############################
lineKS0PiLL = "Hlt2CharmHadD2KS0H_D2KS0Pi"
lineKS0PiDD = "Hlt2CharmHadD2KS0H_D2KS0DDPi"
lineKS0KLL = "Hlt2CharmHadD2KS0H_D2KS0K"
lineKS0KDD = "Hlt2CharmHadD2KS0H_D2KS0DDK"
lineD2PhiPi = "Hlt2CharmHadD2HHH"

from Configurables import LoKi__HDRFilter as StripFilter
from PhysConf.Filters import LoKi_Filters

fltrs = LoKi_Filters( Code = "HLT_PASS_RE('"+lineKS0PiLL+"Decision') | HLT_PASS_RE('"+lineKS0PiDD+"Decision') | HLT_PASS_RE('"+lineKS0KLL+"Decision') | HLT_PASS_RE('"+lineKS0KDD+"Decision') | HLT_PASS_RE('"+lineD2PhiPi+"Decision')")


DaVinci().Simulation   = False
DaVinci().Lumi = True
DaVinci().RootInTES = "/Event/Charm/"
DaVinci().appendToMainSequence([seq_KsPiLL])
DaVinci().appendToMainSequence([seq_KsPiDD])
DaVinci().appendToMainSequence([seq_KsKLL])
DaVinci().appendToMainSequence([seq_KsKDD])
DaVinci().appendToMainSequence([seq_D2PhiPi])
DaVinci().TupleFile = "tuple_D2KS0H_D2PhiPi_MS_2012.root"
DaVinci().InputType='MDST'

################# READ FROM LOCAL FILE ###########################
from GaudiConf import IOHelper
IOHelper().inputFiles([
        #'/eos/lhcb/user/s/smaccoli/00068280_00000252_1.Charm.mdst' #striping20 (2012)
        # '/eos/lhcb/user/s/smaccoli/00068280_00000092_1.Charm.mdst',  #stripping20 (2012 Dw WGSelection2)
        # '/eos/lhcb/user/s/smaccoli/00068280_00001358_1.Charm.mdst',
        #'/eos/lhcb/user/s/smaccoli/00068280_00001836_1.Charm.mdst'
        #'/eos/lhcb/user/s/smaccoli/00068282_00000929_1.Charm.mdst' #stripping20r1 (2011) WGSelection2
        #'/eos/lhcb/user/f/fferrari/forSerena/00022727_00027438_1.charm.mdst' #stripping20r1 (2011)
        ], clear = True)

