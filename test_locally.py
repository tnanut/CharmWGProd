#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
from os.path import abspath, dirname, isdir, isfile, join, relpath
from subprocess import check_call
import sys
from tempfile import NamedTemporaryFile

CI_PROJECT_DIR = abspath(dirname(__file__))

try:
    from wg_prod_ci_tools import load_info, check_proxy
except ImportError:
    sys.path.insert(0, CI_PROJECT_DIR)
    from wg_prod_ci_tools import load_info, check_proxy

check_proxy()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('production_name')
    parser.add_argument('job_name')
    parser.add_argument('--n-events', type=int, default=-1)
    args = parser.parse_args()

    info_fn = abspath(join(CI_PROJECT_DIR, 'productions', args.production_name, 'info.json'))
    if not isfile(info_fn):
        raise ValueError('Failed to find production information at: '+info_fn)

    run_test(info_fn, args.job_name, args.n_events)


def validate_options(application, version, options):
    lb_run_command = 'export CI_PROJECT_DIR={} && export CHARMWGPRODROOT="$CI_PROJECT_DIR" && lb-run -c best {}/{}'.format(CI_PROJECT_DIR, application, version)
    options = "'" + "' '".join(options) + "'"
    with NamedTemporaryFile(suffix='.pkl') as fp:
        fn = fp.name
        check_call('{command} gaudirun.py --output {fn} --dry-run {options}'
                   .format(command=lb_run_command, fn=fn, options=options),
                   shell=True)

        # Ensure that EvtMax has not been set
        python_command = '; '.join([
            'import pickle',
            'options = pickle.load(open("{}", "rb"))'.format(fn),
            'evt_max = options["ApplicationMgr"].EvtMax',
            'assert evt_max < 0, "EvtMax should be -1 but found "+str(evt_max)',
        ])
        check_call("{} python -c '{}'".format(lb_run_command, python_command),
                   shell=True)


def run_test(info_fn, job_name, n_events):
    info = load_info(info_fn)
    if job_name not in info:
        raise ValueError('Failed to find job with name '+job_name+' in '+info_fn)

    job_config = info[job_name]
    COMMAND_TEMPLATE = (
        'set -o pipefail && export CI_PROJECT_DIR={CI_PROJECT_DIR} && export CHARMWGPRODROOT="$CI_PROJECT_DIR" && '
        'lb-run -c best LHCbDIRAC/$LHCBDIRAC_VERSION {CI_PROJECT_DIR}/.ci/dirac/test_production.py'
        ' --options \'{options}\''
        ' --bk-path \'{bookkeeping_path}\''
        ' --app {application}'
        ' --app-version {application_version}'
        ' --output-type {output_type}'
        ' --n-events {n_events}'
        ' --nlfns {n_lfns}'
        ' 2>&1 | tee DIRAC.log'
    )
    production_name = '/'.join(relpath(dirname(info_fn), CI_PROJECT_DIR).split('/')[1:])
    output_dir = join(CI_PROJECT_DIR, 'output', production_name, job_name)
    if isdir(output_dir):
        raise OSError('Output folder '+output_dir+' already exists')
    os.makedirs(output_dir)
    os.chdir(output_dir)
    print('Running job from:', output_dir)

    # TODO Fix the setting of EvtMax in DIRAC
    prod_dir = relpath(dirname(info_fn), CI_PROJECT_DIR)
    options = [join('$${CI_PROJECT_DIR}', prod_dir, fn) for fn in job_config['options']]

    validate_options(job_config['application'], job_config['application_version'], options)

    options += ['$${CI_PROJECT_DIR}/.ci/set_evtmax.py']
    command = COMMAND_TEMPLATE.format(
        options="' '".join(options),
        bookkeeping_path=job_config['bookkeeping_path'],
        application=job_config['application'],
        application_version=job_config['application_version'],
        output_type=job_config['output_type'],
        n_events=n_events,
        n_lfns=job_config['n_lfns'],
        CI_PROJECT_DIR=CI_PROJECT_DIR
    )
    check_call(command, shell=True)
    with open('DIRAC.log', 'rt') as fp:
        if "something wrong with execution" in fp.read():
            raise RuntimeError('Something went wrong when processing the job, '
                               'see logs in '+output_dir)

    return command


if __name__ == '__main__':
    parse_args()
