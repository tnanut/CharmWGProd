# Data package for WG productions with automated testing and submission

Questions can be directed to the Charm WG productions liaisons (Chris Burr and Louis Henry).

Analysts from other working groups are welcome to use this system.

## Table of contents

* [Submitting productions](#submitting-productions)
* [Monitoring the status of productions](#monitoring-the-status-of-productions)
* [Where is the output](#where-is-the-output)
  * [Using XRootD (recommended)](#using-xrootd-recommended)
* [Tips for writing options files](#tips-for-writing-options-files)
  * [Using a single options file for different datasets and avoiding duplication](#using-a-single-options-file-for-different-datasets-and-avoiding-duplication)
  * [Importing custom Python modules](#importing-custom-python-modules)

## Submitting productions

1. Start by cloning this data package and making a new branch for your new production

    ```bash
    git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/WG/CharmWGProd
    cd CharmWGProd
    git checkout -b branch-name
    git push -u origin branch-name
    ```

2. Open a WIP (Work In Progress) merge request in the gitlab interface (a link for this is shown when pushing the branch).
3. If this is a new type of production: Create a folder inside `productions` that is descriptive for the analysis the production is for.
3. If this is for an amendment to an existing production: Remove and obsolete productions from the `info.json` file.
4. Inside this folder create a file called `info.json`. This should contain a dictionary, where the key is the name of the job and the value is another dictionary containing the following keys:
  * `options`: List of options files in order, the file paths should be relative to the directory containing `info.json`.
  * `bookkeeping_path`: The path in the bookkeeping to process
  * `dq_flag`: The data quality flag to require, typically this should be `"OK"` for data and `"ALL"` for MC
  * `application`: The name of the application to use
  * `application_version`: The name of the application to use
  * `output_type`: The "file type" to use for the output in DIRAC. This should capitalised, start with `CHARM_` and be fairly descriptive of the data contained in the ntuple.

    An example `info.json` file for creating four WG productions might look like:

    ```json
    {
        "2015_MagDown": {
            "options": ["main_options.py", "data_type_2015.py"],
            "bookkeeping_path": "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24/90000000/CHARM.MDST",
            "dq_flag": "OK",
            "application": "DaVinci",
            "application_version": "v44r1",
            "output_type": "CHARM_D2HLL_DVNTUPLE.ROOT"
        },
        "2015_MagUp": {
            "options": ["main_options.py", "data_type_2015.py"],
            "bookkeeping_path": "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24/90000000/CHARM.MDST",
            "dq_flag": "OK",
            "application": "DaVinci",
            "application_version": "v44r1",
            "output_type": "CHARM_D2HLL_DVNTUPLE.ROOT"
        },
        "2016_MagDown": {
            "options": ["main_options.py", "data_type_2016.py"],
            "bookkeeping_path": "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28/90000000/CHARM.MDST",
            "dq_flag": "OK",
            "application": "DaVinci",
            "application_version": "v44r1",
            "output_type": "CHARM_D2HLL_DVNTUPLE.ROOT"
        },
        "2016_MagUp": {
            "options": ["main_options.py", "data_type_2016.py"],
            "bookkeeping_path": "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28/90000000/CHARM.MDST",
            "dq_flag": "OK",
            "application": "DaVinci",
            "application_version": "v44r1",
            "output_type": "CHARM_D2HLL_DVNTUPLE.ROOT"
        }
    }
    ```

5. Push your changes using `git push`, tests will then run automatically over a random LFN and 1000 events. A comment will be created on the pending merge request with a link to the production's log and output.
6. The tests can be reproduced locally (replacing `PRODUCTION_NAME` and `JOB_NAME` as appropriate) using:

    ```bash
    /path/to/CharmWGProd/test_locally.py PRODUCTION_NAME JOB_NAME --n-events=-1
    # For example to run the 2016 MagDown job from the productions/d2hll folder
    /path/to/CharmWGProd/test_locally.py d2hll 2016_MagDown --n-events=-1
    ```

7. **_(Only required for Charm analyses.)_** Give a short presentation in the Charm WG
meeting or send an email to the charm mailing list, that shows the final states
and variables that are saved to the ntuples
(examples [1](https://indico.cern.ch/event/687615/contributions/3251342/)
[2](https://indico.cern.ch/event/687613/contributions/3231263/)
[3](https://indico.cern.ch/event/687613/contributions/3233661/)).
The idea is just to make other people aware in case they intend to use similar
ntuples. If so, they can check the variables they need are included. This only
needs to be done for the first production you make and can be skipped if you're
changing an existing production or adding new datasets (year, stripping versions, MC).

8. Once the productions are ready for submission, remove the "WIP" from the merge requests title. It will then be reviewed and submitted shortly after.


## Monitoring the status of productions

See the [monitoring spreadsheet](https://docs.google.com/spreadsheets/d/1-IgnQ1-v8ktnuOTDs7PilZLMUakWGbLmtNqNR2zr4YA/edit?usp=sharing).

## Where is the output

Files should be automatically replicated to LHCb's EOS area at CERN. The [monitoring spreadsheet](https://docs.google.com/spreadsheets/d/1-IgnQ1-v8ktnuOTDs7PilZLMUakWGbLmtNqNR2zr4YA/edit?usp=sharing) shows the path on eos where the files can be found either used the FUSE mount on lxplus or using XRootD directly. Examples are given below for request ID `48280`.

#### **Using XRootD** *(recommended)*

The XRootD client doesn't suport expanding wildcards to the directories must instead be listed. In this case the directories must be explicitly listed:

 ```shell
# First list the parent directory
$ xrdfs root://eoslhcb.cern.ch ls /eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/
/eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000
# Then list each of the subdirectories
$ xrdfs root://eoslhcb.cern.ch ls /eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000001_1.charm_d2hll_dvntuple.root
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000002_1.charm_d2hll_dvntuple.root
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000003_1.charm_d2hll_dvntuple.root
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000004_1.charm_d2hll_dvntuple.root
root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000005_1.charm_d2hll_dvntuple.root
 ```

Almost all software (`ROOT`, `root_numpy`, `root_pandas`, `uproot`, `TBrowser`...) can then load the files using the full XRootD URL which can be made be prepending `root://eoslhcb.cern.ch/` to the path:
```python
import ROOT
tchain = ROOT.TChain("GetIntegratedLuminosity/LumiTuple")
tchain.Add("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000001_1.charm_d2hll_dvntuple.root")
tchain.Add("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000002_1.charm_d2hll_dvntuple.root")
tchain.Add("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000003_1.charm_d2hll_dvntuple.root")
tchain.Add("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000004_1.charm_d2hll_dvntuple.root")
tchain.Add("root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/CHARM_D2HLL_DVNTUPLE.ROOT/00075545/0000/00075545_00000005_1.charm_d2hll_dvntuple.root")
```

NOTE: When using XRootD directly you will need have valid credentials:

* **kerberos**: This can be obtained using `kinit username@CERN.CH` and are valid for 24 hours. Ask in the [~reproducible](https://mattermost.web.cern.ch/lhcb/channels/reproducible
) channel on mattermost for information about securely allowing longer term access, especially for data access from continious integration system.

* **grid proxy**: This can be done using `lhcb-proxy-init` from within the LHCb environment. To do this on a machine without LHCb available, see [this starterkit tutorial](https://lhcb.github.io/starterkit-lessons/using-the-grid/local-grid-proxy.html).

## Tips for writing options files

### Using a single options file for different datasets and avoiding duplication

It is a good idea to avoid duplication in your options files to avoid making copy-and-paste mistakes.
This can be done by having options files that set attributes on the application object.

In this example we will show how four WG productions can share a single options file.
Two of the productions will make ntuples for `D0 -> K- pi+` and the other two will make ntuples for `D0 -> pi+ pi`.
We will also imagine the name of the stripping line changed between 2016 and 2017 data taking.
Let's start by making two files which only set the data taking year:

* `DataType-2016.py`
```python
from Configurables import DaVinci
DaVinci().DataType = '2017'
```
* `DataType-2016.py`
```python
from Configurables import DaVinci
DaVinci().DataType = '2017'
```

As the `TupleFile` property is overridden by the WG productions machinery, it can be used to pass an aribitary string into the options files such as.

* `TupleType-Kpi.py`
```python
from Configurables import DaVinci
DaVinci().TupleFile = 'D0ToKpi.root'
```
* `TupleType-KK.py`
```python
from Configurables import DaVinci
DaVinci().TupleFile = 'D0ToKK.root'
```

If we then set the order of the options files to be:
* `DataType-2016.py`
* `TupleType-Kpi.py`
* `main_options.py`

The main options file is able to query these properties like so:

```python
from Configurables import DaVinci
from Configurables import DecayTreeTuple
import DecayTreeTuple.Configuration

if DaVinci().DataType == '2016':
    lines = {
        'D0ToKpi.root': 'D2hhOldPromptDst2D2KPiLine',
        'D0ToKK.root': 'D2hhOldPromptDst2D2KKLine',
    }
elif DaVinci().DataType in ['2017', '2018']:
    lines = {
        'D0ToKpi.root': 'D2hhPromptDst2D2KPiLine',
        'D0ToKK.root': 'D2hhPromptDst2D2KKLine',
    }
else:
    raise ValueError('Found an unsupported year '+DaVinci().DataType)
decay_descriptors = {
    'D0ToKpi.root': '${D0}[D0 -> ${h1}K- ${h2}pi+]CC',
    'D0ToKK.root': '${D0}[D0 -> ${h1}K- ${h2}K+]CC',
}

job_type = DaVinci().TupleFile.split('.')[0]

dtt = DecayTreeTuple('Tuple'+job_type)
dtt.Inputs = ['/Event/AllStreams/Phys/'+lines[job_type]+'/Particles']
dtt.setDescriptorTemplate(decay_descriptors[job_type])
```

### Importing custom Python modules

You can use the `CHARMWGPRODROOT` environment variable to get the path to your production.
This can then be used to modify the python search paths so you can import your custom modules.
For example, if your production is called `D02HH` and you have a module called `my_d2hh_module.py`:

```python
import os
import sys
sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/D02HH')
import my_d2hh_module
```
